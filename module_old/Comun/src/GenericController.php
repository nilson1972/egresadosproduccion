<?php

namespace Comun;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;


class GenericController extends AbstractActionController
{

    public function printJSon($data)
    {
        return new JsonModel($data);
    }

    public function view()
    {
        $view = new ViewModel();
        return $view;
    }

    public function viewModel($modelo)
    {
        $view = new ViewModel($modelo);
        return $view;
    }

    public function viewModelTemplate($modelo, $tpl)
    {
        $view = new ViewModel($modelo);
        $view->setTemplate($tpl);
        return $view;
    }

    public function viewModelAjax($modelo)
    {
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        return $view;
    }

    public function viewModelAjaxTemplate($modelo, $tpl)
    {
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate($tpl);
        return $view;
    }

    public function viewTemplateAjax()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;
    }



    public function verMensajeError($mensaje)
    {
        $modelo = array(
            'mensaje' => $mensaje
        );
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate('comun/comun/mensaje-error');
        return $view;
    }

    public function verMensajeErrorMultiple($mensaje)
    {
        $modelo = array(
            'mensaje' => $mensaje
        );
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate('comun/comun/mensaje-error-multiple');
        return $view;
    }

    public function verMensajeInfo($mensaje)
    {
        $modelo = array(
            'mensaje' => $mensaje
        );
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate('comun/comun/mensaje-info');
        return $view;
    }

    
    public function getParam1()
    {
        return $this->params()->fromRoute('id1', 0);
        // return $this->params()->fromQuery('id1');
    }

    public function getParam2()
    {
        return $this->params()->fromRoute('id2', 0);
    }

    public function getPostVar($nombre)
    {
        if (is_array($this->getRequest()->getPost($nombre))) {
            return $this->getRequest()->getPost($nombre);
        }else{
            return trim($this->getRequest()->getPost($nombre));    
        }
        
    }


    public function printVar($var)
    {
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }

    /*
   
    public function view_template()
    {
        $view = new ViewModel();
        return $view;
    }

    

    

    public function view_template_ajax()
    {
        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;
    }

    public function view_ajax_template_tpl($tpl)
    {
        // header('Content-type: content="text/html; charset=ISO-8859-1');
        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setTemplate($tpl);
        return $view;
    }

    public function view_ajax_template_model_tpl($template, $modelo)
    {
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate($template);
        return $view;
    }

    public function view_layout_model($layout_tpl, $model)
    {
        $view = new ViewModel($model);
        $layout = $this->layout();
        $layout->setTemplate($layout_tpl);
        return $view;
    }

    /*
    
    private $_error;
    
    
    
    function __construct() {
         
    }

    
    
    

    public function get_parametro_id1_validado_numerico()
    {
        if (!$this->validar_vacio_numerico($this->params()->fromRoute('id1', 0)))
        {
            return $this->view_mensaje_error("Valor no v&aacute;lido en id gestor zona ");
        }        
    }
    
    public function get_parametro_id2()
    {
        return $this->params()->fromRoute('id2', 0);
    }
    
    
    //
    //  Mensajes
    // ---------------------------------------------------------------------------------
    
    
    
    public function view_mensaje_info($mensaje)
    {
        // header('Content-type: content="text/html; charset=ISO-8859-1');
        $modelo = array(
            'mensaje' => $mensaje
        );
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate('comun/comun/mensaje-info');
        return $view;
    }


    public function view_mensaje_existe_identificacion($mensaje, $identificacion, $template)
    {
        // header('Content-type: content="text/html; charset=ISO-8859-1');
        $modelo = array(
            'mensaje' => $mensaje,
            'identificacion' => $identificacion,
            'accion' => $template,
        );
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate('comun/comun/mensaje-existe-identificacion');
        return $view;
    }
    


    // 
    //  Validaciones
    // -----------------------------------------------------------------------------
    
    public function get_error()
    {
        return $this->_error;
    }


    public function validar_campo_no_requerido($campo, $size)
    {
        if (!$this->esta_vacio($campo)) 
        {  
            if ( strlen($campo) > intval($size) )
            {
                return false;
            }
            
        }
    }

    //
    //   Valida si el campo esta vacío, su tamaño y si el campo es entero
    //
    
    public function validar_identificacion($identificacion, $size)
    {                
        if ( strlen($identificacion) > intval($size) )
        {
            $this->_error = "El numero de identificaci&oacute;n excede el valor m&aacute;ximo";
            return false;
        }
        if ($this->esta_vacio($identificacion))
        {
            $this->_error = "El numero de identificaci&oacute;n no puede estar vac&iacute;o";
            return false;
        }
        if (!$this->es_entero($identificacion))
        {
            $this->_error = "El numero de identificaci&oacute;n no es numero v&aacute;lido";
            return false;
        }
        return true;
    }

    public function validar_vacio_numerico($valor)
    {   
        if ($this->esta_vacio($valor))
        {
            return false;
        }
        if (!$this->es_entero($valor))
        {
            return false;
        }
        return true;
    }    

    //
    //   Valida si el campo esta vacío y su tamaño
    //

    public function validar_campo($campo, $size)
    {
        if ($this->esta_vacio($campo)) 
        {
            $this->_error = "Campos vac&iacute;os en los datos enviados";
            return false;
        }
        if ( strlen($campo) > intval($size) )
        {
            $this->_error = "El numero de identificaci&oacute;n excede el valor m&aacute;ximo";
            return false;
        }

    }        

    public function esta_vacio($valor)
    {
        if ($valor=="") {
            return true;
        } else if (empty($valor)) {
            return true;
        } else {
            return false;
        }        
    }

    public function validar_mesa($valor)
    {
        if ( ($valor == 0) || ($valor == '0') )
        {
            return true;
        }
        if ($this->esta_vacio($valor))
        {
            return false;
        }
        if (!$this->es_entero($valor))
        {
            return false;
        }
        return true;
    }
    
    public function es_entero($valor)
    {
        if (ctype_digit($valor)) {
            return true;
        } 
        return false;
    }
    
    // 
    //  Templates 
    // -----------------------------------------------------------------------------


    

    

    public function view_template_ajax_solo_model($modelo) 
    {
        //header('Content-type: content="text/html; charset=ISO-8859-1');
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        return $view;
    }


    // /*   Revisar los metodos siguientes  

    public function view_template_ajax()
    {
        // header('Content-type: content="text/html; charset=ISO-8859-1');
        $view = new ViewModel();
        $view->setTerminal(true);
        return $view;
    }   

    public function view_template_ajax_model($tpl, $modelo)
    {
        header('Content-type: content="text/html; charset=ISO-8859-1');
        $view = new ViewModel($modelo);
        $view->setTerminal(true);
        $view->setTemplate($tpl);
        return $view;
    }

    

    
    */

}