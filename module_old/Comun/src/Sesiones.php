<?php

namespace Comun;


use Zend\Authentication\AuthenticationService;
use Zend\Session\Container;


class Sesiones
{

    private static $container;
    private static $container2;

    public static function initContainer($nombre)
    {
        if (!self::$container2) {
            self::$container2 = new Container($nombre);
        }
    }

    private static function getContainerName($nombre) 
    {
        if (!self::$container2) {
            self::$container2 = new Container($nombre);
        }
        return self::$container2;
    }

    public static function setValueContainer($container, $key, $value)
    {
        try {
            self::getContainerName($container)->$key = $value;
        } catch (\Zend\Session\Exception\RuntimeException $e) {

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getValueContainer($container, $key) 
    {
        try {
            return self::getContainerName($container)->$key;
        } catch (\Zend\Session\Exception\RuntimeException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function deleteContainerName($nombre)
    {
        try {
            self::getContainerName($nombre);
            unset($_SESSION[$nombre]);
            // self::getContainer()->__unset('entrenamiento');
        } catch (\Zend\Session\Exception\RuntimeException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getArrayContainerName($name)
    {
        return self::getContainerName($name)->getArrayCopy();
    }

   

    // ----------------------------------------------------------------

    private static function getContainer() 
    {
        if (!self::$container) {
            self::$container = new Container('asymetrika');
        }
        return self::$container;
    }

    public static function setValue($key, $value) 
    {
        try {
            self::getContainer()->$key = $value;
        } catch (\Zend\Session\Exception\RuntimeException $e) {

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getValue($key) 
    {
        try {
            return self::getContainer()->$key;
        } catch (\Zend\Session\Exception\RuntimeException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getArrayContainer()
    {
        return self::getContainer()->getArrayCopy();
    }

    public static function deleteContainer()
    {

        try {
            self::getContainer();
            unset($_SESSION['entrenamiento']);
            // self::getContainer()->__unset('entrenamiento');
        } catch (\Zend\Session\Exception\RuntimeException $e) {

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function eliminarContainerEmpresar()
    {
        unset($_SESSION['empresa']);
    }


    /*
    |
    |  Métodos para los datos del usuarios
    |  --------------------------------------------------------------------------
    |
    */

    public static function getIdUsuario()
    {
        $auth = new AuthenticationService(); 
        $identity = $auth->getIdentity();            
        return $identity->id;
    }

    public static function getNombre()
    {        
        $auth = new AuthenticationService(); 
        $identity = $auth->getIdentity();
        if (isset($identity->nombre)){
            return $identity->nombre;
        }else{
            return "Error Nombre Usuario";
        }
    }
    public static function getIdTipo()
    {        
        $auth = new AuthenticationService(); 
        $identity = $auth->getIdentity();
        if (isset($identity->nombre)){
            return $identity->id_tipo;
        }else{
            return "Error Nombre Usuario";
        }
    }

   










    /*

    private static $container;
    private static $containerEmpresa;

    //
    // Datos Sesión Detalle Cliente Facturación Empresa
    //

    private static function getContainerEmpresa() 
    {
        if (!self::$containerEmpresa) {
            self::$containerEmpresa = new Container('empresa');
        }

        return self::$containerEmpresa;
    }

    public static function setValueClienteEmpresa($key, $value) 
    {
        try {
            self::getContainerEmpresa()->$key = $value;
        } catch (\Zend\Session\Exception\RuntimeException $e) {

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function eliminarClienteEmpresa($variable)
    {
        try {
            self::getContainerEmpresa()->__unset($variable);
        } catch (\Zend\Session\Exception\RuntimeException $e) {

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getValueClienteEmpresa($key) 
    {
        try {
            return self::getContainerEmpresa()->$key;
        } catch (\Zend\Session\Exception\RuntimeException $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }   

    public static function getArrayContainerEmpresa()
    {
        return self::getContainerEmpresa()->getArrayCopy();
    }

    


    // -----------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------


    

    

    

    public static function deleteValue($key)
    {
        $container = new Container('forzagym');
        unset($container->$key);
    }
    
    

    

    private static function getK()
    {
        $container = new Container('data');
        return $container->k;
    }

    public function encrypt($dato)
    {
        $blockCipher = \Zend\Crypt\BlockCipher::factory('mcrypt', array('algo' => 'aes'));
        $blockCipher->setKey(self::getK());
        return $blockCipher->encrypt($dato);
    }

    public function decrypt($dato)
    {
        $blockCipher = \Zend\Crypt\BlockCipher::factory('mcrypt', array('algo' => 'aes'));
        $blockCipher->setKey(self::getK());
        return $blockCipher->decrypt($dato);
    }

    public static function viewSession()
    {
        echo '<pre>';
        print_r($_SESSION);
        echo '</pre>';
    }




    // Sin revisar -----------------------------------------------
    // --------------------------------------------------------------------------------------



    
    
    
    
    public static function get_id_zona_usuario()
    {
        $auth = new AuthenticationService(); 
        $identity = $auth->getIdentity();            
        return $identity->id_zona;
    }

    public static function setVar($variable, $valor)
    {
        $container = new Container('data');
        $container->$variable = $valor;
    }

    public static function get_variable_sesion($variable)
    {
        $container = new Container('temp');
        return $container->$variable;
    }

    


*/

}