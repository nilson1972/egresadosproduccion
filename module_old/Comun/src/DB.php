<?php 

namespace Comun;



use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;

use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\Paginator\Adapter\DbSelect;

class DB 
{

    private static $resultSet;
    private static $dbAdapter;

    private static $lastInsertId;

    private $verErrores = true; 



    /*
    |--------------------------------------------------------------------------
    | Configuración del adaptador
    |--------------------------------------------------------------------------
    |
    */

    public static function getAdapter() 
    {
        return \Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter();
    }


    /*
    |--------------------------------------------------------------------------
    | Muestra error en try
    |--------------------------------------------------------------------------
    |
    */

    public static function viewError($error) 
    {
        if (self::verErrores) {
            echo $error;
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Transacciones
    |--------------------------------------------------------------------------
    |
    */

    public static function transactionInit()
    {
        DB::getAdapter()->getDriver()->getConnection()->beginTransaction();
    }

    public static function transactionCommit()
    {
        DB::getAdapter()->getDriver()->getConnection()->commit();
        /*
        try{
                self::$dbAdapter->getDriver()->getConnection()->commit();
        } catch (\Exception $e) {
                self::$dbAdapter->getDriver()->getConnection()->rollback();
            
            if ($connection instanceof \Zend\Db\Adapter\Driver\ConnectionInterface) {
                    $connection->rollback();
                }
            
        }*/
    }
    
    public static function transactionRollback()
    {
        DB::getAdapter()->getDriver()->getConnection()->rollback();
    }



    /*
    |--------------------------------------------------------------------------
    | Métodos CRUD
    |--------------------------------------------------------------------------
    |
    */

    public static function insertar($tabla, $datos)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        return $tabla->insert($datos);
    }    

    public static function actualizar($tabla, $set, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        return $tabla->update($set, $where);
    }

    public static function getLastInsertId($seq)
    {        
        return DB::getAdapter()->getDriver()->getLastGeneratedValue($seq);
    }

    public static function getNextValSeq($seq)
    {
        $results = DB::getAdapter()->query("select nextval('".$seq."')", Adapter::QUERY_MODE_EXECUTE);
        $onerow = $results->current();
        return $onerow['nextval'];
    }

    // 


    /*
    |--------------------------------------------------------------------------
    | Métodos para seleccionar un solo registro
    |--------------------------------------------------------------------------
    |
    */

    public static function selectRegistro($tabla, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        $rowset = $tabla->select($where);
        return $rowset->current();
    }

    public static function selectRegistroCampos($tabla, $campos, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        $select = $tabla->getSql()->select();
        $select->columns($campos);
        $select->where($where);
        $rowset = $tabla->selectWith($select);
        return $rowset->current();
    }

    public static function selectRegistroCamposPredicado($tabla, $campos, $where, $predicate)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        $select = $tabla->getSql()->select();
        $select->columns($campos);
        $select->where($where);
        $select->where->addPredicate($predicate);
        // echo $select->getSqlString();
        $rowset = $tabla->selectWith($select);
        return $rowset->current();
    }

    public static function existeRegistro($tabla, $where)
    {
        $artistTable = new TableGateway($tabla, DB::getAdapter());
        $rowset = $artistTable->select($where);
        return !$rowset->current() ? false : true;
    }


    /*
    |--------------------------------------------------------------------------
    | Consultas para seleccionar varios registros
    |--------------------------------------------------------------------------
    |
    */

    public static function selectTabla($table, $campos=false, $where=false, $order=false, $toArray=false, $predicate=false)
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();

        if ($campos != false) {
            $select->columns($campos);
        }

        if ($where != false) {
            $select->where($where);
        }

        if ($predicate != false) {
            $select->where->addPredicate($predicate);
        }

        if ($order != false) {
            $select->order($order);
        }

        if ($toArray != false) {
            $res = $tabla->select($select);
            return $res->toArray();
        }
        
        return $tabla->selectWith($select); 
    }

    public static function selectTablaOrder($table, $order)
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->order($order);
        return $tabla->selectWith($select);
    }

    public static function selectTablaCamposOrder($table, $campos, $where, $order)
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->columns($campos) ;
        $select->where($where)
               ->order($order);
        return $tabla->selectWith($select); 
    }    

    public static function selectRegistrosWhere($tabla, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());

        return $tabla->select($where);
        
    }



    public static function selectRegistrosWhereToArray($tabla, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());

        $r = $tabla->select($where);
        return  $r->toArray();
    }

    public static function selectRegistrosWhereOrder($table, $where, $order, $limit=false)
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->where($where)
               ->order($order);
        if ($limit<>false) {
            $select->limit($limit);
        }

        // $sql = $tabla->getSql();
        // echo $sql->getSqlstringForSqlObject($select);

        return $tabla->selectWith($select);
    }

    public static function getSelectRegistrosWhereOrder($table, $where, $order, $limit=false)
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->where($where)
               ->order($order);
        if ($limit<>false) {
            $select->limit($limit);
        }       

        return $select;
    }

    public static function selectRegistrosOrder($table, $order) 
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->order($order);
        return $tabla->selectWith($select);
    }


    /*
    |--------------------------------------------------------------------------
    |    Consultas Join
    |--------------------------------------------------------------------------
    |
    |  +----------+        +----------+
    |  |  Tabla1  |-------<|  Tabla2  |
    |  +----------+        +----------+
    |
    */

    public static function selectJoin( $tabla1, $campos_tabla1,
                                       $tabla2, $campos_tabla2,
                                       $tabla1_campo, $tabla2_campo,
                                       $where, $order, $paginar=false, $viewSql=false,
                                       $predicate=false, $group=false, $left=false )
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;

        if ($left != false) { 
            $select->join( 
                        array('tabla2' => $tabla2),
                        'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo,  
                        $campos_tabla2,
                        'left'
                      );
        } else {
            $select->join( 
                        array('tabla2' => $tabla2),
                        'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo,  
                        $campos_tabla2
                      );
        }
        

        
        $select->where($where);

        if ($group != false) {
            $select->group($group);
        }

        if ($predicate != false) {
            $select->where->addPredicate($predicate);
        }
        
        $select->order($order);

        // echo $select->getSqlString();

        if ($viewSql) {
            echo $select->getSqlString();
        }

        if ($paginar) {
            return $select;
        }

        $statement = $sql->prepareStatementForSqlObject($select);        
        return $statement->execute();
    }

    public static function selectJoinToArray(  $tabla1, $campos_tabla1,
                                               $tabla2, $campos_tabla2,
                                               $tabla1_campo, $tabla2_campo,
                                               $where, $order, $group=false)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo,  
                    $campos_tabla2
                  );
        $select->where($where);

        if ($group != false) {
            $select->group($group);
        }
        
        $select->order($order);
        // echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        $r = new \Zend\Db\ResultSet\ResultSet();
        $r->initialize($statement->execute());
        return $r->toArray();
    }


    public static function selectJoinRegistro( $tabla1, $campos_tabla1,
                                               $tabla2, $campos_tabla2,
                                               $tabla1_campo, $tabla2_campo,
                                               $where, $order)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo, 
                    $campos_tabla2
                  );
        $select->where($where);
        $select->order($order);
        //  echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);        
        $res = $statement->execute();
        return $res->current();
    }

    public static function selectJoinTresTablasRegistro( $tabla1, $campos_tabla1,
                                                         $tabla2, $campos_tabla2,
                                                         $tabla3, $campos_tabla3,
                                                         $tabla1_pk, $tabla2_fk,
                                                         $tabla2_pk, $tabla3_fk,
                                                         $where, $order )
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_pk.' = '.'tabla2.'.$tabla2_fk, 
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_pk.' = '.'tabla3.'.$tabla3_fk, 
                    $campos_tabla3
                  ) ;
        $select->where($where);
        
        $select->order($order);
        // echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);        
        $res = $statement->execute();
        return $res->current();
    }


    public static function selectJoinTresTablas( $tabla1, $campos_tabla1,
                                                 $tabla2, $campos_tabla2,
                                                 $tabla3, $campos_tabla3,
                                                 $tabla1_pk, $tabla2_fk,
                                                 $tabla2_pk, $tabla3_fk,
                                                 $where, $order, $paginar=false, $toArray=false )
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_pk.' = '.'tabla2.'.$tabla2_fk, 
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_pk.' = '.'tabla3.'.$tabla3_fk, 
                    $campos_tabla3
                  );

        $select->where($where);
        
        $select->order($order);
        // echo  $select->getSqlString();

        if ($paginar) {
            return $select;
        }

        $statement = $sql->prepareStatementForSqlObject($select);

        if ($toArray) {            
            $r = new \Zend\Db\ResultSet\ResultSet();
            $r->initialize($statement->execute());
            return $r->toArray();  
        }

        
        return $statement->execute();
    }



     /*
    |
    |     ________         _________         ________ 
    |    |        |       |         |       |        |          
    |    | tabla2 |>------| Tabla1  |------<| Tabla3 |
    |    |________|       |________ |       |________|
    |
    |
    */


    public static function selectJoinTresTablasIntermedia( $tabla1, $campos_tabla1,
                                                           $tabla2, $campos_tabla2,
                                                           $tabla3, $campos_tabla3,
                                                           $tabla1_fk1, $tabla2_pk,
                                                           $tabla1_fk2, $tabla3_pk,
                                                           $where, $order, $paginar=false, $toArray=false, 
                                                           $predicate=false, $returnRegistro=false )
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_fk1.' = '.'tabla2.'.$tabla2_pk, 
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla1.'.$tabla1_fk2.' = '.'tabla3.'.$tabla3_pk, 
                    $campos_tabla3
                  );

        $select->where($where);

        if ($predicate != false) {
            $select->where->addPredicate($predicate);
        }
        
        $select->order($order);
        // echo  $select->getSqlString();

        if ($paginar) {
            return $select;
        }

        $statement = $sql->prepareStatementForSqlObject($select);

        if ($toArray) {            
            $r = new \Zend\Db\ResultSet\ResultSet();
            $r->initialize($statement->execute());
            return $r->toArray();  
        }


        if ($returnRegistro != false) {
            $res = $statement->execute();
            return $res->current();
        }
        
        return $statement->execute();
    }

    public static function selectJoinTresTablasToArray(  $tabla1, $campos_tabla1,
                                                         $tabla2, $campos_tabla2,
                                                         $tabla3, $campos_tabla3,
                                                         $tabla1_pk, $tabla2_fk,
                                                         $tabla2_pk, $tabla3_fk,
                                                         $where, $order, $paginar=false )
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_pk.' = '.'tabla2.'.$tabla2_fk, 
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_pk.' = '.'tabla3.'.$tabla3_fk, 
                    $campos_tabla3
                  );

        $select->where($where);
        
        $select->order($order);
        // echo  $select->getSqlString();

        if ($paginar) {
            return $select;
        }
        $statement = $sql->prepareStatementForSqlObject($select);        
        $r = new \Zend\Db\ResultSet\ResultSet();
        $r->initialize($statement->execute());
        return $r->toArray();        
    }

    /*
    |
    |                      __________ 
    |                     |          |
    |                     |  tabla3  |
    |                     |__________|
    |                          V      
    |                          |
    |                          |
    |     ________         ____|____         ________ 
    |    |        |       |         |       |        |          
    |    | tabla2 |>------| Tabla1  |------<| Tabla4 |
    |    |________|       |________ |       |________|
    |
    |
    */

    public static function selectJoinCuatroTablasIntermedia( $tabla1, $campos_tabla1,
                                                             $tabla2, $campos_tabla2,
                                                             $tabla3, $campos_tabla3,
                                                             $tabla4, $campos_tabla4,
                                                             $campo_fk_tabla2, $campo_fk_tabla3, $campo_fk_tabla4,
                                                             $where, $order, 
                                                             $paginar=false )
    {

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 

        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;

        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$campo_fk_tabla2.' = '.'tabla2.id',
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla1.'.$campo_fk_tabla3.' = '.'tabla3.id',
                    $campos_tabla3
                  ) 
               ->join( 
                    array('tabla4' => $tabla4),
                    'tabla1.'.$campo_fk_tabla4.' = '.'tabla4.id', 
                    $campos_tabla4
                  ) ;

        $select->where($where);
        
        $select->order($order);
        // echo $select->getSqlString();
        if ($paginar) {
            return $select;
        }
        $statement = $sql->prepareStatementForSqlObject($select);        
        return $statement->execute();
    }





    /*
    |
    | ------------------------------------------------------------------------------------
    |  Join Cuatro tablas sin orden
    |
    | ------------------------------------------------------------------------------------
    |
    */

    public static function selectJoinCuatroTablas(  $tabla1, $campos_tabla1,
                                                    $tabla2, $campos_tabla2,
                                                    $tabla3, $campos_tabla3,
                                                    $tabla4, $campos_tabla4,
                                                    $tabla1_campo, $tabla2_campo1,
                                                    $tabla2_campo2, $tabla3_campo1,
                                                    $tabla3_campo2, $tabla4_campo1,
                                                    $where, $order, 
                                                    $paginar=false, $toArray=false )
    {

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 

        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;

        $select->join( 
                    array('tabla2' => $tabla2),                    
                    'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo1,
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_campo2.' = '.'tabla3.'.$tabla3_campo1,
                    $campos_tabla3
                  ) 
               ->join( 
                    array('tabla4' => $tabla4),
                    'tabla3.'.$tabla3_campo2.' = '.'tabla4.'.$tabla4_campo1,
                    $campos_tabla4
                  ) ;

        $select->where($where);
        
        $select->order($order);

        echo $select->getSqlString();
        
        if ($paginar) {
            return $select;
        }

        $statement = $sql->prepareStatementForSqlObject($select);

        if ($toArray) {            
            $r = new \Zend\Db\ResultSet\ResultSet();
            $r->initialize($statement->execute());
            return $r->toArray();  
        }
        
        return $statement->execute();
    }


    public static function selectJoinCuatroTablasRegistro(  $tabla1, $campos_tabla1,
                                                            $tabla2, $campos_tabla2,
                                                            $tabla3, $campos_tabla3,
                                                            $tabla4, $campos_tabla4,
                                                            $tabla1_pk, $tabla2_fk,
                                                            $tabla2_pk, $tabla3_fk,
                                                            $tabla3_pk, $tabla4_fk,
                                                            $where, $order)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;
        $select->join( 
                    array('tabla2' => $tabla2),
                    'tabla1.'.$tabla1_pk.' = '.'tabla2.'.$tabla2_fk, 
                    $campos_tabla2
                )
                ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_pk.' = '.'tabla3.'.$tabla3_fk, 
                    $campos_tabla3
                ) 
                ->join( 
                    array('tabla4' => $tabla4),
                    'tabla3.'.$tabla3_pk.' = '.'tabla4.'.$tabla4_fk, 
                    $campos_tabla4
                );

        $select->where($where);
        
        $select->order($order);
        // echo $select->getSqlString();        
        $statement = $sql->prepareStatementForSqlObject($select);        
        $res = $statement->execute();
        return $res->current();
    }




    /*
    |
    | ------------------------------------------------------------------------------------
    |  Join Cinco tablas sin orden
    |
    | ------------------------------------------------------------------------------------
    |
    */

    public static function selectJoinCincoTablas(  $tabla1, $campos_tabla1,
                                                   $tabla2, $campos_tabla2,
                                                   $tabla3, $campos_tabla3,
                                                   $tabla4, $campos_tabla4,
                                                   $tabla5, $campos_tabla5,
                                                   $tabla1_campo, $tabla2_campo1,
                                                   $tabla2_campo2, $tabla3_campo1,
                                                   $tabla3_campo2, $tabla4_campo1,
                                                   $tabla4_campo2, $tabla5_campo1,
                                                   $where, $order, 
                                                   $paginar=false, $toArray=false )
    {

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 

        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;

        $select->join( 
                    array('tabla2' => $tabla2),                    
                    'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo1,
                    $campos_tabla2
                  )
               ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_campo2.' = '.'tabla3.'.$tabla3_campo1,
                    $campos_tabla3
                  ) 
               ->join( 
                    array('tabla4' => $tabla4),
                    'tabla3.'.$tabla3_campo2.' = '.'tabla4.'.$tabla4_campo1,
                    $campos_tabla4
                  ) 
               ->join( 
                    array('tabla5' => $tabla5),
                    'tabla4.'.$tabla4_campo2.' = '.'tabla5.'.$tabla5_campo1,
                    $campos_tabla5
                  ) ;

        $select->where($where);
        
        $select->order($order);
        // echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);

        if ($paginar) {
            return $select;
        }
        if ($toArray) {            
            $r = new \Zend\Db\ResultSet\ResultSet();
            $r->initialize($statement->execute());
            return $r->toArray();  
        }
        return $statement->execute();
    }




    /*
    |
    | ------------------------------------------------------------------------------------
    |  Join Seis tablas sin orden
    |
    | ------------------------------------------------------------------------------------
    |
    */

    public static function selectJoinSeisTablas( $tabla1, $campos_tabla1,
                                                 $tabla2, $campos_tabla2,
                                                 $tabla3, $campos_tabla3,
                                                 $tabla4, $campos_tabla4,
                                                 $tabla5, $campos_tabla5,
                                                 $tabla6, $campos_tabla6,
                                                 $tabla1_campo, $tabla2_campo1,
                                                 $tabla2_campo2, $tabla3_campo1,
                                                 $tabla3_campo2, $tabla4_campo1,
                                                 $tabla4_campo2, $tabla5_campo1,
                                                 $tabla5_campo2, $tabla6_campo1,
                                                 $where, $order, 
                                                 $paginar=false, $toArray=false )
    {

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 

        $select->from(array('tabla1' => $tabla1));
        $select->columns($campos_tabla1) ;

        $select->join( 
                    array('tabla2' => $tabla2),                    
                    'tabla1.'.$tabla1_campo.' = '.'tabla2.'.$tabla2_campo1,
                    $campos_tabla2
                  )
                ->join( 
                    array('tabla3' => $tabla3),
                    'tabla2.'.$tabla2_campo2.' = '.'tabla3.'.$tabla3_campo1,
                    $campos_tabla3
                  ) 
                ->join( 
                    array('tabla4' => $tabla4),
                    'tabla3.'.$tabla3_campo2.' = '.'tabla4.'.$tabla4_campo1,
                    $campos_tabla4
                  ) 
                ->join( 
                    array('tabla5' => $tabla5),
                    'tabla4.'.$tabla4_campo2.' = '.'tabla5.'.$tabla5_campo1,
                    $campos_tabla5
                  ) 
                ->join( 
                    array('tabla6' => $tabla6),
                    'tabla5.'.$tabla5_campo2.' = '.'tabla6.'.$tabla6_campo1,
                    $campos_tabla6
                  ) ;

        $select->where($where);
        
        $select->order($order);
        // echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);

        if ($paginar) {
            return $select;
        }
        if ($toArray) {            
            $r = new \Zend\Db\ResultSet\ResultSet();
            $r->initialize($statement->execute());
            return $r->toArray();  
        }
        return $statement->execute();
    }



    


    /*
    |--------------------------------------------------------------------------
    | Paginador
    |--------------------------------------------------------------------------
    |
    */

    public static function getSelectPaginador($tabla)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select(); 
        $select->from($tabla);
        return $select;
    }


    public static function getPaginadorTabla($tabla, $pagina, $filas)
    {
        $select = DB::getSelectPaginador($tabla);
        
        $paginatorAdapter = new DbSelect($select, DB::getAdapter());
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setItemCountPerPage($filas);     // Fila por pagina
        $paginator->setPageRange(3);
        $paginator->setCurrentPageNumber($pagina);   // Numero de página
        return $paginator;
    }

    public static function getPaginadorConsulta($select, $pagina, $filas)
    {
        $paginatorAdapter = new DbSelect($select, DB::getAdapter());
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setItemCountPerPage($filas);     // Fila por pagina
        $paginator->setPageRange(3);
        $paginator->setCurrentPageNumber($pagina);   // Numero de página
        return $paginator;
    }







 
    
    /*    
    public static function init()
    {
        self::$resultSet = new ResultSet;
        self::$dbAdapter = new Sql(\Zend\Db\TableGateway\Feature\GlobalAdapterFeature::getStaticAdapter());
    }
    
    
    
    
    public static function getObjectSQL()
    {        
        return new Sql(DB::getAdapter());
    }

    public static function executeSQL($sql)
    {
        return DB::getAdapter()->query($sql)->execute();
    }

    public static function transactionInit()
    {
        DB::getAdapter()->getDriver()->getConnection()->beginTransaction();
    }
    
    public static function transactionCommit()
    {
        DB::getAdapter()->getDriver()->getConnection()->commit();
        /*
        try{
                self::$dbAdapter->getDriver()->getConnection()->commit();
        } catch (\Exception $e) {
                self::$dbAdapter->getDriver()->getConnection()->rollback();
            
            if ($connection instanceof \Zend\Db\Adapter\Driver\ConnectionInterface) {
                    $connection->rollback();
                }
            
        }*
    }
    
    public static function transactionRollback()
    {
        DB::getAdapter()->getDriver()->getConnection()->rollback();
    }

    

    public static function getLastInsertId($seq)
    {        
        return DB::getAdapter()->getDriver()->getLastGeneratedValue($seq);
    }

    public static function getLastId($seq)
    {
        $sql = "SELECT currval('$seq')";
        $statement = DB::getAdapter()->createStatement();
        $statement->prepare($sql);
        $result = $statement->execute();
        $sequence = $result->getResource()->fetch(\PDO::FETCH_ASSOC);
        return $sequence['currval'];
    }

    public static function eliminar($tabla, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        return $tabla->delete($where);
    }

    

    

    public static function selectTablaCamposOrder($table, $campos, $where, $order)
    {
        $tabla = new TableGateway($table, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->columns($campos) ;
        $select->where($where)
               ->order($order);
        return $tabla->selectWith($select); 
    }

    /**
    *   Consultas selección un registro
    *   ----------------------------------------------------------------------------------
    *

    

    public static function selectRegistroCampos($tabla, $campos, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        $select = $tabla->getSql()->select();
        $select->columns($campos);
        $select->where($where);
        $rowset = $tabla->selectWith($select);
        return $rowset->current();
    }


    /*
    *
    *   Consultas selección varios registros
    *   ----------------------------------------------------------------------------------
    *
    *


    

    public static function actualizar($tabla, $set, $where)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        return $tabla->update($set, $where);
    }

    


    // 
    // Consultas Like
    //

    public static function selectRegistrosLikeLimit($tabla, $campo, $like, $where, $limit)
    {
        $tabla = new TableGateway($tabla, DB::getAdapter());
        $select = $tabla->getSql()->select();
        // $select->like($campo, $like);
        $select->where($where);
        $select->where->like($campo, $like);
        $select->limit(50);
        return $tabla->selectWith($select);
    }

    //
    // Consultas join
    //

    

    

    /*
         --------           --------           --------
        | tabla2 | >------ | Tabla1 | ------< | Tabla3 |
         --------           --------           -------- 
    *

    


    */

}
