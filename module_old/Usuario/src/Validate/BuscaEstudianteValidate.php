<?php

namespace Usuario\Validate;


use Comun\InputFilterGeneric3;


class BuscaEstudianteValidate extends InputFilterGeneric3
{

    public function validarIdentificacion()
    {
        $this->validarDigito([
            'name'  => 'identificacion',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        return $this->validar();
    }

}