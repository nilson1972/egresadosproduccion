<?php

namespace Usuario\Validate;


use Comun\InputFilterGeneric3;


class AutorizaValidate extends InputFilterGeneric3
{

    public function validarAutoriza()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'autoriza',
            'label' => 'Valor no valido para campo Autorizar',
            'required' => true,
        ]);

        $this->ValidarDigito([
            'name' => 'id_estudiante',
            'label' => 'Valor no valido para campo oculto',
            'required' => true,
        ]);
        return $this->validar();
    }

}