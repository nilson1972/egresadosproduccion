<?php

namespace Usuario\Table;


use Comun\DB;

class autorizaTable
{


    private $tablaAutoriza = 'testudiante';

    public function actualizar($id)
    {
        $set = [
            'autoriza' => 'on',
        ];
        $where = [
            'id' => $id,
        ];

        try {
            DB::transactionInit();

            DB::actualizar( $this->tablaAutoriza, $set, $where );

            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

}