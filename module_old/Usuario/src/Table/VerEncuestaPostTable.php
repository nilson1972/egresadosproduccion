<?php

namespace Usuario\Table;


use Comun\DB;

class verencuestapostTable
{


    private $tablaVerEncuestapost = 'tencuestapost';


    public function getExisteEstudianteEncuesta( $idt, $prog )
    {
        return DB::selectRegistro( $this->tablaVerEncuestapost, [
            'id_estudent' => $idt,
            'ha_progpostgraduado' => $prog
        ]);
    }

}