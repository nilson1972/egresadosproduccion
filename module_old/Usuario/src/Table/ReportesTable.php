<?php

namespace Usuario\Table;


use Comun\DB;

class reportesTable
{


    private $tablaEncuesta = 'tencuesta';
    private $tablaEstudiante = 'testudiante';

    //Estudiantes que Realizaron la Encuesta
    public function Selectencuesta()
    {
        $campos_estudiante = [
            'id','nombres', 'apellidos', 'nro_ident'
        ];

        $campos_encuesta = [
            'encuesta_id' => 'id_estudent',
            'programa' => 'ha_progprecursa'
        ];
        return DB::selectJoin( $this->tablaEstudiante, $campos_estudiante,
                               $this->tablaEncuesta,  $campos_encuesta, 'id', 'id_estudent',
                               [],[]);
    }

    //Estudiantes que NO Realizaron la Encuesta
    public function Selectnoencuesta()
    {
        $campos_estudiante = [
            'id','nombres', 'apellidos', 'nro_ident'
        ];

        $campos_encuesta = [
            'encuesta_id' => 'id_estudent'
        ];

        $condicion = [
            'id_estudent' => 'id_estudent'
        ];
        return DB::selectJoin( $this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuesta,  $campos_encuesta, 'id', 'id_estudent',
            $condicion,[]);
    }

    //¿Cuantos Egresados Trabajan Actualmente?
    public function Selecttrabaja()
    {
        $where = [
            'il_trabactualmente' => 'on'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    //¿Cuantos Egresados NO Trabajan Actualmente?
    public function Selectnotrabaja()
    {
        $where = [
            'il_trabactualmente' => ''
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    //¿Cuantos Egresados Estudiaron otra Carrera?
    public function Selectestudotra()
    {
        $where = [
            'ha_estudotracarrera' => 'on'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    //¿Cuantos Egresados Estudiaron otra Carrera y la terminaron?
    public function Selectestudotrasi()
    {
        $where = [
            'ha_estudotracarrera' => 'on',
            'ha_termesacarrera' => 'SI'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    //¿Cuantos Egresados Estudiaron otra Carrera y NO la terminaron?
    public function Selectestudotrano()
    {
        $where = [
            'ha_estudotracarrera' => 'on',
            'ha_termesacarrera' => 'NO'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    //¿Cuantos Egresados NO estudiaron otra Carrera?
    public function Selectnoestudotra()
    {
        $where = [
            'ha_estudotracarrera' => ''
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

}