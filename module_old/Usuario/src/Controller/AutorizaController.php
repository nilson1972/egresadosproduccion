<?php


namespace Usuario\Controller;



use Comun\GenericController;



class AutorizaController extends GenericController
{



    public function __construct()
    {
        $this->tablaAutoriza = new \Usuario\Table\AutorizaTable();
        $this->validateAutoriza = new \Usuario\Validate\AutorizaValidate();
    }


    public function indexAction()
    {

    }

    public function actualizarAction()
    {
        // Validación de Autorizacion
        if ( ! $this->validateAutoriza->validarAutoriza() ) {
            return $this->verMensajeError($this->validateAutoriza->getMensajes());
        }

        if (! $this->tablaAutoriza->actualizar($this->validateAutoriza->getValue('id_estudiante'))){
            return $this->verMensajeError($this->tablaAutoriza->getMensajes());
        }else {
            return $this->verMensajeInfo("Estudiante Autorizado para Encuesta satisactoriamente");
        }
    }

}