<?php


namespace Usuario\Controller;

use Comun\GenericController;

class UsuarioController extends GenericController
{


    public function __construct()
    {
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();

        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();
        $this->tablaVerEncuesta = new \Usuario\Table\VerEncuestaTable();
        $this->tablaVerEncuestaPost = new \Usuario\Table\VerEncuestaPostTable();
        $this->validateVerEncuesta = new \Usuario\Validate\VerEncuestaValidate();
        $this->validateBuscaEstudiante = new \Usuario\Validate\BuscaEstudianteValidate();
    }
	
    public function indexAction()
    {
        return $this->viewModel([
            'estudiantes' => $this->tablaEstudiante->getTotaEstudiantes(),
            'realizadas'  => $this->tablaEstudiante->getTotaEncuestasRealizadas(),
        ]);
    }

    public function actualizaAction(){

        // Validación identificación Estudiante
        if ( ! $this->validateBuscaEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateBuscaEstudiante->getValue('identificacion'));

        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateBuscaEstudiante->getValue('identificacion').
                    "</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya Autorizado para realizar la encuesta
            if ($est['autoriza']=="on"){
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateBuscaEstudiante->getValue('identificacion').
                          "</strong> YA esta Autorizado para realizar la encuesta");
            }else {
                return $this->viewModelAjax([
                    'estudiante' => $est
                ]);
            }
        }
    }

    public function verencuestaAction(){

        // Validación identificación Estudiante
        if ( ! $this->validateBuscaEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        if ( ! $this->validateVerEncuesta->ValidarDatosVerEncuesta() ) {
            return $this->verMensajeError($this->validateVerEncuesta->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateBuscaEstudiante->getValue('identificacion'));
        $uni = $this->tablaUniversidad->Selectuniversidades();
        $prog = $this->tablaProgramas->Selectprogramas();

        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>".$this->validateBuscaEstudiante->getValue('identificacion')."</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya realizo la encuesta
            $enc = $this->tablaBuscaEncuesta->getExisteEstudianteEncuesta($est['id']);
                                                                   //       $this->validateVerEncuesta->getValue('programa'));
            if (! count($enc) == 0) {
              //  return $this->verMensajeError("Estudiante con el Número de identificación <strong>".$this->validateEstudiante->getValue('identificacion')."</strong> ya realizo la encuesta");
                return $this->viewModelAjax([
                    'estudiante' => $est,
                    'encuesta'=> $enc,
                    'universidad'=> $uni,
                    'programas' => $prog
                ]);
            }else {
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateBuscaEstudiante->getValue('identificacion').
                       "</strong> No ha realizado la encuesta, para el programa de codigo <strong>");
                  //       .$this->validateVerEncuesta->getValue('programa')."</strong>...");
            }
        }
    }

    public function verencuestapostAction(){

        // Validación identificación Estudiante
        if ( ! $this->validateBuscaEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        if ( ! $this->validateVerEncuesta->ValidarDatosVerEncuesta() ) {
            return $this->verMensajeError($this->validateVerEncuesta->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateBuscaEstudiante->getValue('identificacion'));
        $uni = $this->tablaUniversidad->Selectuniversidades();
        $unipost = $this->tablaUniversidad->Selectuniversidades();

        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>"
                 .$this->validateBuscaEstudiante->getValue('identificacion').
                  "</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya realizo la encuesta
            $enc = $this->tablaBuscaEncuestaPost->getExisteEstudianteEncuesta($est['id'],
                $this->validateVerEncuesta->getValue('programa'));
            if (! count($enc) == 0) {
                //  return $this->verMensajeError("Estudiante con el Número de identificación <strong>".$this->validateEstudiante->getValue('identificacion')."</strong> ya realizo la encuesta");
                return $this->viewModelAjax([
                    'estudiante' => $est,
                    'encuestapost'=> $enc,
                    'universidad'=> $uni,
                    'univpost'=> $unipost
                ]);
            }else {
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateBuscaEstudiante->getValue('identificacion').
                       "</strong> No ha realizado la encuesta, para el programa de codigo <strong>"
                         .$this->validateVerEncuesta->getValue('programa')."</strong>...");
            }
        }
    }
  
}