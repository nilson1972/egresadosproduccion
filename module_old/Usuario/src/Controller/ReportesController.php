<?php


namespace Usuario\Controller;



use Comun\GenericController;



class ReportesController extends GenericController
{


   public function __construct()
    {
        $this->tablaReportes = new \Usuario\Table\ReportesTable();
     /*   $this->validateReportes = new \Usuario\Validate\ReportesValidate();*/
    }

    public function indexAction()
    {

    }

    public function formEncuestasRealizadasAction(){

        return $this->viewModelAjax([
            'siencuesta' => $this->tablaReportes->Selectencuesta()
        ]);
    }

    public function formEncuestasNoRealizadasAction(){

        return $this->viewModelAjax([
            'noencuesta' => $this->tablaReportes->Selectnoencuesta()
        ]);
    }

    public function formEncuestaIndicadorAction(){

        return $this->viewModelAjax([
            'estudtrabaja' => $this->tablaReportes->Selecttrabaja(),
            'estudnotrabaja' => $this->tablaReportes->Selectnotrabaja(),
            'estudotracarre' => $this->tablaReportes->Selectestudotra(),
            'estudotracarresi' => $this->tablaReportes->Selectestudotrasi(),
            'estudotracarreno' => $this->tablaReportes->Selectestudotrano(),
            'noestudotracarre' => $this->tablaReportes->Selectnoestudotra()

        ]);
    }

    public function formEncuestasPorSedesAction(){

      /*  return $this->viewModelAjax([
            'siencuesta' => $this->tablaReportes->Selectencuesta()
        ]);*/
    }

}