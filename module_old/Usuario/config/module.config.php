<?php

namespace Usuario;


use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'controllers' => [
        'factories' => [
            Controller\UsuarioController::class => InvokableFactory::class,
            Controller\AutorizaController::class => InvokableFactory::class,
            Controller\ReportesController::class => InvokableFactory::class,
        ],
    ],


    'router' => [
        'routes' => [

            'usuario' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/usuario[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\UsuarioController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'autoriza' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/autoriza[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\AutorizaController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],



    'view_manager' => [
        'template_path_stack' => [
            'usuario' => __DIR__ . '/../view',
        ],
    ],
];