<?php

namespace Estudiante;


use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'controllers' => [
        'factories' => [
            Controller\EstudianteController::class => InvokableFactory::class,
            Controller\EncuestaController::class => InvokableFactory::class,
            Controller\EncuestaPostController::class => InvokableFactory::class,
        ],
    ],


    'router' => [
        'routes' => [

            'estudiante' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/estudiante[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\EstudianteController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'encuestapreg' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/encuestapreg[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\EncuestaController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'encuestapost' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/encuestapost[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\EncuestaPostController::class,
                        'action'     => 'index',
                    ],
                ],
            ],



        ],
    ],



    'view_manager' => [
        'template_path_stack' => [
            'estudiante' => __DIR__ . '/../view',
        ],
    ],
];