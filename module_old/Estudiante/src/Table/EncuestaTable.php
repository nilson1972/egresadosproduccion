<?php
namespace Estudiante\Table;


use Comun\DB;

class encuestaTable
{
    private $tablaEncuesta = 'tencuesta';


    public function insertar($datos)
    {
        try {
           DB::transactionInit();
            DB::insertar($this->tablaEncuesta,[
                'fec_encuesta' => date('Y-m-d G:i:s'),
                'id_estudent' => $datos['id_estudiante'],
                'dp_identestud' => $datos['num_id'],
                'dp_lugarexped' => utf8_encode($datos['l_expedicion']),
                'dp_fecnac' => $datos['f_nac'],
                'dp_mpionac' => utf8_encode($datos['nac_municipio']),
                'dp_deptonac' => utf8_encode($datos['nac_depto']),
                'dp_dirresidencia' => utf8_encode($datos['dir_residencial']),
                'dp_telestud' => $datos['rtelestud'],
                'dp_mpioresidencia' => utf8_encode($datos['res_municipio']),
                'dp_deptoresidencia' => utf8_encode($datos['res_depto']),
                'dp_celestud'  => $datos['rcelestud'],
                'dp_emailestud' => $datos['remailestud'],
                'ha_npsaberpro'  => $datos['snp'],
                'ha_progprecursa'  => $datos['programa'],
                'ha_semacaini'  => $datos['sem_ini'],
                'ha_anosemacaini' => $datos['anyo1'],
                'ha_semacafin'  => $datos['sem_fin'],
                'ha_anosemacafin' => $datos['anyo2'],
                'sedeuniv' => $datos['sedeuniv'],
                'ha_porcsatis' => $datos['porcentaje'],
                'ha_estudotracarrera' => $datos['estudio_otra'],
                'ha_termesacarrera' => $datos['termino_otra'],
                'ha_nomunivotracarrera' => $datos['univer_otra'],
                'ha_nomesacarrera' => utf8_encode($datos['otroprograma']),
                'ep_areaporespec' => utf8_encode($datos['especializar']),
                'il_trabactualmente' => $datos['trabaja_actual'],
                'il_fecinilab'  => $datos['fecha_inilaboral'],
                'il_tenidotrabremu' => $datos['trabajo_remunera'],
                'il_ocupesetraba' => utf8_encode($datos['ocupacion_trabajo']),
                'il_nomemplab' => utf8_encode($datos['nombre_empresa']),
                'il_actecoemplab' => utf8_encode($datos['actividad_empresa']),
                'ie_nomgteemp' => utf8_encode($datos['nom_gerente_emp_trab']),
                'ie_nomjefinme' => utf8_encode($datos['nom_jefe_emp_trab']),
                'ie_diremplab' => utf8_encode($datos['direc_emp_trab']),
                'ie_dptoemplab' => utf8_encode($datos['dpto_emp_trab']),
                'ie_mpioemplab' => utf8_encode($datos['mpio_emp_trab']),
                'ie_telemplab' => $datos['telef_emp_trab'],
                'ie_emailemplab' => $datos['email_emp_trab'],
            ]);
          DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

}