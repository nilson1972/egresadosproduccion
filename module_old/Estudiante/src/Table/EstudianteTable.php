<?php

namespace Estudiante\Table;


use Comun\DB;

class estudianteTable
{


    private $tablaEstudiante = 'testudiante';

    private $tablaEncuesta = 'tencuesta';


    public function getEstudianteIdentificacion($id)
    {
        return DB::selectRegistro( $this->tablaEstudiante, [
                                                                'nro_ident' => $id,
                                                           ]);
    }

    public function getTotaEstudiantes()
    {
        $campos = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];
        $where = [
            'id_corte' => 1
        ];
        return DB::selectRegistroCampos( $this->tablaEstudiante, $campos, $where );
    }

    public function getTotaEncuestasRealizadas()
    {
        $camposEncuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(tabla2.id)')
        ];
        $where = [
            'tabla1.id_corte' => 1
        ];
        return DB::selectJoinRegistro( $this->tablaEstudiante, [],
                                       $this->tablaEncuesta, $camposEncuesta,
                                       'id', 'id_estudent',
                                        $where, []);
    }



                
}