<?php

namespace Estudiante\Table;


use Comun\DB;

class programasTable
{


    private $tablaProgramas = 'tprogramas';


    public function Selectprogramas()
    {
        $campos_programas = [
            'id','codigo','nombre'
        ];

        $campo_order = [
            'nombre' => 'nombre'
        ];
        return DB::selectTablaCamposOrder( $this->tablaProgramas, $campos_programas, [], $campo_order);
    }


}