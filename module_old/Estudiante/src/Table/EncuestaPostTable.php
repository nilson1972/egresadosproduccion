<?php
namespace Estudiante\Table;


use Comun\DB;

class encuestapostTable
{
    private $tablaEncuestaPost = 'tencuestapost';

    public function inserpost($datos)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaEncuestaPost,[
                'fec_encuesta' => date('Y-m-d G:i:s'),
                'id_estudent' => $datos['id_estudiante'],
                'rcelestud'  => $datos['rcelestud'],
                'rtelestud' => $datos['rtelestud'],
                'rteltrabestud' => $datos['rteltrabestud'],
                'remailestud' => $datos['remailestud'],
                'ha_npsaberpro'  => $datos['snp'],
                'ha_univpregraduado' => $datos['univer_pre'],
                'ha_progpregraduado' => $datos['progpre'],
                'ha_progpostgraduado' => $datos['progpost'],
                'ha_seminipost'  => $datos['sem_ini'],
                'ha_anoinipost' => $datos['anyo1'],
                'ha_semfinpost'  => $datos['sem_fin'],
                'ha_anofinpost' => $datos['anyo2'],
                'ha_porcsatis' => $datos['porcentaje'],
                'ha_estudotropost' => $datos['estudio_otra'],
                'ha_termesepost' => $datos['termino_otra'],
                'ha_nomesepost' => $datos['otroprograma'],
                'ha_univpostgraduado' => $datos['univer_post'],
                'ep_areaformcontinua' => $datos['especializar'],
                'il_trabactualmente' => $datos['trabaja_actual'],
                'il_fecinilab'  => $datos['fecha_inilaboral'],
                'il_ocupesetraba' => $datos['ocupacion_trabajo'],
                'il_nomemplab' => $datos['nombre_empresa'],
                'il_actecoemplab' => $datos['actividad_empresa'],
                'ie_nomgteemp' => $datos['nom_gerente_emp_trab'],
                'ie_nomjefinme' => $datos['nom_jefe_emp_trab'],
                'ie_diremplab' => $datos['direc_emp_trab'],
                'ie_dptoemplab' => $datos['dpto_emp_trab'],
                'ie_mpioemplab' => $datos['mpio_emp_trab'],
                'ie_telemplab' => $datos['telef_emp_trab'],
                'ie_emailemplab' => $datos['email_emp_trab'],
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

}