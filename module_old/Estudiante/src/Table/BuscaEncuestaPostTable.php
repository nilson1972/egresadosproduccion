<?php

namespace Estudiante\Table;


use Comun\DB;

class buscaencuestapostTable
{


    private $tablaBuscaEncuestaPost = 'tencuestapost';


    public function getExisteEstudianteEncuesta( $idt, $prog )
    {
        return DB::selectRegistro( $this->tablaBuscaEncuestaPost, [
            'id_estudent' => $idt,
            'ha_progpostgraduado' => $prog
        ]);
    }

}