<?php

namespace Estudiante\Table;


use Comun\DB;

class universidadTable
{


    private $tablaUniversidad = 'tuniversidad';


    public function Selectuniversidades()
    {
        $campos_universidad = [
            'codigo','nombre'
        ];

        $campo_order = [
            'nombre' => 'nombre'
            ];
        return DB::selectTablaCamposOrder( $this->tablaUniversidad, $campos_universidad, [], $campo_order);
    }


}