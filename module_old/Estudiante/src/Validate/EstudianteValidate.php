<?php

namespace Estudiante\Validate;


use Comun\InputFilterGeneric3;


class EstudianteValidate extends InputFilterGeneric3
{

    public function validarIdentificacion()
    {
        $this->validarDigito([
            'name'  => 'identificacion',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        /*
        $this->validarAlfaNumericoFiltro([
            'name' => 'programa',
            'label' => 'Valor no valido para campo programa',
            'required' => true,
            'max_lenght' => 10,
            'alnum' => false,
        ]);
        */

        return $this->validar();
    }

}