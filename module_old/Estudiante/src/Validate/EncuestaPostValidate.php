<?php
namespace Estudiante\Validate;


use Comun\InputFilterGeneric3;


class EncuestaPostValidate extends InputFilterGeneric3
{

    public function validarEncuestaPost()
    {
        $this->validarDigito([
            'name'  => 'id_estudiante',
            'label' => 'Valor no valido para campo',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'rcelestud',
            'label' => 'Valor no valido para campo Celular estudiante',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'rtelestud',
            'label' => 'Valor no valido para campo Telefono Residencia estudiante',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'rteltrabestud',
            'label' => 'Valor no valido para campo Telefono trabajo estudiante',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'remailestud',
            'label' => 'Valor no valido para campo email estudiante',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name'  => 'snp',
            'label' => 'Valor no valido para campo año SNP prueba saberPRO',
            'required' => true,
            'max_lenght' => 30,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'univer_pre',
            'label' => 'Valor no valido para campo nombre universidad',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name'  => 'progpre',
            'label' => 'Valor no valido para campo programa pre',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => true,
        ]);

        $this->validarDigito([
            'name'  => 'progpost',
            'label' => 'Valor no valido para campo programa post',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'sem_ini',
            'label' => 'Valor no valido para campo semestre inicial',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'anyo1',
            'label' => 'Valor no valido para campo año inicio',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'sem_fin',
            'label' => 'Valor no valido para campo semestre final',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'anyo2',
            'label' => 'Valor no valido para campo año fin',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'porcentaje',
            'label' => 'Valor no valido para campo año inicio',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'estudio_otra',
            'label' => 'Valor no valido para campo otra carrera',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'termino_otra',
            'label' => 'Valor no valido para campo termino esa carrera',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'otroprograma',
            'label' => 'Valor no valido para campo carrera estudio',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'univer_post',
            'label' => 'Valor no valido para campo nombre universidad',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'especializar',
            'label' => 'Valor no valido para campo legustaria especializarse',
            'required' => true,
            'max_lenght' => 80,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'trabaja_actual',
            'label' => 'Valor no valido para campo trabajo actual',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecha_inilaboral',
            'label' => 'Valor no valido para campo fecha inicio labora',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'ocupacion_trabajo',
            'label' => 'Valor no valido para campo ocupacion',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre_empresa',
            'label' => 'Valor no valido para campo nombre empresa',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'actividad_empresa',
            'label' => 'Valor no valido para campo nombre empresa',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nom_gerente_emp_trab',
            'label' => 'Valor no valido para campo nombre gerente',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nom_jefe_emp_trab',
            'label' => 'Valor no valido para campo nombre jefe inmediato',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'direc_emp_trab',
            'label' => 'Valor no valido para campo direccion empresa',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'dpto_emp_trab',
            'label' => 'Valor no valido para campo departamento empresa',
            'required' => false,
            'max_lenght' => 60,
            'alnum' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'mpio_emp_trab',
            'label' => 'Valor no valido para campo municipio empresa',
            'required' => false,
            'max_lenght' => 60,
            'alnum' => true,
        ]);

        $this->validarDigito([
            'name' => 'telef_emp_trab',
            'label' => 'Valor no valido para campo año inicio',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'email_emp_trab',
            'label' => 'Valor no valido para campo email empresa',
            'required' => false,
            'max_lenght' => 100,
        ]);

        return $this->validar();
    }
}