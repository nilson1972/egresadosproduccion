<?php


namespace Estudiante\Controller;



use Comun\GenericController;



class EncuestaController extends GenericController
{



    public function __construct()
    {
        $this->tablaEncuesta = new \Estudiante\Table\EncuestaTable();
        $this->validateEncuesta = new \Estudiante\Validate\EncuestaValidate();
    }

    public function insertarAction()
    {
        // Validación Encuesta
        if ( ! $this->validateEncuesta->validarEncuesta() ) {
            return $this->verMensajeError($this->validateEncuesta->getMensajes());
        }

        if (! $this->tablaEncuesta->insertar($this->validateEncuesta->getValues())){
            return $this->verMensajeError($this->tablaEncuesta->getMensajes());
        }else {
            return $this->viewModelAjax([
                'mensaje' => "Encuesta registrada satisactoriamente",
            ]);
        }
    }

}