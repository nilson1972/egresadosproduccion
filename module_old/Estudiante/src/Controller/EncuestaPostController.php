<?php

namespace Estudiante\Controller;



use Comun\GenericController;



class EncuestaPostController extends GenericController
{



    public function __construct()
    {
        $this->tablaEncuestaPost = new \Estudiante\Table\EncuestaPostTable();
        $this->validateEncuestaPost = new \Estudiante\Validate\EncuestaPostValidate();
    }

    public function inserpostAction()
    {
        // Validación EncuestaPost
        if ( ! $this->validateEncuestaPost->validarEncuestaPost() ) {
            return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
        }

        if (! $this->tablaEncuestaPost->inserpost($this->validateEncuestaPost->getValues())){
            return $this->verMensajeError($this->tablaEncuestaPost->getMensajes());
        }else {
            return $this->verMensajeInfo("Encuesta registrada satisactoriamente");
        }
    }

}