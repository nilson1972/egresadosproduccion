<?php


namespace Estudiante\Controller;



use Comun\GenericController;



class EstudianteController extends GenericController
{



	public function __construct()
    {
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
    }
	
    public function indexAction()
    {

    }

    public function buscarAction()
    {
        // Validación identificación del estudiante

        if ( ! $this->validateEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateEstudiante->getValue('identificacion'));
        $uni = $this->tablaUniversidad->Selectuniversidades();
        $prog = $this->tablaProgramas->Selectprogramas();


        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>"
                                          .$this->validateEstudiante->getValue('identificacion').
                                          "</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya realizo la encuesta
            $enc = $this->tablaBuscaEncuesta->getExisteEstudianteEncuesta($est['id']);
                                    //     $this->validateEstudiante->getValue('programa'));
            if (! count($enc) == 0) { //($this->tablaBuscaEncuesta->getExisteEstudianteEncuesta($est['id'], 'programa')){
               return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                                             .$this->validateEstudiante->getValue('identificacion').
                                             "</strong> ya realizo la encuesta, para el programa de codigo <strong>");
                                         //    .$this->validateEstudiante->getValue('programa')."</strong>");

            }else {
                //preguntar si ya Autorizado para realizar la encuesta
                if ($est['autoriza']=="on"){
                   return $this->viewModelAjax([
                       'estudiante' => $est,
                       'universidad'=> $uni,
                       'programas' => $prog
                   ]);
                }else {
                    return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                                                  .$this->validateEstudiante->getValue('identificacion').
                                                  "</strong> No esta Autorizado para realizar la encuesta");
                }
            }
        }
    }


    public function formpostAction()
    {
        // Validación identificación del estudiante

        if ( ! $this->validateEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateEstudiante->getValue('identificacion'));
        $uni = $this->tablaUniversidad->Selectuniversidades();
        $unipost = $this->tablaUniversidad->Selectuniversidades();


        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>".$this->validateEstudiante->getValue('identificacion')."</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya realizo la encuesta
            $encpost = $this->tablaBuscaEncuestaPost->getExisteEstudianteEncuesta($est['id'],
                $this->validateEstudiante->getValue('programa'));
            if (! count($encpost) == 0) {
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateEstudiante->getValue('identificacion').
                    "</strong> ya realizo la encuesta, para el programa de postgrado codigo <strong>"
                    .$this->validateEstudiante->getValue('programa')."</strong>");

            }else {
                //preguntar si ya Autorizado para realizar la encuesta
                if ($est['autoriza']=="on"){
                    return $this->viewModelAjax([
                        'estudiante' => $est,
                        'universidad'=> $uni,
                        'univpost' => $unipost
                    ]);
                }else {
                    return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                        .$this->validateEstudiante->getValue('identificacion').
                        "</strong> No esta Autorizado para realizar la encuesta de postgrado");
                }
            }
        }
    }

}