<?php

namespace ManageAcl;
 

use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Role\GenericRole;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Authentication\AuthenticationService; 


class Module
{
    
    
    public function onBootstrap(MvcEvent $e)
    {
        $this->initAcl($e);
        $e->getApplication()->getEventManager()->attach('route', array($this, 'checkAcl'));
    } 

    public function initAcl2(MvcEvent $e)
    {
        $acl = new Acl();

        $index = new GenericRole('index');
        $acl->addRole($index);
        $acl->addResource(new GenericResource('home'));
        $acl->addResource(new GenericResource('application'));
        $acl->addResource(new GenericResource('autenticacion'));
        $acl->allow('index', 'home');
        $acl->allow('index', 'application');
        $acl->allow('index', 'autenticacion');

        $user = new GenericRole('user');
        $acl->addRole($user);
        $acl->addResource(new GenericResource('index'));
        $acl->addResource(new GenericResource('user'));
        $acl->addResource(new GenericResource('plan'));
        // $acl->addResource(new GenericResource('autenticacion'));

        $acl->allow('user', 'index');
        $acl->allow('user', 'user');
        $acl->allow('user', 'autenticacion');

        $acl->allow('user', 'plan', 'planes');
        $acl->allow('user', 'plan', 'form-nuevo');


        $e->getViewModel()->acl=$acl;
    }
    
    public function initAcl(MvcEvent $e)
    {
        
        $acl = new \Zend\Permissions\Acl\Acl();
              
        //Incluimos la lista de roles y permisos, nos devuelve un array
        $roles = require_once __DIR__ . '/../config/acl.roles.php'; // '../config/acl.roles.php';


        foreach($roles as $role => $resources)
        {
            //Indicamos que el rol será genérico
            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);

            //Añadimos el rol al ACL
            $acl->addRole($role);

            foreach($resources["allow"] as $key => $value)
            {
                if (is_array($value)) 
                {
                    foreach($value as $privilege)
                    {
                        if(!$acl->hasResource($key))
                        {
                            $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($key));
                        }
                        $acl->allow($role, $key, $privilege);
                    }
                }else
                {
                    if(!$acl->hasResource($value))
                    {
                        $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($value));
                    }
                    $acl->allow($role, $value);
                }   
            }

            foreach ($resources["deny"] as $resource) 
            {
                //Si el recurso no existe lo añadimos
                if(!$acl->hasResource($resource))
                {
                    $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($resource));
                }
                //Denegamos a ese rol ese recurso
                $acl->deny($role, $resource);
            }
            
	    }	         
	    
        //Establecemos la lista de control de acceso
        $e->getViewModel()->acl=$acl;
    }
    
    public function checkAcl(MvcEvent $e)
    {

        $route=$e->getRouteMatch()->getMatchedRouteName();

        $auth=new \Zend\Authentication\AuthenticationService();
        if ($auth->hasIdentity()) {
            $identi=$auth->getStorage()->read();
            switch ($identi->id_tipo) {
                case 1:
                    $userRole = 'usuario';
                    break;
                case 2:
                    $userRole = 'usuario';
                    break;
            }

            
        }else{
            $userRole = 'index';
        }

        // $controller    = $e->getRouteMatch()->getParam('controller');
        $action = $e->getRouteMatch()->getParam('action');

        if(!$e->getViewModel()->acl->isAllowed($userRole, $route, $action)) {
            $e->getRouteMatch()
                    ->setParam('controller', 'Autenticacion\Controller\AutenticacionController')
                    ->setParam('action', 'logout');
        }


        

        /*
        $route=$e->getRouteMatch()->getMatchedRouteName();

        echo "1";




       
	         
	    //Instanciamos el servicio de autenticacion
	    $auth=new \Zend\Authentication\AuthenticationService();
	    $identi=$auth->getStorage()->read();
	         
	    // Establecemos nuestro rol
        $auth = new AuthenticationService();

        $identi = Sesiones::getArrayContainerName('usuario');
        
        if ($auth2->hasIdentity()) 
        {
            $identity = $auth2->getIdentity();            

            switch ($identity->id_tipo) {
                case 1:
                    $userRole = 'admin';
                    break;
                case 2:
                    $userRole = 'user';
                    break;
                case 3:
                    $userRole = 'registro';
                    break;                
            }
        }
        else 
        {
            $userRole = 'index';
        }

        $controller    = $e->getRouteMatch()->getParam('controller');
        $action        = $e->getRouteMatch()->getParam('action');

        if(!$e->getViewModel()->acl->isAllowed($userRole, $route, $action)) 
        {
            // Redirección a login
            $e->getRouteMatch()
                    ->setParam('controller', 'Autenticacion\Controller\AutenticacionController')
                    ->setParam('action', 'logout');
        }

        /*
	    	         
	    //Comprobamos si no está permitido para ese rol esa ruta
	    if(!$e->getViewModel()->acl->isAllowed($userRole, $route)) 
        {
            // Redirección a login
            $e->getRouteMatch()
                    ->setParam('controller', 'Autenticacion\Controller\Autenticacion')
                    ->setParam('action', 'logout');

            /*
            //Devolvemos un error 404
	        $response = $e->getResponse();
	        $response->getHeaders()->addHeaderLine('Location', $e->getRequest()->getBaseUrl() . '/404');
	        $response->setStatusCode(404);
            */
        // }
    }

  
}