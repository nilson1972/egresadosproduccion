<?php

return [
    'index' => [
        "allow" => [
            "home",
            "autenticacion",
            "logout",
            "estudiante",
            "encuestapreg"
        ],
        "deny" => [
            "usuario"
        ],
    ],
    'estudiante' => [
        "allow" => [
            "home",
            "autenticacion",
            "logout",
            'estudiante'
        ],
        "deny" => [
            "usuario"
        ],
    ],
    'usuario' => [
        "allow" => [
            "index",
            "usuario",
            "reportes"
        ],
        "deny" => [],
    ],

];

?>