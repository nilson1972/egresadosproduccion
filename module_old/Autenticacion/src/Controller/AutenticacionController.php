<?php

namespace Autenticacion\Controller;



use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\Crypt\Password\Bcrypt;
use Zend\Mvc\Controller\AbstractActionController;


use Comun\DB;
use Autenticacion\Validation\LoginValidation;
use Zend\Authentication\Adapter\DbTable\CredentialTreatmentAdapter as AuthAdapter;


use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;


class AutenticacionController extends AbstractActionController
{

	const SUCCESS = 1;
    const FAILURE = 0;
    const FAILURE_IDENTITY_NOT_FOUND = -1;
    const FAILURE_IDENTITY_AMBIGUOUS = -2;
    const FAILURE_CREDENTIAL_INVALID = -3;
    const FAILURE_UNCATEGORIZED = -4;


    public function autenticarAction()
    {

		if ($this->getRequest()->isPost()) 
        {

        	$validation = new LoginValidation();

        	if (!$validation->validar()) {  

                $this->flashMessenger()->addErrorMessage($validation->getMensajes());	                
                return $this->redirect()->toRoute('home');
                // print_r($validation->getMensajes());
                // return $this->redirect()->toUrl($this->getRequest()->getBaseUrl().'/application');
            }


            $auth = new AuthenticationService();
            $auth->setStorage(new SessionStorage());

			$authAdapter = new AuthAdapter(
				    DB::getAdapter(),
                        'usuario',
				    'username',
				    'password',
				    'MD5(?)'
			);


			// echo md5($validation->getValue('password')); exit;

			// 

        	$authAdapter
			    ->setIdentity($validation->getValue('username'))
			    ->setCredential($validation->getValue('password'));


			// $result = $authAdapter->authenticate();
		    $result = $auth->authenticate($authAdapter);


		    // echo md5($validation->getValue('password')); exit;
		    

			if (! $result->isValid()) {  

				return $this->redirect()->toRoute('home');				
				/*
				switch ($result->getCode()) {

				    case self::FAILURE_IDENTITY_NOT_FOUND:
				        // do stuff for nonexistent identity 
				        break;

				    case self::FAILURE_CREDENTIAL_INVALID:
				        // do stuff for invalid credential 
				        break;

				    case self::SUCCESS:
				        // do stuff for successful authentication
				        break;

				    default:
				        // do stuff for other failure 
				        break;
				}
			    // Authentication failed; print the reasons why:
			    foreach ($result->getMessages() as $message) {
			        echo "$message\n";
			    }
			    */
			} else {

				$columnsToReturn = [
				    'id',
				    'id_tipo',
				    'username',
				    'nombre',
				    'activo'
				];


				$identidad = $authAdapter->getResultRowObject($columnsToReturn);

				if ($identidad->activo == '0') {
                    $this->flashMessenger()->addMessage("Usuario Inactivo - code 4");
                    // return $this->redirect()->toRoute('home');
                }

                $auth->getStorage()->write($identidad);

               	$container = new Container('data');
               	$container->k = md5(uniqid(rand(), true).time());


               	// Log de ingreso
               	// $logUser = new \Autenticacion\Table\UsuarioLog();
               	//   $logUser->setLog($this->getRequest(), $identidad->id);



               	switch ($identidad->id_tipo) {
               		case '1':
               			return $this->redirect()->toRoute('usuario');
               			break;
               		case '2':
               			return $this->redirect()->toRoute('usuario');
               			break;
               		default:
               			return $this->redirect()->toRoute('home');
               			break;
               	}

				/*
				echo "<pre>";
				print_r($authAdapter->getResultRowObject($columnsToReturn));
				echo "</pre>";
			    // Authentication succeeded; the identity ($username) is stored
			    // in the session:
			    // $result->getIdentity() === $auth->getIdentity()
			    // $result->getIdentity() === $username
			    */
			}

			
        }
    }

    public function logoutAction()
    {
    	$auth = new \Zend\Authentication\AuthenticationService();
        $auth->clearIdentity();

        session_destroy();
        return $this->redirect()->toRoute('home');
    }


}