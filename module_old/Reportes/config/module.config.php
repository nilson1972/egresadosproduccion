<?php

namespace Reportes;


use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'controllers' => [
        'factories' => [
            Controller\ReportesController::class => InvokableFactory::class,
        ],
    ],


    'router' => [
        'routes' => [

            'reportes' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/reportes[/:action[/:id]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ReportesController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],



    'view_manager' => [
        'template_path_stack' => [
            'reportes' => __DIR__ . '/../view',
        ],
    ],
];