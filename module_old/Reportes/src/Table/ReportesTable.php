<?php

namespace Reportes\Table;


use Comun\DB;

class reportesTable
{


    private $tablaEncuesta = 'tencuesta';
    private $tablaEstudiante = 'testudiante';
    private $tablaSedes = 'tsedes';
    private $tablaEncuestapost = 'tencuestapost';


    //Estudiantes que Realizaron la Encuesta preg y post
    public function Selectencuesta()
    {
        $campos_estudiante = [
            'id','nombres', 'apellidos', 'nro_ident'
        ];

        $campos_encuesta = [
            'encuesta_id' => 'id_estudent',
            'programa' => 'ha_progprecursa'
        ];
        return DB::selectJoin( $this->tablaEstudiante, $campos_estudiante,
                               $this->tablaEncuesta,  $campos_encuesta, 'id', 'id_estudent',
                               [],[]);
    }

    public function Selectencuestapost()
    {
        $campos_estudiante = [
            'id','nombres', 'apellidos', 'nro_ident'
        ];

        $campos_encuesta = [
            'encuesta_id' => 'id_estudent',
            'programa' => 'ha_progpostgraduado'
        ];
        return DB::selectJoin( $this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuestapost,  $campos_encuesta, 'id', 'id_estudent',
            [],[],[],[],[],[],[]);
    }

    //Estudiantes que NO Realizaron la Encuesta
    public function Selectnoencuesta()
    {
        $campos_estudiante = [
            'id','nombres', 'apellidos', 'nro_ident', 'programa'
        ];

        $campos_encuesta = [
            'encuesta_id' => 'id_estudent'
        ];
        $order = [
            'tabla1.programa'
        ];

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla2.id_estudent');
        return DB::selectJoin( $this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuesta,  $campos_encuesta, 'id', 'id_estudent',
            [], $order, false, false,
            $predicate, false, true);
    }

    //estudiantes que hicieron la encuesta por sedes preg
    public function Selectjoinencuestaxsede()
    {
      /*  $select = $db->select()
            ->from(array('s' => 'tsede'),
                array('nombre'))
            ->join(array('e' => 'encuesta'),
                's.codigo = e.sedeuniv',
                array('cantidad' => 'COUNT(e.id_estudent)'))
            ->group('s.nombre'); */

          $campos_sedes = [
              'nombre'
          ];

          $campos_encuesta = [
              'encuesta_id' => 'id_estudent'
          ];

          $group = [
              'nombre'
          ];
          return DB::selectJoin( $this->tablaSedes, $campos_sedes,
                                 $this->tablaEncuesta, $campos_encuesta, 'codigo', 'sedeuniv',
                                 [],[],[],[],[],$group,[]);
    }

    //¿Cuantos Egresados Trabajan Actualmente? preg y post
    public function Selecttrabaja()
    {
        $where = [
            'il_trabactualmente' => 'on'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    public function Selecttrabajapost()
    {
        $where = [
            'il_trabactualmente' => 'on'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuestapost,  $where);
    }

    //¿Cuantos Egresados NO Trabajan Actualmente? preg y post
    public function Selectnotrabaja()
    {
        $where = [
            'il_trabactualmente' => ''
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    public function Selectnotrabajapost()
    {
        $where = [
            'il_trabactualmente' => ''
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuestapost,  $where);
    }

    //¿Cuantos Egresados Estudiaron otra Carrera? preg y post
    public function Selectestudotra()
    {
        $where = [
            'ha_estudotracarrera' => 'on'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    public function Selectestudotrapost()
    {
        $where = [
            'ha_estudotropost' => 'on'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuestapost,  $where);
    }

    //¿Cuantos Egresados Estudiaron otra Carrera y la terminaron? preg y post
    public function Selectestudotrasi()
    {
        $where = [
            'ha_estudotracarrera' => 'on',
            'ha_termesacarrera' => 'SI'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    public function Selectestudotrasipost()
    {
        $where = [
            'ha_estudotropost' => 'on',
            'ha_termesepost' => 'SI'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuestapost,  $where);
    }

    //¿Cuantos Egresados Estudiaron otra Carrera y NO la terminaron? preg y post
    public function Selectestudotrano()
    {
        $where = [
            'ha_estudotracarrera' => 'on',
            'ha_termesacarrera' => 'NO'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    public function Selectestudotranopost()
    {
        $where = [
            'ha_estudotropost' => 'on',
            'ha_termesepost' => 'NO'
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuestapost,  $where);
    }

    //¿Cuantos Egresados NO estudiaron otra Carrera? preg y post
    public function Selectnoestudotra()
    {
        $where = [
            'ha_estudotracarrera' => ''
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuesta,  $where);
    }

    public function Selectnoestudotrapost()
    {
        $where = [
            'ha_estudotropost' => ''
        ];
        return DB::selectRegistrosWhere( $this->tablaEncuestapost,  $where);
    }

}