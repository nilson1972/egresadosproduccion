<?php


namespace Reportes\Controller;



use Comun\GenericController;



class ReportesController extends GenericController
{


   public function __construct()
    {
        $this->tablaReportes = new \Reportes\Table\ReportesTable();
    }

    public function indexAction()
    {
        return $this->viewModelAjax([

        ]);
    }

    public function indexpostAction()
    {
        return $this->viewModelAjax([

        ]);
    }

    public function formEncuestasRealizadasAction(){

        return $this->viewModelAjax([
            'siencuesta' => $this->tablaReportes->Selectencuesta(),
            'siencuestapost' => $this->tablaReportes->Selectencuestapost()
        ]);
    }

    public function formEncuestasNoRealizadasAction(){

        return $this->viewModelAjax([
            'noencuesta' => $this->tablaReportes->Selectnoencuesta()
        ]);
    }

    public function formEncuestaIndicadorAction(){

        return $this->viewModelAjax([
            'estudtrabaja' => $this->tablaReportes->Selecttrabaja(),
            'estudnotrabaja' => $this->tablaReportes->Selectnotrabaja(),
            'estudotracarre' => $this->tablaReportes->Selectestudotra(),
            'estudotracarresi' => $this->tablaReportes->Selectestudotrasi(),
            'estudotracarreno' => $this->tablaReportes->Selectestudotrano(),
            'noestudotracarre' => $this->tablaReportes->Selectnoestudotra()

        ]);
    }

    public function formEncuestasPorSedesAction(){

        return $this->viewModelAjax([
            'encuestaxsed' => $this->tablaReportes->Selectjoinencuestaxsede()
        ]);
    }

    //postgrado

    public function encuestasPostRealizadasAction(){

        return $this->viewModelAjax([
            'siencuestapost' => $this->tablaReportes->Selectencuestapost()
        ]);
    }

    public function encuestasPostIndicadorAction(){

        return $this->viewModelAjax([
            'estudtrabajapost' => $this->tablaReportes->Selecttrabajapost(),
            'estudnotrabajapost' => $this->tablaReportes->Selectnotrabajapost(),
            'estudotracarrepost' => $this->tablaReportes->Selectestudotrapost(),
            'estudotracarresipost' => $this->tablaReportes->Selectestudotrasipost(),
            'estudotracarrenopost' => $this->tablaReportes->Selectestudotranopost(),
            'noestudotracarrepost' => $this->tablaReportes->Selectnoestudotrapost()

        ]);
    }
}