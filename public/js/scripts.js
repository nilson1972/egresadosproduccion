function ajaxGet(url, contenedor) 
{
        $.ajax({
            method: "GET",
            cache: false,
            url: url,
            beforeSend: function( xhr ) {
                  $("#"+contenedor).html("<i class='spinner loading green icon'></i>Cargando...");
            },
            success: function( data ) {
                  $("#"+contenedor).html(data);
            },
            statusCode: {
                404: function() {
                  $("#"+contenedor).html("<i class='info circle green icon'></i>Objeto no encontrado");
                },
                500: function(data) {
                  $("#"+contenedor).html("<i class='info circle green icon'></i>Error interno");
                }
            }
        });
}; 


function ajaxPost(url, contenedor, datos) 
{
        $.ajax({
            method: "POST",
            cache: false,
            url: url,
            data: datos,
            beforeSend: function( xhr ) {
                  $("#"+contenedor).html("<i class='spinner loading green icon'></i>Cargando...");
            },
            success: function( data ) {
                  $("#"+contenedor).html(data);
            }
        });
}; 



function buttonPostValidation(boton, accion, formulario, contenedor) 
{
    $(boton).api(
    {
            action: accion,
            method : 'POST',
            serializeForm: true,
            beforeSend: function(settings) {
                $(formulario).form('validate form');
                if ($(formulario).form('is valid')) {
                    $(boton).addClass('disabled');
                    return true;
                } else{
                    return false;
                };
            },
            onComplete: function(response)
            {
                $(boton).removeClass('disabled');
                $('#'+contenedor).html(response);
            },
    });
};

function buttonGET(boton, accion, contenedor) 
{
    $(boton).api(
    {
            action: accion,
            method : 'GET',
            serializeForm: true,
            beforeSend: function(settings) {
                $("#"+contenedor).html("<i class='spinner loading green icon'></i>Cargando...");
            },
            onComplete: function(response)
            {
                $('#'+contenedor).html(response);
            },
    });
};

function transicionSiguiente(container_actual, container_siguiente){

        $(container_actual).transition({
            animation : 'fly right',
            duration: '500ms',
            onComplete: function(){
                $(container_siguiente).transition('fly left', '500ms');
            }
        });
}

function transicionAnterior(container_actual, container_anterior){
        $(container_actual).transition({
                animation : 'fly left',
                duration: '500ms',
                onComplete: function(){
                        $(container_anterior).transition('fly right', '500ms');
                }
        });
}
