<?php

namespace Reportes\Table;


use Comun\DB;

class reportesTable
{


    private $tablaEncuesta = 'tencuestapre';
    private $tablaEncuestapost = 'tencuestapost';
    private $tablaEstudiante = 'testudiante';
    private $tablaProgramas = 'tprogramas';
    private $tablaAutorizados = 'tautorizados';
    private $tablaEstudios = 'testudios';
    private $tablaSedes = 'tsedes';
    private $tablaActas = 'tactas';
    private $tablainscritos = 'tinscripeventos';
    private $tablaDatosActualizados = 'tdatosestudactualizados';
    private $tablaRespuestasdatosact = 'tpreg_prog_mejoras_o_fortalezas';
    private $tablaEsudiosRealizados = 'testudios_realiza';
    private $tablaPais = 'tpaises';
    private $tablaCiudad = 'tciudades';


    //Lista de estudiantes que Realizaron la Encuesta preg

    public function Selectencuesta($idcorte)
    {
        ini_set('max_execution_time', 300);
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id_autorizado','dp_identestud', 'fec_encuesta'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.id_corte' => $idcorte
        ];
        return DB::selectJoinTresTablas($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuesta, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progprecursa', 'id',
            $where, []);
    }

    public function Selectencuestapost($idcorte)
    {
        ini_set('max_execution_time', 300);
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id_autorizado','dp_identestud', 'fec_encuesta'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.id_corte' => $idcorte,
        ];
        return DB::selectJoinTresTablas($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuestapost, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progpostgraduado', 'id',
            $where, []);
    }

    public function Selectencuestaporprog($idcorte, $prog)
    {
        ini_set('max_execution_time', 300);
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id_autorizado','dp_identestud', 'fec_encuesta'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.ha_progprecursa' => $prog,
            'tabla2.id_corte' => $idcorte,
        ];

        return DB::selectJoinTresTablas($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuesta, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progprecursa', 'id',
            $where, []);
    }

    public function Selectencuestaporprogpost($idcorte, $prog)
    {
        ini_set('max_execution_time', 300);
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id_autorizado','dp_identestud', 'fec_encuesta'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.ha_progpostgraduado' => $prog,
            'tabla2.id_corte' => $idcorte,
        ];

        return DB::selectJoinTresTablas($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuestapost, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progpostgraduado', 'id',
            $where, []);
    }


    //Estudiantes que NO Realizaron la Encuesta pre y post
    public function Selectnoencuesta($idcorte)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'id', 'nombres', 'apellidos', 'nro_ident'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.id = tabla2.id_estudiante',
            [],
            'left'
        )
          ->join(
             array('tabla3' => $this->tablaEncuesta),
              'tabla2.id = tabla3.id_autorizado',
              [],
                'left'
          )
            ->join(
                array('tabla4' => $this->tablaProgramas),
                'tabla2.id_programa = tabla4.id',
                ['nombre'],
                'left'
            );
            /*->join(
                array('tabla5' => $this->tablaEstudios),
                'tabla4.id_estudio = tabla5.id',
                [],
                'left'
            );*/

        $select->where([
            'tabla2.id_corte' => $idcorte,
            'tabla4.id_estudio' => 1,
        ]);

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla3.id_autorizado');
        $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Selectnoencuestapost($idcorte)
    {
        ini_set('max_execution_time', 300);
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'id', 'nombres', 'apellidos', 'nro_ident'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.id = tabla2.id_estudiante',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEncuestapost),
                'tabla2.id = tabla3.id_autorizado',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaProgramas),
                'tabla2.id_programa = tabla4.id',
                ['nombre'],
                'left'
            );
            /*->join(
                array('tabla5' => $this->tablaEstudios),
                'tabla4.id_estudio = tabla5.id',
                [],
                'left'
            );*/

        $select->where([
            'tabla2.id_corte' => $idcorte,
            'tabla4.id_estudio' => 2,
        ]);

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla3.id_autorizado');
        $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function Selecencparaacta($idcorte, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuesta));
        $select->columns([
            'id_enc'=>'id', 'id_autorizado', 'dp_identestud', 'fec_encuesta'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id_est'=>'id', 'nombres', 'apellidos',],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progprecursa = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaActas),
                'tabla4.id_autorizado = tabla1.id_autorizado',
                [],
                'left'
            );

        $select->where([
            'tabla1.ha_progprecursa' => $prog,
            'tabla1.id_corte' => $idcorte,
        ]);

        $select->order(['tabla2.apellidos', 'tabla2.nombres']);

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Selecencpostparaacta($idcorte, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuestapost));
        $select->columns([
            'id_enc'=>'id', 'id_autorizado', 'dp_identestud', 'fec_encuesta'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id_est'=>'id', 'nombres', 'apellidos',],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progpostgraduado = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaActas),
                'tabla4.id_autorizado = tabla1.id_autorizado',
                [],
                'left'
            );

        $select->where([
            'tabla1.ha_progpostgraduado' => $prog,
            'tabla1.id_corte' => $idcorte,
        ]);

        $select->order(['tabla2.apellidos', 'tabla2.nombres']);

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaestudconactas($idcorte)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuesta));
        $select->columns([
            'id_enc'=>'id', 'id_autorizado', 'dp_identestud', 'fec_encuesta'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id_est'=>'id', 'nombres', 'apellidos',],
            'right'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progprecursa = tabla3.id',
                ['nombre'],
                'right'
            )
            ->join(
                array('tabla4' => $this->tablaActas),
                'tabla4.id_autorizado = tabla1.id_autorizado',
                ['carnet_entregado', 'nro_actgral'],
                'right'
            );

        $select->where([
            //'tabla1.ha_progprecursa' => $prog,
            'tabla1.id_corte' => $idcorte,
        ]);

        $select->order(['tabla2.nombres', 'tabla2.apellidos']);

        //$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        //$select->where->addPredicate($predicate);

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaestudconactaspos($idcorte)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuestapost));
        $select->columns([
             'id_enc'=>'id', 'id_autorizado', 'dp_identestud', 'fec_encuesta'
        ]) ;
        $select->join(
           array('tabla2' => $this->tablaEstudiante),
           'tabla1.id_estudent = tabla2.id',
              ['id_est'=>'id', 'nombres', 'apellidos',],
           'right'
        )
        ->join(
            array('tabla3' => $this->tablaProgramas),
            'tabla1.ha_progpostgraduado = tabla3.id',
            ['nombre'],
            'right'
        )
        ->join(
            array('tabla4' => $this->tablaActas),
            'tabla4.id_autorizado = tabla1.id_autorizado',
            [],
            'right'
        );

        $select->where([
           //'tabla1.ha_progprecursa' => $prog,
             'tabla1.id_corte' => $idcorte,
        ]);

    //  $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
    //  $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function verestudconactas($idautor)
    {
        $where = [
            'id_autorizado' =>$idautor,
        ];
        return DB::selectRegistro( $this->tablaActas, $where);
    }

//enceustas
    public function Listaencuestadosporsexopre($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuesta));
        $select->columns([
            'id', 'dp_identestud', 'fec_encuesta', 'dp_dirresidencia', 'dp_telestud', 'dp_mpioresidencia', 'dp_deptoresidencia',
            'dp_celestud', 'dp_emailestud', 'ha_npsaberpro', 'ha_anosemacaini', 'ha_semacaini', 'ha_anosemacafin', 'ha_semacafin',
            utf8_encode('ep_areaporespec'), 'il_trabactualmente', 'trabsectprofe','il_ocupesetraba',
            utf8_encode('il_nomemplab')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
        )
        ->join(
            array('tabla3' => $this->tablaProgramas),
           'tabla1.ha_progprecursa = tabla3.id',
             ['nombre'],
             'left'
        )
        ->join(
        array('tabla4' => $this->tablaRespuestasdatosact),
           'tabla1.id_prog_mejorar = tabla4.id',
             ['resp_mejorar' => utf8_decode('respuesta')],
             'left'
        )
        ->join(
        array('tabla5' => $this->tablaRespuestasdatosact),
           'tabla1.id_prog_fortaleza = tabla5.id',
             ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
       );

        if ($prog<>0) {
            if ($sexo == 'F') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                    'tabla1.ha_progprecursa' => $prog,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                    'tabla1.ha_progprecursa' => $prog,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla1.ha_progprecursa' => $prog,
                ]);
            }
            //$predicate1 = new \Zend\Db\Sql\Predicate\In('tabla1.ha_progprecursa', $prog);
            //$select->where->addPredicate($predicate1);
        } else {
            if ($sexo == 'F') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                ]);
            }
        }

        $predicate2 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_encuesta', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

    public function Listaencuestadosporsexopos($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuestapost));
        $select->columns([
            'id', 'dp_identestud', 'fec_encuesta','dp_dirresidencia', 'dp_telestud', 'dp_mpioresidencia', 'dp_deptoresidencia',
            'dp_celestud', 'dp_emailestud', 'ha_npsaberpro', 'ha_anoinipost', 'ha_seminipost', 'ha_anofinpost', 'ha_semfinpost',
            utf8_encode('ep_areaformcontinua'), 'il_trabactualmente', 'trabsectprofe', 'il_ocupesetraba',
            utf8_encode('il_nomemplab')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progpostgraduado = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaRespuestasdatosact),
                'tabla1.id_prog_mejorar = tabla4.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaRespuestasdatosact),
                'tabla1.id_prog_fortaleza = tabla5.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($prog<>0) {
            if ($sexo == 'F') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                    'tabla1.ha_progpostgraduado' => $prog,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                    'tabla1.ha_progpostgraduado' => $prog,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla1.ha_progpostgraduado' => $prog,
                ]);
            }
            //$predicate1 = new \Zend\Db\Sql\Predicate\In('tabla1.ha_progpostgraduado', $prog);
            //$select->where->addPredicate($predicate1);
        } else{
            if ($sexo == 'F') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla2.sexo' => $sexo,
                ]);
            }
        }

        $predicate2 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_encuesta', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaencuestadosporsexotrabajopre($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuesta));
        $select->columns([
            'id', 'dp_identestud', 'fec_encuesta', 'dp_dirresidencia', 'dp_telestud', 'dp_mpioresidencia',
            'dp_deptoresidencia', 'dp_celestud', 'dp_emailestud', 'il_nomemplab', 'il_fecinilab', 'il_ocupesetraba'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progprecursa = tabla3.id',
                ['nombre'],
                'left'
            );

        if ($sexo=='F') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progprecursa' => $prog,
            ]);
        }
        if ($sexo=='M') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progprecursa' => $prog,
            ]);
        }
        if ($sexo=='A') {
            $select->where([
                'tabla1.ha_progprecursa' => $prog,
            ]);
        }

        $predicate1 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_encuesta', $fec1, $fec2);
        $predicate2 = new \Zend\Db\Sql\Predicate\IsNotNull('tabla1.il_trabactualmente');
        $select->where->addPredicate($predicate1);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

    public function Listaencuestadosporsexotrabajopos($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuestapost));
        $select->columns([
            'id', 'dp_identestud', 'fec_encuesta', 'dp_dirresidencia', 'dp_telestud', 'dp_mpioresidencia', 'dp_deptoresidencia',
            'dp_celestud', 'dp_emailestud', 'il_nomemplab', 'il_fecinilab', 'il_ocupesetraba'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progpostgraduado = tabla3.id',
                ['nombre'],
                'left'
            );

        if ($sexo=='F') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progpostgraduado' => $prog,
            ]);
        }
        if ($sexo=='M') {
            $select->where([
               'tabla2.sexo' => $sexo,
               'tabla1.ha_progpostgraduado' => $prog,
            ]);
        }
        if ($sexo=='A') {
            $select->where([
                'tabla1.ha_progpostgraduado' => $prog,
            ]);
        }

        $predicate1 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_encuesta', $fec1, $fec2);
        $predicate2 =  new \Zend\Db\Sql\Predicate\IsNotNull('tabla1.il_trabactualmente');
        $select->where->addPredicate($predicate1);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaencuestaporsexonotrabajopre($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuesta));
        $select->columns([
            'id', 'dp_identestud', 'fec_encuesta', 'dp_dirresidencia', 'dp_telestud', 'dp_mpioresidencia', 'dp_deptoresidencia',
            'dp_celestud', 'dp_emailestud', 'il_nomemplab', 'il_fecinilab', 'il_ocupesetraba'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progprecursa = tabla3.id',
                ['nombre'],
                'left'
            );

        if ($sexo=='F') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progprecursa' => $prog,
            ]);
        }
        if ($sexo=='M') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progprecursa' => $prog,
            ]);
        }
        if ($sexo=='A') {
            $select->where([
                'tabla1.ha_progprecursa' => $prog,
            ]);
        }

        $predicate1 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_encuesta', $fec1, $fec2);
        $predicate2 =  new \Zend\Db\Sql\Predicate\IsNull('tabla1.il_trabactualmente');
        $select->where->addPredicate($predicate1);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaencuestaporsexonotrabajopos($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuestapost));
        $select->columns([
            'id', 'dp_identestud', 'fec_encuesta', 'dp_dirresidencia', 'dp_telestud', 'dp_mpioresidencia', 'dp_deptoresidencia',
            'dp_celestud', 'dp_emailestud', 'il_nomemplab', 'il_fecinilab', 'il_ocupesetraba'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla1.ha_progpostgraduado = tabla3.id',
                ['nombre'],
                'left'
            );

        if ($sexo=='F') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progpostgraduado' => $prog,
            ]);
        }
        if ($sexo=='M') {
            $select->where([
                'tabla2.sexo' => $sexo,
                'tabla1.ha_progpostgraduado' => $prog,
            ]);
        }
        if ($sexo=='A') {
            $select->where([
                'tabla1.ha_progpostgraduado' => $prog,
            ]);
        }

        $predicate1 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_encuesta', $fec1, $fec2);
        $predicate2 =  new \Zend\Db\Sql\Predicate\IsNull('tabla1.il_trabactualmente');
        $select->where->addPredicate($predicate1);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

//graduados
    public function Listagraduadosporsexopre($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuesta),
            'tabla1.id_autorizado = tabla2.id_autorizado',
            ['id_estudent', 'dp_identestud', 'ha_npsaberpro', 'dp_lugarexped', 'dp_fecnac', 'ha_semacaini',
             'ha_anosemacaini', 'ha_semacafin', 'ha_anosemacafin', 'ep_areaporespec'],
            'left'
            )
            ->join(
            array('tabla3' => $this->tablaEstudiante),
            'tabla2.id_estudent = tabla3.id',
            ['id', 'nombres', 'apellidos', 'sexo'],
            'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.ha_progprecursa = tabla5.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla6' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_mejorar = tabla6.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla7' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_fortaleza = tabla7.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($prog<>0) {
            if ($sexo == 'F') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                    'tabla2.ha_progprecursa' => $prog,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                    'tabla2.ha_progprecursa' => $prog,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla2.ha_progprecursa' => $prog,
                ]);
            }
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.ha_progprecursa', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
            if ($sexo == 'F') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.estado' => 'V',
                ]);
            }
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

    public function Listagraduadosporsexopos($sexo, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuestapost),
            'tabla1.ID_AUTORIZADO = tabla2.ID_AUTORIZADO',
            ['id_estudent', 'dp_identestud', 'ha_npsaberpro', 'dp_lugarexped', 'dp_fecnac', 'ha_semacaini'=>'ha_seminipost',
                'ha_anosemacaini'=>'ha_anoinipost', 'ha_semacafin'=>'ha_semfinpost', 'ha_anosemacafin'=>'ha_anofinpost',
                'ep_areaporespec'=>utf8_decode('ep_areaformcontinua')],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudent = tabla3.id',
                ['id', 'nombres', 'apellidos', 'sexo'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.ha_progpostgraduado = tabla5.id',
                [utf8_decode('nombre')],
                'left'
            )
            ->join(
                array('tabla6' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_mejorar = tabla6.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla7' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_fortaleza = tabla7.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($prog<>0) {
            if ($sexo == 'F') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                    'tabla2.ha_progpostgraduado' => $prog,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                    'tabla2.ha_progpostgraduado' => $prog,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla2.ha_progpostgraduado' => $prog,
                ]);
            }
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.ha_progpostgraduado', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
            if ($sexo == 'F') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla3.sexo' => $sexo,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.estado' => 'V',
                ]);
            }
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

    public function Listagraduadosactaporfecha($acta, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();

        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuesta),
            'tabla1.ID_AUTORIZADO = tabla2.ID_AUTORIZADO',
            ['id_estudent', 'dp_identestud', 'ha_npsaberpro', 'dp_lugarexped', 'dp_fecnac', 'ha_semacaini', 'ha_anosemacaini',
                'ha_semacafin', 'ha_anosemacafin', 'ep_areaporespec'],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudent = tabla3.id',
                ['id', 'nombres', 'apellidos', 'sexo'],
                'inner'
            )
            ->join(
                array('tabla4' => $this->tablaProgramas),
                'tabla2.ha_progprecursa = tabla4.id',
                ['nombre'],
                'inner'
            )
            ->join(
                array('tabla5' => $this->tablaDatosActualizados),
                'tabla3.id = tabla5.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )->join(
                array('tabla6' => $this->tablaRespuestasdatosact),
                'tabla5.id_prog_mejorar = tabla6.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla7' => $this->tablaRespuestasdatosact),
                'tabla5.id_prog_fortaleza = tabla7.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($prog<>0) {
                $select->where([
                    'tabla5.estado' => 'V',
                    'tabla1.nro_actgral' => $acta,
                    'tabla2.ha_progprecursa' => $prog,
                ]);
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.ha_progprecursa', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
                $select->where([
                    'tabla5.estado' => 'V',
                    'tabla1.nro_actgral' => $acta,
                ]);
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

    public function Listagraduadosactaporfechapos($acta, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuestapost),
            'tabla1.ID_AUTORIZADO = tabla2.ID_AUTORIZADO',
            ['id_estudent', 'dp_identestud', 'ha_npsaberpro', 'dp_lugarexped', 'dp_fecnac', 'ha_semacaini'=>'ha_seminipost',
                'ha_anosemacaini'=>'ha_anoinipost', 'ha_semacafin'=>'ha_semfinpost', 'ha_anosemacafin'=>'ha_anofinpost',
                'ep_areaporespec'=>'ep_areaformcontinua'],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudent = tabla3.id',
                ['id', 'nombres', 'apellidos', 'sexo'],
                'inner'
            )
            ->join(
                array('tabla4' => $this->tablaProgramas),
                'tabla2.ha_progpostgraduado = tabla4.id',
                ['nombre'],
                'inner'
            )
            ->join(
                array('tabla5' => $this->tablaDatosActualizados),
                'tabla3.id = tabla5.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla6' => $this->tablaRespuestasdatosact),
                'tabla5.id_prog_mejorar = tabla6.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla7' => $this->tablaRespuestasdatosact),
                'tabla5.id_prog_fortaleza = tabla7.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($prog<>0) {
            $select->where([
                'tabla5.estado' => 'V',
                'tabla1.nro_actgral' => $acta,
                'tabla2.ha_progpostgraduado' => $prog,
            ]);
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.ha_progprecursa', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
            $select->where([
                'tabla5.estado' => 'V',
                'tabla1.nro_actgral' => $acta,
            ]);
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

    public function ListagraduadosTrabajan($sexo, $tpest, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]);
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.ID_AUTORIZADO = tabla2.ID',
            [],
            'inner'
            )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudiante = tabla3.id',
                ['id', 'nombres', 'apellidos', 'sexo', 'nro_ident'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                ['nomprog' => 'nombre'],
                'left'
            )
            ->join(
                array('tabla6' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_mejorar = tabla6.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla7' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_fortaleza = tabla7.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($prog<>0) {
            if ($sexo == 'F') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.trabajo_act' => 'on',
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.trabajo_act' => 'on',
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.trabajo_act' => 'on',
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.id_programa', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
            if ($sexo == 'F') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.trabajo_act' => 'on',
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.trabajo_act' => 'on',
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.trabajo_act' => 'on',
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $select->where->addPredicate($predicate2);
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function graduadosTrabajanUbicacion($sexo, $tpest, $fec1, $fec2, $pais, $ciud)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]);
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.id_autorizado = tabla2.id',
            [],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudiante = tabla3.id',
                ['id', 'nombres', 'apellidos', 'sexo', 'nro_ident'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                ['direc_residen', 'ciud_res', 'celular','email','trabajo_act', 'empleado', 'empresario',
                    'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa', 'pais_lab', 'ciud_lab',
                    'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaEsudiosRealizados),
                'tabla4.id = tabla5.id_datosactualiza',
                ['id_titulo_obtenido', 'nivel_acade', 'id_universidad', 'ano_grado'],
                'left'
            )
            ->join(
                array('tabla9' => $this->tablaPais),
                'tabla4.pais_lab = tabla9.Codigo',
                [utf8_decode('Pais')],
                'left'
            )
            ->join(
                array('tabla10' => $this->tablaCiudad),
                'tabla4.ciud_lab = tabla10.id',
                ['ciudlab' => utf8_decode('ciudad')],
                'left'
            )
            ->join(
                array('tabla11' => $this->tablaCiudad),
                'tabla4.ciud_res = tabla11.id',
                ['ciudres' => utf8_decode('ciudad')],
                'left'
            )
            ->join(
                array('tabla6' => $this->tablaProgramas),
                'tabla5.id_titulo_obtenido = tabla6.id',
                ['nomprog' => 'nombre'],
                'left'
            )
            ->join(
                array('tabla7' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_mejorar = tabla7.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla8' => $this->tablaRespuestasdatosact),
                'tabla4.id_prog_fortaleza = tabla8.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        if ($sexo == 'A') {
            $select->where([
                'tabla4.empleado' => 'on',
                'tabla4.estado' => 'V',
                'tabla4.pais_lab' => $pais,
                'tabla4.ciud_lab' => $ciud,
                'tabla6.id_estudio' => $tpest,
            ]);
        }else{
            $select->where([
                'tabla3.sexo' => $sexo,
                'tabla4.empleado' => 'on',
                'tabla4.estado' => 'V',
                'tabla4.pais_lab' => $pais,
                'tabla4.ciud_lab' => $ciud,
                'tabla6.id_estudio' => $tpest,
            ]);
        }

        $predicate =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $select->where->addPredicate($predicate);
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function ListagraduadosNoTrabajan($sexo, $tpest, $fec1, $fec2, $prog)
    {
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_actas', 'nro_actgral', 'nro_actind', 'nro_diploma'
        ]);
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.ID_AUTORIZADO = tabla2.ID',
            [],
            'left'
            )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudiante = tabla3.id',
                ['id', 'nombres', 'apellidos', 'nro_ident', 'sexo'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                ['nomprog' => 'nombre'],
                'left'
            );

        if ($prog<>0) {
            if ($sexo == 'F') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.id_programa', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
            if ($sexo == 'F') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
            if ($sexo == 'M') {
                $select->where([
                    'tabla3.sexo' => $sexo,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
            if ($sexo == 'A') {
                $select->where([
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        $predicate3 =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.trabajo_act');
        $select->where->addPredicate($predicate2);
        $select->where->addPredicate($predicate3);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function EstadoCarnetEstudProgramas($carnet, $tpest, $fec1, $fec2, $prog)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'idacta'=>'id', 'id_autorizado', 'fec_entrecarnet', 'carnet_entregado'
        ]);
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.id_autorizado = tabla2.id',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudiante = tabla3.id',
                ['id', 'nombres', 'apellidos', 'nro_ident', 'sexo'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                ['direc_residen', 'celular','email','trabajo_act', 'empleado', 'empresario', 'pensionado',
                    'estudiante', 'desempleado', 'trabsectprofe', 'nom_empresa', 'sector', 'dir_empresa',
                    'tel_empresa', 'cargo_empresa', 'tiempo_vinculacion'],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                ['nomprog' => 'nombre'],
                'left'
            );

        if ($prog<>0) {
            if ($carnet == '1') {
                $select->where([
                    'tabla1.carnet_entregado' => $carnet,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            if ($carnet == '2') {
                $select->where([
                    'tabla1.carnet_entregado' => $carnet,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            if ($carnet == '3') {
                $select->where([
                    'tabla1.carnet_entregado' => $carnet,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                    'tabla2.id_programa' => $prog,
                ]);
            }
            //$predicate1 =  new \Zend\Db\Sql\Predicate\In('tabla2.id_programa', $prog);
            //$select->where->addPredicate($predicate1);
        }else{
            if ($carnet == '1') {
                $select->where([
                    'tabla1.carnet_entregado' => $carnet,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
            if ($carnet == '2') {
                $select->where([
                    'tabla1.carnet_entregado' => $carnet,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
            if ($carnet == '3') {
                $select->where([
                    'tabla1.carnet_entregado' => $carnet,
                    'tabla4.estado' => 'V',
                    'tabla5.id_estudio' => $tpest,
                ]);
            }
        }

        $predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_entrecarnet', $fec1, $fec2);
        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function Selecnrograduadosporprog($idprog){
        ini_set('max_execution_time', 300); //5 minutos
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(tabla1.id)'),
            'ano' => new \Zend\Db\Sql\Expression('EXTRACT(YEAR FROM tabla1.fec_actas)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla2.id = tabla1.id_autorizado',
            [],
            ''
        )
        ->join(
            array('tabla3' => $this->tablaEncuesta),
            'tabla2.id = tabla3.id_autorizado',
            [],
            ''
        )
        ->join(
            array('tabla4' => $this->tablaSedes),
            'tabla3.sedeuniv = tabla4.id',
            ['id', 'nomsede'=>'nombre'],
            ''
        )
        ->join(
            array('tabla5' => $this->tablaProgramas),
            'tabla2.id_programa = tabla5.id',
            ['id', 'nomprog'=>'nombre'],
            ''
        );

        $select->where([
            'tabla5.id' => $idprog,
        ]);

        $select->group(['tabla4.id']);
        $select->group(['tabla5.id']);
        $select->group(new \Zend\Db\Sql\Expression("EXTRACT(YEAR FROM tabla1.fec_actas)"));

        //$predicate2 =  new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actas', $fec1, $fec2);
        //$select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        return $statement->execute();
    }

}