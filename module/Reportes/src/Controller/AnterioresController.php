<?php

namespace Reportes\Controller;

use Comun\GenericController;


class AnterioresController extends GenericController
{

    public function __construct()
    {
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();
        $this->tablaVerEncuesta = new \Usuario\Table\VerEncuestaTable();
        $this->tablaVerEncuestaPost = new \Usuario\Table\VerEncuestaPostTable();
        $this->validateVerEncuesta = new \Usuario\Validate\VerEncuestaValidate();
        $this->validateBuscaEstudiante = new \Usuario\Validate\BuscaEstudianteValidate();
        $this->tablaReportes = new \Reportes\Table\ReportesTable();
        $this->validateReportes = new \Reportes\Validate\ReportesValidate();
        $this->tablaEncuestaAnteriores = new \Reportes\Table\EncuestaAnterioresTable();
        $this->validateEncuestaAnteriores = new \Reportes\Validate\EncuestaAnterioresValidate();
    }

    public function indexAction()
    {

    }

    /*public function seleccionarPeriodoEncuestaAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'cortevigente' => $corte,
        ]);
    }*/


    // Pregrado
    public function encuestasAnterioresRealizadasPreAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'Autoriencant' => $this->tablaEncuestaAnteriores->getAnterTotalAutoriza((int)$this->getParam1()),
            'realizencant'  => $this->tablaEncuestaAnteriores->getTotalEncAntRealiz((int)$this->getParam1()),
            'siencuesta' => $this->tablaReportes->Selectencuesta((int)$this->getParam1()),
        ]);
    }

    public function encuestasAnterioresNoRealizadasPreAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'Autoriencant' => $this->tablaEncuestaAnteriores->getAnterTotalAutoriza((int)$this->getParam1()),
            'realizencant'  => $this->tablaEncuestaAnteriores->getTotalEncAntRealiz((int)$this->getParam1()),
            'noencuesta' => $this->tablaReportes->Selectnoencuesta((int)$this->getParam1()),
        ]);
    }

    public function anterioresSeleccionarProgramaAction()
    {
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }

    public function encuestasAnterioresRealizadasPorProgAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'siencuestaporprog' => $this->tablaReportes->Selectencuestaporprog($corteinac['id'],
                $this->validateReportes->getValue('programa')),
        ]);
    }

    public function selecFechaEncuestasAnterioresRealizadasPreAction(){
        return $this->viewModelAjax([

        ]);
    }

    public function encuestasAnterioresRealizadasFechaPreAction(){
        if ( ! $this->validateEncuestaAnteriores->validarFechas() ) {
           return $this->verMensajeError($this->validateEncuestaAnteriores->getMensajes());
        }
        return $this->viewModelAjax([
            'siencuestaporfec' => $this->tablaEncuestaAnteriores->Selectencuestasporfechas(
                $this->validateEncuestaAnteriores->getValue('fecha1'),
                $this->validateEncuestaAnteriores->getValue('fecha2')),
            'fec1' =>$this->validateEncuestaAnteriores->getValue('fecha1'),
            'fec2' =>$this->validateEncuestaAnteriores->getValue('fecha2'),
        ]);
    }

    // Postgrado
    public function encuestasAnterioresRealizadasPosAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'Autoriencantpos' => $this->tablaEncuestaAnteriores->getAnterTotalAutorizapos((int)$this->getParam1()),
            'realizencantpos'  => $this->tablaEncuestaAnteriores->getTotalEncAntRealizpos((int)$this->getParam1()),
            'siencuestapos' => $this->tablaReportes->Selectencuestapost((int)$this->getParam1()),
        ]);
    }

    public function encuestasAnterioresNoRealizadasPosAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'Autoriencantpos' => $this->tablaEncuestaAnteriores->getAnterTotalAutorizapos((int)$this->getParam1()),
            'realizencantpos'  => $this->tablaEncuestaAnteriores->getTotalEncAntRealizpos((int)$this->getParam1()),
            'noencuestapos' => $this->tablaReportes->Selectnoencuestapost((int)$this->getParam1()),
        ]);
    }

    public function anterioresSeleccionarProgramaPosAction()
    {
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }

    public function encuestasAnterioresRealizadasPorProgPosAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'siencuestaporprogpos' => $this->tablaReportes->Selectencuestaporprogpost($corteinac['id'],
                $this->validateReportes->getValue('programa')),
        ]);
    }

    public function selecFechaEncuestasAnterioresRealizadasPosAction(){
        return $this->viewModelAjax([

        ]);
    }

    public function encuestasAnterioresRealizadasFechaPsAction(){
        if ( ! $this->validateEncuestaAnteriores->validarFechas() ) {
            return $this->verMensajeError($this->validateEncuestaAnteriores->getMensajes());
        }
        return $this->viewModelAjax([
            'siencuestaporfecpos' => $this->tablaEncuestaAnteriores->Selectencuestasporfechaspos(
                $this->validateEncuestaAnteriores->getValue('fecha1'),
                $this->validateEncuestaAnteriores->getValue('fecha2')),
            'fec1' =>$this->validateEncuestaAnteriores->getValue('fecha1'),
            'fec2' =>$this->validateEncuestaAnteriores->getValue('fecha2'),
        ]);
    }

    public function seleccionarProgramaNroActaAction()
    {
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }

    public function verListaEncuestadosParaActaAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            /*'parametro1'=>(int)$this->getParam1(),
            'progra' => $this->validateReportes->getValue('programa'),*/
            'cortesinactivos' => $corteinac,
            'listaparaactas' => $this->tablaReportes->Selecencparaacta($corteinac['id'],
                                                                       $this->validateReportes->getValue('programa')),
        ]);
    }

    public function seleccionarProgramaNroActaPosAction()
    {
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }


    public function verListaEncuestadosParaActaPosAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'listaparaactas' => $this->tablaReportes->Selecencpostparaacta($corteinac['id'],
                                                                           $this->validateReportes->getValue('programa')),
        ]);
    }


    public function verListaEstudConActaAction(){
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'listanroactas' => $this->tablaReportes->Listaestudconactas($corteinac['id']),
        ]);
    }

    public function verListaEstudConActaPosAction(){
        $corteinac = $this->tablaEncuestaAnteriores->getperiodoInactivoEncuesta((int)$this->getParam1());
        return $this->viewModelAjax([
            'cortesinactivos' => $corteinac,
            'listanroactas' => $this->tablaReportes->Listaestudconactaspos($corteinac['id']),
        ]);
    }

    public function seleccionarProgramaNroGraduadosAction()
    {
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }

    public function verNroGraduadosPorProgramasAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $programa = $this->tablaEncuestaAnteriores->getprograma($this->validateReportes->getValue('programa'));

        return $this->viewModelAjax([
            'listagraduados' => $this->tablaReportes->Selecnrograduadosporprog($this->validateReportes->getValue('programa')),
            'programas' => $programa
        ]);
    }
}
