<?php

namespace Reportes;

use Zend\ModuleManager\Feature\ConfigProviderInterface;

use Comun\Sesiones;

class Module implements ConfigProviderInterface
{


	public function onBootstrap($e)
    {
        $app = $e->getParam('application');
        $app->getEventManager()->attach('dispatch', [$this, 'setLayout']);

        /*
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->usuario = Sesiones::getNombre();
        */
    }


    public function setLayout($e)
    {
        $matches    = $e->getRouteMatch();
        $controller = $matches->getParam('controller');

        if (false === strpos($controller, __NAMESPACE__)) {
            // not a controller from this module
            return;
        }        

        $viewModel = $e->getViewModel();
        $request = $e->getRequest();
        
        if (!$request->isXmlHttpRequest()) {
            $viewModel->setTemplate('layout/layout-reporte');
        }
        
    }


    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}