<?php

namespace Reportes\Validate;


use Comun\InputFilterGeneric3;

class ReportesValidate extends InputFilterGeneric3
{

    public function validarPrograma()
    {
        $this->validarDigito([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarSexo()
    {
        $this->validarAlfaNumericoFiltro([
            'name'  => 'sexo',
            'label' => 'Valor no valido para seleccion del campo sexo',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fecha1',
            'label' => 'fecha inicial contiene un valor no valido',
            'required' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fecha2',
            'label' => 'fecha final contiene un valor no valido',
            'required' => false,
        ]);

        /*$this->validarArrayEntero([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);*/

        $this->validarDigito([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarDatos()
    {
        $this->validarFecha([
            'name'  => 'fecha1',
            'label' => 'fecha inicial contiene un valor no valido',
            'required' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fecha2',
            'label' => 'fecha final contiene un valor no valido',
            'required' => false,
        ]);

        /*$this->validarArrayEntero([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);*/

        $this->validarDigito([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'carnet',
            'label' => 'Valor no valido para seleccion del campo Estado Carnet',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarNroacta()
    {
        $this->validarFecha([
            'name'  => 'fecha1',
            'label' => 'fecha inicial contiene un valor no valido',
            'required' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fecha2',
            'label' => 'fecha final contiene un valor no valido',
            'required' => false,
        ]);

        /*$this->validarArrayEntero([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);*/

        $this->validarDigito([
            'name'  => 'programa',
            'label' => 'Valor no valido para seleccion del campo Programa',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'nroactagral',
            'label' => ' Nro acta gral ',
            'required' => true,
        ]);

        return $this->validar();
    }

}