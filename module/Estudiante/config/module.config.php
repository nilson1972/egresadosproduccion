<?php

namespace Estudiante;


use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'controllers' => [
        'factories' => [
            Controller\EstudianteController::class => InvokableFactory::class,
            Controller\EncuestaController::class => InvokableFactory::class,
            Controller\ActualizarController::class => InvokableFactory::class,
            Controller\InscripcionController::class => InvokableFactory::class,
            Controller\RuletaController::class => InvokableFactory::class,
            Controller\EgresadoController::class => InvokableFactory::class,
        ],
    ],


    'router' => [
        'routes' => [

            'estudiante' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/estudiante[/:action[/:id1]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\EstudianteController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'encuesta' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/encuesta[/:action[/:id1[/:id2[/:id3]]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                        'id2'     => '[a-zA-Z0-9_\-]+',
                        'id3'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\EncuestaController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'actualizar' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/actualizar[/:action[/:id1[/:id2[/:id3[/:id4]]]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                        'id2'     => '[a-zA-Z0-9_\-]+',
                        'id3'     => '[a-zA-Z0-9_\-]+',
                        'id4'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ActualizarController::class,
                        'action'     => 'index-actualiza',
                    ],
                ],
            ],

            'inscripcion' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/inscripcion[/:action[/:id1[/:id2]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                        'id2'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\InscripcionController::class,
                        'action'     => 'index-valida-inscrip-evento',
                    ],
                ],
            ],

            'ruleta' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/ruleta[/:action[/:id1[/:id2]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                        'id2'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\RuletaController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'egresado' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/egresado[/:action[/:id1[/:id2]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                        'id2'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\EgresadoController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],



    'view_manager' => [
        'template_path_stack' => [
            'estudiante' => __DIR__ . '/../view',
        ],
    ],
];