<?php


namespace Estudiante\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class ActualizarController extends GenericController
{
    public function __construct()
    {
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->validateActualiza = new \Estudiante\Validate\ActualizarValidate();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
    }

    public function indexActualizaAction()
    {

    }

    public function validaforactualizaAction()
    {
        return $this->viewModelAjax([

        ]);

    }

    public function tratadatosactualizaAction() {
        // Validación identificación Estudiante
        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateActualiza->getValue('identificacion'));
        $datosact = $this->tablaActualiza->getBuscarEstudActualiza($est['id']);
        //print_r($datosact);exit();
        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateActualiza->getValue('identificacion').
                "</strong> no existe. Comuniquese con la Oficina de Atención al Egresado, 
                               a traves del correo: egresados@correo.unicordoba.edu.co");
        }else{
            return $this->viewModelAjax([
                'identificacion' => $this->validateActualiza->getValue('identificacion')
            ]);
        }
    }

    public function actualizarAction(){
        // Validación identificación Estudiante
        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateActualiza->getValue('identificacion'));
        $datosact = $this->tablaActualiza->getBuscarEstudActualiza($est['id']);
        //print_r($datosact);exit();
        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateActualiza->getValue('identificacion').
                "</strong> no existe. Comuniquese con la Oficina de Atención al Egresado, 
                               a traves del correo: egresados@correo.unicordoba.edu.co");
        }else{
            return $this->viewModelAjax([
                'oficina' => $this->getParam3(),
                'estudiante' => $est,
                'datosvig' => $datosact,
                'act_economica' => $this->tablaActualiza->Listaacteconom(),
                'ingresosm' => $this->tablaActualiza->Listaingresos(),
                'estcivil' => $this->tablaActualiza->Listaestadoscivil(),
                'estudios' => $this->tablaActualiza->ListaTitulos(),
                'titulosactualiza' => $this->tablaActualiza->ListaTitulosActEtudiante($datosact['id']),
                'paisnac' => $this->tablaActualiza->Listadepaises(),
                'pais' => $this->tablaActualiza->Listadepaises(),
                'pais2' => $this->tablaActualiza->Listadepaises(),
                'ciudadnac' => $this->tablaActualiza->listadeciudades($datosact['ciud_nacim']),
                'ciudad' => $this->tablaActualiza->listadeciudades($datosact['ciud_res']),
                'ciudad2' => $this->tablaActualiza->listadeciudades($datosact['ciud_lab']),
                'universidades' => $this->tablaUniversidad->Selectuniversidades(),
                'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
                'respuestas1'=>$this->tablaActualiza->RespuestasFortaleza($datosact['id_prog_fortaleza']),
                //'otraunivprogpre' => $this->tablaUniversidad->Selectuniversidades(),
                //'progpre' => $prog1,
                //'progpos' => $prog2,
            ]);
        }
    }

    public function fortalezaAction()
    {
        return $this->viewModelAjax([
            'respuestas2'=>$this->tablaActualiza->ListadeRespuestas2((int)$this->getParam1()),
        ]);
    }

    public function ciudadesAction()
    {
        return $this->viewModelAjax([
            'ciudades' => $this->tablaActualiza->Seleccionarciudad($this->getParam1()),
        ]);
    }

    public function ciudadesReporAction()
    {
        return $this->viewModelAjax([
            'ciudades' => $this->tablaActualiza->Seleccionarciudad($this->getParam1()),
        ]);
    }

    public function insertarActualizacionAction()
    {
        if (!$this->validateActualiza->validarDatosActualizados()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes(),'1');
        }
        $area_especif='';
        $pobla_benef='';
        $otro_cual='';
        $guardotodo = 0;
        $user = Sesiones::getUsuarioname();
        if ($user==""){
            $user='estudiante';
        }
        $oficina = $_POST["ofic"];
        $paisres = $this->tablaActualiza->Busquedadepaises($this->validateActualiza->getValue('paisres'));
        $paislab = $this->tablaActualiza->Busquedadepaises($this->validateActualiza->getValue('paislab'));
        $ciudres = $this->tablaActualiza->Busquedadeciudades($this->validateActualiza->getValue('ciudres'));
        $ciudlab = $this->tablaActualiza->Busquedadeciudades($this->validateActualiza->getValue('ciudlab'));

        $yaactualizo = $this->tablaActualiza->getBuscarEstudActualiza($this->validateActualiza->getValue('id_estudiante'));

        if (!count($yaactualizo) == 0) {
            if ($_POST['ctrl'] > 0) {
                $actua_datos=$this->tablaActualiza->actulizarestadovigen($this->validateActualiza->getValues(),
                    $paisres['Pais'], $paislab['Pais'], $ciudres['ciudad'], $ciudlab['ciudad'],
                    $area_especif, $pobla_benef, $otro_cual, $oficina, $user);
                //echo $actua_datos;exit();
                $iddatosact=$this->tablaActualiza->BuscaIDactualiza($this->validateActualiza->getValue('id_estudiante'));
                $idact=$iddatosact['id'];
                //print_r($idact);exit();
                if ($actua_datos==0){
                    $tam = $_POST['Cnt'];
                    for ($i = 0; $i <= $tam; $i++) {
                        if (isset($_POST['titulo' . $i])) {
                            if (!$this->validateActualiza->validarDatosEstudRealiza($i)) {
                                return $this->verMensajeError('error al validar titulos');
                            }
                            if (!$this->tablaActualiza->insertActulizaEstudios($this->validateActualiza->getValue('titulo'.$i),
                                $this->validateActualiza->getValue('nivelacad'.$i),
                                $this->validateActualiza->getValue('iduniversidad'.$i),
                                $this->validateActualiza->getValue('anogrado'.$i),$user,$idact)){
                                return $this->verMensajeError('Error, al ingresar estudios realizados');
                            }else{
                                $guardotodo=1;
                            }
                        }else{
                            return $this->verMensajeError("Error...! Verifique los datos en Información Académica");
                        }
                    }
                    if ($guardotodo==1){
                        $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                            $this->validateActualiza->getValue('id_estudiante'),
                            $this->validateActualiza->getValue('tipodoc'),
                            $this->validateActualiza->getValue('fec_nacim'),
                            $this->validateActualiza->getValue('dir_residencial'),
                            $this->validateActualiza->getValue('rcelestud'),
                            $this->validateActualiza->getValue('remailestud'),
                            $this->validateActualiza->getValue('est_civil'),$user);
                        //echo $actua_destud;exit();
                        if ($actua_destud==0){
                            return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente,
                                                          o no se pudo actualizar la información en la tabla estudiantes");
                        }
                        return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente......!");
                    }
                } else {
                    $tam = $_POST['Cnt'];
                    for ($i = 0; $i <= $tam; $i++) {
                        if (isset($_POST['titulo' . $i])) {
                            if (!$this->validateActualiza->validarDatosEstudRealiza($i)) {
                                return $this->verMensajeError('error al validar titulos');
                            }
                            if (!$this->tablaActualiza->insertActulizaEstudios($this->validateActualiza->getValue('titulo'.$i),
                                $this->validateActualiza->getValue('nivelacad'.$i),
                                $this->validateActualiza->getValue('iduniversidad'.$i),
                                $this->validateActualiza->getValue('anogrado'.$i),$user,$idact)){
                                return $this->verMensajeError('Error, al ingresar estudios realizados');
                            }else{
                                $guardotodo=1;
                            }
                        }else{
                            return $this->verMensajeError("Error...! Verifique los datos en Información Académica");
                        }
                    }
                    if ($guardotodo==1){
                        $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                            $this->validateActualiza->getValue('id_estudiante'),
                            $this->validateActualiza->getValue('tipodoc'),
                            $this->validateActualiza->getValue('fec_nacim'),
                            $this->validateActualiza->getValue('dir_residencial'),
                            $this->validateActualiza->getValue('rcelestud'),
                            $this->validateActualiza->getValue('remailestud'),
                            $this->validateActualiza->getValue('est_civil'),$user);
                        //echo $actua_destud;exit();
                        if ($actua_destud==0){
                            return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente,
                                                          o no se pudo actualizar la información en la tabla estudiantes");
                        }
                        return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente......!");
                    }
                }

            } else {
                $actua_datos=$this->tablaActualiza->actulizarestadovigen($this->validateActualiza->getValues(),
                    $paisres['Pais'], $paislab['Pais'], $ciudres['ciudad'], $ciudlab['ciudad'],
                    $area_especif, $pobla_benef, $otro_cual, $oficina, $user);
                //echo $actua_datos;exit();
                if ($actua_datos==0){
                    $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                        $this->validateActualiza->getValue('id_estudiante'),
                        $this->validateActualiza->getValue('tipodoc'),
                        $this->validateActualiza->getValue('fec_nacim'),
                        $this->validateActualiza->getValue('dir_residencial'),
                        $this->validateActualiza->getValue('rcelestud'),
                        $this->validateActualiza->getValue('remailestud'),
                        $this->validateActualiza->getValue('est_civil'),$user);
                    //echo $actua_destud;exit();
                    if ($actua_destud==0){
                        return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente,
                                                          o no se pudo actualizar la información en la tabla estudiantes");
                    }
                    return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente......!");
                } else {
                    $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                        $this->validateActualiza->getValue('id_estudiante'),
                        $this->validateActualiza->getValue('tipodoc'),
                        $this->validateActualiza->getValue('fec_nacim'),
                        $this->validateActualiza->getValue('dir_residencial'),
                        $this->validateActualiza->getValue('rcelestud'),
                        $this->validateActualiza->getValue('remailestud'),
                        $this->validateActualiza->getValue('est_civil'),$user);
                    //echo $actua_destud;exit();
                    if ($actua_destud==0){
                        return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente,
                                                          o no se pudo actualizar la información en la tabla estudiantes");
                    }
                    return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente......!");
                }
            }

        } else {
            if ($_POST['ctrl'] > 0) {
                $iddatosact=$this->tablaActualiza->insertactuliza($this->validateActualiza->getValues(),
                    $paisres['Pais'], $paislab['Pais'], $ciudres['ciudad'], $ciudlab['ciudad'],
                    $area_especif, $pobla_benef, $otro_cual, $oficina, $user);

                if ($iddatosact==0){
                    return $this->verMensajeError($this->validateActualiza->getMensajes());
                } else {
                    $tam = $_POST['Cnt'];
                    for ($i = 0; $i <= $tam; $i++) {
                        if (isset($_POST['titulo' . $i])) {
                            if (!$this->validateActualiza->validarDatosEstudRealiza($i)) {
                                return $this->verMensajeError($this->validateActualiza->getMensajes());
                            }
                            if (!$this->tablaActualiza->insertActulizaEstudios($this->validateActualiza->getValue('titulo'.$i),
                                $this->validateActualiza->getValue('nivelacad'.$i),
                                $this->validateActualiza->getValue('iduniversidad'.$i),
                                $this->validateActualiza->getValue('anogrado'.$i),$user,$iddatosact)){
                                return $this->verMensajeError($this->validateActualiza->getMensajes());
                            }else{
                                $guardotodo=1;
                            }
                        }else{
                            return $this->verMensajeError("Error...! al parecer NO HAY datos en la Sección Información Académica");
                        }
                    }
                    if ($guardotodo==1){
                        $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                            $this->validateActualiza->getValue('id_estudiante'),
                            $this->validateActualiza->getValue('tipodoc'),
                            $this->validateActualiza->getValue('fec_nacim'),
                            $this->validateActualiza->getValue('dir_residencial'),
                            $this->validateActualiza->getValue('rcelestud'),
                            $this->validateActualiza->getValue('remailestud'),
                            $this->validateActualiza->getValue('est_civil'),$user);
                        //echo $actua_destud;exit();
                        if ($actua_destud==0){
                            return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente.,
                                                          o no se pudo actualizar la información en la tabla estudiantes");
                        }
                        return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente......!");
                    }
                }
            }else{
                return $this->verMensajeError("Error...! Verifique los datos en Información Académica");
            }
        }
    }

    public function consultaactualizxnomAction(){

        // Validación del nombre del Estudiante
        if ( ! $this->validateActualiza->validarNombres() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $estxnom = $this->tablaActualiza->buscarestudactxnom($this->validateActualiza->getValue('nombres'));
        if (count($estxnom) == 0) {
            return $this->verMensajeError("No existen registros con El Parametro de busqueda <strong>"
                .$this->validateActualiza->getValue('nombres').
                "</strong> Consulte con la Oficina de Atención al Egresado");
        }else{
            return $this->viewModelAjax([
                'estudactulizados' => $estxnom
            ]);
        }
    }

    public function seleccionarFechasAction(){

        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas(1),
        ]);

    }

    public function listaestudactualizadosAction(){
        $fec1=$_POST['fecha1'];
        $fec2=$_POST['fecha2'];
        $prog=$_POST['programa'];
        $fmactua=$_POST['formactualiza'];
            return $this->viewModelAjax([
                'estudactulizados' => $this->tablaActualiza->listaestudactuliza($fec1,$fec2,$prog,$fmactua),
                'fec1'=> $fec1,
                'fec2'=> $fec2,
                'prog'=> $prog,
                'fact'=> $fmactua,
                'nroegreactual' => $this->tablaActualiza->Nroegresadosactualiza($fec1,$fec2,$prog,$fmactua),
            ]);
    }

    public function listaestudactualizanteriorAction(){

        return $this->viewModelAjax([
            'estudactulizanter' => $this->tablaActualiza->listaestudactulizanter(),
        ]);

    }

    public function verdatosactualizadosAction(){
        $est = $this->tablaEstudiante->getEstudianteId((int)$this->getParam1());
        $datosact = $this->tablaActualiza->getSelecEstudActualiza((int)$this->getParam2());
        $progpre = $this->tablaProgramas->getProgramaEscojido($datosact['id_progpre']);
        $progpos = $this->tablaProgramas->getProgramaEscojido($datosact['id_progpos']);
        $paisres = $this->tablaActualiza->Busquedadepaises($datosact['pais_res']);
        $ciudres = $this->tablaActualiza->Busquedadeciudades($datosact['ciud_res']);
        $paislab = $this->tablaActualiza->Busquedadepaises($datosact['pais_lab']);
        $ciudlab = $this->tablaActualiza->Busquedadeciudades($datosact['ciud_lab']);
        return $this->viewModelAjax([
            'estudiante' => $est,
            'datosactualizados' => $datosact,
            'pais' => $this->tablaActualiza->Listadepaises(),
            'pais2' => $this->tablaActualiza->Listadepaises(),
            'ciudad' => $this->tablaActualiza->listadeciudades($datosact['ciud_res']),
            'ciudad2' => $this->tablaActualiza->listadeciudades($datosact['ciud_lab']),
            'act_economica' => $this->tablaActualiza->Listaacteconom(),
            'ingresosm' => $this->tablaActualiza->Listaingresos(),
            'estcivil' => $this->tablaActualiza->Listaestadoscivil(),
            'estudios' => $this->tablaActualiza->ListaTitulos(),
            'titulosactualiza' => $this->tablaActualiza->ListaTitulosActEtudiante($datosact['id']),
            'paisnac' => $this->tablaActualiza->Listadepaises(),
            'ciudadnac' => $this->tablaActualiza->listadeciudades($datosact['ciud_nacim']),
            'universidades' => $this->tablaUniversidad->Selectuniversidades(),
            'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
            'respuestas1'=>$this->tablaActualiza->RespuestasFortaleza($datosact['id_prog_fortaleza']),
            /*'progpre' => $progpre,
            'progpos' => $progpos,
            'otraunivprogpos' => $this->tablaUniversidad->Selectuniversidades(),
            'otraunivprogpre' => $this->tablaUniversidad->Selectuniversidades(),*/
        ]);
    }

    /*Ver datos de actualizacion del egresado desde estudiante*/
    public function verdatosactualizadoestudAction(){
        $est = $this->tablaEstudiante->getEstudianteId((int)$this->getParam1());
        $datosact = $this->tablaActualiza->SelecEstudActualiza((int)$this->getParam1());
        $progpre = $this->tablaProgramas->getProgramaEscojido($datosact['id_progpre']);
        $progpos = $this->tablaProgramas->getProgramaEscojido($datosact['id_progpos']);
        $paisres = $this->tablaActualiza->Busquedadepaises($datosact['pais_res']);
        $ciudres = $this->tablaActualiza->Busquedadeciudades($datosact['ciud_res']);
        $paislab = $this->tablaActualiza->Busquedadepaises($datosact['pais_lab']);
        $ciudlab = $this->tablaActualiza->Busquedadeciudades($datosact['ciud_lab']);
        return $this->viewModelAjax([
            'estudiante' => $est,
            'datosactualizados' => $datosact,
            'pais' => $this->tablaActualiza->Listadepaises(),
            'pais2' => $this->tablaActualiza->Listadepaises(),
            'ciudad' => $this->tablaActualiza->listadeciudades($datosact['ciud_res']),
            'ciudad2' => $this->tablaActualiza->listadeciudades($datosact['ciud_lab']),
            'act_economica' => $this->tablaActualiza->Listaacteconom(),
            'ingresosm' => $this->tablaActualiza->Listaingresos(),
            'estcivil' => $this->tablaActualiza->Listaestadoscivil(),
            'estudios' => $this->tablaActualiza->ListaTitulos(),
            'titulosactualiza' => $this->tablaActualiza->ListaTitulosActEtudiante($datosact['id']),
            'paisnac' => $this->tablaActualiza->Listadepaises(),
            'ciudadnac' => $this->tablaActualiza->listadeciudades($datosact['ciud_nacim']),
            'universidades' => $this->tablaUniversidad->Selectuniversidades(),
            'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
            'respuestas1'=>$this->tablaActualiza->RespuestasFortaleza($datosact['id_prog_fortaleza']),
            /*'progpre' => $progpre,
            'progpos' => $progpos,
            'otraunivprogpos' => $this->tablaUniversidad->Selectuniversidades(),
            'otraunivprogpre' => $this->tablaUniversidad->Selectuniversidades(),*/
        ]);
    }
    /***********************************************************/

    public function seleccionarFechasCumpleAction(){

        return $this->viewModelAjax([

        ]);

    }

    public function cumpleanosEgresadosAction(){
        $fec1=$_POST['fecha1'];
        $fec2=$_POST['fecha2'];
        return $this->viewModelAjax([
            'egresadoscumple' => $this->tablaActualiza->listaegresadoscumple($fec1,$fec2),
            'fec1'=> $fec1,
            'fec2'=> $fec2,
        ]);
    }

    // Reportes en Excell
    public function reporteDatosActualizadosExcelAction()
    {
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R','S',
            'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB'
        ];

        $fec1 = $this->getParam1();
        $fec2 = $this->getParam2();
        $prog = (int)$this->getParam3();
        $fmact = $this->getParam4();

        $actdatosvigentes = $this->tablaActualiza->listaestudactuliza($fec1,$fec2,$prog,$fmact);

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Egresados Act. Datos")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA ACTUALIZA')
            ->setCellValue('B1', 'IDENTIFICACION')
            ->setCellValue('C1', 'NOMBRES')
            ->setCellValue('D1', 'APELLIDOS')
            ->setCellValue('E1', 'SEXO')
            ->setCellValue('F1', 'FECHA NACIMIENTO')
            ->setCellValue('G1', 'PROGRAMA-PRE')
            ->setCellValue('H1', 'DIRECCION')
            ->setCellValue('I1', 'TEL. FIJO')
            ->setCellValue('J1', 'CELULAR')
            ->setCellValue('K1', 'E-MAIL')
            ->setCellValue('L1', 'TRABAJA ACTUALMENTE')
            ->setCellValue('M1', 'EMPRESA LABORA')
            ->setCellValue('N1', 'CARGO')
            ->setCellValue('O1', 'DIRECCION EMPRESA')
            ->setCellValue('P1', 'TELEFONO EMPRESA')
            ->setCellValue('Q1', 'PROGRAMAS')
            ->setCellValue('R1', 'TIPO DE ESTUDIOS A SEGUIR')
            ->setCellValue('S1', 'EXPECTATIVAS DE SEGUIR FORMANDOSE')
            ->setCellValue('T1', 'EN QUE DEBE MEJORAR EL PROGRAMA')
            ->setCellValue('U1', 'FORTALEZAS DEL PROGRAMA')
            ->setCellValue('V1', 'SECTOR DE SU PROFESION?')
            ->setCellValue('W1', 'EN QUE SECTOR DE TRABAJO ESPECIFICO SE ENCUENTRA')
            ->setCellValue('X1', 'POBLACION QUE SE BENEFICIA?')
            ->setCellValue('Y1', 'OTRA - CUAL?')
            ->setCellValue('Z1', 'EN QUE DEBE MEJORAR EL PROGRAMA')
            ->setCellValue('AA1', 'FORTALEZAS DEL PROGRAMA')
            ->setCellValue('AB1', 'SEDE DE ESTUDIO')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(200);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(200);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(200);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(200);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AA')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('AB')->setWidth(70);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:AB1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:AB1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($actdatosvigentes as $value) {

            $progpre = $this->tablaProgramas->getTituloEscojido($value['id_progpre']);
            $nomprogpre = $progpre['titulo_obtenido'];
            $progpos = $this->tablaProgramas->getTituloEscojido($value['id_titulo_obtenido']);
            $nomprogpos = $progpos['titulo_obtenido'];
            //$programa = $this->tablaProgramas->getTituloEscojido($value['id_titulo_obtenido']);
            //$nomprograma = $programa['titulo_obtenido'];

            $nomsede = $this->tablaActualiza->getSede($value['sede']);
            $sede = $nomsede['nombresede'];

            if ($value['trabajo_act']=='on'){
                $trabaja='Si';
            }else{
                $trabaja='No';
            }

            if ($value['ncapac_diploma']=='on'){
                $tipoestud='Diplomado';
            }else{
                if ($value['ncapac_espec']=='on'){
                    $tipoestud='Especialización';
                }else{
                    if ($value['ncapac_mestria']=='on') {
                        $tipoestud = 'Maestria';
                    }else{
                        if ($value['ncapac_docto']=='on') {
                            $tipoestud = 'Doctorado';
                        }
                    }
                }
            }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fec_actualizadatos'])
                ->setCellValue('B'.$fila, $value['nro_ident'])
                ->setCellValue('C'.$fila, $value['nombres'])
                ->setCellValue('D'.$fila, $value['apellidos'])
                ->setCellValue('E'.$fila, $value['sexo'])
                ->setCellValue('F'.$fila, $value['fec_nacimiento'])
                ->setCellValue('G'.$fila, $nomprogpre)
                ->setCellValue('H'.$fila, $value['direc_residen'])
                ->setCellValue('I'.$fila, $value['tel_fijo'])
                ->setCellValue('J'.$fila, $value['celular'])
                ->setCellValue('K'.$fila, $value['email'])
                ->setCellValue('L'.$fila, $trabaja)
                ->setCellValue('M'.$fila, $value['nom_empresa'])
                ->setCellValue('N'.$fila, $value['cargo_empresa'])
                ->setCellValue('O'.$fila, $value['dir_empresa'])
                ->setCellValue('P'.$fila, $value['tel_empresa'])
                ->setCellValue('Q'.$fila, $nomprogpos)
                ->setCellValue('R'.$fila, $tipoestud)
                ->setCellValue('S'.$fila, $value['ncapac_tema'])
                ->setCellValue('T'.$fila, $value['prog_debe_mejorar'])
                ->setCellValue('U'.$fila, $value['prog_fortaleza'])
                ->setCellValue('V'.$fila, $value['trabsectprofe'])
                ->setCellValue('W'.$fila, $value['areaespecif'])
                ->setCellValue('X'.$fila, $value['poblabenef'])
                ->setCellValue('Y'.$fila, $value['otrocualdpte'])
                ->setCellValue('Z'.$fila, $value['resp_mejorar'])
                ->setCellValue('AA'.$fila, $value['resp_fortaleza'])
                ->setCellValue('AB'.$fila, $sede)
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        };


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Egresados Actualizados');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Egresados Actualizados_'.'.xlsx';

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }
    /******************/

    public function respuestasActualizaDatosAction()
    {
        return $this->viewModelAjax([

        ]);

    }

    public function guardarPreguntasAction()
    {
        if (!$this->validateActualiza->ValidarDatosRespuestaActualiza()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $respuesta = $this->tablaActualiza->getExisteRespuesta($this->validateActualiza->getValue('respuesta'));
        //print_r($user);exit();
        if (is_null($respuesta)) {
            $username = Sesiones::getUsuarioname();
            if (!$this->tablaActualiza->insertarRespuesta($this->validateActualiza->getValues(),$username)) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            } else {
                return $this->verMensajeInfo("La Respuesta fue registrada satisactoriamente...!");
            }
        } else {
            return $this->verMensajeError("La Respuesta <strong>"
                . $this->validateActualiza->getValue('respuesta') .
                "</strong> Ya fue registrada....!");

        }
    }

    public function editRespuestaAction()
    {
        return $this->viewModelAjax([
           'editrespuestas'=>$this->tablaActualiza->SelectRespuesta((int)$this->getParam1()),
        ]);
    }

    public function actualizarPreguntasAction()
    {
        if (!$this->validateActualiza->ValidarDatosRespuestaActualiza()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $idresp =$_POST['id_resp'];

            $username = Sesiones::getUsuarioname();
            if (!$this->tablaActualiza->actualizaRespuesta($this->validateActualiza->getValue('respuesta'),$idresp,$username)) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            } else {
                return $this->verMensajeInfo("La Respuesta fue actualizada satisactoriamente...!");
            }

    }

    public function creaProgramasAction()
    {
        $usertipo = Sesiones::getIdTipo();
        return $this->viewModelAjax([
            'tipouser' =>$usertipo,
        ]);

    }

    public function guardaProgramasAction()
    {
        if (!$this->validateActualiza->ValidarDatosPrograma()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $progra = $this->tablaActualiza->getExistePrograma($this->validateActualiza->getValue('nombre'),
            $this->validateActualiza->getValue('tipoprog'));
        //print_r($user);exit();
        if (is_null($progra)) {
            $username = Sesiones::getUsuarioname();
            if (!$this->tablaActualiza->insertarProgramas($this->validateActualiza->getValues(),$username)) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            } else {
                return $this->verMensajeInfo("El Programa fue registrado satisactoriamente...!");
            }
        } else {
            return $this->verMensajeError("El Programa <strong>"
                . $this->validateActualiza->getValue('nombre') .
                "</strong> Ya Existe en la Tabla Programas....!");

        }
    }

    public function editProgramasAction()
    {
        return $this->viewModelAjax([
            'editprogramas'=>$this->tablaActualiza->SelectPrograma((int)$this->getParam1()),
        ]);
    }

    public function actualizaProgramasAction()
    {
        if (!$this->validateActualiza->ValidarDatosPrograma()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $idprogra =$_POST['idprog'];

        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->actualizaPrograma($this->validateActualiza->getValues(),$idprogra,$username)) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        } else {
            return $this->verMensajeInfo("El Programa fue actualizado satisactoriamente...!");
        }
    }

    public function editUniversidadAction()
    {
        return $this->viewModelAjax([
            'edituniversidad'=>$this->tablaActualiza->SelectUniversidad((int)$this->getParam1()),
        ]);
    }

    public function actualizaUniversidadAction()
    {
        if (!$this->validateActualiza->ValidarDatosUniversidad()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $iduniver =$_POST['id_univ'];

        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->actualizaUniversidad($this->validateActualiza->getValues(),$iduniver,$username)) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        } else {
            return $this->verMensajeInfo("Los Datos de la Universidad fueron actualizados satisactoriamente...!");
        }
    }
}