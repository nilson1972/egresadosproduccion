<?php

namespace Estudiante\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class InscripcionController extends GenericController
{
    public function __construct()
    {
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->validateActualiza = new \Estudiante\Validate\ActualizarValidate();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();

    }

    public function indexValidaInscripEventoAction()
    {
        $nomevento = $this->tablaEstudiante->EventoVigente();
        return $this->viewModel([
            'eventovigente' => $nomevento,
        ]);
    }

    public function tratadatosinscripcionAction() {
        // Validación identificación Estudiante
        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateActualiza->getValue('identificacion'));

        if (count($est) == 0) {
            return $this->verMensajeError("El Número de identificación <strong>"
                . $this->validateActualiza->getValue('identificacion') .
                "</strong> NO existe. Comuniquese con la Oficina de Atención al Egresado, 
                               a traves del correo: egresados@correo.unicordoba.edu.co");
        } else {
            $yainscevento = $this->tablaActualiza->getBuscarEstudInscevento($est['id'], $_POST['idevento']);
            if ($yainscevento['total'] == 0) {
                return $this->viewModelAjax([
                    'identificacion' => $this->validateActualiza->getValue('identificacion'),
                    'idevento'       => $_POST['idevento']
                ]);
            } else {
                return $this->verMensajeError("Estudiante con Nro de indetificación <strong>"
                    . $this->validateActualiza->getValue('identificacion') .
                    "</strong> Ya esta inscrito al evento de Egresados");
            }
        }
    }

    public function actualizarinscripAction(){

        /* Validación identificación Estudiante
        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateActualiza->getValue('identificacion'));
        $datosact = $this->tablaActualiza->getBuscarEstudActualiza($est['id']);
        $prog1 = $this->tablaProgramas->Selectprogramas((int)$this->getParam1());
        $prog2 = $this->tablaProgramas->Selectprogramas((int)$this->getParam2());
        //$evento = $this->tablaActualiza->getBuscaEventoVigente();
        //if ($evento['id'] <> 0) {

            if (count($est) == 0) {
                return $this->verMensajeError("El Número de identificación <strong>"
                    . $this->validateActualiza->getValue('identificacion') .
                    "</strong> NO existe. Comuniquese con la Oficina de Atención al Egresado, 
                               a traves del correo: egresados@correo.unicordoba.edu.co");
            } else {
                $yainscevento = $this->tablaActualiza->getBuscarEstudInscevento($est['id'], $_POST['idevento']);
                if ($yainscevento['total'] == 0) {*/
                    $est = $this->tablaEstudiante->getEstudianteIdentificacion($_POST['identificacion']);
                    $datosact = $this->tablaActualiza->getBuscarEstudActualiza($est['id']);
                    return $this->viewModelAjax([
                        'estudiante' => $est,
                        'datosvig' => $datosact,
                        'act_economica' => $this->tablaActualiza->Listaacteconom(),
                        'ingresosm' => $this->tablaActualiza->Listaingresos(),
                        'estcivil' => $this->tablaActualiza->Listaestadoscivil(),
                        'estudios' => $this->tablaActualiza->ListaTitulos(),
                        'titulosactualiza' => $this->tablaActualiza->ListaTitulosActEtudiante($datosact['id']),
                        'paisnac' => $this->tablaActualiza->Listadepaises(),
                        'pais' => $this->tablaActualiza->Listadepaises(),
                        'pais2' => $this->tablaActualiza->Listadepaises(),
                        'ciudadnac' => $this->tablaActualiza->listadeciudades($datosact['ciud_nacim']),
                        'ciudad' => $this->tablaActualiza->listadeciudades($datosact['ciud_res']),
                        'ciudad2' => $this->tablaActualiza->listadeciudades($datosact['ciud_lab']),
                        'universidades' => $this->tablaUniversidad->Selectuniversidades(),
                        'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
                        'respuestas1'=>$this->tablaActualiza->RespuestasFortaleza($datosact['id_prog_fortaleza']),
                        'idevento'=>$_POST['idevento'],
                        //'progpre' => $prog1,
                        //'progpos' => $prog2,
                        //'otraunivprogpos' => $this->tablaUniversidad->Selectuniversidades(),
                        //'otraunivprogpre' => $this->tablaUniversidad->Selectuniversidades(),
                    ]);
                /*} else {
                    return $this->verMensajeError("Estudiante con Nro de indetificación <strong>"
                        . $this->validateActualiza->getValue('identificacion') .
                        "</strong> Ya esta inscrito al evento de Egresados");
                }
            }
        //} else {
        //    return $this->verMensajeError("Apreciado Egresado las inscripciones al evento de Egresados ya fueron cerradas ");
        //}*/

    }

    public function fortalezaAction()
    {
        return $this->viewModelAjax([
            'respuestas2'=>$this->tablaActualiza->ListadeRespuestas2((int)$this->getParam1()),
        ]);
    }

    public function insertarActualizainscripAction()
    {
        // Validación Actualización de datos
        if ( ! $this->validateActualiza->validarDatosActualizados() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }
        //$evento = $this->tablaActualiza->getBuscaEventoVigente();
        if (! $this->tablaActualiza->insertarinscrip($this->validateActualiza->getValue('id_estudiante'), $_POST['id_evento'])){
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }else{
            $area_especif=$_POST['areaespecif'];
            $pobla_benef=$_POST['poblabenef'];
            $otro_cual=$_POST['otrocualdpte'];
            $oficina='INS';
            $guardotodo = 0;
            $user = Sesiones::getUsuarioname();
            $paisres= $this->tablaActualiza->Busquedadepaises($this->validateActualiza->getValue('paisres'));
            $paislab= $this->tablaActualiza->Busquedadepaises($this->validateActualiza->getValue('paislab'));
            $ciudres= $this->tablaActualiza->Busquedadeciudades($this->validateActualiza->getValue('ciudres'));
            $ciudlab= $this->tablaActualiza->Busquedadeciudades($this->validateActualiza->getValue('ciudlab'));

            $yaactualizo = $this->tablaActualiza->getBuscarEstudActualiza($this->validateActualiza->getValue('id_estudiante'));

            if (! $yaactualizo==0){
                if ($_POST['ctrl'] > 0) {
                    $actua_datos=$this->tablaActualiza->actulizarestadovigen($this->validateActualiza->getValues(),
                        $paisres['Pais'], $paislab['Pais'], $ciudres['ciudad'], $ciudlab['ciudad'],
                        $area_especif, $pobla_benef, $otro_cual, $oficina, $user);
                    //echo $actua_datos;exit();
                    $iddatosact=$this->tablaActualiza->BuscaIDactualiza($this->validateActualiza->getValue('id_estudiante'));
                    $idact=$iddatosact['id'];
                    //print_r($idact);exit();
                    if ($actua_datos==0){
                        $tam = $_POST['Cnt'];
                        for ($i = 0; $i <= $tam; $i++) {
                            if (isset($_POST['titulo' . $i])) {
                                if (!$this->validateActualiza->validarDatosEstudRealiza($i)) {
                                    return $this->verMensajeError('error al validar titulos');
                                }
                                if (!$this->tablaActualiza->insertActulizaEstudios($this->validateActualiza->getValue('titulo'.$i),
                                    $this->validateActualiza->getValue('nivelacad'.$i),
                                    $this->validateActualiza->getValue('iduniversidad'.$i),
                                    $this->validateActualiza->getValue('anogrado'.$i),$user,$idact)){
                                    return $this->verMensajeError('Error, al ingresar estudios realizados');
                                }else{
                                    $guardotodo=1;
                                }
                            }else{
                                return $this->verMensajeError("Error...! Verifique los datos en Información Académica");
                            }
                        }
                        if ($guardotodo==1){
                            $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                                $this->validateActualiza->getValue('id_estudiante'),
                                $this->validateActualiza->getValue('tipodoc'),
                                $this->validateActualiza->getValue('fec_nacim'),
                                $this->validateActualiza->getValue('dir_residencial'),
                                $this->validateActualiza->getValue('rcelestud'),
                                $this->validateActualiza->getValue('remailestud'),
                                $this->validateActualiza->getValue('est_civil'),$user);
                            //echo $actua_destud;exit();
                            if ($actua_destud==1){
                                return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente....0!");
                            }
                            return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente...1!");
                        }
                    } else {
                        $tam = $_POST['Cnt'];
                        for ($i = 0; $i <= $tam; $i++) {
                            if (isset($_POST['titulo' . $i])) {
                                if (!$this->validateActualiza->validarDatosEstudRealiza($i)) {
                                    return $this->verMensajeError('error al validar titulos');
                                }
                                if (!$this->tablaActualiza->insertActulizaEstudios($this->validateActualiza->getValue('titulo'.$i),
                                    $this->validateActualiza->getValue('nivelacad'.$i),
                                    $this->validateActualiza->getValue('iduniversidad'.$i),
                                    $this->validateActualiza->getValue('anogrado'.$i),$user,$idact)){
                                    return $this->verMensajeError('Error, al ingresar estudios realizados');
                                }else{
                                    $guardotodo=1;
                                }
                            }else{
                                return $this->verMensajeError("Error...! Verifique los datos en Información Académica");
                            }
                        }
                        if ($guardotodo==1){
                            $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                                $this->validateActualiza->getValue('id_estudiante'),
                                $this->validateActualiza->getValue('tipodoc'),
                                $this->validateActualiza->getValue('fec_nacim'),
                                $this->validateActualiza->getValue('dir_residencial'),
                                $this->validateActualiza->getValue('rcelestud'),
                                $this->validateActualiza->getValue('remailestud'),
                                $this->validateActualiza->getValue('est_civil'),$user);
                            //echo $actua_destud;exit();
                            if ($actua_destud==1){
                                return $this->verMensajeInfo("Se Actualizaron Los Datos satisfactoriamente....0!");
                            }
                            return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente.....1!");
                        }
                    }

                } else {
                    $actua_datos=$this->tablaActualiza->actulizarestadovigen($this->validateActualiza->getValues(),
                        $paisres['Pais'], $paislab['Pais'], $ciudres['ciudad'], $ciudlab['ciudad'],
                        $area_especif, $pobla_benef, $otro_cual, $oficina, $user);
                    //echo $actua_datos;exit();
                    if ($actua_datos==0){
                        return $this->verMensajeError('No hubieron cambios en los datos registrados,
                                                          o no se pudo actualizar la información');
                    } else {
                        $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                            $this->validateActualiza->getValue('id_estudiante'),
                            $this->validateActualiza->getValue('tipodoc'),
                            $this->validateActualiza->getValue('fec_nacim'),
                            $this->validateActualiza->getValue('dir_residencial'),
                            $this->validateActualiza->getValue('rcelestud'),
                            $this->validateActualiza->getValue('remailestud'),
                            $this->validateActualiza->getValue('est_civil'),$user);
                        //echo $actua_destud;exit();
                        if ($actua_destud==0){
                            return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente....0!");
                        }
                        return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente....1!");
                    }
                }
            }else{
                if ($_POST['ctrl'] > 0) {
                    $iddatosact=$this->tablaActualiza->insertactuliza($this->validateActualiza->getValues(),
                        $paisres['Pais'], $paislab['Pais'], $ciudres['ciudad'], $ciudlab['ciudad'],
                        $area_especif, $pobla_benef, $otro_cual, $oficina, $user);

                    if ($iddatosact==0){
                        return $this->verMensajeError($this->validateActualiza->getMensajes());
                    } else {
                        $tam = $_POST['Cnt'];
                        for ($i = 0; $i <= $tam; $i++) {
                            if (isset($_POST['titulo' . $i])) {
                                if (!$this->validateActualiza->validarDatosEstudRealiza($i)) {
                                    return $this->verMensajeError($this->validateActualiza->getMensajes());
                                }
                                if (!$this->tablaActualiza->insertActulizaEstudios($this->validateActualiza->getValue('titulo'.$i),
                                    $this->validateActualiza->getValue('nivelacad'.$i),
                                    $this->validateActualiza->getValue('iduniversidad'.$i),
                                    $this->validateActualiza->getValue('anogrado'.$i),$user,$iddatosact)){
                                    return $this->verMensajeError($this->validateActualiza->getMensajes());
                                }else{
                                    $guardotodo=1;
                                }
                            }else{
                                return $this->verMensajeError("Error...! al parecer NO HAY datos en la Sección Información Académica");
                            }
                        }
                        if ($guardotodo==1){

                            $actua_destud=$this->tablaEstudiante->actualizadatosestudiante(
                                $this->validateActualiza->getValue('id_estudiante'),
                                $this->validateActualiza->getValue('tipodoc'),
                                $this->validateActualiza->getValue('fec_nacim'),
                                $this->validateActualiza->getValue('dir_residencial'),
                                $this->validateActualiza->getValue('rcelestud'),
                                $this->validateActualiza->getValue('remailestud'),
                                $this->validateActualiza->getValue('est_civil'),$user);
                            //echo $actua_destud;exit();
                            if ($actua_destud==0){
                                return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente....0!");
                            }
                            return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente....1!");
                        }
                    }
                }else{
                    return $this->verMensajeError("Error...! Verifique los datos en Información Académica");
                }
            }
        }
    }

    public function seleccionarEventosVigAction(){
        return $this->viewModelAjax([
            'listaeventos' => $this->tablaActualiza->listaEventosVigentes(),
        ]);
    }

    public function listaestudisncrieventoAction(){

        if ( ! $this->validateActualiza->validarIdEvento() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscritos' => $this->tablaActualiza->Listaestudinscritos($this->validateActualiza->getValue('evento')),
            'nroestudinscritos' => $this->tablaActualiza->Nroestudinscritos($this->validateActualiza->getValue('evento')),
            'idevento' => $this->validateActualiza->getValue('evento'),
        ]);

    }

    public function listainscritosidentAction(){

        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscriident' => $this->tablaActualiza->Listaestudinscriident($this->validateActualiza->getValue('identificacion')),
        ]);

    }

    public function listainscritosnombAction(){

        if ( ! $this->validateActualiza->validarNombres() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscrinomb' => $this->tablaActualiza->Listainscritosnomb($this->validateActualiza->getValue('nombres')),
        ]);

    }

    public function seleccionarEventosAntAction(){
        return $this->viewModelAjax([
            'listaeventos' => $this->tablaActualiza->Listadeeventosnovigente(),
        ]);
    }

    public function listaestudinscrieventanteAction(){

        if ( ! $this->validateActualiza->validarIdEvento() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscrieventant' => $this->tablaActualiza->Listainscritoseventante($this->validateActualiza->getValue('evento')),
            'nroestudinscritos' => $this->tablaActualiza->Nroestudinscritos($this->validateActualiza->getValue('evento')),
            'idevento' => $this->validateActualiza->getValue('evento')
        ]);

    }

    public function tiposdeeventosAction(){

        $usertipo = Sesiones::getIdTipo();
        return $this->viewModelAjax([
            'tipouser' =>$usertipo,
        ]);

    }

    public function guardatipoeventoAction()
    {
        if (!$this->validateActualiza->ValidarDatosTipoEvento()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $tipoeven = $this->tablaActualiza->getExisteTipoEvento($this->validateActualiza->getValue('tipoevento'));
        //print_r($user);exit();
        if (is_null($tipoeven)) {
            $username = Sesiones::getUsuarioname();
            if (!$this->tablaActualiza->insertarTipoEvento($this->validateActualiza->getValues(),$username)) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            } else {
                return $this->verMensajeInfo("El Tipo de Evento fue registrado satisactoriamente...!");
            }
        } else {
            return $this->verMensajeError("El Tipo de Evento <strong>"
                . $this->validateActualiza->getValue('tipoevento') .
                "</strong> Ya Existe en la Tabla Tipo de Eventos....!");

        }
    }

    public function editatipoeventoAction()
    {
        return $this->viewModelAjax([
            'editatipoevento'=>$this->tablaActualiza->SelectTipoevento((int)$this->getParam1()),
        ]);
    }

    public function actualizatipoeventoAction()
    {
        if (!$this->validateActualiza->ValidarDatosTipoEvento()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $idtpeven =$_POST['idtipo'];

        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->actualizaTipoeventos($this->validateActualiza->getValues(),$idtpeven,$username)) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        } else {
            return $this->verMensajeInfo("El Tipo de Evento fue actualizado satisactoriamente...!");
        }
    }

    public function nuevoEventoAction(){

        $tipoeven = $this->tablaActualiza->ListaTiposEventos();
        return $this->viewModelAjax([
            'eventos' => $this->tablaActualiza->Listaeventos(),
            'tipoevento' => $tipoeven,
        ]);

    }

    public function inserteventoAction()
    {
        if ( ! $this->validateActualiza->validarDatosEventoNuevo() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }
        /*$filas=$this->tablaActualiza->actualizaestadoevento();*/
        $user=Sesiones::getUsuarioname();
        //if ($filas==0){
        //    return $this->verMensajeError("No se pudo actualizar el estado del evento");
        //}else {
            if (! $this->tablaActualiza->insertarevento($this->validateActualiza->getValues(),$user)){
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            }else {
                return $this->verMensajeInfo("Evento guardado satisfactoriamente....!");
            }
        //}
    }

    public function verdatoseventoAction()
    {
        $tipoeven = $this->tablaActualiza->ListaTiposEventos();
        return $this->viewModelAjax([
            'evento' => $this->tablaActualiza->SelecEvento((int)$this->getParam1()),
            'tipoevento' => $tipoeven,
        ]);
    }

    public function updateEventoAction()
    {
        if (!$this->validateActualiza->validarDatosEventoNuevo()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $idevento =$_POST['idevento'];
        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->actualizaEvento($this->validateActualiza->getValues(),$idevento,$username)) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        } else {
            return $this->verMensajeInfo("El Evento fue actualizado satisfactoriamente...!");
        }
    }

    public function cambiarelestadoeventoAction()
    {
        $idevento = (int)$this->getParam1();
        $esta = $this->getParam2();
        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->cambiaEstadoEvento($idevento,$esta,$username)) {
            return $this->verMensajeError('El Evento no pudo ser cerrado');
        } else {
            return $this->verMensajeInfo("El Evento fue actualizado satisfactoriamente...!");
        }
    }

    // Reportes en Excell
    public function reporteEstudInscripEventoExcelAction()
    {
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes
        $letras= [
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'
        ];

        $idevento = $this->getParam1();
        $parametro= $this->getParam2();

        if ($parametro==1) {
            $estudinscritos = $this->tablaActualiza->Listaestudinscritos($idevento);
        }else{
            $estudinscritos = $this->tablaActualiza->Listainscritoseventante($idevento);
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Inscrip. Eventos")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA INSCRIPCION')
            ->setCellValue('B1', 'IDENTIFICACION')
            ->setCellValue('C1', 'NOMBRES')
            ->setCellValue('D1', 'APELLIDOS')
            ->setCellValue('E1', 'PROGRAMA')
            //->setCellValue('F1', 'PROGRAMA-POS')
            ->setCellValue('G1', 'EVENTO')
            ->setCellValue('H1', 'CELULAR')
            ->setCellValue('I1', 'E-MAIL')
            ->setCellValue('J1', 'TRABAJA?')
            ->setCellValue('K1', 'NOMBRE EMPRESA')
            ->setCellValue('L1', 'DIRECCION EMPRESA')
            ->setCellValue('M1', 'TEL EMPRESA')
            ->setCellValue('N1', 'CARGO')
            ->setCellValue('O1', 'AREA DE SU FORMACION')
            ->setCellValue('P1', 'AREA ESPECIFICA')
            ->setCellValue('Q1', 'POBLACION BENEFICIADA')
            ->setCellValue('R1', 'OTRA-CUAL?')
            ->setCellValue('S1', 'TIPO DE ESTUDIOS A SEGUIR')
            ->setCellValue('T1', 'EXPECTATIVAS DE SEGUIR FORMANDOSE')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(150);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(200);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:T1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:T1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($estudinscritos as $value) {

            $progpre = $this->tablaProgramas->getProgramaEscojido($value['id_programa']);
            $nomprogpre = utf8_decode($progpre['nombre']);
            //$progpos = $this->tablaProgramas->getProgramaEscojido($value['id_progpos']);
            //$nomprogpos = utf8_decode($progpos['nombre']);

            if ($value['trabajo_act']=='on'){
                $trabaja='Si';
            }else{
                $trabaja='No';
            }

            if ($value['ncapac_diploma']=='on'){
                $tipoestud='Diplomado';
            }else{
                if ($value['ncapac_espec']=='on'){
                    $tipoestud='Especialización';
                }else{
                    if ($value['ncapac_mestria']=='on') {
                        $tipoestud = 'Maestria';
                    }else{
                        if ($value['ncapac_docto']=='on') {
                            $tipoestud = 'Doctorado';
                        }
                    }
                }
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fecha'])
                ->setCellValue('B'.$fila, $value['nro_ident'])
                ->setCellValue('C'.$fila, $value['nombres'])
                ->setCellValue('D'.$fila, $value['apellidos'])
                ->setCellValue('E'.$fila, $nomprogpre)
                //->setCellValue('F'.$fila, $nomprogpos)
                ->setCellValue('G'.$fila, $value['nombre'])
                ->setCellValue('H'.$fila, $value['celular'])
                ->setCellValue('I'.$fila, $value['email'])
                ->setCellValue('J'.$fila, $trabaja)
                ->setCellValue('K'.$fila, $value['nom_empresa'])
                ->setCellValue('L'.$fila, $value['dir_empresa'])
                ->setCellValue('M'.$fila, $value['tel_empresa'])
                ->setCellValue('N'.$fila, $value['cargo_empresa'])
                ->setCellValue('O'.$fila, $value['trabsectprofe'])
                ->setCellValue('P'.$fila, $value['areaespecif'])
                ->setCellValue('Q'.$fila, $value['poblabenef'])
                ->setCellValue('R'.$fila, $value['otrocualdpte'])
                ->setCellValue('S'.$fila, $tipoestud)
                ->setCellValue('T'.$fila, $value['ncapac_tema'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        };


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Inscrip. a Eventos');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Inscrip. Eventos_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }
    /******************/

}