<?php
/**
 * Created by PhpStorm.
 * User: GRED
 * Date: 11/10/2017
 * Time: 02:55 PM
 */

namespace Estudiante\Controller;

use Comun\GenericController;

class RuletaController extends GenericController
{
    public function __construct()
    {
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->validateActualiza = new \Estudiante\Validate\ActualizarValidate();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
    }

    public function indexAction()
    {
        return $this->viewModel([
            'inscripevento' => $this->tablaActualiza->Listaestudinscritos(),
            'inscripevento2' => $this->tablaActualiza->Listaestudinscritos(),
        ]);
    }

}