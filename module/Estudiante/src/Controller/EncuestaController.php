<?php
namespace Estudiante\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class EncuestaController extends GenericController
{
    public function __construct()
    {
        $this->tablaEncuesta = new \Estudiante\Table\EncuestaTable();
        $this->validateEncuesta = new \Estudiante\Validate\EncuestaValidate();
        $this->tablaEncuestaPost = new \Estudiante\Table\EncuestaPostTable();
        $this->validateEncuestaPost = new \Estudiante\Validate\EncuestaPostValidate();
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->validateActualiza = new \Estudiante\Validate\ActualizarValidate();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();

        $this->tablaVerEncuesta = new \Usuario\Table\VerEncuestaTable();
        $this->tablaVerEncuestaPost = new \Usuario\Table\VerEncuestaPostTable();
        $this->validateVerEncuesta = new \Usuario\Validate\VerEncuestaValidate();
        $this->validateBuscaEstudiante = new \Usuario\Validate\BuscaEstudianteValidate();

        $this->tablaEncuestaAnteriores = new \Reportes\Table\EncuestaAnterioresTable();
        $this->tablaReportes = new \Reportes\Table\ReportesTable();
    }

    public function insertarpreAction()
    {
        // Validación Encuesta
        $error1=1; $error2=1; $act=0;
        if ( ! $this->validateEncuesta->validarEncuesta() ) {
            return $this->verMensajeError($this->validateEncuesta->getMensajes());
        }

        $paisres= $this->tablaActualiza->Busquedadepaises($this->validateEncuesta->getValue('paisres'));
        $ciudres= $this->tablaActualiza->Busquedadeciudades($this->validateEncuesta->getValue('ciudres'));
        $paislab= $this->tablaActualiza->Busquedadepaises($this->validateEncuesta->getValue('paislab'));
        $ciudlab= $this->tablaActualiza->Busquedadeciudades($this->validateEncuesta->getValue('ciudlab'));
        if (! $this->tablaEncuesta->insertar($this->validateEncuesta->getValues(),$paisres['Pais'], $ciudres['ciudad'],
                                                                                  $paislab['Pais'], $ciudlab['ciudad'])){
            return $this->verMensajeError($this->validateEncuesta->getMensajes());
        }else{
            if ( ! $this->validateActualiza->validarIdEstudiante() ) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            }
            if (isset($_POST['trabaja_actual'])) {
                $empleado = 'on';
                $ingresos = 1;
                $empresario = "";
                $pensionado = "";
                $estudiante = "";
                $desempleado = "";
                $tiempo_vincula = "12 meses";
            } else {
                $empleado = "";
                $ingresos = 0;
                $empresario = "";
                $pensionado = "";
                $estudiante = "on";
                $desempleado = "";
                $tiempo_vincula = "";
            }
            $recono="NO";
            $ncapa_espec='on';
            $nivel="Profesional";
            $universidad= 7;
            $yaactualizo = $this->tablaActualiza->getBuscarEstudActualiza($this->validateActualiza->getValue('id_estudiante'));
            if (!count($yaactualizo) == 0) {
                $actual_datos = $this->tablaActualiza->actualizaciondatosegresados($this->validateEncuesta->getValue('id_estudiante'),
                                                                                   $this->validateEncuesta->getValue('paisnac'),
                                                                                   $this->validateEncuesta->getValue('ciudnac'),
                                                                                   $this->validateEncuesta->getValue('dir_residencial'),
                                                                                   $this->validateEncuesta->getValue('paisres'),
                                                                                   $this->validateEncuesta->getValue('ciudres'),
                                                                                   $this->validateEncuesta->getValue('rcelestud'),
                                                                                   $this->validateEncuesta->getValue('remailestud'),
                                                                                   $_POST['est_civil'],$empleado,$empresario,$pensionado,
                                                                                   $estudiante,$desempleado,
                                                                                   $this->validateEncuesta->getValue('nombre_empresa'),
                                                                                   $this->validateEncuesta->getValue('actividad_empresa'),
                                                                                   $this->validateEncuesta->getValue('sectoremp'),
                                                                                   $this->validateEncuesta->getValue('direc_emp_trab'),
                                                                                   $this->validateEncuesta->getValue('paislab'),
                                                                                   $this->validateEncuesta->getValue('ciudlab'),
                                                                                   $this->validateEncuesta->getValue('ocupacion_trabajo'),
                                                                                   $this->validateEncuesta->getValue('sectprof'),$ingresos,
                                                                                   $tiempo_vincula,$recono,
                                                                                   $this->validateEncuesta->getValue('mejorar'),
                                                                                   $this->validateEncuesta->getValue('fortaleza'),
                                                                                   $ncapa_espec,
                                                                                   $this->validateEncuesta->getValue('especializar'),
                                                                                   $paisres['Pais'],$ciudres['ciudad'],$paislab['Pais'],
                                                                                   $ciudlab['ciudad']);

                $user="estudiante";
                $iddatosact=$this->tablaActualiza->BuscaIDactualiza($this->validateActualiza->getValue('id_estudiante'));
                $idact=$iddatosact['id'];
                if (! $actual_datos == 0) {
                    if (!$this->tablaActualiza->insertActulizaEstudios($this->validateEncuesta->getValue('programa'),
                                                                       $nivel, $universidad,
                                                                       $this->validateEncuesta->getValue('anyo2'),
                                                                       $user,$idact)){
                        return $this->verMensajeError($this->validateEncuesta->getMensajes());
                    }
                }else{
                    if (!$this->tablaActualiza->insertActulizaEstudios($this->validateEncuesta->getValue('programa'),
                                                                       $nivel, $universidad,
                                                                       $this->validateEncuesta->getValue('anyo2'),
                                                                       $user,$idact)){
                        return $this->verMensajeError($this->validateEncuesta->getMensajes());
                    }
                }
                $act=1;
            }else {
                $inser_act = $this->tablaActualiza->insertactulizadesdeencuesta($this->validateEncuesta->getValue('id_estudiante'),
                    $this->validateEncuesta->getValue('paisnac'),
                    $this->validateEncuesta->getValue('ciudnac'),
                    $this->validateEncuesta->getValue('dir_residencial'),
                    $this->validateEncuesta->getValue('paisres'),
                    $this->validateEncuesta->getValue('ciudres'),
                    $this->validateEncuesta->getValue('rcelestud'),
                    $this->validateEncuesta->getValue('remailestud'),
                    $_POST['est_civil'], $empleado, $empresario, $pensionado,
                    $estudiante, $desempleado,
                    $this->validateEncuesta->getValue('nombre_empresa'),
                    $this->validateEncuesta->getValue('actividad_empresa'),
                    $this->validateEncuesta->getValue('sectoremp'),
                    $this->validateEncuesta->getValue('direc_emp_trab'),
                    $this->validateEncuesta->getValue('paislab'),
                    $this->validateEncuesta->getValue('ciudlab'),
                    $this->validateEncuesta->getValue('ocupacion_trabajo'),
                    $this->validateEncuesta->getValue('sectprof'), $ingresos,
                    $tiempo_vincula, $recono,
                    $this->validateEncuesta->getValue('mejorar'),
                    $this->validateEncuesta->getValue('fortaleza'),
                    $ncapa_espec,
                    $this->validateEncuesta->getValue('especializar'),
                    $this->validateEncuesta->getValue('sedeuniv'),
                    $paisres['Pais'], $ciudres['ciudad'], $paislab['Pais'], $ciudlab['ciudad']);
                if (!$inser_act == 0) {
                    $user="estudiante";
                    if (!$this->tablaActualiza->insertActulizaEstudios($this->validateEncuesta->getValue('programa'),
                                                                       $nivel, $universidad,
                                                                       $this->validateEncuesta->getValue('anyo2'),
                                                                       $user,$inser_act)){
                        return $this->verMensajeError($this->validateEncuesta->getMensajes());
                    }
                    $act=1;
                } else {
                    return $this->verMensajeError($this->validateEncuesta->getMensajes());
                }
            }

            $fila2 = $this->tablaEstudiante->actualizaidentestudiante($this->validateEncuesta->getValue('id_estudiante'),
                                                                      $this->validateEncuesta->getValue('num_id'),
                                                                      $this->validateEncuesta->getValue('l_expedicion'),
                                                                      $this->validateEncuesta->getValue('f_nac'),
                                                                      $this->validateEncuesta->getValue('dir_residencial'),
                                                                      $this->validateEncuesta->getValue('rcelestud'),
                                                                      $this->validateEncuesta->getValue('remailestud'),
                                                                      $_POST['est_civil']);
            if ($fila2 == 0) {
                $error2=0;
            }
            //$error=0 no actualiza, 1 actualiza en tautorizados id_corte, 2 error en la actualizacion
            $error3 = 0;

            $actidcorte = $this->tablaEncuesta->actualizaCorteAutorizado($this->validateEncuesta->getValue('id_autorizado'),
                                                                         $this->validateEncuesta->getValue('id_corte'));
            if (! $actidcorte == 0) {
                $error3 = 1;
            }else {
                $error3 = 2;
            }

            return $this->viewModelAjax([
                'mensaje' => "Encuesta de Pregrado registrada satisactoriamente...Act ".$act." Est ".$error2. "Act. IdCorte Autorizado".$error3. "...!!!",
            ]);
        }
    }

    public function insertarposAction()
    {
        // Validación EncuestaPost
        $error1=1; $error2=1; $act=0;
        if ( ! $this->validateEncuestaPost->validarEncuestaPost() ) {
            return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
        }

        $paisres= $this->tablaActualiza->Busquedadepaises($this->validateEncuestaPost->getValue('paisres'));
        $ciudres= $this->tablaActualiza->Busquedadeciudades($this->validateEncuestaPost->getValue('ciudres'));
        $paislab= $this->tablaActualiza->Busquedadepaises($this->validateEncuestaPost->getValue('paislab'));
        $ciudlab= $this->tablaActualiza->Busquedadeciudades($this->validateEncuestaPost->getValue('ciudlab'));

        if (! $this->tablaEncuestaPost->inserpost($this->validateEncuestaPost->getValues(),$paisres['Pais'],$ciudres['ciudad'],
                                                                                           $paislab['Pais'],$ciudlab['ciudad'])){
            return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
        }else {
            if ( ! $this->validateActualiza->validarIdEstudiante() ) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            }
            if (isset($_POST['trabaja_actual'])) {
                $empleado = 'on';
                $ingresos = 1;
                $empresario = "";
                $pensionado = "";
                $estudiante = "";
                $desempleado = "";
                $tiempo_vincula = "12 meses";
            } else {
                $empleado = "";
                $ingresos = 0;
                $empresario = "";
                $pensionado = "";
                $estudiante = "on";
                $desempleado = "";
                $tiempo_vincula = "";
            }
            $recono="NO";
            $ncapa_espec='on';
            $nivel="Especializacion";
            $universidad= 7;
            $sede=1;
            $yaactualizo = $this->tablaActualiza->getBuscarEstudActualiza($this->validateActualiza->getValue('id_estudiante'));
            if (!count($yaactualizo) == 0) {
                $actual_datos = $this->tablaActualiza->actualizaciondatosegresados($this->validateEncuestaPost->getValue('id_estudiante'),
                    $this->validateEncuestaPost->getValue('paisnac'),
                    $this->validateEncuestaPost->getValue('ciudnac'),
                    $this->validateEncuestaPost->getValue('dir_residencial'),
                    $this->validateEncuestaPost->getValue('paisres'),
                    $this->validateEncuestaPost->getValue('ciudres'),
                    $this->validateEncuestaPost->getValue('rcelestud'),
                    $this->validateEncuestaPost->getValue('remailestud'),
                    $_POST['est_civil'],$empleado,$empresario,$pensionado,
                    $estudiante,$desempleado,
                    $this->validateEncuestaPost->getValue('nombre_empresa'),
                    $this->validateEncuestaPost->getValue('actividad_empresa'),
                    $this->validateEncuestaPost->getValue('sectoremp'),
                    $this->validateEncuestaPost->getValue('direc_emp_trab'),
                    $this->validateEncuestaPost->getValue('paislab'),
                    $this->validateEncuestaPost->getValue('ciudlab'),
                    $this->validateEncuestaPost->getValue('ocupacion_trabajo'),
                    $this->validateEncuestaPost->getValue('sectprof'),$ingresos,
                    $tiempo_vincula,$recono,
                    $this->validateEncuestaPost->getValue('mejorar'),
                    $this->validateEncuestaPost->getValue('fortaleza'),
                    $ncapa_espec,
                    $this->validateEncuestaPost->getValue('especializar'),
                    //$sede,
                    $paisres['Pais'],$ciudres['ciudad'],$paislab['Pais'],
                    $ciudlab['ciudad']);

                $user="estudiante";
                $iddatosact=$this->tablaActualiza->BuscaIDactualiza($this->validateActualiza->getValue('id_estudiante'));
                $idact=$iddatosact['id'];
                if (! $actual_datos == 0) {
                    if (!$this->tablaActualiza->insertActulizaEstudios($this->validateEncuestaPost->getValue('progpost'),
                        $nivel, $universidad,
                        $this->validateEncuestaPost->getValue('anyo2'),
                        $user,$idact)){
                        return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
                    }
                }else{
                    if (!$this->tablaActualiza->insertActulizaEstudios($this->validateEncuestaPost->getValue('progpost'),
                        $nivel, $universidad,
                        $this->validateEncuestaPost->getValue('anyo2'),
                        $user,$idact)){
                        return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
                    }
                }
                $act=1;
            }else {
                $inser_act = $this->tablaActualiza->insertactulizadesdeencuesta($this->validateEncuestaPost->getValue('id_estudiante'),
                    $this->validateEncuestaPost->getValue('paisnac'),
                    $this->validateEncuestaPost->getValue('ciudnac'),
                    $this->validateEncuestaPost->getValue('dir_residencial'),
                    $this->validateEncuestaPost->getValue('paisres'),
                    $this->validateEncuestaPost->getValue('ciudres'),
                    $this->validateEncuestaPost->getValue('rcelestud'),
                    $this->validateEncuestaPost->getValue('remailestud'),
                    $_POST['est_civil'], $empleado, $empresario, $pensionado,
                    $estudiante, $desempleado,
                    $this->validateEncuestaPost->getValue('nombre_empresa'),
                    $this->validateEncuestaPost->getValue('actividad_empresa'),
                    $this->validateEncuestaPost->getValue('sectoremp'),
                    $this->validateEncuestaPost->getValue('direc_emp_trab'),
                    $this->validateEncuestaPost->getValue('paislab'),
                    $this->validateEncuestaPost->getValue('ciudlab'),
                    $this->validateEncuestaPost->getValue('ocupacion_trabajo'),
                    $this->validateEncuestaPost->getValue('sectprof'), $ingresos,
                    $tiempo_vincula, $recono,
                    $this->validateEncuestaPost->getValue('mejorar'),
                    $this->validateEncuestaPost->getValue('fortaleza'),
                    $ncapa_espec,
                    $this->validateEncuestaPost->getValue('especializar'), $sede,
                    $paisres['Pais'], $ciudres['ciudad'], $paislab['Pais'], $ciudlab['ciudad']);

                if (!$inser_act == 0) {
                    $user="estudiante";
                    if (!$this->tablaActualiza->insertActulizaEstudios($this->validateEncuestaPost->getValue('progpost'),
                        $nivel, $universidad, $this->validateEncuestaPost->getValue('anyo2'), $user, $inser_act)){
                        return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
                    }
                    $act=1;
                } else {
                    return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
                }
            }
            $fila2 = $this->tablaEstudiante->actualizaidentestudiante($this->validateEncuestaPost->getValue('id_estudiante'),
                                                                      $this->validateEncuestaPost->getValue('num_id'),
                                                                      $this->validateEncuestaPost->getValue('l_expedicion'),
                                                                      $this->validateEncuestaPost->getValue('f_nac'),
                                                                      $this->validateEncuestaPost->getValue('dir_residencial'),
                                                                      $this->validateEncuestaPost->getValue('rcelestud'),
                                                                      $this->validateEncuestaPost->getValue('remailestud'),
                                                                      $_POST['est_civil']);
            if ($fila2 == 0) {
                $error2=0;
            }

            //$error=0 no actualiza, 1 actualiza en tautorizados id_corte, 2 error en la actualizacion
            $error3 = 0;

            $actidcorte = $this->tablaEncuesta->actualizaCorteAutorizado($this->validateEncuestaPost->getValue('id_autorizado'),
                                                                         $this->validateEncuestaPost->getValue('id_corte'));
            if (! $actidcorte == 0) {
                $error3 = 1;
            }else {
                $error3 = 2;
            }

            return $this->viewModelAjax([
                'mensaje' => "Encuesta de Postgrado registrada satisactoriamente...Act ".$act." Est ".$error2. "Act. IdCorte Autorizado ".$error3. "...!!!",
            ]);
        }
    }

    public function consultaencuestaAction(){

        // Validación identificación Estudiante
        if ( ! $this->validateBuscaEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateBuscaEstudiante->getValue('identificacion'));
        //echo gettype($est);exit();
        //echo is_null($est);exit();
        if (is_null($est)) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateBuscaEstudiante->getValue('identificacion').
                "</strong> Verifique si el egresado existe en la Base de datos");
        }else{
            //preguntar si ya realizo la encuesta
            $enc = $this->tablaBuscaEncuesta->getConsultaExisteEstudianteEncuestaid($est['id']);
            //echo gettype($enc);exit();
            if (is_null($enc)) {
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateBuscaEstudiante->getValue('identificacion').
                    "</strong> No tiene encuestas realizadas. <strong>");
            }else {
                $datosencuesta = $this->tablaBuscaEncuesta->Buscarencuesta($est['id']);
                return $this->viewModelAjax([
                    'viewencuesta' => $datosencuesta,
                ]);
            }
        }
    }

    public function consultaestudxnombresAction(){

        // Validación del nombre del Estudiante
        if ( ! $this->validateBuscaEstudiante->validarNombres() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        $estxnom = $this->tablaBuscaEncuesta->Buscarestudiantexnom($this->validateBuscaEstudiante->getValue('nombres'));
        if (is_null($estxnom)) {
            return $this->verMensajeError("No existen registros con El Parametro de busqueda <strong>"
                .$this->validateBuscaEstudiante->getValue('nombres').
                "</strong> Verifique si el egresado existe en la Base de datos");
        }else{
            return $this->viewModelAjax([
                'viewencuesta' => $estxnom,
            ]);
        }
    }

    public function verencuestaAction(){
        $est = $this->tablaEstudiante->getEstudianteId((int)$this->getParam1());
        //$est['id']
        $enc = $this->tablaBuscaEncuesta->getConsultaExisteEstudianteEncuesta((int)$this->getParam2());
        $unipreotra = $this->tablaUniversidad->getUniversidadEscogida($enc['ha_nomunivotracarrera']);
        $prog = $this->tablaProgramas->Programas();
        return $this->viewModelAjax([
            'estudiante' => $est,
            'encuesta' => $enc,
            'univpreotra' => $unipreotra,
            'programas' => $prog,
            'estcivil' => $this->tablaActualiza->Listaestadoscivil(),
            'act_economica' => $this->tablaActualiza->Listaacteconom(),
            'paisnac' => $this->tablaActualiza->Listadepaises(),
            'paisres' => $this->tablaActualiza->Listadepaises(),
            'paislab' => $this->tablaActualiza->Listadepaises(),
            'ciudnac' => $this->tablaActualiza->listadeciudades($enc['ciud_nac']),
            'ciudres' => $this->tablaActualiza->listadeciudades($enc['ciud_res']),
            'ciudlab' => $this->tablaActualiza->listadeciudades($enc['ciud_lab']),
            'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
            'respuestas1'=>$this->tablaActualiza->ListadeRespuestas(),
        ]);
    }

    public function consultaencuestapostAction(){

        // Validación identificación Estudiante
        if ( ! $this->validateBuscaEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateBuscaEstudiante->getValue('identificacion'));
        if (is_null($est)) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateBuscaEstudiante->getValue('identificacion').
                "</strong> Verifique si el egresado existe en la Base de datos");
        }else{
            //preguntar si ya realizo la encuesta
            $enc = $this->tablaBuscaEncuesta->getConsultaExisteEstudianteEncuestapostid($est['id']);
            if (is_null($enc)) {
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateBuscaEstudiante->getValue('identificacion').
                    "</strong> No tiene encuestas realizadas. <strong>");
            }else {
                $datosencuesta = $this->tablaBuscaEncuesta->Buscarencuestapost($est['id']);
                return $this->viewModelAjax([
                    'viewencuestapost' => $datosencuesta,
                ]);
            }
        }
    }

    public function consultaestudxnombresposAction(){

        // Validación del nombre del Estudiante
        if ( ! $this->validateBuscaEstudiante->validarNombres() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }

        $estxnom = $this->tablaBuscaEncuesta->Buscarestudiantexnompos($this->validateBuscaEstudiante->getValue('nombres'));
        if (is_null($estxnom)) {
            return $this->verMensajeError("No existen registros con El Parametro de busqueda <strong>"
                .$this->validateBuscaEstudiante->getValue('nombres').
                "</strong> Verifique si el egresado existe en la Base de datos");
        }else{
            return $this->viewModelAjax([
                'viewencuesta' => $estxnom
            ]);
        }
    }

    public function verencuestapostAction(){
        $est = $this->tablaEstudiante->getEstudianteId((int)$this->getParam1());
        //$est['id']
        $enc = $this->tablaBuscaEncuesta->getConsultaExisteEstudianteEncuestapost((int)$this->getParam2());
        $unipre = $this->tablaUniversidad->getUniversidadEscogida($enc['ha_univpregraduado']);
        $unipos = $this->tablaUniversidad->getUniversidadEscogida($enc['ha_univpostgraduado']);
        $progpos = $this->tablaProgramas->getProgramaEscojido($enc['ha_progpostgraduado']);
        $progpre = $this->tablaProgramas->getProgramaEscojido($enc['id_progpreunicor']);
        return $this->viewModelAjax([
            'estudiante' => $est,
            'encuestapost' => $enc,
            'univpre' => $unipre,
            'univpost' => $unipos,
            'programas' => $progpos,
            'progpreunicor' => $progpre,
            'estcivil' => $this->tablaActualiza->Listaestadoscivil(),
            'act_economica' => $this->tablaActualiza->Listaacteconom(),
            'paisnac' => $this->tablaActualiza->Listadepaises(),
            'paisres' => $this->tablaActualiza->Listadepaises(),
            'paislab' => $this->tablaActualiza->Listadepaises(),
            'ciudnac' => $this->tablaActualiza->listadeciudades($enc['ciud_nac']),
            'ciudres' => $this->tablaActualiza->listadeciudades($enc['ciud_res']),
            'ciudlab' => $this->tablaActualiza->listadeciudades($enc['ciud_lab']),
            'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
            'respuestas1'=>$this->tablaActualiza->ListadeRespuestas(),
        ]);
    }

    public function addnroactaAction(){
        $datos = $this->tablaBuscaEncuesta->Buscadatosparaactas((int)$this->getParam1());
        $prog = $this->tablaProgramas->getProgramaEscojido($datos['ha_progprecursa']);
        return $this->viewModelAjax([
            'datosparaacta' => $datos,
            'programas' => $prog
        ]);
    }

    public function addnroactaposAction(){
        $datos = $this->tablaBuscaEncuesta->Buscadatosparaactaspos((int)$this->getParam1());
        $prog = $this->tablaProgramas->getProgramaEscojido($datos['ha_progpostgraduado']);
        return $this->viewModelAjax([
            'datosparaacta' => $datos,
            'programas' => $prog
        ]);
    }

    public function insertactasAction()
    {
        // Validación Encuesta
        if ( ! $this->validateEstudiante->validarDatosActas() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $user=Sesiones::getUsuarioname();
        $estacta = $this->tablaEstudiante->getEstudianteexisteacta($this->validateEstudiante->getValue('id_autorizado'));
        $parametro=$_POST['idcorte'];
        $programa=$_POST['procurso'];
        $tipoestud=$_POST['tipoestud'];
        if (is_null($estacta)) {
            if (!$this->tablaEstudiante->insertarActas($this->validateEstudiante->getValues(),$user)) {
                return $this->verMensajeError($this->validateEstudiante->getMensajes());
            } else {
                $filas2=$this->tablaEstudiante->actualizanomape($this->validateEstudiante->getValue('id_estud'),
                    $this->validateEstudiante->getValue('nombres'),
                    $this->validateEstudiante->getValue('apellidos'),
                    $user);
                if ($filas2==0){
                    /*return $this->verMensajeInfo("Informacion de actas fue registrado satisactoriamente,
                                                         los nombres y apellidos del estudiante no fueron actualizados");*/
                    return $this->viewModelAjax([
                        'mensaje' => "Informacion de actas fue registrado satisactoriamente, 
                                                         los nombres y apellidos del estudiante no fueron actualizados...!",
                        'parametro1' => $parametro,
                        'progra' => $programa,
                        'estudio' => $tipoestud,
                    ]);
                } else {
                    $filas1=$this->tablaEstudiante->actualizaestadoestudpre($this->validateEstudiante->getValue('id_estud'));
                    if ($filas1==0){
                        /*return $this->verMensajeInfo("Informacion de actas fue registrado satisactoriamente y los
                                                          nombres y apellidos del estudiante fueron actualizados, pendiente estado");*/
                        return $this->viewModelAjax([
                            'mensaje' => "Informacion de actas fue registrado satisactoriamente y los
                                                          nombres y apellidos del estudiante fueron actualizados, pendiente estado...!",
                            'parametro1' => $parametro,
                            'progra' => $programa,
                            'estudio' => $tipoestud,
                        ]);
                    }else{
                        /*return $this->verMensajeInfo("Informacion de actas fue registrado satisactoriamente y los
                                                          nombres y apellidos del estudiante fueron actualizados y su estado");*/
                        return $this->viewModelAjax([
                            'mensaje' => "Informacion de actas fue registrado satisactoriamente y los
                                                          nombres y apellidos del estudiante fueron actualizados y su estado..!",
                            'parametro1' => $parametro,
                            'progra' => $programa,
                            'estudio' => $tipoestud,
                        ]);
                    }
                }
            }
        }else{
            /*return $this->verMensajeInfo("Estudiante con número de actas ya registrado....!");*/
            return $this->viewModelAjax([
                'mensaje' => "Estudiante con número de actas ya registrado...!",
                'parametro1' => $parametro,
                'progra' => $programa,
                'estudio' => $tipoestud,
            ]);
        }
    }

    //ver las actas desde el listado estudiantes con actas
    public function vernroactaAction(){
        $tpest=(int)$this->getParam2();
        if ($tpest==1) {
            $datosest = $this->tablaBuscaEncuesta->Buscadatosparaactas((int)$this->getParam1());
            $prog = $this->tablaProgramas->getProgramaEscojido($datosest['ha_progprecursa']);
        }else{
            $datosest = $this->tablaBuscaEncuesta->Buscadatosparaactaspos((int)$this->getParam1());
            $prog = $this->tablaProgramas->getProgramaEscojido($datosest['ha_progpostgraduado']);
        }

        $actas = $this->tablaReportes->verestudconactas($datosest['id_autorizado']);
        return $this->viewModelAjax([
            'datosparaacta' => $datosest,
            'datosveracta' => $actas,
            'programas' => $prog
        ]);
    }

    //ver una acta desde la consulta verestudiantes
    public function vernroactaestudgraduadoAction(){
        // (int)$this->getParam1() => trae el id_autorizado de la tabla autorizados
        // (int)$this->getParam2() => trae el tipo de estudio pre o post
        $tpest=(int)$this->getParam2();
        $encuesta = $this->tablaBuscaEncuesta->SiEncuesta($this->getParam1(), $this->getParam2());
        if (is_null($encuesta)) {
            return $this->verMensajeInfo("Informacion...Egresado sin Encuesta y sin Actas registradas...!");
        }else{
            if ($tpest == 1) {
                $datosest = $this->tablaBuscaEncuesta->Selecdatosparaactas((int)$this->getParam1());
                $prog = $this->tablaProgramas->getProgramaEscojido($datosest['ha_progprecursa']);
            } else {
                $datosest = $this->tablaBuscaEncuesta->Selecdatosparaactaspos((int)$this->getParam1());
                $prog = $this->tablaProgramas->getProgramaEscojido($datosest['ha_progpostgraduado']);
            }

            $actas = $this->tablaReportes->verestudconactas((int)$this->getParam1());
            return $this->viewModelAjax([
                'datosparaacta' => $datosest,
                'datosveracta' => $actas,
                'programas' => $prog
            ]);
        }
    }

    public function updatecarnetAction()
    {
        if ( ! $this->validateEstudiante->validarCarnetActas() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }
        //print_r($this->validateEstudiante->getValues());exit();
        $user=Sesiones::getUsuarioname();
        $fila=$this->tablaEstudiante->actualizacarnetactas($this->validateEstudiante->getValues(),$user);
        if ($fila==0 ){
            return $this->verMensajeInfo("la Informacion Actas y carnet NO se actualizo.......");
        } else {
            return $this->verMensajeInfo("la Informacion Actas y carnet Fue actualizada satisactoriamente....!");
        }
    }
}