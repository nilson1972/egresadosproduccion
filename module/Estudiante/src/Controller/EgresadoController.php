<?php

namespace Estudiante\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class EgresadoController extends GenericController
{
    public function __construct()
    {
        $this->tablaEgresado = new \Estudiante\Table\EgresadoTable();
        $this->validateEgresado = new \Estudiante\Validate\EgresadoValidate();
    }

    public function indexAction()
    {
    }

    public function consultaegresadoAction()
    {
        // Validación identificación del egresado
        if ( ! $this->validateEgresado->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateEgresado->getMensajes());
        }

        $datosestud = $this->tablaEgresado->Selecestudiante($this->validateEgresado->getValue('identificacion'));

        $datosautoriza = $this->tablaEgresado->Selecidautoriza($datosestud['id']);

        $siesegresado = $this->tablaEgresado->verificaegresado($datosautoriza['id']);

        $datosacta = $this->tablaEgresado->Selecegresado($datosestud['id']);

        if (is_null($siesegresado)) {
            return $this->verMensajeInfo("Usuario con identificacion "
                . $this->validateEgresado->getValue('identificacion') .
                "<strong>". " NO exite en la base de datos de Egresados..Comuniquese con la Oficina de Atención al Egresado, 
                               a traves del correo: egresados@correo.unicordoba.edu.co </strong> ");
        }else{
            $this->tablaEgresado->actualizanrocosnultasestudiante($datosestud['id'], $datosestud['nroconsultas']);
            return $this->viewModelAjax([
                'datosacta' => $datosacta,
            ]);
        }
        /*$nombre="";
        $nro_ident="";
        foreach ($datosestud as $d){
            $nro_ident = $d["nro_ident"];
            $nombre = $d["nombres"];
        }
        var_dump($nro_ident);*/
    }

}