<?php

namespace Estudiante\Validate;


use Comun\InputFilterGeneric3;


class ActualizarValidate extends InputFilterGeneric3
{

    public function validarIdentificacion()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'identificacion',
            'label' => 'Valor no valido para campo Identificacion',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarIdEstudiante()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'id_estudiante',
            'label' => 'Valor no valido para campo Id_Estudiante',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarNombres()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'El campo Nombres Contiene un Valor no valido ',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function validarIdEvento()
    {
        $this->validarDigito([
            'name'  => 'evento',
            'label' => 'Valor no valido para seleccion del campo Evento',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarDatosActualizados()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'tipodoc',
            'label' => ' tipo de documento ',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'dir_residencial',
            'label' => ' dirección residencia ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paisres',
            'label' => ' pais de residencia ',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudres',
            'label' => ' ciudad de residencia ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'remailestud',
            'label' => ' email ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'rcelestud',
            'label' => ' Telefono(s) ',
            'required' => true,
            'max_lenght' => 30,
            'alnum' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fec_nacim',
            'label' => ' Fecha Nacimiento ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paisnac',
            'label' => ' pais de nacimiento ',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudnac',
            'label' => ' ciudad de nacimiento ',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'est_civil',
            'label' => ' Estado Civil ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'empleado',
            'label' => ' empleado ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'empresario',
            'label' => ' empresario ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'pensionado',
            'label' => ' pensionado ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'estudiante',
            'label' => ' estudiante ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'desempleado',
            'label' => ' desempleado ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre_empresa',
            'label' => ' nombre empresa actual ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'act_economica',
            'label' => ' actividad economica ',
            'required' => false,
            'max_lenght' => 150,
            'alnum' => false,
        ]);

        /*$this->validarDigito([
            'name' => 'act_economica',
            'label' => ' act_economica ',
            'required' => false,
        ]);*/

        $this->validarAlfaNumericoFiltro([
            'name' => 'sector',
            'label' => ' sector ',
            'required' => false,
            'max_lenght' => 15,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'direc_emp_trab',
            'label' => ' dirección empresa actual ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paislab',
            'label' => ' pais donde labora ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudlab',
            'label' => ' ciudad donde labora ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'cargo_trab',
            'label' => ' cargo empresa actual ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'sectprof',
            'label' => ' sector de su profesion ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ingmensual',
            'label' => ' ingreso mensual ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'tiempovinc',
            'label' => ' tiempo de vinculacion ',
            'required' => false,
            'max_lenght' => 25,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'tienerecono',
            'label' => ' tiene algun reconocimiento ',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'tporecono',
            'label' => ' tipo de reconocimiento ',
            'required' => false,
            'max_lenght' => 12,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'descriprecono',
            'label' => ' descripcion del reconocimiento ',
            'required' => false,
            'max_lenght' => 200,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'instrecono',
            'label' => ' institución del reconocimiento ',
            'required' => false,
            'max_lenght' => 150,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'anorecono',
            'label' => ' año del reconocimiento ',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'mejorar',
            'label' => ' prog_debe_mejorar ',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'fortaleza',
            'label' => ' prog_fortaleza ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'especiali',
            'label' => ' especialización ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'maestria',
            'label' => ' maestria ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'doctorado',
            'label' => ' doctorado ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'diplomado',
            'label' => ' diplomado ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'tema',
            'label' => ' Tema a sugerir ',
            'required' => false,
            'max_lenght' => 200,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'aceptacion',
            'label' => ' aceptacion ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'id_estudiante',
            'label' => ' id_estudiante ',
            'required' => false,
        ]);

        return $this->validar();
    }

    public function validarDatosEstudRealiza($i){

        $this->validarDigito([
            'name'  => 'titulo'.$i,
            'label' => ' titulo obtenido ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nivelacad'.$i,
            'label' => ' nivel academico del titulo ',
            'required' => true,
            'max_lenght' => 20,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'iduniversidad'.$i,
            'label' => ' universidad titulo obtenido ',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'anogrado'.$i,
            'label' => ' año del titulo ',
            'required' => true,
        ]);

        return $this->validar();

    }

    public function validarDatosEventoNuevo()
    {
        $this->validarFecha([
            'name'  => 'feciniinscrip',
            'label' => ' Fecha Inicio Inscripción ',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecfininscrip',
            'label' => ' fecha Finaliza Inscripción ',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecinievento',
            'label' => ' Fecha Inicio Evento',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecfinevento',
            'label' => ' fecha Finaliza Evento',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'evento',
            'label' => ' Nombres Del evento ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
           'name'   => 'tpevento',
            'label' => ' Tipo del evento ',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function ValidarDatosRespuestaActualiza()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'respuesta',
            'label' => ' respuesta ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function ValidarDatosPrograma()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre',
            'label' => ' nombre del programa ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'tipoprog',
            'label' => ' Tipo de programa ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'ofertado',
            'label' => ' programa ofertado ',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function ValidarDatosUniversidad()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'codigo',
            'label' => ' codigo universidad ',
            'required' => true,
            'max_lenght' => 5,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre',
            'label' => ' nombre universidad ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'ciudad',
            'label' => ' ciudad universidad ',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function ValidarDatosTipoEvento()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'tipoevento',
            'label' => ' Tipo de evento ',
            'required' => true,
            'max_lenght' => 80,
            'alnum' => false,
        ]);

        return $this->validar();
    }
}