<?php

namespace Estudiante\Validate;


use Comun\InputFilterGeneric3;


class EstudianteValidate extends InputFilterGeneric3
{

    public function validarIdentificacion()
    {
        $this->validarDigito([
            'name' => 'identificacion',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);
        return $this->validar();
    }

    public function validarNroIdentEstud()
    {
        $this->validarDigito([
            'name' => 'nro_ident',
            'label' => 'Valor no valido para campo Nro identificación',
            'required' => true,
        ]);
        return $this->validar();
    }

    public function validarPrograma()
    {
        $this->validarDigito([
            'name' => 'programa',
            'label' => 'Valor no valido para campo programa',
            'required' => true,
        ]);
        return $this->validar();
    }

    public function validarAutorizacion()
    {
        $this->validarDigito([
            'name' => 'identificacion',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'programa',
            'label' => 'Valor no valido para campo programa',
            'required' => true,
            'max_lenght' => 10,
            'alnum' => false,
        ]);


        return $this->validar();
    }


    public function validarDatosEstudiantes()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'tipodoc',
            'label' => ' tipo de documento ',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'nro_ident',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'l_expedicion',
            'label' => 'Valor no valido para campo Ciudad de expedicion',
            'required' => true,
            'max_lenght' => 50,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'Valor no valido para campo Nombres',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'apellidos',
            'label' => 'Valor no valido para campo Apellidos',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fec_nacim',
            'label' => ' Fecha Nacimiento ',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'est_civil',
            'label' => ' Estado Civil ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'sexo',
            'label' => 'Valor no valido para campo Sexo',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'dir_residencial',
            'label' => 'Valor no valido para campo Direccion',
            'required' => true,
            'max_lenght' => 200,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'rcelestud',
            'label' => 'Valor no valido para campo Nro Celular',
            'required' => true,
            'max_lenght' => 25,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'email',
            'label' => 'Valor no valido para campo Email',
            'required' => true,
            'max_lenght' => 200,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'corte',
            'label' => 'Valor no valido para campo corte',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarCorte()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'corte',
            'label' => 'Valor no valido para campo Corte Periodo',
            'required' => true,
            'max_lenght' => 6,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'estado',
            'label' => 'Valor no valido para campo Estado',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);
        return $this->validar();
    }

    public function validarDatosActas()
    {
        $this->validarFecha([
            'name'  => 'fec_actas',
            'label' => 'Valor no valido para campo fecha de actas',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'actagral',
            'label' => 'Valor no valido para campo acta general',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'actaind',
            'label' => 'Valor no valido para campo acta individual',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'nro_diplo',
            'label' => 'Valor no valido para campo numero de diploma',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'carnet',
            'label' => 'Valor no valido para campo Carnet entregado',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecentcarnet',
            'label' => 'Valor no valido para campo fecha entrega carnet',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'periodo_grado',
            'label' => 'Valor no valido para campo periodo',
            'required' => true,
            'max_lenght' => 6,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'id_autorizado',
            'label' => 'Valor no valido para campo id_autorizacion',
            'required' => false,
        ]);

        $this->validarDigito([
            'name'  => 'id_estud',
            'label' => 'Id estudiante',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'Nombres',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'apellidos',
            'label' => 'Apellidos',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function validarCarnetActas()
    {
        $this->validarDigito([
            'name' => 'idtactas',
            'label' => 'Valor no validado',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fec_actas',
            'label' => 'Valor no valido para campo fecha acta',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'actagral',
            'label' => 'Valor no valido para campo acta general',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'actaind',
            'label' => 'Valor no valido para campo acta individual',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'nro_diplo',
            'label' => 'Valor no valido para campo numero de diploma',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'carnet',
            'label' => 'Valor no valido para campo Carnet entregado',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecentcarnet',
            'label' => 'Valor no valido para campo fecha entrega carnet',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'periodo_grado',
            'label' => 'Valor no valido para campo periodo',
            'required' => true,
            'max_lenght' => 6,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function validarIdentNombres()
    {
        $this->validarDigito([
            'name'  => 'num_id',
            'label' => 'num_id',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'Nombres',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'l_expedic',
            'label' => 'Lugar de expedicion',
            'required' => true,
            'max_lenght' => 30,
            'alnum' => false,
        ],false);

        $this->validarAlfaNumericoFiltro([
            'name' => 'apellidos',
            'label' => 'Apellidos',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'sexo',
            'label' => 'sexo',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'saber_pro',
            'label' => 'saber_pro',
            'required' => true,
            'max_lenght' => 14,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'f_nac',
            'label' => 'fecha de nacimiento',
            'required' => true,
            'max_lenght' => 10,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'id_encuesta',
            'label' => 'id_encuesta',
            'required' => false,
        ]);

        $this->validarDigito([
            'name'  => 'id_estud',
            'label' => 'id_estud',
            'required' => false,
        ]);

        return $this->validar();
    }

    public function ValidarVerestudAutoriza()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nro_ident',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'programa',
            'label' => 'Valor no valido para campo programa',
            'required' => true,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'corte',
            'label' => 'Valor no valido para campo corte',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validardatosactualizaestudiante()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nro_ident',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'Nombres',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'apellidos',
            'label' => 'Apellidos',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'fec_nacim',
            'label' => 'fecha de nacimiento',
            'required' => true,
            'max_lenght' => 10,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'sexo',
            'label' => 'sexo',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'est_civil',
            'label' => 'Valor no valido para estado civil',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'dir_resid',
            'label' => 'Valor no valido para campo Direccion',
            'required' => true,
            'max_lenght' => 200,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'rcelestud',
            'label' => 'Valor no valido para campo Nro Celular',
            'required' => true,
            'max_lenght' => 25,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'email',
            'label' => 'Valor no valido para campo Email',
            'required' => true,
            'max_lenght' => 200,
            'alnum' => false,
        ]);

        return $this->validar();
    }
}