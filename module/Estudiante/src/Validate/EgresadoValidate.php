<?php

namespace Estudiante\Validate;

use Comun\InputFilterGeneric3;

class EgresadoValidate extends InputFilterGeneric3
{
    public function validarIdentificacion()
    {
        $this->validarDigito([
            'name' => 'identificacion',
            'label' => 'Identificación',
            'required' => true,
        ]);
        return $this->validar();
    }

}