<?php
namespace Estudiante\Validate;


use Comun\InputFilterGeneric3;


class EncuestaValidate extends InputFilterGeneric3
{

    public function validarEncuesta()
    {
        $this->validarDigito([
            'name' => 'id_estudiante',
            'label' => 'Valor no valido para campo ID Estudiante',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'id_autorizado',
            'label' => 'Valor no valido para campo ID Autoriza',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'id_corte',
            'label' => 'Valor no valido para campo ID Corte',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'num_id',
            'label' => 'Valor no valido para campo Nro identificacion actual',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'l_expedicion',
            'label' => 'Valor no valido para campo Municipio de expedicion',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarFecha([
            'name'  => 'f_nac',
            'label' => 'Valor no valido para campo fecha de nacimiento',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paisnac',
            'label' => ' Pais de nacimiento',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudnac',
            'label' => ' Ciudad de Nacimiento',
            'required' => true,
        ]);

        /*$this->validarAlfaNumericoFiltro([
            'name' => 'nac_municipio',
            'label' => 'Valor no valido para campo Municipio de nacimiento',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nac_depto',
            'label' => 'Valor no valido para campo Departamento de nacimiento',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);*/

        $this->validarAlfaNumericoFiltro([
            'name' => 'dir_residencial',
            'label' => 'Valor no valido para campo Direccion de residencia',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paisres',
            'label' => ' Pais de nacimiento',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudres',
            'label' => ' Ciudad de Residencia',
            'required' => true,
        ]);

        /*$this->validarAlfaNumericoFiltro([
            'name' => 'res_municipio',
            'label' => 'Valor no valido para campo Municipio de residencia',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'res_depto',
            'label' => 'Valor no valido para campo Departamento de residencia',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);*/

        $this->validarDigito([
            'name' => 'rtelestud',
            'label' => 'Valor no valido para campo Telefono Residencia estudiante',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'rcelestud',
            'label' => 'Valor no valido para campo Celular estudiante',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'remailestud',
            'label' => 'Valor no valido para campo email estudiante',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name'  => 'snp',
            'label' => 'Valor no valido para campo año SNP prueba saberPRO',
            'required' => true,
            'max_lenght' => 30,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name'  => 'programa',
            'label' => 'Valor no valido para campo programa',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'sem_ini',
            'label' => 'Valor no valido para campo semestre inicial',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'anyo1',
            'label' => 'Valor no valido para campo año inicio',
            'required' => true,
        ]);

        $this->validarDigito([
            'name'  => 'sem_fin',
            'label' => 'Valor no valido para campo semestre final',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'anyo2',
            'label' => 'Valor no valido para campo año fin',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'sedeuniv',
            'label' => 'Valor no valido para campo en que sede termino',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'porcentaje',
            'label' => 'Valor no valido para campo año inicio',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'estudio_otra',
            'label' => 'Valor no valido para campo otra carrera',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'termino_otra',
            'label' => 'Valor no valido para campo termino esa carrera',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

 /*       $this->validarAlfaNumericoFiltro([
            'name' => 'univer_otra',
            'label' => 'Valor no valido para campo nombre universidad',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]); */
        $this->validarDigito([
            'name' => 'univer_otra',
            'label' => 'Valor no valido para campo nombre otra universidad',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'otroprograma',
            'label' => 'Valor no valido para campo otra carrera estudio',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'especializar',
            'label' => 'Valor no valido para campo legustaria especializarse',
            'required' => true,
            'max_lenght' => 80,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'trabaja_actual',
            'label' => 'Valor no valido para campo trabajo actual',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'sectprof',
            'label' => ' sector trabajo ',
            'required' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fecha_inilaboral',
            'label' => 'Valor no valido para campo fecha inicio labora',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'trabajo_remunera',
            'label' => 'Valor no valido para campo trabajo remunerado',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'ocupacion_trabajo',
            'label' => 'Valor no valido para campo ocupacion',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre_empresa',
            'label' => 'Valor no valido para campo nombre empresa',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'actividad_empresa',
            'label' => 'actividad_empresa',
            'required' => false,
            'max_lenght' => 150,
            'alnum' => false,
        ]);

        /*$this->validarDigito([
            'name' => 'actividad_empresa',
            'label' => 'Valor no valido para campo actividad empresa',
            'required' => false,
        ]);*/

        $this->validarAlfaNumericoFiltro([
            'name' => 'sectoremp',
            'label' => ' sectoremp ',
            'required' => false,
            'max_lenght' => 15,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nom_gerente_emp_trab',
            'label' => 'Valor no valido para campo nombre gerente',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nom_jefe_emp_trab',
            'label' => 'Valor no valido para campo nombre jefe inmediato',
            'required' => false,
            'max_lenght' => 80,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'direc_emp_trab',
            'label' => 'Valor no valido para campo direccion empresa',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paislab',
            'label' => ' Pais donde labora',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudlab',
            'label' => ' Ciudad donde labora',
            'required' => false,
        ]);

        /*$this->validarAlfaNumericoFiltro([
            'name' => 'dpto_emp_trab',
            'label' => 'Valor no valido para campo departamento empresa',
            'required' => false,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'mpio_emp_trab',
            'label' => 'Valor no valido para campo municipio empresa',
            'required' => false,
            'max_lenght' => 60,
            'alnum' => false,
        ]);*/

        $this->validarDigito([
            'name' => 'telef_emp_trab',
            'label' => 'Valor no valido para campo año inicio',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'email_emp_trab',
            'label' => 'Valor no valido para campo email empresa',
            'required' => false,
            'max_lenght' => 100,
        ]);

        $this->validarDigito([
            'name' => 'mejorar',
            'label' => ' prog_debe_mejorar ',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'fortaleza',
            'label' => ' prog_fortaleza ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'aceptacion',
            'label' => ' aceptacion ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        return $this->validar();
    }
}