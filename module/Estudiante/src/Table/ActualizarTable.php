<?php
namespace Estudiante\Table;

use Comun\DB;
use Zend\Filter\MonthSelect;

class ActualizarTable
{
    private $tablaAutoriza = 'tautorizados';
    private $tablaProgramas = 'tprogramas';
    private $tablaCorte = 'tcorte';
    private $tablaEstudiante = 'testudiante';
    private $tablaDatosactualizados = 'tdatosestudactualizados';
    private $tablaInscripeventos = 'tinscripeventos';
    private $tablaEventos = 'teventos';
    private $tablaPaises = 'tpaises';
    private $tablaCiudades = 'tciudades';
    private $tablaRespuestaActual = 'tpreg_prog_mejoras_o_fortalezas';
    private $tablaUniversidades = 'tuniversidad';
    private $tablaActas = 'tactas';
    private $tablaEstadosCivil = 'testados_civil';
    private $tablaEstudiosRealiza = "testudios_realiza";
    private $tablaTitulos = "ttitulos";
    private $tablaIngresm = 'tingresos_mensuales';
    private $tablaActividadesEcono = 'tactividades_economicas';
    private $tablaSedes = 'tsedes';
    private $tablaTipoEvento = 'ttiposdeevento';

    public function insertactuliza($datos,$paisres,$paislab,$ciudres,$ciudlab,$area_especif,$pobla_benef,
                                   $otro_cual,$oficina,$usercrea)
    {
        try {
            DB::transactionInit();
            DB::insertaractualiza($this->tablaDatosactualizados,[
                'fec_actualizadatos' => date('Y-m-d'),
                'id_estudiante' => $datos['id_estudiante'],
                'pais_nacim' => $datos['paisnac'],
                'ciud_nacim' => $datos['ciudnac'],
                'direc_residen' => $datos['dir_residencial'],
                'pais_res' => $datos['paisres'],
                'ciud_res' => $datos['ciudres'],
                'celular' => $datos['rcelestud'],
                'email' => $datos['remailestud'],
                'id_estad_civil' => $datos['est_civil'],
                'empleado' => $datos['empleado'],
                'empresario' => $datos['empresario'],
                'pensionado' => $datos['pensionado'],
                'estudiante' => $datos['estudiante'],
                'desempleado' => $datos['desempleado'],
                'nom_empresa' => $datos['nombre_empresa'],
                'actecoemplabora' => $datos['act_economica'],
                //'id_acteconomica' => $datos['act_economica'],
                'sector' => $datos['sector'],
                'dir_empresa' => $datos['direc_emp_trab'],
                'pais_lab' => $datos['paislab'],
                'ciud_lab' => $datos['ciudlab'],
                'cargo_empresa' => $datos['cargo_trab'],
                'trabsectprofe' => $datos['sectprof'],
                'areaespecif' => $area_especif,
                'poblabenef' => $pobla_benef,
                'otrocualdpte' => $otro_cual,
                'id_ingresos_mens' => $datos['ingmensual'],
                'tiempo_vinculacion' => $datos['tiempovinc'],
                'reconocimiento' => $datos['tienerecono'],
                'tipo_reconoc' => $datos['tporecono'],
                'descrip_reconoc' => $datos['descriprecono'],
                'inst_reconocimiento' => $datos['instrecono'],
                'ano_reconoc' => $datos['anorecono'],
                'id_prog_mejorar' => $datos['mejorar'],
                'id_prog_fortaleza' => $datos['fortaleza'],
                'ncapac_espec' => $datos['especiali'],
                'ncapac_mestria' => $datos['maestria'],
                'ncapac_docto' => $datos['doctorado'],
                'ncapac_diploma' => $datos['diplomado'],
                'ncapac_tema' => $datos['tema'],
                'auto_recolec_datos' => $datos['aceptacion'],
                'fulldirecresi' => $datos['dir_residencial'].", ".$paisres.", ".$ciudres,
                'fulldireclabo' => $datos['direc_emp_trab'].", ".$paislab.", ".$ciudlab,
                'forma_actualiza' => $oficina,
                'user_crea' => $usercrea,
            ]);
            DB::transactionCommit();
            $idultimo = DB::getLasInsertId();
            return $idultimo;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function insertActulizaEstudios($titulo,$nivel,$univer,$ano,$userc,$idact)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaEstudiosRealiza,[
                'id_datosactualiza' => $idact,
                'id_titulo_obtenido' => $titulo,
                'nivel_acade' => $nivel,
                'id_universidad' => $univer,
                'ano_grado' => $ano,
                'user_crea' => $userc
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    /*public function insertactulizainscrip($datos,$paisres,$ciudres,$paislab,$ciudlab)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaDatosactualizados,[
                'fec_actualizadatos' => date('Y-m-d'),
                'id_estudiante' => $datos['id_estudiante'],
                'direc_residen' => $datos['dir_residencial'],
                'pais_res' => $datos['paisres'],
                'ciud_res' => $datos['ciudres'],
                'celular' => $datos['rcelestud'],
                'email' => $datos['remailestud'],
                'tel_fijo' => $datos['tel_fijo'],
                'id_progpre' => $datos['progpre'],
                'progpreotrauniv' => $datos['progpreotra'],
                'id_otraunivpre' => $datos['nomotraunivpre'],
                'id_progpos' => $datos['progpos'],
                'progposotrauniv' => $datos['progposotra'],
                'id_otraunivpos' => $datos['nomotraunivpos'],
                'trabajo_act' => $datos['trabaja_actual'],
                'trabsectprofe' => $datos['sectprof'],
                'nom_empresa' => $datos['nombre_empresa'],
                'dir_empresa' => $datos['direc_emp_trab'],
                'pais_lab' => $datos['paislab'],
                'ciud_lab' => $datos['ciudlab'],
                'tel_empresa' => $datos['telef_emp_trab'],
                'cargo_empresa' => $datos['cargo_trab'],
                'expec_formacion' => $datos['expec_formac'],
                'id_prog_mejorar' => $datos['mejorar'],
                'id_prog_fortaleza' => $datos['fortaleza'],
                'fulldirecresi' => $datos['dir_residencial'].", ".$paisres.", ".$ciudres,
                'fulldireclabo' => $datos['direc_emp_trab'].", ".$paislab.", ".$ciudlab,
                'forma_actualiza' => 'INS',
                'user_crea' => 'estudiante',
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }*/

    public function insertactulizadesdeencuesta($idest, $npaisnac, $nciudnac, $dirres, $paisres, $ciudres, $cel, $email, $est_civil,
                                                $empleado, $empresario, $pensionado, $estud, $desemple, $nomemp, $act_econo, $sector,
                                                $diremp, $paislab, $ciudlab, $cargo, $sectprof, $ingmens, $tiempovinc, $reconoc,
                                                $mejorar, $fortaleza, $ncapac_espe, $expfor, $sede, $npaisres, $nciudres, $npaislab, $nciudlab)
    {
        try {
            DB::transactionInit();
            DB::insertaractualiza($this->tablaDatosactualizados,[
                                'fec_actualizadatos' => date('Y-m-d'),
                                'id_estudiante' => $idest,
                                'pais_nacim' => $npaisnac,
                                'ciud_nacim' => $nciudnac,
                                'direc_residen' => $dirres,
                                'pais_res' => $paisres,
                                'ciud_res' => $ciudres,
                                'celular' => $cel,
                                'email' => $email,
                                'id_estad_civil' => $est_civil,
                                'empleado' => $empleado,
                                'empresario' => $empresario,
                                'pensionado' => $pensionado,
                                'estudiante' => $estud,
                                'desempleado' => $desemple,
                                'nom_empresa' => $nomemp,
                                'actecoemplabora' => $act_econo,
                                //'id_acteconomica' => $act_econo,
                                'sector' => $sector,
                                'dir_empresa' => $diremp,
                                'pais_lab' => $paislab,
                                'ciud_lab' => $ciudlab,
                                'cargo_empresa' => $cargo,
                                'trabsectprofe' => $sectprof,
                                'id_ingresos_mens' => $ingmens,
                                'tiempo_vinculacion' => $tiempovinc,
                                'reconocimiento' => $reconoc,
                                'id_prog_mejorar' => $mejorar,
                                'id_prog_fortaleza' => $fortaleza,
                                'ncapac_mestria' => $ncapac_espe,
                                'ncapac_tema' => $expfor,
                                'sede' => $sede,
                                'auto_recolec_datos' => 'on',
                                'fulldirecresi' => $dirres." ,".$npaisres." ,".$nciudres,
                                'fulldireclabo' => $diremp." ,".$npaislab." ,".$nciudlab,
                                'forma_actualiza' => 'ENC',
                                'user_crea' => 'estudiante',
            ]);
            DB::transactionCommit();
            $idultimo = DB::getLasInsertId();
            return $idultimo;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function actulizarestadovigen($datos,$paisres,$paislab,$ciudres,$ciudlab,$area_especif,$pobla_benef,
                                         $otro_cual,$oficina,$usercrea)
    {
        $set=[
            'fec_actualizadatos' => date('Y-m-d'),
            'pais_nacim' => $datos['paisnac'],
            'ciud_nacim' => $datos['ciudnac'],
            'direc_residen' => $datos['dir_residencial'],
            'pais_res' => $datos['paisres'],
            'ciud_res' => $datos['ciudres'],
            'celular' => $datos['rcelestud'],
            'email' => $datos['remailestud'],
            'id_estad_civil' => $datos['est_civil'],
            'empleado' => $datos['empleado'],
            'empresario' => $datos['empresario'],
            'pensionado' => $datos['pensionado'],
            'estudiante' => $datos['estudiante'],
            'desempleado' => $datos['desempleado'],
            'nom_empresa' => $datos['nombre_empresa'],
            'actecoemplabora' => $datos['act_economica'],
            //'id_acteconomica' => $datos['act_economica'],
            'sector' => $datos['sector'],
            'dir_empresa' => $datos['direc_emp_trab'],
            'pais_lab' => $datos['paislab'],
            'ciud_lab' => $datos['ciudlab'],
            'cargo_empresa' => $datos['cargo_trab'],
            'trabsectprofe' => $datos['sectprof'],
            'areaespecif' => $area_especif,
            'poblabenef' => $pobla_benef,
            'otrocualdpte' => $otro_cual,
            'id_ingresos_mens' => $datos['ingmensual'],
            'tiempo_vinculacion' => $datos['tiempovinc'],
            'reconocimiento' => $datos['tienerecono'],
            'tipo_reconoc' => $datos['tporecono'],
            'descrip_reconoc' => $datos['descriprecono'],
            'inst_reconocimiento' => $datos['instrecono'],
            'ano_reconoc' => $datos['anorecono'],
            'id_prog_mejorar' => $datos['mejorar'],
            'id_prog_fortaleza' => $datos['fortaleza'],
            'ncapac_espec' => $datos['especiali'],
            'ncapac_mestria' => $datos['maestria'],
            'ncapac_docto' => $datos['doctorado'],
            'ncapac_diploma' => $datos['diplomado'],
            'ncapac_tema' => $datos['tema'],
            'auto_recolec_datos' => $datos['aceptacion'],
            'fulldirecresi' => $datos['dir_residencial'].", ".$paisres.", ".$ciudres,
            'fulldireclabo' => $datos['direc_emp_trab'].", ".$paislab.", ".$ciudlab,
            'forma_actualiza' => $oficina,
            'user_crea' => $usercrea,
        ];

        $where=[
            'id_estudiante' => $datos['id_estudiante']
        ];

        try {
            return DB::actualizar($this->tablaDatosactualizados, $set, $where);
        }catch (Exception $e) {
            return false;
        }
    }

    public function insertarinscrip($idest, $idevento)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaInscripeventos,[
                'fecha' => date('Y-m-d G:i:s'),
                'id_estudent' => $idest,
                'id_evento' => $idevento
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function insertarevento($datos, $usercrea)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaEventos,[
                'nombre' => strtoupper($datos['evento']),
                'tipo_de_evento' => $datos['tpevento'],
                'fec_ini_inscrip' => $datos['feciniinscrip'],
                'fec_fin_inscrip' => $datos['fecfininscrip'],
                'fecha_evento' => $datos['fecinievento'],
                'fecha_finaliza' => $datos['fecfinevento'],
                'user_crea' => $usercrea,
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function Listaacteconom()
    {
        return DB::selectTablaOrder( $this->tablaActividadesEcono, []);
    }

    public function Listaingresos()
    {
        return DB::selectTablaOrder( $this->tablaIngresm, []);
    }

    public function Listaestadoscivil()
    {
        return DB::selectTablaOrder( $this->tablaEstadosCivil, []);
    }

    public function ListaTitulos()
    {
        return DB::selectTablaOrder( $this->tablaTitulos, []);
    }

    public function ListaTitulosActEtudiante($iddatact)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiosRealiza));
        $select->columns([
            'id', 'id_datosactualiza', 'id_titulo_obtenido', 'nivel_acade',
            'id_universidad', 'ano_grado'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaTitulos),
            'tabla1.id_titulo_obtenido = tabla2.id',
            ['titulo_obtenido'],
            'left'
        );
        $select->join(
            array('tabla3' => $this->tablaUniversidades),
            'tabla1.id_universidad = tabla3.id',
            ['nombre'],
            'left'
        );

        $select->where(['tabla1.id_datosactualiza'=>$iddatact]);

        //$select->group(['tabla2.nombre']);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        //$resul = $statement->execute();
        return $statement->execute();
    }

    public function listadepaises()
    {
        return DB::selectTablaOrder( $this->tablaPaises, []);
    }

    public function listadeciudades($idciud)
    {
        $campos_ciudades =[
            'id', 'paises_codigo', 'ciudad'
            ];

        $where = [
            'id' =>$idciud,
        ];

        return DB::selectTablaCamposOrder( $this->tablaCiudades, $campos_ciudades, $where, []);
    }

    //lista de ciudades dependiendo de un pais
    public function Seleccionarciudad($codpais){
        ini_set('max_execution_time', 300);
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array( 'tabla1' => $this->tablaCiudades ) );
        $select->columns(
            [
                'id'            => 'id',
                'paises_codigo' => 'paises_codigo',
                'ciudad'        => 'ciudad',
            ]
        ) ;

        $select->where(['tabla1.paises_codigo'=>$codpais]);

        //$select->where->like('tabla1.ciudad', '%'.$ciud.'%');

        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        //$res = $statement->execute();
        return $statement->execute();
    }

    //selecciona un solo pais
    public function Busquedadepaises($cod){
        $campos_paises =[
            'id', 'Codigo', 'Pais'
        ];

        $where = [
            'Codigo' =>$cod,
        ];

        return DB::selectRegistroCampos( $this->tablaPaises, $campos_paises, $where, []);
    }

    //selecciona una sola ciudad
    public function Busquedadeciudades($idciud){
        $campos_ciudades =[
            'id', 'paises_codigo', 'ciudad'
        ];

        $where = [
            'id' =>$idciud,
        ];

        return DB::selectRegistroCampos( $this->tablaCiudades, $campos_ciudades, $where, []);
    }

    public function getProgramaEscojido($idest, $idprog)
    {
        $where = [
            'id_estudiante' =>$idest,
            'id_programa' =>$idprog,
        ];
        return DB::selectRegistro( $this->tablaAutoriza, $where);
    }

    public function getVerProgramasEstud($idest)
    {
        $campos_autorizados = [
            'id', 'id_estudiante'
        ];

        $campos_programas = [
            'id_estudio', 'nombre'
        ];

        $campos_corte = [
            'nombrecorte' => 'nombre'
        ];

        $where = [
            'tabla1.id_estudiante' => $idest,
        ];
        return DB::selectJoinTresTablasIntermedia( $this->tablaAutoriza, $campos_autorizados,
            $this->tablaProgramas, $campos_programas,
            $this->tablaCorte, $campos_corte,
            'id_programa', 'id',
            'id_corte', 'id',
            $where, []);
    }

    public function getTotalEstudactualiza()
    {
        //V=VIGENTE, A=ANTERIOR
        $campos_datosact = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'estado' => 'V',
        ];

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('forma_actualiza');

        return DB::selectRegistroCamposPredicado($this->tablaDatosactualizados, $campos_datosact, $where, $predicate);
    }

    public function getTotalEstudactualizaXenc()
    {
        //V=VIGENTE, A=ANTERIOR
        $campos_datosact = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'estado' => 'V',
        ];

        $predicate =  new \Zend\Db\Sql\Predicate\IsNotNull('forma_actualiza');

        return DB::selectRegistroCamposPredicado($this->tablaDatosactualizados, $campos_datosact, $where, $predicate);
    }

    public function getTotalEstudinscrip()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaInscripeventos));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression(' COUNT(tabla1.id)'),
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEventos),
            'tabla1.id_evento = tabla2.id',
            ['nombre'],
            'inner'
        );

        $select->where(['tabla2.estado'=>'VIG']);

        $select->group(['tabla2.nombre']);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        $resul = $statement->execute();
        return $resul->current();

    }

    public function getBuscaEventoVigente($ideve)
    {
        $where = [
            'id'=>$ideve,
            'estado'=>'VIG'
        ];
        return DB::selectRegistro( $this->tablaEventos, $where);
    }

    public function SelecEvento($ideve)
    {
        /*$where = [
            'id'=>$ideve,
        ];
        return DB::selectRegistro( $this->tablaEventos, $where);*/

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaTipoEvento));
        $select->columns([
            'idtipo'=>'id', 'tipo'=>'tipoevento',
        ]);
        $select->join(
            array('tabla2' => $this->tablaEventos),
            'tabla1.id = tabla2.tipo_de_evento',
            ['id', 'tipo_de_evento', 'fec_ini_inscrip', 'fec_fin_inscrip', 'fecha_evento', 'nombre', 'fecha_finaliza', 'estado'],
            ''
        );

        $select->where([
            'tabla2.id' => $ideve
        ]);

        //$predicate =  new \Zend\Db\Sql\Predicate\Like('tabla2.nombres', '%'.$nomb.'%');
        //$select->where->addPredicate($predicate);

        //$select->order(['tabla2.fecha_evento']);

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        $resul = $statement->execute();

        return $resul->current();
    }

    public function actualizaEvento($datos,$ideven,$usermodi)
    {
        $set = [
            'nombre'=>strtoupper(utf8_encode($datos['evento'])),
            'tipo_de_evento' => $datos['tpevento'],
            'fec_ini_inscrip'=>$datos['feciniinscrip'],
            'fec_fin_inscrip'=>$datos['fecfininscrip'],
            'fecha_evento'=>$datos['fecinievento'],
            'fecha_finaliza'=>$datos['fecfinevento'],
            'user_modi'=>$usermodi,
        ];

        $where = [
            'id'=>$ideven,
        ];

        return DB::actualizar( $this->tablaEventos, $set, $where);
    }

    public function cambiaEstadoEvento($ideven, $est, $usermodi)
    {
        $set = [
            'estado'=>$est,
            'user_modi'=>$usermodi,
        ];

        $where = [
            'id'=>$ideven,
        ];

        return DB::actualizar( $this->tablaEventos, $set, $where);
    }

    public function BuscaIDactualiza($idestud)
    {
        $campo_id = [
            'id'
        ];

        $where = [
            'id_estudiante' =>$idestud,
            'estado'=>'V'
        ];
        return DB::selectRegistroCampos( $this->tablaDatosactualizados, $campo_id, $where);
    }

    public function getBuscarEstudActualiza( $idest )
    {
        $where = [
            'id_estudiante' =>$idest,
            'estado'=>'V'
        ];
        return DB::selectRegistro( $this->tablaDatosactualizados, $where);
    }

    public function getBuscarEstudInscevento( $idest, $ideven )
    {
        $campos_inscrip = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'id_estudent' =>$idest,
            'id_evento' =>$ideven,
        ];
        return DB::selectRegistroCampos( $this->tablaInscripeventos, $campos_inscrip, $where);
    }

    public function actualizaciondatosegresados($idest, $npaisnac, $nciudnac, $dirres, $paisres, $ciudres, $cel, $email, $est_civil,
                                                $empleado, $empresario, $pensionado, $estud, $desemple, $nomemp, $act_econo, $sector,
                                                $diremp, $paislab, $ciudlab, $cargo, $sectprof, $ingmens, $tiempovinc, $reconoc,
                                                $mejorar, $fortaleza, $ncapac_espe, $expfor, $npaisres, $nciudres, $npaislab, $nciudlab)
    {
        $set=[
            'fec_actualizadatos' => date('Y-m-d'),
            'id_estudiante' => $idest,
            'pais_nacim' => $npaisnac,
            'ciud_nacim' => $nciudnac,
            'direc_residen' => $dirres,
            'pais_res' => $paisres,
            'ciud_res' => $ciudres,
            'celular' => $cel,
            'email' => $email,
            'id_estad_civil' => $est_civil,
            'empleado' => $empleado,
            'empresario' => $empresario,
            'pensionado' => $pensionado,
            'estudiante' => $estud,
            'desempleado' => $desemple,
            'nom_empresa' => $nomemp,
            'actecoemplabora' => $act_econo,
            //'id_acteconomica' => $act_econo,
            'sector' => $sector,
            'dir_empresa' => $diremp,
            'pais_lab' => $paislab,
            'ciud_lab' => $ciudlab,
            'cargo_empresa' => $cargo,
            'trabsectprofe' => $sectprof,
            'id_ingresos_mens' => $ingmens,
            'tiempo_vinculacion' => $tiempovinc,
            'reconocimiento' => $reconoc,
            'id_prog_mejorar' => $mejorar,
            'id_prog_fortaleza' => $fortaleza,
            'ncapac_mestria' => $ncapac_espe,
            'ncapac_tema' => $expfor,
            'auto_recolec_datos' => 'on',
            'fulldirecresi' => $dirres." ,".$npaisres." ,".$nciudres,
            'fulldireclabo' => $diremp." ,".$npaislab." ,".$nciudlab,
            'forma_actualiza' => 'ENC',
            'user_crea' => 'estudiante',
        ];

        $where=[
            'id_estudiante' => $idest
        ];

        try {
            return DB::actualizar($this->tablaDatosactualizados, $set, $where);
        }catch (Exception $e) {
            return false;
        }
    }

    public function buscarestudactxnom($nom){
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'idest' => 'id', 'nombres', 'apellidos', 'nro_ident'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaDatosactualizados),
            'tabla1.id = tabla2.id_estudiante',
            ['id', 'fec_actualizadatos'],
            'inner'
        );

        $select->where(['tabla2.estado'=>'V']);

        $select->where->like('tabla1.nombres', '%'.$nom.'%');

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Nroegresadosactualiza($fec1,$fec2,$prog,$fmactua){

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaDatosactualizados));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(DISTINCT tabla1.id)'),
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEstudiosRealiza),
            'tabla1.id = tabla2.id_datosactualiza',
            [],
            'inner'
        );

        if ($prog<>0) {
            if ($fmactua<>"TOD") {
                $select->where(['tabla1.estado' => 'V',
                    'tabla2.id_titulo_obtenido' => $prog,
                    'tabla2.id_universidad' => 7,
                    'tabla1.forma_actualiza' => $fmactua]);
            }else{
                $select->where(['tabla1.estado' => 'V',
                    'tabla2.id_universidad' => 7,
                    'tabla2.id_titulo_obtenido' => $prog]);
            }
        }else{
            if ($fmactua<>"TOD") {
                $select->where(['tabla1.estado' => 'V',
                    'tabla1.forma_actualiza' => $fmactua]);
            }else{
                $select->where(['tabla1.estado' => 'V']);
            }
        }

        //$select->group(['tabla1.id']);

        $predicate2 = new \Zend\Db\Sql\Predicate\Between('tabla1.fec_actualizadatos', $fec1, $fec2);

        $select->where->addPredicate($predicate2);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        $resul = $statement->execute();
        return $resul->current();

    }

    public function listaestudactuliza($fec1, $fec2, $prog, $fmactua){
        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'idest'=>'id', 'nombres', 'apellidos', 'nro_ident', 'sexo', 'fec_nacimiento'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaDatosactualizados),
            'tabla1.id = tabla2.id_estudiante',
            ['id','fec_actualizadatos','direc_residen','pais_res','ciud_res','celular','email','tel_fijo',
             'nom_empresa','dir_empresa','tel_empresa','cargo_empresa','pais_lab','ciud_lab','expec_formacion',
             'trabajo_act', 'trabsectprofe', 'areaespecif', 'poblabenef','otrocualdpte', 'ncapac_diploma',
             'ncapac_espec', 'ncapac_mestria', 'ncapac_docto', 'ncapac_tema',
             'id_progpre','id_progpos','prog_debe_mejorar','prog_fortaleza', 'sede'],
            'inner'
        )
        ->join(
             array('tabla3' => $this->tablaRespuestaActual),
            'tabla2.id_prog_mejorar = tabla3.id',
             ['resp_mejorar' => utf8_decode('respuesta')],
            'left'
        )
        ->join(
             array('tabla4' => $this->tablaRespuestaActual),
             'tabla2.id_prog_fortaleza = tabla4.id',
             ['resp_fortaleza' => utf8_decode('respuesta')],
             'left'
        )
        ->join(
             array('tabla5' => $this->tablaEstudiosRealiza),
             'tabla2.id = tabla5.id_datosactualiza',
             ['id_titulo_obtenido'],
             'left'
        );
        if ($prog<>0) {
            if ($fmactua<>"TOD") {
                $select->where(['tabla2.estado' => 'V',
                    //'tabla2.id_progpre' => $prog,
                    'tabla5.id_titulo_obtenido' => $prog,
                    'tabla5.id_universidad' => 7,
                    'tabla2.forma_actualiza' => $fmactua]);
            }else{
                $select->where(['tabla2.estado' => 'V',
                    'tabla5.id_universidad' => 7,
                    'tabla5.id_titulo_obtenido' => $prog]);
            }
        }else{
            if ($fmactua<>"TOD") {
                $select->where(['tabla2.estado' => 'V',
                    'tabla2.forma_actualiza' => $fmactua]);
            }else{
                $select->where(['tabla2.estado' => 'V']);
            }
        }

        $predicate2 = new \Zend\Db\Sql\Predicate\Between('tabla2.fec_actualizadatos', $fec1, $fec2);

        $select->where->addPredicate($predicate2);

        $select->order(['tabla2.fec_actualizadatos DESC']);

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function getSede($idsede)
    {
        $campos_sede =[
            'id', 'nombresede' => 'nombre'
        ];

        $where = [
            'id' =>$idsede,
        ];

        return DB::selectRegistroCampos( $this->tablaSedes, $campos_sede, $where, []);
    }
    public function listaestudactulizanter(){
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'idest'=>'id', 'nombres', 'apellidos', 'nro_ident', 'sexo', 'fec_nacimiento'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaDatosactualizados),
            'tabla1.id = tabla2.id_estudiante',
            ['id','fec_actualizadatos','direc_residen','pais_res','ciud_res','celular','email','tel_fijo',
                'nom_empresa','dir_empresa','tel_empresa','cargo_empresa','pais_lab','ciud_lab','expec_formacion',
                'trabajo_act', 'trabsectprofe', 'areaespecif', 'poblabenef','otrocualdpte',
                'id_progpre','id_progpos','prog_debe_mejorar','prog_fortaleza'],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaRespuestaActual),
                'tabla2.id_prog_mejorar = tabla3.id',
                ['resp_mejorar' => utf8_decode('respuesta')],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaRespuestaActual),
                'tabla2.id_prog_fortaleza = tabla4.id',
                ['resp_fortaleza' => utf8_decode('respuesta')],
                'left'
            );

        $select->where(['tabla2.estado'=>'A']);

        $select->order(['tabla2.fec_actualizadatos DESC']);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function getSelecEstudActualiza( $idtabla )
    {
        $where = [
            'id' =>$idtabla,
        ];
        return DB::selectRegistro( $this->tablaDatosactualizados, $where);
    }

    /*Busqueda desde la vista estudiante*/
    public function SelecEstudActualiza( $idestud )
    {
        $where = [
            'id_estudiante' =>$idestud,
            'estado' => 'V'
        ];
        return DB::selectRegistro( $this->tablaDatosactualizados, $where);
    }
    /***********************************************************************/
    public function  Nroestudinscritos($idevenvig){
        $campos_nroinscritos = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'id_evento' => $idevenvig,
        ];

        return DB::selectRegistroCampos($this->tablaInscripeventos, $campos_nroinscritos, $where);
    }

    public function Listaestudinscritos($idevenvig){
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaInscripeventos));
        $select->columns([
            'id','id_evento','id_estudent','fecha'
        ]);
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'nro_ident'],
            'left'
        )
        ->join(
            array('tabla3' => $this->tablaEventos),
            'tabla1.id_evento = tabla3.id',
            [utf8_decode('nombre')],
            'left'
        )
         ->join(
            array('tabla4' => $this->tablaDatosactualizados),
            'tabla2.id = tabla4.id_estudiante',
            ['celular','email', 'trabajo_act', utf8_decode('nom_empresa'),'dir_empresa','tel_empresa','cargo_empresa',
                'trabsectprofe', utf8_decode('areaespecif'), utf8_decode('poblabenef'),utf8_decode('otrocualdpte'),
                'id_progpre', 'id_progpos', 'ncapac_diploma', 'ncapac_espec', 'ncapac_mestria', 'ncapac_docto',
                utf8_decode('ncapac_tema')],
            'left'
        )
        ->join(
            array('tabla5' => $this->tablaAutoriza),
            'tabla2.id = tabla5.id_estudiante',
            ['id_programa'],
            'left'
        );

        $select->where([
            'tabla3.id' => $idevenvig,
            'tabla3.estado' => 'VIG',
            'tabla4.estado' => 'V',
            //'tabla4.forma_actualiza' => 'INS'
        ]);

        $select->order(['tabla2.nombres', 'tabla2.apellidos']);

        /*$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        $select->where->addPredicate($predicate);*/

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listainscritoseventante($idevenant)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaInscripeventos));
        $select->columns([
            'id','id_evento','id_estudent','fecha'
        ]);
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'nro_ident'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEventos),
                'tabla1.id_evento = tabla3.id',
                ['nombre', 'fecha_evento'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosactualizados),
                'tabla2.id = tabla4.id_estudiante',
                ['celular','email', 'trabajo_act', utf8_decode('nom_empresa'),'dir_empresa','tel_empresa','cargo_empresa',
                    'trabsectprofe', utf8_decode('areaespecif'), utf8_decode('poblabenef'),utf8_decode('otrocualdpte'),
                    'id_progpre', 'id_progpos', 'ncapac_diploma', 'ncapac_espec', 'ncapac_mestria', 'ncapac_docto',
                    utf8_decode('ncapac_tema')],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaAutoriza),
                'tabla2.id = tabla5.id_estudiante',
                ['id_programa'],
                'left'
            );

        $select->where([
            'tabla3.id' => $idevenant,
            'tabla3.estado' => 'INA',
            'tabla4.estado' => 'V',
            //'tabla4.forma_actualiza' => 'INS'
        ]);

        //$predicate =  new \Zend\Db\Sql\Predicate\Like('tabla2.nombres', '%'.$nomb.'%');
        //$select->where->addPredicate($predicate);

        $select->order(['tabla2.nombres', 'tabla2.apellidos']);

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaestudinscriident($ident)
    {

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaInscripeventos));
        $select->columns([
            'id', 'id_estudent', 'fecha'
        ]);
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'nro_ident'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEventos),
                'tabla1.id_evento = tabla3.id',
                ['nombre'],
                'left'
            );

        $select->where([
            'tabla3.estado' => 'VIG',
            'tabla2.nro_ident' => $ident,
        ]);

        $select->order(['tabla2.nombres', 'tabla2.apellidos']);

        /*$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        $select->where->addPredicate($predicate);*/

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listainscritosnomb($nomb)
    {

        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaInscripeventos));
        $select->columns([
            'id', 'id_estudent', 'fecha'
        ]);
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudent = tabla2.id',
            ['id', 'nombres', 'apellidos', 'nro_ident'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEventos),
                'tabla1.id_evento = tabla3.id',
                ['nombre'],
                'left'
            );

        $select->where([
            'tabla3.estado' => 'VIG',
        ]);

        $predicate =  new \Zend\Db\Sql\Predicate\Like('tabla2.nombres', '%'.$nomb.'%');
        $select->where->addPredicate($predicate);

        $select->order(['tabla2.nombres', 'tabla2.apellidos']);

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listaeventos()
    {
        /*$campos_evento = [
            'id', 'fecha_evento', 'nombre', 'fecha_finaliza', 'estado'
        ];

        $campo_order = [
            'fecha_evento'
        ];

        $select->where([
            'tabla3.estado' => 'VIG',
        ]);

        return DB::selectTablaCamposOrder($this->tablaEventos, $campos_evento, [], $campo_order);*/
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaTipoEvento));
        $select->columns([
            'idtipo'=>'id', 'tipo'=>'tipoevento',
        ]);
        $select->join(
            array('tabla2' => $this->tablaEventos),
            'tabla1.id = tabla2.tipo_de_evento',
            ['id', 'fec_ini_inscrip', 'fec_fin_inscrip', 'fecha_evento', 'nombre', 'fecha_finaliza', 'estado'],
            ''
        );

        //$predicate =  new \Zend\Db\Sql\Predicate\Like('tabla2.nombres', '%'.$nomb.'%');
        //$select->where->addPredicate($predicate);

        $select->order(['tabla2.fecha_evento']);

        //echo $select->getSqlString();

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Listadeeventosnovigente()
    {
        $campos_evento = [
            'id', 'fecha_evento', utf8_decode('nombre'), 'estado'
        ];

        $where = [
            'estado'=>'INA',
        ];

        $campo_order = [
            'fecha_evento'
        ];

        return DB::selectTablaCamposOrder($this->tablaEventos, $campos_evento, $where, $campo_order);
    }

    public function listaEventosVigentes()
    {
        $campos_evento = [
            'id', 'fecha_evento', utf8_decode('nombre'), 'estado'
        ];

        $where = [
            'estado'=>'VIG',
        ];

        $campo_order = [
            'fecha_evento'
        ];

        return DB::selectTablaCamposOrder($this->tablaEventos, $campos_evento, $where, $campo_order);
    }

    public function actualizaestadoevento()
    {
        $set=[
            'estado'=>'INA',
        ];

        $where=[
            'estado'=>'VIG',
        ];

        return DB::actualizar($this->tablaEventos, $set, $where);
    }

    public function getExisteRespuesta($resp)
    {
        $where = [
            'respuesta'=>$resp,
        ];

        return DB::selectRegistro( $this->tablaRespuestaActual, $where);
    }

    public function insertarRespuesta($datos,$username)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaRespuestaActual,[
                'user_crea' => $username,
                'respuesta' => strtoupper(utf8_encode($datos['respuesta'])),
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function ListadeRespuestas()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaRespuestaActual));
        $select->columns(
            [
                'id_respuesta'      => 'id',
                'descrip_respuesta' => utf8_decode('respuesta'),
            ]
        );
        /*$select->where([
            'tabla1.activo' => 1,
        ]);*/
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function ListadeRespuestas2($idescogido)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaRespuestaActual));
        $select->columns(
            [
                'id_respuesta'      => 'id',
                'descrip_respuesta' => utf8_decode('respuesta'),
            ]
        );
        $select->where->notEqualTo('tabla1.id',$idescogido);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function RespuestasFortaleza($idresp)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaRespuestaActual));
        $select->columns(
            [
                'id_respuesta'      => 'id',
                'descrip_respuesta' => utf8_decode('respuesta'),
            ]
        );
        $select->where([
            'tabla1.id' => $idresp,
        ]);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        //$res = $statement->execute();
        return $statement->execute();
    }

    public function SelectRespuesta($idresp)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaRespuestaActual));
        $select->columns(
            [
                'id_respuesta'      => 'id',
                'descrip_respuesta' => utf8_decode('respuesta'),
            ]
        );
        $select->where([
            'tabla1.id' => $idresp,
        ]);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function actualizaRespuesta($resp, $idresp, $user)
    {
        $set = [
            'respuesta'=>strtoupper(utf8_encode($resp)),
            'user_modi'=>$user,
        ];

        $where = [
            'id'=>$idresp,
        ];

        return DB::actualizar( $this->tablaRespuestaActual, $set, $where);
    }

    public function getExisteTipoEvento($tipo)
    {
        $where = [
            'tipoevento' => $tipo,
        ];

        return DB::selectRegistro( $this->tablaTipoEvento, $where);
    }

    public function insertarTipoEvento($datos,$username)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaTipoEvento,[
                'tipoevento' => strtoupper(utf8_encode($datos['tipoevento'])),
                'user_crea' => $username,
                'user_modi' => $username,
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function getExistePrograma($prog, $tipo)
    {
        $where = [
            'nombre'=>$prog,
            'id_estudio'=>$tipo,
        ];

        return DB::selectRegistro( $this->tablaProgramas, $where);
    }

    public function insertarProgramas($datos,$username)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaProgramas,[
                'user_crea' => $username,
                'nombre' => strtoupper(utf8_encode($datos['nombre'])),
                'id_estudio' =>$datos['tipoprog'],
                'ofertado' =>$datos['ofertado'],
                'codigo' => 0,
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function ListadeProgramas()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaProgramas));
        $select->columns(
            [
                'id_progra'    => 'id',
                'nom_programa' => utf8_decode('nombre'),
                'tipo_progra'  => 'id_estudio',
                'ofertado'     => 'ofertado'
            ]
        );
        /*$select->where([
            'tabla1.activo' => 1,
        ]);*/
        $select->order('tabla1.id_estudio');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function ListaTiposEventos()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaTipoEvento));
        $select->columns(
            [
                'id_tipo'    => 'id',
                'tipoevento' => 'tipoevento',

            ]
        );

        $select->order('tabla1.tipoevento');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function SelectTipoevento($idtipo)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaTipoEvento));
        $select->columns(
            [
                'id_tipo'    => 'id',
                'tipoevento' => 'tipoevento',
            ]
        );
        $select->where([
            'tabla1.id' => $idtipo,
        ]);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function actualizaTipoeventos($datos, $idtipo, $user)
    {
        $set = [
            'tipoevento'    =>strtoupper(utf8_encode($datos['tipoevento'])),
            'user_modi' =>$user,
        ];

        $where = [
            'id'=>$idtipo,
        ];

        return DB::actualizar( $this->tablaTipoEvento, $set, $where);
    }

    public function SelectPrograma($idprog)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaProgramas));
        $select->columns(
            [
                'id_progra'    => 'id',
                'nom_programa' => utf8_decode('nombre'),
                'tipo_progra'  => 'id_estudio',
                'ofertado'     => 'ofertado'
            ]
        );
        $select->where([
            'tabla1.id' => $idprog,
        ]);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function actualizaPrograma($datos, $idprog, $user)
    {
        $set = [
            'nombre'    =>strtoupper(utf8_encode($datos['nombre'])),
            'id_estudio'=>$datos['tipoprog'],
            'ofertado'  =>$datos['ofertado'],
            'user_modi' =>$user,
        ];

        $where = [
            'id'=>$idprog,
        ];

        return DB::actualizar( $this->tablaProgramas, $set, $where);
    }

    public function ListadeUniversidades()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaUniversidades));
        $select->columns(
            [
                'id_univer'    => 'id',
                'nom_univer' => 'nombre',
                'cod_univer'  => 'codigo',
                'sede_ppal'     => 'ciudad'
            ]
        );
        /*$select->where([
            'tabla1.activo' => 1,
        ]);*/
        $select->order('tabla1.nombre');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function SelectUniversidad($iduniv)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaUniversidades));
        $select->columns(
            [
                'id_univer'  => 'id',
                'nom_univer' => utf8_decode('nombre'),
                'cod_univer' => 'codigo',
                'sede_ppal'  => 'ciudad'
            ]
        );
        $select->where([
            'tabla1.id' => $iduniv,
        ]);
        //$select->order('tabla1.nombre');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function actualizaUniversidad($datos, $iduniv, $user)
    {
        $set = [
            'codigo'    =>$datos['codigo'],
            'nombre'    =>strtoupper(utf8_encode($datos['nombre'])),
            'ciudad'    =>strtoupper(utf8_encode($datos['ciudad'])),
            'user_modi' =>$user,
        ];

        $where = [
            'id'=>$iduniv,
        ];

        return DB::actualizar( $this->tablaUniversidades, $set, $where);
    }

    public function listaegresadoscumple($fec1,$fec2)
    {
        $fec1= new \Zend\Db\Sql\Expression("EXTRACT(MONTH FROM '".$fec1."')");
        $fec2= new \Zend\Db\Sql\Expression("EXTRACT(MONTH FROM '".$fec2."')");
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'idest'=>'id', utf8_decode('nombres'), utf8_decode('apellidos'), 'nro_ident', 'fec_nacimiento'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutoriza),
            'tabla1.id = tabla2.id_estudiante',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaActas),
                'tabla2.id = tabla3.id_autorizado',
                [],
                'left'
            );
        $mes = new \Zend\Db\Sql\Expression("EXTRACT(MONTH FROM tabla1.fec_nacimiento)");
        $predicate = new \Zend\Db\Sql\Predicate\Between($mes, $fec1, $fec2);

        $select->where->addPredicate($predicate);

        $select->order(new \Zend\Db\Sql\Expression("EXTRACT(DAY FROM tabla1.fec_nacimiento)"));


        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }
}