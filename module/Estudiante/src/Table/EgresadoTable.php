<?php

namespace Estudiante\Table;

use Comun\DB;

use Zend\db\TableGateway\TableGateway;

class EgresadoTable
{
    private $sql;
    private $tablaEstudiante = 'testudiante';
    private $tablaAutorizados = 'tautorizados';
    private $tablaActas = 'tactas';
    private $tablaProgramas = 'tprogramas';

    /* validar si ya existe */
    public function Selecestudiante($nro_ident)
    {
        $campos = [
            'id', 'nro_ident', 'nombres', 'apellidos', 'nroconsultas'
        ];

        $where = [
            'nro_ident'=>$nro_ident,
        ];

        return DB::selectRegistroCampos( $this->tablaEstudiante, $campos, $where);
    }

    public function Selecidautoriza($idestud)
    {
        $campos = [
            'id', 'id_programa'
        ];

        $where = [
            'id_estudiante'=>$idestud,
        ];

        return DB::selectRegistroCampos( $this->tablaAutorizados, $campos, $where);
    }

    public function ProgramaEscojido($idprog)
    {
        $campos_programas = [
            'id','id_estudio','nomprog'=>'nombre'
        ];

        $where = [
            'id' =>$idprog,
        ];
        return DB::selectRegistroCampos( $this->tablaProgramas, $campos_programas, $where);
    }

    public function  verificaegresado($iduato)
    {
        $campos_actas = [
            'idacta'=>'id', 'id_autorizado', 'nro_actgral', 'nro_actind', 'nro_diploma', 'fec_actas', 'periodo_grado'
        ];

        $where = [
            'id_autorizado' =>$iduato,
        ];
        return DB::selectRegistroCampos( $this->tablaActas, $campos_actas, $where);
    }

    public function Selecegresado($idestudiante)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaAutorizados));
        $select->columns([

        ]);
        $select->join(
            array('tabla2' => $this->tablaEstudiante),
            'tabla1.id_estudiante = tabla2.id',
            ['idest'=>'id', 'nombres', 'apellidos', 'nro_ident']
        );
        $select->join(
            array('tabla3' => $this->tablaProgramas),
            'tabla3.id = tabla1.id_programa',
            ['nomprog'=>'nombre']
        );
        $select->join(
            array('tabla4' => $this->tablaActas),
            'tabla4.id_autorizado = tabla1.id',
            [],
            'right'
        );

        $select->where(['tabla1.id_estudiante'=>$idestudiante]);

        //$select->group(['tabla2.nombre']);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        //$resul = $statement->execute();
        return $statement->execute();
    }

    public function actualizanrocosnultasestudiante($idest, $consultas)
    {
        $set=[
            'nroconsultas' => $consultas+1,

        ];

        $where=[
            'id' => $idest,
        ];

        return DB::actualizar($this->tablaEstudiante, $set, $where);
    }

}