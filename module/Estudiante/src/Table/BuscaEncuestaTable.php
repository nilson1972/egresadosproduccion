<?php

namespace Estudiante\Table;


use Comun\DB;

class buscaencuestaTable
{


    private $tablaBuscaEncuesta = 'tencuestapre';
    private $tablaEncuestapost = 'tencuestapost';
    private $tablaEstudiante = 'testudiante';
    private $tablaProgramas = 'tprogramas';
    private $tablaAutorizados = 'tautorizados';
    private $tablaEstudios = 'testudios';
    private $tablaCorte = 'tcorte';


    public function getExisteEstudianteEncuesta( $idest, $idprog )
    {
        $where = [
            'id_estudent' =>$idest,
            'ha_progprecursa' =>$idprog,
         /*   'id_corte' =>$idcorte, */
        ];
        return DB::selectRegistro( $this->tablaBuscaEncuesta, $where);
    }

    public function getConsultaExisteEstudianteEncuestaid($idest)
    {
        $where = [
            'id_estudent' =>$idest,
        ];
        return DB::selectRegistro( $this->tablaBuscaEncuesta, $where);
    }

    public function getConsultaExisteEstudianteEncuesta( $idauto )
    {
        $where = [
            'id_autorizado' =>$idauto,
        ];
        return DB::selectRegistro( $this->tablaBuscaEncuesta, $where);
    }

    public function Buscarencuesta($idest)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'id', 'nombres', 'apellidos'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaBuscaEncuesta),
            'tabla1.id = tabla2.id_estudent',
            ['id_autorizado','dp_identestud', 'fec_encuesta'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla2.ha_progprecursa = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaCorte),
                'tabla2.id_corte = tabla4.id' ,
                ['nombrecorte' => 'nombre'],
                'left'
            );

        $select->where([
            'tabla2.id_estudent' => $idest,
        ]);

        /*  $predicate =  'false';
          $select->where->addPredicate($predicate); */

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();

    }

    public function getConsultaExisteEstudianteEncuestapostid($idest)
    {
        $where = [
            'id_estudent' =>$idest,
        ];
        return DB::selectRegistro( $this->tablaEncuestapost, $where);
    }

    public function getConsultaExisteEstudianteEncuestapost( $idauto )
    {
        $where = [
            'id_autorizado' =>$idauto,
        ];
        return DB::selectRegistro( $this->tablaEncuestapost, $where);
    }

    public function Buscarencuestapost($idest)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'id', 'nombres', 'apellidos'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuestapost),
            'tabla1.id = tabla2.id_estudent',
            ['id_autorizado','dp_identestud', 'fec_encuesta'],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla2.ha_progpostgraduado = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaCorte),
                'tabla2.id_corte = tabla4.id' ,
                ['nombrecorte' => 'nombre'],
                'left'
            );

        $select->where([
            'tabla2.id_estudent' => $idest,
        ]);

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Buscarestudiantexnom($nom)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'id', 'nombres', 'apellidos'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaBuscaEncuesta),
            'tabla1.id = tabla2.id_estudent',
            ['id_autorizado','dp_identestud', 'fec_encuesta'],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla2.ha_progprecursa = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaCorte),
                'tabla2.id_corte = tabla4.id' ,
                ['nombrecorte' => 'nombre'],
                'left'
            );

        $select->where->Like('tabla1.nombres', '%'.$nom.'%');

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Buscarestudiantexidentifpre($identif)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaBuscaEncuesta));
        $select->columns([
            'id', 'dp_identestud', 'dp_lugarexped', 'id_autorizado', 'ha_npsaberpro', 'dp_fecnac', 'dp_mpionac', 'dp_deptonac'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.id_autorizado = tabla2.id',
            [],
            'left'
        )
            ->join(
            array('tabla3' => $this->tablaEstudiante),
            'tabla2.id_estudiante = tabla3.id',
            ['idest'=>'id', 'nombres', 'apellidos', 'sexo'],
            'left'
        );

        $select->where([
            'dp_identestud' => $identif,
        ]);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        $res = $statement->execute();
        return $res->current();
    }

    public function Buscarestudiantexnompos($nom)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns([
            'id', 'nombres', 'apellidos'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuestapost),
            'tabla1.id = tabla2.id_estudent',
            ['id_autorizado','dp_identestud', 'fec_encuesta'],
            'inner'
        )
            ->join(
                array('tabla3' => $this->tablaProgramas),
                'tabla2.ha_progpostgraduado = tabla3.id',
                ['nombre'],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaCorte),
                'tabla2.id_corte = tabla4.id' ,
                ['nombrecorte' => 'nombre'],
                'left'
            );

        $select->where->Like('tabla1.nombres', '%'.$nom.'%');

        $statement = $sql->prepareStatementForSqlObject($select);

        return $statement->execute();
    }

    public function Buscarestudiantexidentifpos($identif)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEncuestapost));
        $select->columns([
            'id', 'dp_identestud', 'id_autorizado', 'ha_npsaberpro', 'dp_fecnac', 'dp_mpionac', 'dp_deptonac'
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.id_autorizado = tabla2.id',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiante),
                'tabla2.id_estudiante = tabla3.id',
                ['idest'=>'id', 'nombres', 'apellidos', 'sexo'],
                'left'
            );

        $select->where([
            'dp_identestud' => $identif,
        ]);

        $statement = $sql->prepareStatementForSqlObject($select);
        //echo $select->getSqlString();
        $res = $statement->execute();
        return $res->current();
    }
/***revisando*****/
    public function Buscadatosparaactas($idenc)
    {
        $campos_encuesta = [
            'id', 'dp_identestud', 'id_autorizado', 'dp_emailestud', 'dp_celestud', 'ha_npsaberpro',
            'fec_encuesta','ha_progprecursa', 'id_corte'
        ];

        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.id' => $idenc,
        ];

        return DB::selectJoinTresTablasRegistro($this->tablaEstudiante, $campos_estudiante,
            $this->tablaBuscaEncuesta, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progprecursa', 'id',
            $where, []);
    }

    public function Buscadatosparaactaspos($idenc)
    {
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id', 'dp_identestud', 'id_autorizado', 'dp_emailestud', 'dp_celestud', 'ha_npsaberpro',
            'fec_encuesta','ha_progpostgraduado', 'id_corte'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.id' => $idenc,
        ];

        return DB::selectJoinTresTablasRegistro($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuestapost, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progpostgraduado', 'id',
            $where, []);
    }

    /***para buscar datos del acta desde verestudiante*****/
    public function Selecdatosparaactas($idaut)
    {
        $campos_encuesta = [
            'id', 'dp_identestud', 'id_autorizado', 'dp_emailestud', 'dp_celestud', 'ha_npsaberpro',
            'fec_encuesta','ha_progprecursa', 'id_corte'
        ];

        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.id_autorizado' => $idaut,
        ];

        return DB::selectJoinTresTablasRegistro($this->tablaEstudiante, $campos_estudiante,
            $this->tablaBuscaEncuesta, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progprecursa', 'id',
            $where, []);
    }

    public function Selecdatosparaactaspos($idaut)
    {
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id', 'dp_identestud', 'id_autorizado', 'dp_emailestud', 'dp_celestud', 'ha_npsaberpro',
            'fec_encuesta','ha_progpostgraduado'
        ];

        $campos_programas = [
            'nombre'
        ];

        $where = [
            'tabla2.id_autorizado' => $idaut,
        ];

        return DB::selectJoinTresTablasRegistro($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuestapost, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progpostgraduado', 'id',
            $where, []);
    }

    public function SiEncuesta($idauto,$tipoestudio)
    {
        if ($tipoestudio==1){
            $where = [
                'id_autorizado' =>$idauto,
            ];
            return DB::selectRegistro( $this->tablaBuscaEncuesta, $where);
        }else{
            $where = [
                'id_autorizado' =>$idauto,
            ];
            return DB::selectRegistro( $this->tablaEncuestapost, $where);
        }
    }

}