<?php

namespace Estudiante\Table;


use Comun\DB;

class buscaencuestapostTable
{


    private $tablaBuscaEncuestaPost = 'tencuestapost';


    public function getExisteEstudianteEncuesta( $idest, $idpro )
    {
        return DB::selectRegistro( $this->tablaBuscaEncuestaPost, [
            'id_estudent' => $idest,
            'ha_progpostgraduado' => $idpro
        ]);
    }

}