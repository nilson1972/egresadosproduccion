<?php

namespace Autenticacion;

use Zend\Router\Http\Segment;
use Zend\Router\Http\Literal;
use Zend\ServiceManager\Factory\InvokableFactory;

return [

    'controllers' => [
        'factories' => [
            Controller\AutenticacionController::class  => InvokableFactory::class,
        ],
    ],



 
    'router' => [
        'routes' => [

            'autenticacion' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/autenticacion[/:action]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\AutenticacionController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'logout' => [
                'type'    => Literal::class,
                'options' => [
                    'route' => '/autenticacion/logout',
                    'defaults' => [
                        'controller' => Controller\AutenticacionController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],


        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            'autenticacion' => __DIR__ . '/../view',
        ],
    ],
];