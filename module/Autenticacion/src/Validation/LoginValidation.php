<?php

namespace Autenticacion\Validation;



use Zend\InputFilter\InputFilter;
use Zend\Validator\StringLength;
use Zend\I18n\Validator\Alnum;

class LoginValidation extends InputFilter
{

  private $mensaje_error;


  /*

     Validaciones
     -----------------------------
     1. Vacio
     2. Lestras y numeros
     3. Longitud
     4. StringTag
     5. HTML Entityes

  */


  public function __construct() 
  {
      $this->add([
            'name'      => 'username',
            'required'  => true,
            'validators' => [
                    [
                          'name' => 'NotEmpty',
                          'options' => [
                                'messages' => [
                                    'isEmpty' => 'Nombre de usuario vacío',
                                ]
                          ],
                    ],
                    [
                          'name' => 'alnum',
                          'options' => [
                                'allowWhiteSpace' => false,
                                'messages' => [
                                    'alnumInvalid'  => 'Solo se permiten letras o numeros en Nombre de usuario',
                                    'notAlnum'      => 'Nombre de usuario presenta caracteres que no son alfanuméricos'
                                ]
                          ],
                    ],
                    [
                          'name' => 'StringLength',
                          'options' => [
                                'min' => 1,
                                'max' => 15,
                                'messages' => [
                                    'stringLengthTooLong'  => "Nombre de usuario supera los '%max%' caracteres"
                                ]
                          ],
                    ],
            ],
            'filters' => [
                    [
                        'name' => 'htmlentities',
                        'options' => [
                            'quotestyle'  => ENT_QUOTES,
                        ],
                    ],
                    [
                        'name' => 'striptags'
                    ],
                    [
                        'name' => 'StringTrim'
                    ],
                    [
                        'name' => 'StringToLower'
                    ],
            ],
      ]);

      $this->add([
            'name'       => 'password',
            'required'   => true,
            'validators' => [
                    [
                        'name' => 'NotEmpty',
                        'options' => [
                            'messages' => [
                                'isEmpty' => 'Nombre de usuario vacío',
                            ]
                        ],
                    ],
                    [
                        'name' => 'StringLength',
                        'options' => [
                            'min' => 1,
                            'max' => 15,
                            'messages' => [
                                'stringLengthTooLong'  => "Password supera los '%max%' caracteres"
                            ]
                        ],
                    ],
            ],            
            'filters' => [
                    [
                        'name' => 'htmlentities',
                        'options' => [
                            'quotestyle'  => ENT_QUOTES,
                        ],
                    ],
                    [
                        'name' => 'striptags'
                    ],
                    [
                        'name' => 'StringTrim'
                    ],
                    [
                        'name' => 'StringToLower'
                    ],
            ],
      ]);

      
      $this->setData($_POST);

   }


   public function validar()
   {
      if (!$this->isValid()) {            
            foreach ($this->getInvalidInput() as $error) 
            {
               $arr = $error->getMessages();
               foreach ($error->getMessages() as $valor) 
               {
                  $this->mensaje_error.= $valor."\n";
               }
            }
            return false;
        }
        return true;
   }


   public function getMensajes()
    {
      return $this->mensaje_error;
    }



    public function validar_username($username)
    {
          // Validar tamaño

          $this->validator = new StringLength(array('min' => 1, 'max' => 10));
          $this->validator->setMessages( 
               array(
                    \Zend\Validator\StringLength::TOO_SHORT => 'Error en nombre de usuario.',
                    \Zend\Validator\StringLength::TOO_LONG  => 'Error en nombre de usuario..'
               )
          );
          if (!$this->validator->isValid($username)) {
               return false;
          }

          // Validar alfanumerico

          $this->validator = new Alnum();
          if (!$this->validator->isValid($username)) {
               return false;
          }

          return true;
    }


    public function validar_password($password)
    {
          $this->validator = new StringLength(array('min' => 1, 'max' => 8));
          $this->validator->setMessages( 
               array(
                    \Zend\Validator\StringLength::TOO_SHORT => 'Usuario o Contraseña invalida.',
                    \Zend\Validator\StringLength::TOO_LONG  => 'Usuario o Contraseña invalida..'
               )
          );
          return $this->validator->isValid($password);
    }
    

    public function getMessage()
    {
          foreach ($this->validator->getMessages() as $messageId => $message) {
             $mensaje.=$message." ";
          }
          return $mensaje;          
    }

    /*

    public function __construct() {
        $this->add(array(
            'name' => 'username',
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'min' => 1,
                        'max' => 8,
                    ),
                ),
            ),            
        ));
    }

    */

}