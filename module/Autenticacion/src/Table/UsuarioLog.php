<?php


/*

CREATE TABLE public.usuario_log_ingreso
(
   id serial NOT NULL, 
   id_usuario integer NOT NULL, 
   fecha_hora timestamp without time zone NOT NULL, 
   ip character varying(30) NOT NULL, 
   CONSTRAINT pk_usuario_log_ingreso_id PRIMARY KEY (id), 
   CONSTRAINT fk_usuario_log_ingreso_if_usuario FOREIGN KEY (id_usuario) REFERENCES public.usuario (id) ON UPDATE RESTRICT ON DELETE RESTRICT
) 
WITH (
  OIDS = FALSE
)
;



ALTER TABLE public.usuario_log_ingreso ADD COLUMN user_agent character varying(255);
ALTER TABLE public.usuario_log_ingreso ALTER COLUMN user_agent SET NOT NULL;

ALTER TABLE public.usuario_log_ingreso ADD COLUMN platform character varying(255) NOT NULL;



*/

namespace Autenticacion\Table;

use Comun\DB;

class UsuarioLog
{

	private $tablaUsuarioLogIngreso = 'usuario_log_ingreso';

	public function setLog($request, $idUsuario)
	{
		// $method = $request->getMethod();
		$remote = new \Zend\Http\PhpEnvironment\RemoteAddress;
		// $remoteAddr = $request->getServer('REMOTE_ADDR');

		$navegador = get_browser(null, true);

		$data = [
			'id_usuario' => $idUsuario,
			'fecha_hora' => date('Y-m-d G:i:s'),
			'ip' 		 => $remote->getIpAddress(),
			'user_agent' => $request->getServer('HTTP_USER_AGENT'),
			'platform'   => $navegador['platform'],
			'browser'    => $navegador['browser'],
		];
		
		DB::insertar($this->tablaUsuarioLogIngreso, $data);

	}
}
