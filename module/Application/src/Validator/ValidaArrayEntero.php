<?php
namespace Application\Validator;

use Zend\Validator\AbstractValidator;

class ValidaArrayEntero extends AbstractValidator
{
    const NOT_ARRAY = 'NOT_ARRAY';
    const VAL_NOT_NUMERIC = 'VAL_NOT_NUMERIC';

    protected $messageTemplates = [
        self::NOT_ARRAY => "No representa un array",
        self::VAL_NOT_NUMERIC => "Uno de los valores en el array no es numérico",
    ];


    public function isValid($value)
    {
        $this->setValue($value);

        if (!is_array($value)) {
            $this->error(self::NOT_ARRAY);
            return false;
        }

        $validator = new \Zend\Validator\Digits();
        foreach ($value as $key ) {
            if (!$validator->isValid($key)) {
                $this->error(self::VAL_NOT_NUMERIC);
                return false;
            }
        }

        return true;
    }
}