<?php

namespace Reportes;


use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'controllers' => [
        'factories' => [
            Controller\ReportesController::class => InvokableFactory::class,
            Controller\AnterioresController::class => InvokableFactory::class,
        ],
    ],


    'router' => [
        'routes' => [

            'reportes' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/reportes[/:action[/:id1[/:id2[/:id3[/:id4[/:id5]]]]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[a-zA-Z0-9_\-]+',
                        'id2'     => '[a-zA-Z0-9_\-]+',
                        'id3'     => '[a-zA-Z0-9_\-]+',
                        'id4'     => '[a-zA-Z0-9_\-]+',
                        'id5'     => '[a-zA-Z0-9_\-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ReportesController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

            'anteriores' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/anteriores[/:action[/:id1[/:id2]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[0-9]+',
                        'id2'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\AnterioresController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],



    'view_manager' => [
        'template_path_stack' => [
            'reporte' => __DIR__ . '/../view',
        ],
    ],
];