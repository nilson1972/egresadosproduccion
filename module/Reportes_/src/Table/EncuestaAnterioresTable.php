<?php
namespace Reportes\Table;

use Comun\DB;


class EncuestaAnterioresTable
{

    private $tablaEstudiante = 'testudiante';
    private $tablaAutorizados = 'tautorizados';
    private $tablaProgramas = 'tprogramas';
    private $tablaEstudios = 'testudios';
    private $tablaCorte = 'tcorte';
    private $tablaEncuesta = 'tencuestapre';
    private $tablaEncuestapost = 'tencuestapost';


    /*Selecciona los periodos inactivos de la encuesta*/
    public function periodoInactivosEncuesta()
    {
        $campos_corte = [
            'id', 'nombre', 'estado'
        ];

        $order = [
            'nombre'
        ];

        /*$where = [
            'estado'=>'I',
        ];*/

        return DB::selectTablaCamposOrder($this->tablaCorte, $campos_corte, [], $order);

    }

    /*Selecciona un periodo inactivo de la encuesta*/
    public function getperiodoInactivoEncuesta($idcortinac)
    {
        $campos_corte = [
            'id', 'nombre', 'estado'
        ];

        $where = [
            'id' => $idcortinac,
            //'estado'=>'I',
        ];


        return DB::selectRegistroCampos($this->tablaCorte, $campos_corte, $where);

    }

    public function getAnterTotalAutoriza($idcortinac)
    {
        $campos_autorizados = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(tabla1.id)')
        ];
        $where = [
            'tabla2.id_estudio' => 1,
            'tabla1.id_corte' => $idcortinac,
        ];

        return DB::selectJoinTresTablasRegistro( $this->tablaAutorizados, $campos_autorizados,
            $this->tablaProgramas,  [],
            $this->tablaEstudios, [],
            'id_programa', 'id',
            'id_estudio', 'id',
            $where,[]);

    }

    public function getTotalEncAntRealiz($idcortinac)
    {
        $camposEncuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'id_corte' => $idcortinac,
        ];

        return DB::selectRegistroCampos($this->tablaEncuesta, $camposEncuesta, $where);
    }

    public function Selectencuestasporfechas($fec1, $fec2)
    {
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id_autorizado','dp_identestud', 'fec_encuesta'
        ];

        $campos_programas = [
            'nombre'
        ];

        $predicate = new \Zend\Db\Sql\Predicate\Between('tabla2.fec_encuesta', $fec1, $fec2);

        return DB::selectJoinTresTablas($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuesta, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progprecursa', 'id',
            [], [], false, $predicate);
    }

    //Postgrado
    public function getAnterTotalAutorizapos($idcortinac)
    {
        $campos_autorizados = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(tabla1.id)')
        ];
        $where = [
            'tabla2.id_estudio' => 2,
            'tabla1.id_corte' => $idcortinac,
        ];

        return DB::selectJoinTresTablasRegistro( $this->tablaAutorizados, $campos_autorizados,
            $this->tablaProgramas,  [],
            $this->tablaEstudios, [],
            'id_programa', 'id',
            'id_estudio', 'id',
            $where,[]);

    }

    public function getTotalEncAntRealizpos($idcortinac)
    {
        $camposEncuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'id_corte' => $idcortinac,
        ];

        return DB::selectRegistroCampos($this->tablaEncuestapost, $camposEncuesta, $where);
    }

    public function Selectencuestasporfechaspos($fec1, $fec2)
    {
        $campos_estudiante = [
            'id', 'nombres', 'apellidos'
        ];

        $campos_encuesta = [
            'id_autorizado','dp_identestud', 'fec_encuesta'
        ];

        $campos_programas = [
            'nombre'
        ];

        $predicate = new \Zend\Db\Sql\Predicate\Between('tabla2.fec_encuesta', $fec1, $fec2);

        return DB::selectJoinTresTablas($this->tablaEstudiante, $campos_estudiante,
            $this->tablaEncuestapost, $campos_encuesta,
            $this->tablaProgramas, $campos_programas,
            'id', 'id_estudent',
            'ha_progpostgraduado', 'id',
            [], [], false, $predicate);
    }

}