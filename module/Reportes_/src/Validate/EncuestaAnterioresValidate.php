<?php
/**
 * Created by PhpStorm.
 * User: GRED
 * Date: 03/08/2017
 * Time: 09:10 AM
 */

namespace Reportes\Validate;

use Comun\InputFilterGeneric3;

class EncuestaAnterioresValidate extends InputFilterGeneric3
{

    public function validarCorte()
    {
        $this->validarDigito([
            'name'  => 'corte',
            'label' => ' Periodo o Corte tiene un valor no valido para la seleccion ',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarFechas()
    {
        $this->validarFecha([
        'name'  => 'fecha1',
        'label' => 'fecha inicial contiene un valor no valido',
        'required' => false,
        ]);

        $this->validarFecha([
            'name'  => 'fecha2',
            'label' => 'fecha final contiene un valor no valido',
            'required' => false,
        ]);

        return $this->validar();
    }

}