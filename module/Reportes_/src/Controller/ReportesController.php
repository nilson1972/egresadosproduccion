<?php

namespace Reportes\Controller;


use Comun\GenericController;


class ReportesController extends GenericController
{


   public function __construct()
    {
        $this->tablaReportes = new \Reportes\Table\ReportesTable();
        $this->validateReportes = new \Reportes\Validate\ReportesValidate();
        $this->tablaEncuestaAnteriores = new \Reportes\Table\EncuestaAnterioresTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
    }

    public function indexAction()
    {

    }

    public function formEncuestasRealizadasAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'cortevig' => $corte,
            'siencuesta' => $this->tablaReportes->Selectencuesta($corte['id']),
        ]);
    }

    public function formEncuestasNoRealizadasAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'cortevig' => $corte,
            'noencuesta' => $this->tablaReportes->Selectnoencuesta($corte['id']),
        ]);
    }

    public function seleccionarProgramaAction()
    {
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }

    public function formEncuestasRealizadasPorProgAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'cortevig' => $corte,
            'siencuestaporprog' => $this->tablaReportes->Selectencuestaporprog($corte['id'],
                                                                               $this->validateReportes->getValue('programa')),
        ]);
    }

    public function formEncuestasRealizadasPostAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'cortevig' => $corte,
            'siencuestapost' => $this->tablaReportes->Selectencuestapost($corte['id']),
        ]);
    }

    public function formEncuestasNoRealizadasPostAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'cortevig' => $corte,
            'noencuestapost' => $this->tablaReportes->Selectnoencuestapost($corte['id']),
        ]);
    }

    public function seleccionarProgramaPostAction(){
        return $this->viewModelAjax([
            'prog' => $this->tablaProgramas->Selectprogramas((int)$this->getParam1()),
        ]);
    }

    public function formEncuestasRealizadasPorProgPostAction(){
        if ( ! $this->validateReportes->validarPrograma() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'cortevig' => $corte,
            'siencuestaporprogpost' => $this->tablaReportes->Selectencuestaporprogpost($corte['id'],
                                                                                       $this->validateReportes->getValue('programa')),
        ]);
    }

    public function listadeestudianteAction(){
        $nom_estud=$_POST['nombre_estud'];
        return $this->viewModelAjax([
            'lista' => $this->tablaEstudiante->listaestudiantes($nom_estud),
        ]);
    }

// selector de opciones otros
    public function seleccionarTipoestudioSexoAction()
    {
        $nromod = (int)$this->getParam1();
        return $this->viewModelAjax([
            'modulo' => $nromod,
        ]);
    }

// selector de opcion carnet
    public function seleccionarCarnetEstadoAction()
    {
        return $this->viewModelAjax([

        ]);
    }

// selector de opcion nro acta
    public function seleccionarNroactaEgresadosAction()
    {
        return $this->viewModelAjax([

        ]);
    }

//  seleccionar programas otros reporte
    public  function programasReportesAction()
    {
        $programas = $this->tablaProgramas->Selectprogramas((int)$this->getParam1());
        return $this->viewModelAjax([
            'programas' => $programas,
        ]);
    }

    public function reporEncuestasRealizadasSexoAction()
    {
    //  $progra= $_POST['programa'];
    //  para mostrar un arrays
    //  print_r($progra);
        if (!$this->validateReportes->validarSexo()) {
               return $this->verMensajeError($this->validateReportes->getMensajes());
        }

        $tpest = (int)$this->getParam1();
        if ($tpest==1){
            $encuestasexo = $this->tablaReportes->Listaencuestadosporsexopre($this->validateReportes->getValue('sexo'),
                $this->validateReportes->getValue('fecha1'),
                $this->validateReportes->getValue('fecha2'),
                $this->validateReportes->getValue('programa'));
        }else{
           if ($tpest==2){
               $encuestasexo = $this->tablaReportes->Listaencuestadosporsexopos($this->validateReportes->getValue('sexo'),
                   $this->validateReportes->getValue('fecha1'),
                   $this->validateReportes->getValue('fecha2'),
                   $this->validateReportes->getValue('programa'));
           }
        }
        return $this->viewModelAjax([
            'encuestaporsexo' => $encuestasexo,
            'sexo' => $this->validateReportes->getValue('sexo'),
            'estudio' => (int)$this->getParam1(),
            'fec1'=> $this->validateReportes->getValue('fecha1'),
            'fec2'=> $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporEncuestasTrabajanSexoAction()
    {
        if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $tpest = (int)$this->getParam1();
        if ($tpest==1){
            $enctrabajosexo = $this->tablaReportes->Listaencuestadosporsexotrabajopre($this->validateReportes->getValue('sexo'),
                $this->validateReportes->getValue('fecha1'),
                $this->validateReportes->getValue('fecha2'),
                $this->validateReportes->getValue('programa'));
        }else{
            if ($tpest==2){
                $enctrabajosexo = $this->tablaReportes->Listaencuestadosporsexotrabajopos($this->validateReportes->getValue('sexo'),
                    $this->validateReportes->getValue('fecha1'),
                    $this->validateReportes->getValue('fecha2'),
                    $this->validateReportes->getValue('programa'));
            }
        }
        return $this->viewModelAjax([
            'encuestatrabajosexo' => $enctrabajosexo,
            'sexo' => $this->validateReportes->getValue('sexo'),
            'estudio' => (int)$this->getParam1(),
            'fec1'=> $this->validateReportes->getValue('fecha1'),
            'fec2'=> $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporEncuestasNoTrabajanSexoAction()
    {
        if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $tpest = (int)$this->getParam1();
        if ($tpest==1){
            $encnotrabajosexo = $this->tablaReportes->Listaencuestaporsexonotrabajopre($this->validateReportes->getValue('sexo'),
                $this->validateReportes->getValue('fecha1'),
                $this->validateReportes->getValue('fecha2'),
                $this->validateReportes->getValue('programa'));
        }else{
            if ($tpest==2){
                $encnotrabajosexo = $this->tablaReportes->Listaencuestaporsexonotrabajopos($this->validateReportes->getValue('sexo'),
                    $this->validateReportes->getValue('fecha1'),
                    $this->validateReportes->getValue('fecha2'),
                    $this->validateReportes->getValue('programa'));
            }
        }
        return $this->viewModelAjax([
            'encnotrabajosexo' => $encnotrabajosexo,
            'sexo' => $this->validateReportes->getValue('sexo'),
            'estudio' => (int)$this->getParam1(),
            'fec1'=> $this->validateReportes->getValue('fecha1'),
            'fec2'=> $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporGraduadosSexoAction()
    {
        if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }
        $tpest = (int)$this->getParam1();
        //print_r($_POST["programa"]);exit();
        if ($tpest==1){
            $graduadossexo = $this->tablaReportes->Listagraduadosporsexopre($this->validateReportes->getValue('sexo'),
                $this->validateReportes->getValue('fecha1'),
                $this->validateReportes->getValue('fecha2'),
                $this->validateReportes->getValue('programa'));
        }else{
            if ($tpest==2){
                $graduadossexo = $this->tablaReportes->Listagraduadosporsexopos($this->validateReportes->getValue('sexo'),
                    $this->validateReportes->getValue('fecha1'),
                    $this->validateReportes->getValue('fecha2'),
                    $this->validateReportes->getValue('programa'));
            }
        }
        return $this->viewModelAjax([
            'graduadosporsexo' => $graduadossexo,
            'sexo' => $this->validateReportes->getValue('sexo'),
            'estudio' => (int)$this->getParam1(),
            'fec1'=> $this->validateReportes->getValue('fecha1'),
            'fec2'=> $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporNroactaEgresadosProgramaAction()
    {
        if ( ! $this->validateReportes->validarNroacta() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }

        $tpest = (int)$this->getParam1();
        //print_r($_POST["programa"]);exit();
        if ($tpest==1) {
            $graduadosacta = $this->tablaReportes->Listagraduadosactaporfecha($this->validateReportes->getValue('nroactagral'),
                $this->validateReportes->getValue('fecha1'),
                $this->validateReportes->getValue('fecha2'),
                $this->validateReportes->getValue('programa'));
        }else{
            if ($tpest==2){
                $graduadosacta = $this->tablaReportes->Listagraduadosactaporfechapos($this->validateReportes->getValue('nroactagral'),
                    $this->validateReportes->getValue('fecha1'),
                    $this->validateReportes->getValue('fecha2'),
                    $this->validateReportes->getValue('programa'));
            }
        }

        return $this->viewModelAjax([
            'graduadosporactagral' => $graduadosacta,
            'estudio' => (int)$this->getParam1(),
            'acta' => $this->validateReportes->getValue('nroactagral'),
            'fec1'=> $this->validateReportes->getValue('fecha1'),
            'fec2'=> $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporGraduadosTrabajanSexoAction()
    {
        if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }

        $graduadostrabsexo = $this->tablaReportes->ListagraduadosTrabajan($this->validateReportes->getValue('sexo'),
                                                                          (int)$this->getParam1(),
            $this->validateReportes->getValue('fecha1'),
            $this->validateReportes->getValue('fecha2'),
            $this->validateReportes->getValue('programa'));

        return $this->viewModelAjax([
            'graduadostrabporsexo' => $graduadostrabsexo,
            'sexo' => $this->validateReportes->getValue('sexo'),
            'estudio' => (int)$this->getParam1(),
            'fec1'=> $this->validateReportes->getValue('fecha1'),
            'fec2'=> $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporGraduadosNoTrabajanSexoAction()
    {
        if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }

        $graduadosnotrabsexo = $this->tablaReportes->ListagraduadosNoTrabajan($this->validateReportes->getValue('sexo'),
            (int)$this->getParam1(), $this->validateReportes->getValue('fecha1'),
            $this->validateReportes->getValue('fecha2'),
            $this->validateReportes->getValue('programa'));

        return $this->viewModelAjax([
            'graduadosnotrabporsexo' => $graduadosnotrabsexo,
            'sexo' => $this->validateReportes->getValue('sexo'),
            'estudio' => (int)$this->getParam1(),
            'fec1' => $this->validateReportes->getValue('fecha1'),
            'fec2' => $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }

    public function reporEstadoCarnetProgramaAction()
    {
        if ( ! $this->validateReportes->validarDatos() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }

        $estadocarnetestud = $this->tablaReportes->EstadoCarnetEstudProgramas($this->validateReportes->getValue('carnet'),
            (int)$this->getParam1(), $this->validateReportes->getValue('fecha1'),
            $this->validateReportes->getValue('fecha2'),
            $this->validateReportes->getValue('programa'));

        return $this->viewModelAjax([
            'estadocarnetestudprograma' => $estadocarnetestud,
            'carnet' => $this->validateReportes->getValue('carnet'),
            'estudio' => (int)$this->getParam1(),
            'fec1' => $this->validateReportes->getValue('fecha1'),
            'fec2' => $this->validateReportes->getValue('fecha2'),
            'prog'=> $this->validateReportes->getValue('programa'),
        ]);
    }


    // Reportes en Excell
    public function reporteEncuestaPreExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U'
        ];

        $sexo = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();

        if ($tpest==1){
            $encuestasexo = $this->tablaReportes->Listaencuestadosporsexopre($sexo, $fec1, $fec2, $prog);
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Encuestados Pregrado")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA ENCUESTA')
            ->setCellValue('B1', 'IDENTIFICACION')
            ->setCellValue('C1', 'NOMBRES')
            ->setCellValue('D1', 'APELLIDOS')
            ->setCellValue('E1', 'SEXO')
            ->setCellValue('F1', 'PROGRAMA')
            ->setCellValue('G1', 'DIRECCION')
            ->setCellValue('H1', 'MUNICIPIO')
            ->setCellValue('I1', 'DEPARTAMENTO')
            ->setCellValue('J1', 'TEL. FIJO')
            ->setCellValue('K1', 'CELULAR')
            ->setCellValue('L1', 'E-MAIL')
            ->setCellValue('M1', 'PRUEBAS ECAES')
            ->setCellValue('N1', 'AÑO INICIO')
            ->setCellValue('O1', 'SEMESTRE INICIO')
            ->setCellValue('P1', 'AÑO FIN')
            ->setCellValue('Q1', 'SEMESTRE FIN')
            ->setCellValue('R1', 'AREA FORMACION DESEA ESPECIALIZARSE')
            ->setCellValue('S1', 'TRABAJA ACTUALMENTE')
            ->setCellValue('T1', 'OCUPACION')
            ->setCellValue('U1', 'NOMBRE DE LA EMPRESA')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(100);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:U1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:U1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($encuestasexo as $value) {
            if ($value['il_trabactualmente']=='on'){
                $trabaja='Si';
            }else{
                $trabaja='No';
            }
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fec_encuesta'])
                ->setCellValue('B'.$fila, $value['dp_identestud'])
                ->setCellValue('C'.$fila, $value['nombres'])
                ->setCellValue('D'.$fila, $value['apellidos'])
                ->setCellValue('E'.$fila, $value['sexo'])
                ->setCellValue('F'.$fila, $value['nombre'])
                ->setCellValue('G'.$fila, $value['dp_dirresidencia'])
                ->setCellValue('H'.$fila, $value['dp_mpioresidencia'])
                ->setCellValue('I'.$fila, $value['dp_deptoresidencia'])
                ->setCellValue('J'.$fila, $value['dp_telestud'])
                ->setCellValue('K'.$fila, $value['dp_celestud'])
                ->setCellValue('L'.$fila, $value['dp_emailestud'])
                ->setCellValue('M'.$fila, $value['ha_npsaberpro'])
                ->setCellValue('N'.$fila, $value['ha_anosemacaini'])
                ->setCellValue('O'.$fila, $value['ha_semacaini'])
                ->setCellValue('P'.$fila, $value['ha_anosemacafin'])
                ->setCellValue('Q'.$fila, $value['ha_semacafin'])
                ->setCellValue('R'.$fila, $value['ep_areaporespec'])
                ->setCellValue('S'.$fila, $trabaja)
                ->setCellValue('T'.$fila, $value['il_ocupesetraba'])
                ->setCellValue('U'.$fila, $value['il_nomemplab'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Encuestados Pregrado');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Encuestados Pregrado_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteEncuestaPosExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U'
        ];

        $sexo = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();
        //echo $fec1; echo $fec2;exit();
        if ($tpest==2){
            $encuestasexo = $this->tablaReportes->Listaencuestadosporsexopos($sexo, $fec1, $fec2, $prog);
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Encuestados Postgrado")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA ENCUESTA')
            ->setCellValue('B1', 'IDENTIFICACION')
            ->setCellValue('C1', 'NOMBRES')
            ->setCellValue('D1', 'APELLIDOS')
            ->setCellValue('E1', 'SEXO')
            ->setCellValue('F1', 'PROGRAMA')
            ->setCellValue('G1', 'DIRECCION')
            ->setCellValue('H1', 'MUNICIPIO')
            ->setCellValue('I1', 'DEPARTAMENTO')
            ->setCellValue('J1', 'TEL. FIJO')
            ->setCellValue('K1', 'CELULAR')
            ->setCellValue('L1', 'E-MAIL')
            ->setCellValue('M1', 'PRUEBAS ECAES')
            ->setCellValue('N1', 'AÑO INICIO')
            ->setCellValue('O1', 'SEMESTRE INICIO')
            ->setCellValue('P1', 'AÑO FIN')
            ->setCellValue('Q1', 'SEMESTRE FIN')
            ->setCellValue('R1', 'AREA FORMACION DESEA SEGUIR ESPECIALIZANDOSE')
            ->setCellValue('S1', 'TRABAJA ACTUALMENTE')
            ->setCellValue('T1', 'OCUPACION')
            ->setCellValue('U1', 'NOMBRE DE LA EMPRESA')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(100);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:U1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:U1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($encuestasexo as $value) {
            if ($value['il_trabactualmente']=='on'){
                $trabaja='Si';
            }else{
                $trabaja='No';
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fec_encuesta'])
                ->setCellValue('B'.$fila, $value['dp_identestud'])
                ->setCellValue('C'.$fila, $value['nombres'])
                ->setCellValue('D'.$fila, $value['apellidos'])
                ->setCellValue('E'.$fila, $value['sexo'])
                ->setCellValue('F'.$fila, $value['nombre'])
                ->setCellValue('G'.$fila, $value['dp_dirresidencia'])
                ->setCellValue('H'.$fila, $value['dp_mpioresidencia'])
                ->setCellValue('I'.$fila, $value['dp_deptoresidencia'])
                ->setCellValue('J'.$fila, $value['dp_telestud'])
                ->setCellValue('K'.$fila, $value['dp_celestud'])
                ->setCellValue('L'.$fila, $value['dp_emailestud'])
                ->setCellValue('M'.$fila, $value['ha_npsaberpro'])
                ->setCellValue('N'.$fila, $value['ha_anoinipost'])
                ->setCellValue('O'.$fila, $value['ha_seminipost'])
                ->setCellValue('P'.$fila, $value['ha_anofinpost'])
                ->setCellValue('Q'.$fila, $value['ha_semfinpost'])
                ->setCellValue('R'.$fila, $value['ep_areaformcontinua'])
                ->setCellValue('S'.$fila, $trabaja)
                ->setCellValue('T'.$fila, $value['il_ocupesetraba'])
                ->setCellValue('U'.$fila, $value['il_nomemplab'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Encuestados Postgrado');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Encuestados Postgrado_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteEncTrabSexoExcelAction()
{


    $letras= [
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O'
    ];

    /*if ( ! $this->validateReportes->validarSexo() ) {
        return $this->verMensajeError($this->validateReportes->getMensajes());
    }*/

    $sexo = $this->getParam1();
    $tpest = (int)$this->getParam2();
    $fec1 = $this->getParam3();
    $fec2 = $this->getParam4();
    $prog = (int)$this->getParam5();
    //echo $tpest; echo $sexo;exit();
    if ($tpest==1){
        $enctrabajosexo = $this->tablaReportes->Listaencuestadosporsexotrabajopre($sexo, $fec1, $fec2, $prog);
    }else{
        if ($tpest==2){
            $enctrabajosexo = $this->tablaReportes->Listaencuestadosporsexotrabajopos($sexo, $fec1, $fec2, $prog);
        }
    }

    $objPHPExcel = new \PHPExcel();

    $objPHPExcel->getProperties()->setCreator("Egresados")
        ->setLastModifiedBy("Egresados")
        ->setTitle("Reporte Encuestados que laboran")
        ->setSubject("")
        ->setDescription("")
        ->setKeywords("")
        ->setCategory("");

    $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'FECHA ENCUESTA')
        ->setCellValue('B1', 'IDENTIFICACION')
        ->setCellValue('C1', 'NOMBRES')
        ->setCellValue('D1', 'APELLIDOS')
        ->setCellValue('E1', 'SEXO')
        ->setCellValue('F1', 'PROGRAMA')
        ->setCellValue('G1', 'DIRECCION')
        ->setCellValue('H1', 'MUNICIPIO')
        ->setCellValue('I1', 'DEPARTAMENTO')
        ->setCellValue('J1', 'TEL. FIJO')
        ->setCellValue('K1', 'CELULAR')
        ->setCellValue('L1', 'E-MAIL')
        ->setCellValue('M1', 'NOMBRE EMPRESA LABORA')
        ->setCellValue('N1', 'FECHA INICIO LABORAL')
        ->setCellValue('O1', 'CARGO OCUPADO')
    ;

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(150);
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(60);
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(60);
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(80);
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
    $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(60);

    $objPHPExcel->getActiveSheet()
        ->getStyle('A1:O1')
        ->getFill()
        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('FFFF00');

    $objPHPExcel->getActiveSheet()->getStyle('A1:O1')->getFont()->setBold(true);

    // Datos de las celdas

    $fila=2;

    foreach ($enctrabajosexo as $value) {

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$fila, $value['fec_encuesta'])
            ->setCellValue('B'.$fila, $value['dp_identestud'])
            ->setCellValue('C'.$fila, $value['nombres'])
            ->setCellValue('D'.$fila, $value['apellidos'])
            ->setCellValue('E'.$fila, $value['sexo'])
            ->setCellValue('F'.$fila, $value['nombre'])
            ->setCellValue('G'.$fila, $value['dp_dirresidencia'])
            ->setCellValue('H'.$fila, $value['dp_mpioresidencia'])
            ->setCellValue('I'.$fila, $value['dp_deptoresidencia'])
            ->setCellValue('J'.$fila, $value['dp_telestud'])
            ->setCellValue('K'.$fila, $value['dp_celestud'])
            ->setCellValue('L'.$fila, $value['dp_emailestud'])
            ->setCellValue('M'.$fila, $value['il_nomemplab'])
            ->setCellValue('N'.$fila, $value['il_fecinilab'])
            ->setCellValue('O'.$fila, $value['il_ocupesetraba'])
        ;

        //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
        //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

        foreach ($letras as $l) {
            $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        }

        $fila++;
    }


    // ===============================================================
    //   Finalizar Excel
    // ===============================================================

    $objPHPExcel->getActiveSheet()->setTitle('Reporte Encuestados que laboran');
    $objPHPExcel->setActiveSheetIndex(0);

    $nombre_archivo='Reporte Encuestados que laboran_'.'.xlsx';


    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0


    $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    exit;

}

    public function reporteEncNoTrabSexoExcelAction()
    {


        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'
        ];

        /*if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }*/

        $sexo = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();
        //echo $tpest; echo $sexo;exit();
        if ($tpest==1){
            $encnotrabajosexo = $this->tablaReportes->Listaencuestaporsexonotrabajopre($sexo, $fec1, $fec2, $prog);
        }else{
            if ($tpest==2){
                $encnotrabajosexo = $this->tablaReportes->Listaencuestaporsexonotrabajopos($sexo, $fec1, $fec2, $prog);
            }
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Encuestados que NO laboran")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA ENCUESTA')
            ->setCellValue('B1', 'IDENTIFICACION')
            ->setCellValue('C1', 'NOMBRES')
            ->setCellValue('D1', 'APELLIDOS')
            ->setCellValue('E1', 'SEXO')
            ->setCellValue('F1', 'PROGRAMA')
            ->setCellValue('G1', 'DIRECCION')
            ->setCellValue('H1', 'MUNICIPIO')
            ->setCellValue('I1', 'DEPARTAMENTO')
            ->setCellValue('J1', 'TEL. FIJO')
            ->setCellValue('K1', 'CELULAR')
            ->setCellValue('L1', 'E-MAIL')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(60);


        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:L1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:L1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($encnotrabajosexo as $value) {

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fec_encuesta'])
                ->setCellValue('B'.$fila, $value['dp_identestud'])
                ->setCellValue('C'.$fila, $value['nombres'])
                ->setCellValue('D'.$fila, $value['apellidos'])
                ->setCellValue('E'.$fila, $value['sexo'])
                ->setCellValue('F'.$fila, $value['nombre'])
                ->setCellValue('G'.$fila, $value['dp_dirresidencia'])
                ->setCellValue('H'.$fila, $value['dp_mpioresidencia'])
                ->setCellValue('I'.$fila, $value['dp_deptoresidencia'])
                ->setCellValue('J'.$fila, $value['dp_telestud'])
                ->setCellValue('K'.$fila, $value['dp_celestud'])
                ->setCellValue('L'.$fila, $value['dp_emailestud'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Encuestados NO laboran');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Encuestados que NO laboran_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteGraduadosSexoExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z'
        ];

        /*if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }*/


        /* print_r($prog);
         echo $sexo.'<br>'.$tpest.'<br>'.$fec1.'<br>'.$fec2;exit();
        $sexo = $_POST['sex'];
        $tpest = $_POST['est'];
        $fec1 = $_POST['fec1'];
        $fec2 = $_POST['fec2'];
        $prog = $_POST['prog'];

        echo $prog.'<br>'.$sexo.'<br>'.$tpest.'<br>'.$fec1.'<br>'.$fec2;exit();*/

        $sexo = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();
        if ($tpest==1){
            $graduadossexo = $this->tablaReportes->Listagraduadosporsexopre($sexo, $fec1, $fec2, $prog);
        }else{
            if ($tpest==2){
                $graduadossexo = $this->tablaReportes->Listagraduadosporsexopos($sexo, $fec1, $fec2, $prog);
            }
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Graduados por sexo")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA GRADO')
            ->setCellValue('B1', 'NÚMERO ACTA GRAL')
            ->setCellValue('C1', 'NÚMERO ACTA INDIV')
            ->setCellValue('D1', 'NÚMERO DIPLOMA')
            ->setCellValue('E1', 'NOMBRES')
            ->setCellValue('F1', 'APELLIDOS')
            ->setCellValue('G1', 'IDENTIFICACION')
            ->setCellValue('H1', 'LUGAR DE EXPED.')

            ->setCellValue('I1', 'FEC. NACIMIENTO')
            ->setCellValue('J1', 'SEXO')
            ->setCellValue('K1', 'PROGRAMA')
            ->setCellValue('L1', 'DIRECCION')

            ->setCellValue('M1', 'TEL. FIJO')
            ->setCellValue('N1', 'CELULAR')
            ->setCellValue('O1', 'E-MAIL')
            ->setCellValue('P1', 'NP SABER PRO')
            ->setCellValue('Q1', 'SEM. INICIO')
            ->setCellValue('R1', 'DEL AÑO')
            ->setCellValue('S1', 'SEM. FINALIZO')
            ->setCellValue('T1', 'DEL AÑO')
            ->setCellValue('U1', 'EXPECTATIVAS DE POSTGRADO')
            ->setCellValue('V1', 'TRABAJA?')
            ->setCellValue('W1', 'NOMBRE DE LA EMPRESA')
            ->setCellValue('X1', 'DIRECCION EMPRESA')

            ->setCellValue('Y1', 'CARGO')
            ->setCellValue('Z1', 'TELEFONO EMPRESA')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(120);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(120);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:Z1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($graduadossexo as $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fec_actas'])
                ->setCellValue('B'.$fila, $value['nro_actgral'])
                ->setCellValue('C'.$fila, $value['nro_actind'])
                ->setCellValue('D'.$fila, $value['nro_diploma'])
                ->setCellValue('E'.$fila, $value['nombres'])
                ->setCellValue('F'.$fila, $value['apellidos'])
                ->setCellValue('G'.$fila, $value['dp_identestud'])
                ->setCellValue('H'.$fila, $value['dp_lugarexped'])
                ->setCellValue('I'.$fila, $value['dp_fecnac'])
                ->setCellValue('J'.$fila, $value['sexo'])
                ->setCellValue('K'.$fila, $value['nombre'])
                ->setCellValue('L'.$fila, $value['direc_residen'])
                ->setCellValue('M'.$fila, $value['tel_fijo'])
                ->setCellValue('N'.$fila, $value['celular'])
                ->setCellValue('O'.$fila, $value['email'])
                ->setCellValue('P'.$fila, $value['ha_npsaberpro'])
                ->setCellValue('Q'.$fila, $value['ha_semacaini'])
                ->setCellValue('R'.$fila, $value['ha_anosemacaini'])
                ->setCellValue('S'.$fila, $value['ha_semacafin'])
                ->setCellValue('T'.$fila, $value['ha_anosemacafin'])
                ->setCellValue('U'.$fila, $value['ep_areaporespec'])
                ->setCellValue('V'.$fila, $value['trabajo_act'])
                ->setCellValue('W'.$fila, $value['nom_empresa'])
                ->setCellValue('X'.$fila, $value['dir_empresa'])
                ->setCellValue('Y'.$fila, $value['cargo_empresa'])
                ->setCellValue('Z'.$fila, $value['tel_empresa'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Graduados por sexo');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Graduados por sexo_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteNroactaGraduadosExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
            'Y', 'Z'
        ];

        $acta = $this->getParam1();
        $fec1 = $this->getParam2();
        $fec2 = $this->getParam3();
        $prog = (int)$this->getParam4();
        $tpprog = (int)$this->getParam5();
        if ($tpprog==1) {
            $graduadosnroacta = $this->tablaReportes->Listagraduadosactaporfecha($acta, $fec1, $fec2, $prog);
        }else{
            if ($tpprog==2) {
                $graduadosnroacta = $this->tablaReportes->Listagraduadosactaporfechapos($acta, $fec1, $fec2, $prog);
            }
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Graduados por Nro de Acta General")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA GRADO')
            ->setCellValue('B1', 'NÚMERO ACTA GRAL')
            ->setCellValue('C1', 'NÚMERO ACTA INDIV')
            ->setCellValue('D1', 'NÚMERO DIPLOMA')
            ->setCellValue('E1', 'NOMBRES')
            ->setCellValue('F1', 'APELLIDOS')
            ->setCellValue('G1', 'IDENTIFICACION')
            ->setCellValue('H1', 'LUGAR DE EXPED.')

            ->setCellValue('I1', 'FEC. NACIMIENTO')
            ->setCellValue('J1', 'SEXO')
            ->setCellValue('K1', 'PROGRAMA')
            ->setCellValue('L1', 'DIRECCION')

            ->setCellValue('M1', 'TEL. FIJO')
            ->setCellValue('N1', 'CELULAR')
            ->setCellValue('O1', 'E-MAIL')
            ->setCellValue('P1', 'NP SABER PRO')
            ->setCellValue('Q1', 'SEM. INICIO')
            ->setCellValue('R1', 'DEL AÑO')
            ->setCellValue('S1', 'SEM. FINALIZO')
            ->setCellValue('T1', 'DEL AÑO')
            ->setCellValue('U1', 'EXPECTATIVAS DE POSTGRADO')
            ->setCellValue('V1', 'TRABAJA?')
            ->setCellValue('W1', 'NOMBRE DE LA EMPRESA')
            ->setCellValue('X1', 'DIRECCION EMPRESA')

            ->setCellValue('Y1', 'CARGO')
            ->setCellValue('Z1', 'TELEFONO EMPRESA')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(120);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(120);
        $objPHPExcel->getActiveSheet()->getColumnDimension('X')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Y')->setWidth(80);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Z')->setWidth(30);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:Z1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:Z1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($graduadosnroacta as $value) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fec_actas'])
                ->setCellValue('B'.$fila, $value['nro_actgral'])
                ->setCellValue('C'.$fila, $value['nro_actind'])
                ->setCellValue('D'.$fila, $value['nro_diploma'])
                ->setCellValue('E'.$fila, $value['nombres'])
                ->setCellValue('F'.$fila, $value['apellidos'])
                ->setCellValue('G'.$fila, $value['dp_identestud'])
                ->setCellValue('H'.$fila, $value['dp_lugarexped'])
                ->setCellValue('I'.$fila, $value['dp_fecnac'])
                ->setCellValue('J'.$fila, $value['sexo'])
                ->setCellValue('K'.$fila, $value['nombre'])
                ->setCellValue('L'.$fila, $value['direc_residen'])
                ->setCellValue('M'.$fila, $value['tel_fijo'])
                ->setCellValue('N'.$fila, $value['celular'])
                ->setCellValue('O'.$fila, $value['email'])
                ->setCellValue('P'.$fila, $value['ha_npsaberpro'])

                ->setCellValue('Q'.$fila, $value['ha_semacaini'])
                ->setCellValue('R'.$fila, $value['ha_anosemacaini'])
                ->setCellValue('S'.$fila, $value['ha_semacafin'])
                ->setCellValue('T'.$fila, $value['ha_anosemacafin'])
                ->setCellValue('U'.$fila, $value['ep_areaporespec'])

                ->setCellValue('V'.$fila, $value['trabajo_act'])
                ->setCellValue('W'.$fila, $value['nom_empresa'])
                ->setCellValue('X'.$fila, $value['dir_empresa'])
                ->setCellValue('Y'.$fila, $value['cargo_empresa'])
                ->setCellValue('Z'.$fila, $value['tel_empresa'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Graduados Actas Gral');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Graduados por acta gral_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteGraduadosTrabSexoExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q'
        ];

        /*if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }*/

        $sexo = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();
        //echo $tpest; echo $sexo;exit();
        $graduadostrabsexo = $this->tablaReportes->ListagraduadosTrabajan($sexo, $tpest, $fec1, $fec2, $prog);

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Graduados que laboran")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'IDENTIFICACION')
            ->setCellValue('B1', 'NOMBRES')
            ->setCellValue('C1', 'APELLIDOS')
            ->setCellValue('D1', 'SEXO')
            ->setCellValue('E1', 'DIRECCION')
            ->setCellValue('F1', 'TEL. FIJO')
            ->setCellValue('G1', 'CELULAR')
            ->setCellValue('H1', 'E-MAIL')
            ->setCellValue('I1', 'FECHA GRADO')
            ->setCellValue('J1', 'PROGRAMA')
            ->setCellValue('K1', 'NÚMERO ACTA GRAL')
            ->setCellValue('L1', 'NÚMERO ACTA INDIV')
            ->setCellValue('M1', 'NÚMERO DIPLOMA')
            ->setCellValue('N1', 'NOMBRE EMPRESA')
            ->setCellValue('O1', 'DIRECCION EMPRESA')
            ->setCellValue('P1', 'TELEFONO EMPRESA')
            ->setCellValue('Q1', 'CARGO')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(100);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:Q1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($graduadostrabsexo as $value) {

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['nro_ident'])
                ->setCellValue('B'.$fila, $value['nombres'])
                ->setCellValue('C'.$fila, $value['apellidos'])
                ->setCellValue('D'.$fila, $value['sexo'])
                ->setCellValue('E'.$fila, $value['direc_residen'])
                ->setCellValue('F'.$fila, $value['tel_fijo'])
                ->setCellValue('G'.$fila, $value['celular'])
                ->setCellValue('H'.$fila, $value['email'])
                ->setCellValue('I'.$fila, $value['fec_actas'])
                ->setCellValue('J'.$fila, $value['nomprog'])
                ->setCellValue('K'.$fila, $value['nro_actgral'])
                ->setCellValue('L'.$fila, $value['nro_actind'])
                ->setCellValue('M'.$fila, $value['nro_diploma'])
                ->setCellValue('N'.$fila, $value['nom_empresa'])
                ->setCellValue('O'.$fila, $value['dir_empresa'])
                ->setCellValue('P'.$fila, $value['tel_empresa'])
                ->setCellValue('Q'.$fila, $value['cargo_empresa'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Graduados Laboran');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Graduados Laboran por sexo_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteGraduadosNoTrabSexoExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'
        ];

        /*if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }*/

        $sexo = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();
        //echo $tpest; echo $sexo;exit();
        $graduadosnotrabsexo = $this->tablaReportes->ListagraduadosNoTrabajan($sexo, $tpest, $fec1, $fec2, $prog);

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Graduados que No laboran")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'IDENTIFICACION')
            ->setCellValue('B1', 'NOMBRES')
            ->setCellValue('C1', 'APELLIDOS')
            ->setCellValue('D1', 'SEXO')
            ->setCellValue('E1', 'DIRECCION')
            ->setCellValue('F1', 'TEL. FIJO')
            ->setCellValue('G1', 'CELULAR')
            ->setCellValue('H1', 'E-MAIL')
            ->setCellValue('I1', 'FECHA GRADO')
            ->setCellValue('J1', 'PROGRAMA')
            ->setCellValue('K1', 'NÚMERO ACTA GRAL')
            ->setCellValue('L1', 'NÚMERO ACTA INDIV')
            ->setCellValue('M1', 'NÚMERO DIPLOMA')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(30);
        /*$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(100);*/

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:M1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($graduadosnotrabsexo as $value) {

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['nro_ident'])
                ->setCellValue('B'.$fila, $value['nombres'])
                ->setCellValue('C'.$fila, $value['apellidos'])
                ->setCellValue('D'.$fila, $value['sexo'])
                ->setCellValue('E'.$fila, $value['direc_residen'])
                ->setCellValue('F'.$fila, $value['tel_fijo'])
                ->setCellValue('G'.$fila, $value['celular'])
                ->setCellValue('H'.$fila, $value['email'])
                ->setCellValue('I'.$fila, $value['fec_actas'])
                ->setCellValue('J'.$fila, $value['nomprog'])
                ->setCellValue('K'.$fila, $value['nro_actgral'])
                ->setCellValue('L'.$fila, $value['nro_actind'])
                ->setCellValue('M'.$fila, $value['nro_diploma'])
                /*->setCellValue('N'.$fila, $value['nom_empresa'])
                ->setCellValue('O'.$fila, $value['dir_empresa'])
                ->setCellValue('P'.$fila, $value['tel_empresa'])
                ->setCellValue('Q'.$fila, $value['cargo_empresa'])*/
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Graduados NO Laboran');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Graduados No Laboran por sexo_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }

    public function reporteEstadoCarnetExcelAction()
    {
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'
        ];

        /*if ( ! $this->validateReportes->validarSexo() ) {
            return $this->verMensajeError($this->validateReportes->getMensajes());
        }*/

        $est = $this->getParam1();
        $tpest = (int)$this->getParam2();
        $fec1 = $this->getParam3();
        $fec2 = $this->getParam4();
        $prog = (int)$this->getParam5();

        $estadocarnetestud = $this->tablaReportes->EstadoCarnetEstudProgramas($est, $tpest, $fec1, $fec2, $prog);

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Estado Carnets")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'IDENTIFICACION')
            ->setCellValue('B1', 'NOMBRES')
            ->setCellValue('C1', 'APELLIDOS')
            ->setCellValue('D1', 'SEXO')
            ->setCellValue('E1', 'DIRECCION')
            ->setCellValue('F1', 'TEL. FIJO')
            ->setCellValue('G1', 'CELULAR')
            ->setCellValue('H1', 'E-MAIL')
            ->setCellValue('I1', 'PROGRAMA')
            ->setCellValue('J1', 'FECHA PROCESO')
            ->setCellValue('K1', 'ESTADO DEL CARNET')
        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(50);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(150);
        $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(35);
        $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        /*$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(100);*/

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:K1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:K1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($estadocarnetestud as $value) {
            if ($value['carnet_entregado']==1){
                $estado='Entregado';
            }
            if ($value['carnet_entregado']==2){
                $estado='Por Entregar';
            }
            if ($value['carnet_entregado']==3){
                $estado='En Tramite';
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['nro_ident'])
                ->setCellValue('B'.$fila, $value['nombres'])
                ->setCellValue('C'.$fila, $value['apellidos'])
                ->setCellValue('D'.$fila, $value['sexo'])
                ->setCellValue('E'.$fila, $value['direc_residen'])
                ->setCellValue('F'.$fila, $value['tel_fijo'])
                ->setCellValue('G'.$fila, $value['celular'])
                ->setCellValue('H'.$fila, $value['email'])
                ->setCellValue('I'.$fila, $value['nomprog'])
                ->setCellValue('J'.$fila, $value['fec_entrecarnet'])
                ->setCellValue('K'.$fila, $estado)
                /*->setCellValue('Q'.$fila, $value['cargo_empresa'])*/
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        }


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Estado de Carnet');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte estado de Carnet_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }


    /**PENDIENTES POR TERMINAR */
    public function formEncuestaIndicadorAction(){

        return $this->viewModelAjax([
            'estudtrabaja' => $this->tablaReportes->Selecttrabaja(),
            'estudnotrabaja' => $this->tablaReportes->Selectnotrabaja(),
            'estudotracarre' => $this->tablaReportes->Selectestudotra(),
            'estudotracarresi' => $this->tablaReportes->Selectestudotrasi(),
            'estudotracarreno' => $this->tablaReportes->Selectestudotrano(),
            'noestudotracarre' => $this->tablaReportes->Selectnoestudotra()

        ]);
    }

    public function formEncuestasPorSedesAction(){

        return $this->viewModelAjax([
            'encuestaxsed' => $this->tablaReportes->Selectjoinencuestaxsede()
        ]);
    }


}