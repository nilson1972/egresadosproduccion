<?php
 
namespace Comun;
 
 
use Zend\InputFilter\InputFilter;

class InputFilterGeneric extends InputFilter
{

    private $mensaje_error;



    private $filtro = array(
            array(
                'name'    => 'htmlentities',
                'options' => array(
                    'quoteStyle' => ENT_QUOTES,
                    // 'charset'    => 'iso-8859-1',
                    'charset'    => 'UTF-8',
                ),
            ),
            array('name' => 'StripTags'),
            array('name' => 'StringTrim'),
            // array('name' => 'StringToUpper'),
    );





    public function getMensajes()
    {
        return $this->mensaje_error;
    }



    /*
    |--------------------------------------------------------------------------
    | Valida los datos asignados  
    |--------------------------------------------------------------------------
    |
    |  @return boolean
    */

    public function validar()
    {
        $this->setData($_POST);
        if (!$this->isValid()) {
            foreach ($this->getInvalidInput() as $error) {
                $arr = $error->getMessages();
                foreach ($error->getMessages() as $valor) {
                    $this->mensaje_error.= $valor."<br>";
                }
            }
            return false;
        }
        return true;
    }



    /*
    | Valida un numero con solo digitos sin longitud máxima
    |
    |   @params [
    |       id,
    |       label,
    |       required
    |   ]  
    |
    | @return boolean
    |*/
    protected function validarDigito($params) 
    {
        return array(
            'name' => $params['id'],
            'required' => $params['required'],
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$params['label'].'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'digits',
                    'options' => array(
                        'messages' => array(
                                'digitsInvalid'     => 'El campo <b>'.$params['label'].'</b> tiene un valor NO v&aacute;lido',
                                'notDigits'         => 'El campo <b>'.$params['label'].'</b> tiene valores que no son digitos',
                                'digitsStringEmpty' => 'El campo <b>'.$params['label'].'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
            ),
            'filters' => $this->filtro,
        );
    }

    /*
    | Valida un numero con solo digitos sin longitud máxima
    |
    |   @params [
    |       id,
    |       label,
    |       required,
    |       max_lenght,
    |       alnum
    |   ]  
    |
    | @return boolean
    |*/
    
    protected function validarTexto($params)
    {

        $validador = [
            [
                'name' => 'notempty',
                'options' => [
                    'messages' => [
                        'isEmpty' => 'El campo <b>'.$params['label'].'</b> NO puede estar vac&iacute;o',
                    ]
                ]
            ],
        ];

        // Validar Max Lenght
        if (isset($params['max_lenght'])) {
            $validador = [
                [
                    'name' => 'string_length',
                    'options' => [
                        'min' => 1,
                        'max' => $params['max_lenght'],
                        'messages' => [
                            'stringLengthTooLong'  => 'La cantidad de caracteres en el campo <b>'.$params['label'].'</b> es muy larga'
                        ]
                    ],
                ],
            ];
        }

        // Validar Alphanum
        if (isset($params['alnum'])) {
            $validador = [
                    [
                        'name' => 'alnum',
                        'options' => [
                            'messages' => [
                                'alnumInvalid'  => 'Solo se permiten letras o numeros en campo <b>'.$params['label'].'</b>',
                                'notAlnum'      => 'El campo <b>'.$params['label'].'</b> presenta caracteres que no son alfanuméricos'
                            ]
                        ],
                    ],
                ];
        }

        return [
            'name' => $params['id'],
            'required' => $params['required'],
            'validators' => $validador,
            'filters'  => $this->filtro,
        ];
        
    }


    /*
    |
    | Valida un numero entero
    |
    |   @params [
    |       valor,
    |       campo
    |   ]  
    |
    | @return boolean
    |*/

    public function validarEntero($params)
    {
        $validator = new \Zend\I18n\Validator\IsInt();

        $validator->setMessage(array(
                \Zend\I18n\Validator\IsInt::NOT_INT => 'La entrada no parece ser un valor entero',
                \Zend\I18n\Validator\IsInt::INVALID => 'La entrada no parece ser un valor numérico válido'
        ));

        if (!$validator->isValid($params['valor'])) {
            foreach ($validator->getMessages() as $messageId => $message) {
                foreach ($message as $params['valor']) {
                    $this->mensaje_error.= 'Error en campo <strong>'.$params['campo'].': </strong>'.$params['valor']."<br>";
                }
            }
            return false;
        }
        return true;
    }



















    

    /**
     * Valida un numero solo digitos con longitud máxima
     *
     * @param  string campo
     * @param  string requerido (sin comillas)
     * @param  string msgNotEmpty 
     * @param  string msgDigitInvalid
     * @param  string msgNotDigits
     * @param  string msgDigitsStringEmpty
     *
     * @return boolean
     *
    protected function validarDigitoMaxLenght( $campo, $textoCampo, $requerido, $longitud)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'digits',
                    'options' => array(
                        'messages' => array(
                                'digitsInvalid'     => 'El campo <b>'.$textoCampo.'</b> tiene un valor NO v&aacute;lido',
                                'notDigits'         => 'El campo <b>'.$textoCampo.'</b> tiene valores que no son digitos',
                                'digitsStringEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'string_length',
                    'options' => array(
                        'max' => $longitud,
                        'messages' => array( 
                                'stringLengthTooLong' => 'El campo <b>'.$textoCampo.'</b> excede su longitud',
                                // 'stringLengthInvalid' => $msgStringLengthInvalid,
                        )
                    ),
                ),                      
            ),
            'filters' => $this->filtro,
        );
    }


    /**
     * Valida un numero solo digitos con longitud máxima
     *
     * @param  string campo
     * @param  string requerido (sin comillas)
     * @param  string msgNotEmpty 
     * @param  string msgDigitInvalid
     * @param  string msgNotDigits
     * @param  string msgDigitsStringEmpty
     *
     * @return boolean
     *
    protected function validarDigitoMaxLenghtMinMax( $campo, $textoCampo, $requerido, $longitud, $min, $max)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'digits',
                    'options' => array(
                        'messages' => array(
                                'digitsInvalid'     => 'El campo <b>'.$textoCampo.'</b> tiene un valor NO v&aacute;lido',
                                'notDigits'         => 'El campo <b>'.$textoCampo.'</b> tiene valores que no son digitos',
                                'digitsStringEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'string_length',
                    'options' => array(
                        'max' => $longitud,
                        'messages' => array( 
                                'stringLengthTooLong' => 'El campo <b>'.$textoCampo.'</b> excede su longitud',
                                // 'stringLengthInvalid' => $msgStringLengthInvalid,
                        )
                    ),
                ),
                array(
                    'name' => 'greater_than',
                    'options' => array(
                        'min' => $min,
                        'inclusive' => true,
                        'messages' => array( 
                                // 'notGreaterThan' => 'El campo <b>'.$textoCampo.'</b> No es mayor que '.$min,
                                'notGreaterThanInclusive' => 'El campo <b>'.$textoCampo.'</b> es menor que '.$min,
                        )
                    ),
                ),
                array(
                    'name' => 'less_than',
                    'options' => array(
                        'max' => $max,
                        'inclusive' => true,
                        'messages' => array( 
                                // 'notLessThan' => 'El campo <b>'.$textoCampo.'</b> No es mayor que '.$min,
                                'notLessThanInclusive' => 'El campo <b>'.$textoCampo.'</b> es mayor que '.$max,
                        )
                    ),
                ),                      
            ),
            'filters' => $this->filtro,
        );
    }


    /**
     * Valida solo longitud y vacío 
     *
     * @param  string campo
     * @param  string requerido (sin comillas)
     * @param  int string_length_max
     *
    protected function validarStringMaxLenght($campo, $textoCampo, $requerido, $string_length_max)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,           
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'string_length',
                    'options' => array(
                        'min' => 1,
                        'max' => $string_length_max,
                        'messages' => array(
                            'stringLengthTooLong'  => 'La cantidad de caracteres en el campo <b>'.$textoCampo.'</b> es muy larga'
                        )
                    ),
                ),
            ),
            'filters'  => $this->filtro,
        );
    }

    
 


    protected function validarFecha($campo, $textoCampo, $requerido)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,           
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'date',
                    'options' => array(
                        'format' => 'Y-m-d',
                        'messages' => array(
                            'dateInvalidDate'  => 'El campo fecha <b>'.$campo.'</b> no parace ser una fecha valida',
                            'dateFalseFormat'  => 'El campo fecha <b>'.$campo.'</b> no tiene el formato AÑO-MES-DIA',
                            
                        )
                    ),
                ),
            ),
            'filters'  => $this->filtro,
        );
    }

    protected function validarEmail($campo, $textoCampo, $requerido)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,           
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'emailaddress',
                    'options' => array(
                        'messages' => array(
                            'emailAddressInvalidFormat'  => 'Campo <b>'.$textoCampo.'</b> tiene formato de correo no válido'
                        )
                    ),
                ),
            ),
            'filters'  => $this->filtro,
        );
    }

    protected function validarEnArreglo($campo, $textoCampo, $requerido, $array)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,

            'validators' => array(              
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                            'isEmpty' => 'El campo <b>'.$textoCampo.'</b> NO puede estar vac&iacute;o',
                        )
                    ),
                ),
                array(
                    'name' => 'inarray',
                    'options' => array(
                        'haystack' => $array,
                        'messages' => array(
                            'notInArray'  => 'El valor del campo '.$campo.' no se encuentra en el haystack',                            
                        )
                    ),
                ),
            ),
        );
    }

    public function validarFechaSimple($dia, $mes, $anyo)
    {
        return checkdate($mes, $dia, $anyo);
    }

    public function validarFechaValidator($fecha)
    {
        $validator = new Zend\Validator\Date(array('format' => 'Y'));
        $validator->isValid($fecha);
    }

    

    public function validarDigito($valor, $campo)
    {
        $validator = new \Zend\Validator\Digits();
        $validator->setMessage(array(
            \Zend\Validator\Digits::NOT_DIGITS =>
                'La entrada debe contener solo digitos',
        ));
        if (!$validator->isValid($valor)) {
            foreach ($validator->getMessages() as $messageId => $message) {
                foreach ($message as $valor) {
                    $this->mensaje_error.= 'Error en campo <strong>'.$campo.': </strong>'.$valor."<br>";
                }
            }
            return false;
        }
        return true;
        // return $validator->isValid($valor); 
    }

    













    //
    //  No revisados
    // ----------------------------------------------------------------------------------------------------
    // ----------------------------------------------------------------------------------------------------

    

    

    /**
     * Valida un campo para valores solo alfanumericos
     *
     * @param  string campo
     * @param  string requerido (sin comillas)
     * @param  string allowWhiteSpace 
     * @param  string msgIsEmpty 
     * @param  string alnumInvalid
     * @param  string notAlnum
     * @param  string alnumStringEmpty
     * @param  string string_length_max
     * @param  string stringLengthTooLong
     *
    protected function validarSoloAlfanumericos($campo, $requerido, $allowWhiteSpace, $msgIsEmpty,  $alnumInvalid, $notAlnum, $alnumStringEmpty, 
                                            $string_length_max, $stringLengthTooLong)
    {
        return array(
            'name' => $campo,
            'required' => $requerido,
            'filters'  => $this->filtro,
            'validators' => array(
                array(
                    'name' => 'notempty',
                    'options' => array(
                        'messages' => array(
                                'isEmpty'     => $msgIsEmpty,
                        )
                    ),
                ),
                array(
                    'name' => 'alnum',
                    'options' => array(
                        'allowWhiteSpace' => $allowWhiteSpace,
                        'messages' => array(
                                'alnumInvalid'     => $alnumInvalid,
                                'notAlnum'         => $notAlnum,
                                'alnumStringEmpty' => $alnumStringEmpty,
                        )
                    ),
                ),
                array(
                    'name' => 'string_length',
                    'options' => array(
                        'max' => $string_length_max,
                        'messages' => array(
                                'stringLengthTooLong'  => $stringLengthTooLong
                        )
                    ),
                ),
            )                   
        );
    }

    

    

    */
    

}
