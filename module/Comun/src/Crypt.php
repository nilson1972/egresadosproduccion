<?php

namespace Comun;


class Crypt
{

    private static $k = "db9s5572ca";

    public static function init() {
        // self::$k  = md5(uniqid(rand(), true).time());
    }

    public static function encrypt($dato)
    {
        $blockCipher = \Zend\Crypt\BlockCipher::factory('mcrypt', array('algo' => 'aes'));
        $blockCipher->setKey(self::$k);
        return $blockCipher->encrypt($dato);
    }

    public static function decrypt($dato)
    {
        $blockCipher = \Zend\Crypt\BlockCipher::factory('mcrypt', array('algo' => 'aes'));
        $blockCipher->setKey(self::$k);
        return $blockCipher->decrypt($dato);
    }



    public static function encrypt3( $string ) {
          $algorithm = 'rijndael-128'; // You can use any of the available
          $key = md5( "gsdfbh345hjb3kjb5tkj3bktukt4iu43ef", true); // bynary raw 16 byte dimension.
          $iv_length = mcrypt_get_iv_size( $algorithm, MCRYPT_MODE_CBC );
          $iv = mcrypt_create_iv( $iv_length, MCRYPT_RAND );
          $encrypted = mcrypt_encrypt( $algorithm, $key, $string, MCRYPT_MODE_CBC, $iv );
          $result = base64_encode( $iv . $encrypted );
          return $result;
    }

    public static function decrypt3( $string ) {
          $algorithm =  'rijndael-128';
          $key = md5( "gsdfbh345hjb3kjb5tkj3bktukt4iu43ef", true );
          $iv_length = mcrypt_get_iv_size( $algorithm, MCRYPT_MODE_CBC );
          $string = base64_decode( $string );
          $iv = substr( $string, 0, $iv_length );
          $encrypted = substr( $string, $iv_length );
          $result = mcrypt_decrypt( $algorithm, $key, $encrypted, MCRYPT_MODE_CBC, $iv );
          return $result;
    }


}