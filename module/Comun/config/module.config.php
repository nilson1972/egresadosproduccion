<?php

namespace Comun;

return [    

    'view_manager' => [
        'template_path_stack' => [
            'comun' => __DIR__ . '/../view',
        ],
    ],

];