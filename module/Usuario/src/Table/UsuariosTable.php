<?php
/**
 * Created by PhpStorm.
 * User: GRED
 * Date: 18/09/2018
 * Time: 05:35 PM
 */

namespace Usuario\Table;

use Comun\DB;

use Zend\db\TableGateway\TableGateway;

class UsuariosTable
{
    private $sql;

    private $tablaUsuario = 'usuario';

    public function insertarUsuarios($datos)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaUsuario,[
                'nombre' => utf8_encode($datos['nro_ident']),
                'username' => utf8_encode($datos['l_expedicion']),
                'password' => $datos['nombres'],
                'email' => $datos['apellidos'],
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function SelecUsuarios()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaUsuario));
        $select->columns(
            [
                'id_usuario' => 'id',
                'nom_del_usu' => 'nombre',
                'user_name' => 'username',
                'clave' => 'password',
                'activo' => 'activo',
                'tipo_user' => 'id_tipo',
                'correo' => 'email',
            ]
        );
        $select->where([
            'tabla1.activo' => 1,
        ]);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function SeleccionaUsuario($iduser)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaUsuario));
        $select->columns(
            [
                'id_usuario' => 'id',
                'nom_del_usu' => 'nombre',
                'user_name' => 'username',
                'clave' => 'password',
                'activo' => 'activo',
                'tipo_user' => 'id_tipo',
                'correo' => 'email',
            ]
        );
        $select->where([
            'tabla1.id' => $iduser,
        ]);
        //$select->where->like('tabla1.activo', '%'.$variable.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function getExisteUsuario($user)
    {
        $where = [
            'username'=>$user,
        ];

        return DB::selectRegistro( $this->tablaUsuario, $where);
    }

    public function insertarUsuario($datos,$username)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaUsuario,[
                'user_crea' => $username,
                'nombre' => strtoupper($datos['nombre']),
                'username' => strtolower($datos['usuario']),
                'password' => md5(strtolower($datos['password'])),
                'email' => $datos['correo'],
                'id_tipo' => $datos['tipouser'],
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function updateUsuario($datos,$user)
    {
        $set = [
            'nombre'=>$datos['nombre'],
            'username'=>$datos['usuario'],
            'id_tipo'=>$datos['tipouser'],
            'email'=>$datos['correo'],
            'activo'=>$datos['estadouser'],
            'user_modi'=>$user,
        ];

        $where = [
            'id'=>$datos['idusuaescogido'],
        ];

        return DB::actualizar( $this->tablaUsuario, $set, $where);
    }

    public function cambioClaveUsuario($datos,$user)
    {
        $set = [
            'password'=>md5(strtolower($datos['nuevopassword'])),
            'user_modi'=>$user,
        ];

        $where = [
            'id'=>$datos['idusuaescogido'],
        ];

        return DB::actualizar( $this->tablaUsuario, $set, $where);
    }

    public function verificaClaveActual($iduser,$claveact)
    {
        $where = [
            'password'=>md5(strtolower($claveact)),
            'id'=>$iduser,
        ];

        return DB::selectRegistro( $this->tablaUsuario, $where);
    }

}