<?php
namespace Usuario\Validate;

use Comun\InputFilterGeneric3;

class UsuarioValidate extends InputFilterGeneric3
{
    public function ValidarDatosUsuario()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre',
            'label' => ' nombre del usuario ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'usuario',
            'label' => ' nombre de user ',
            'required' => true,
            'max_lenght' => 16,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'password',
            'label' => ' password de user ',
            'required' => true,
            'max_lenght' => 16,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'correo',
            'label' => ' email de user ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'tipouser',
            'label' => ' tipo de usuario',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function ValidarDatosUpdateUsuario()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre',
            'label' => ' nombre del usuario ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'usuario',
            'label' => ' nombre de user ',
            'required' => false,
            'max_lenght' => 16,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'correo',
            'label' => ' email de user ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'tipouser',
            'label' => ' tipo de usuario',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'idusuaescogido',
            'label' => ' ID de usuario',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'estadouser',
            'label' => ' estado del usuario',
            'required' => false,
        ]);

        return $this->validar();
    }

    public function ValidaNuevoPassword()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'actualpassword',
            'label' => ' actual password',
            'required' => true,
            'max_lenght'=> 16,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nuevopassword',
            'label' => ' nuevo password',
            'required' => true,
            'max_lenght'=> 16,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'idusuaescogido',
            'label' => ' ID de usuario',
            'required' => true,
        ]);

        return $this->validar();
    }
}