<?php

namespace Usuario\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class UsuarioController extends GenericController
{
    public function __construct()
    {
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();
        $this->tablaVerEncuesta = new \Usuario\Table\VerEncuestaTable();
        $this->tablaVerEncuestaPost = new \Usuario\Table\VerEncuestaPostTable();
        $this->validateVerEncuesta = new \Usuario\Validate\VerEncuestaValidate();
        $this->validateBuscaEstudiante = new \Usuario\Validate\BuscaEstudianteValidate();
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->tablaEncuestaAnteriores = new \Reportes\Table\EncuestaAnterioresTable();
        $this->tablaReportes = new \Reportes\Table\ReportesTable();
        $this->tablaEncuesta = new \Estudiante\Table\EncuestaTable();
        $this->tablaEncuestaPos = new \Estudiante\Table\EncuestaPostTable();
        $this->tablaUsuarios = new \Usuario\Table\UsuariosTable();
        $this->validateUsuario = new \Usuario\Validate\UsuarioValidate();
    }

    public function indexAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
            'cortevigente' => $corte,
            'estudiantes' => $this->tablaEstudiante->getTotaEstudiantes($corte['id']),
            'realizadas'  => $this->tablaEstudiante->getTotaEncuestasRealizadas($corte['id']),
        ]);
    }

    public function indexPostAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
            'cortevigente' => $corte,
            'estudiantes' => $this->tablaEstudiante->getTotaEstudiantespost($corte['id']),
            'realizadas'  => $this->tablaEstudiante->getTotaEncuestasRealizadaspost($corte['id']),
        ]);
    }

    public function indexEstudAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModel([
            'cortevigente' => $corte,
            'totalestud' => $this->tablaEstudiante->getTotalEstudiantes(),
            'estudpre' => $this->tablaEstudiante->getTotaEstudiantes($corte['id']),
            'estudpost' => $this->tablaEstudiante->getTotaEstudiantespost($corte['id']),
        ]);
    }

    public function indexParametrosAction()
    {
        return $this->viewModel([

        ]);
    }

    public function indexEncuestasAnterioresAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
        ]);
    }

    public function indexEncuestasAnterioresPosAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
        ]);
    }

    public function indexActasAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
        ]);
    }

    public function indexMenuActualizaAction()
    {
        return $this->viewModel([
            'estudactualizados' => $this->tablaActualiza->getTotalEstudactualiza(),
            'estudactualizxenc' => $this->tablaActualiza->getTotalEstudactualizaXenc(),
        ]);
    }

    public function indexInscripeventoAction()
    {
        $estinsc = $this->tablaActualiza->getTotalEstudinscrip();
        return $this->viewModel([
            'estudinscritos' => $estinsc,
        ]);
    }

    public function indexReporencuestaAction()
    {
        return $this->viewModel([
            'sexomasculinopre' => $this->tablaEncuesta->getTotalEncSexoMas(),
            'sexofemeninopre' => $this->tablaEncuesta->getTotalEncSexoFem(),
            'sexomasculinopos' => $this->tablaEncuestaPos->getTotalEncSexoMas(),
            'sexofemeninopos' => $this->tablaEncuestaPos->getTotalEncSexoFem(),
            'trabmasculinopre' => $this->tablaEncuesta->getTotalEncTrabaMas(),
            'trabfemeninopre' => $this->tablaEncuesta->getTotalEncTrabaFem(),
            'trabmasculinopos' => $this->tablaEncuestaPos->getTotalEncTrabaMas(),
            'trabfemeninopos' => $this->tablaEncuestaPos->getTotalEncTrabaFem(),
            'notrabmasculinopre' => $this->tablaEncuesta->getTotalEncNoTrabaMas(),
            'notrabfemeninopre' => $this->tablaEncuesta->getTotalEncNoTrabaFem(),
            'notrabmasculinopos' => $this->tablaEncuestaPos->getTotalEncNoTrabaMas(),
            'notrabfemeninopos' => $this->tablaEncuestaPos->getTotalEncNoTrabaFem(),
        ]);
    }

    public function indexReporgraduadosAction()
    {
        return $this->viewModel([
            'sexomascgraduadopre' =>$this->tablaEncuesta->getTotalGraduadoMas(),
            'sexofemegraduadopre' =>$this->tablaEncuesta->getTotalGraduadoFem(),
            'sexomascgraduadopos' =>$this->tablaEncuestaPos->getTotalGraduadoMas(),
            'sexofemegraduadopos' =>$this->tablaEncuestaPos->getTotalGraduadoFem(),
            'gradtrabmasculinopre' =>$this->tablaEncuesta->getTotalGraduadoTrabMas(),
            'gradtrabfemeninopre' =>$this->tablaEncuesta->getTotalGraduadoTrabFem(),
            'gradtrabmasculinopos' =>$this->tablaEncuestaPos->getTotalGraduadoTrabMas(),
            'gradtrabfemeninopos' =>$this->tablaEncuestaPos->getTotalGraduadoTrabFem(),
            'gradnotrabmasculinopre' =>$this->tablaEncuesta->getTotalGraduadoNoTrabMas(),
            'gradnotrabfemeninopre' =>$this->tablaEncuesta->getTotalGraduadoNoTrabFem(),
            'gradnotrabmasculinopos' =>$this->tablaEncuestaPos->getTotalGraduadoNoTrabMas(),
            'gradnotrabfemeninopos' =>$this->tablaEncuestaPos->getTotalGraduadoNoTrabFem(),
        ]);
    }

    public function creaUsuarioAction()
    {
        $usertipo = Sesiones::getIdTipo();
        $userid = Sesiones::getIdUsuario();
        return $this->viewModelAjax([
            'tipouser' =>$usertipo,
            'idusuario' =>$userid,
        ]);
    }

    public function guardaUserAction()
    {
        if (!$this->validateUsuario->ValidarDatosUsuario()) {
            return $this->verMensajeError($this->validateUsuario->getMensajes());
        }

        $user = $this->tablaUsuarios->getExisteUsuario($this->validateUsuario->getValue('usuario'));
        //print_r($user);exit();
        if (is_null($user)) {
            $username = Sesiones::getUsuarioname();
            if (!$this->tablaUsuarios->insertarUsuario($this->validateUsuario->getValues(),$username)) {
                return $this->verMensajeError($this->validateUsuario->getMensajes());
            } else {
                return $this->verMensajeInfo("El Usuario fue registrado satisactoriamente...!");
            }
        } else {
            return $this->verMensajeError("El Usuario <strong>"
                . $this->validateUsuario->getValue('usuario') .
                "</strong> Ya Existe en la Tabla Usuario....!");

        }
    }

    public function editUsuarioAction()
    {
        $userid = Sesiones::getIdUsuario();
        $usertipo = Sesiones::getIdTipo();
        return $this->viewModelAjax([
            'idusuario' =>$userid,
            'tipouser' =>$usertipo,
            'editusuario' =>$this->tablaUsuarios->SeleccionaUsuario((int)$this->getParam1()),
        ]);
    }

    public function actualizaUserAction()
    {
        if (!$this->validateUsuario->ValidarDatosUpdateUsuario()) {
            return $this->verMensajeError($this->validateUsuario->getMensajes());
        }

        $username = Sesiones::getUsuarioname();
        if (!$this->tablaUsuarios->updateUsuario($this->validateUsuario->getValues(),$username)) {
            return $this->verMensajeError($this->validateUsuario->getMensajes());
        } else {
            return $this->verMensajeInfo("El Usuario fue Actualizado satisactoriamente...!");
        }
    }

    public function cambioContrasenaAction()
    {
        $userid = Sesiones::getIdUsuario();
        return $this->viewModelAjax([
            'idusuario' =>$userid,
            'editusuario' =>$this->tablaUsuarios->SeleccionaUsuario((int)$this->getParam1()),
        ]);
    }

    public function actualizapasswordAction()
    {
        if (!$this->validateUsuario->ValidaNuevoPassword()) {
            return $this->verMensajeError($this->validateUsuario->getMensajes());
        }

        $claveactok=$this->tablaUsuarios->verificaClaveActual($this->validateUsuario->getValue('idusuaescogido'),
                                                              $this->validateUsuario->getValue('actualpassword'));
        //var_dump($claveactok);echo $cla;exit();

        if (is_null($claveactok)) {
            return $this->verMensajeError("La Clave Actual es incorrecta..Intente el cambio nuevamente...!");
        }else{
            $username = Sesiones::getUsuarioname();
            if (!$this->tablaUsuarios->cambioClaveUsuario($this->validateUsuario->getValues(), $username)) {
                return $this->verMensajeError($this->validateUsuario->getMensajes());
            } else {
                return $this->verMensajeInfo("El Usuario fue Actualizado satisactoriamente...!");
            }
        }
    }

}