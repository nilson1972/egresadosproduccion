<?php

namespace Usuario;


use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;


return [
    'controllers' => [
        'factories' => [
            Controller\UsuarioController::class => InvokableFactory::class,
        ],
    ],


    'router' => [
        'routes' => [

            'usuario' => [
                'type'    => Segment::class,
                'options' => [
                    'route' => '/usuario[/:action[/:id1[/:id2]]]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id1'     => '[0-9]+',
                        'id2'     => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\UsuarioController::class,
                        'action'     => 'index',
                    ],
                ],
            ],

        ],
    ],



    'view_manager' => [
        'template_path_stack' => [
            'usuario' => __DIR__ . '/../view',
        ],
    ],
];