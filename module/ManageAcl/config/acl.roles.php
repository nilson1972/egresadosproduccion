<?php

return [
    'index' => [
        "allow" => [
            'home',
            'autenticacion',
            'logout',
            'estudiante',
            'egresado',
            'encuesta',
            'actualizar',
            'inscripcion',
            'ruleta'
        ],
        "deny" => [
            "usuario"
        ],
    ],
    'estudiante' => [
        "allow" => [
            "home",
            "autenticacion",
            "logout",
            'estudiante',
            'encuesta',
            'actualizar',
            'inscripcion'
        ],
        "deny" => [
            "usuario"
        ],
    ],
    'egresado' => [
        "allow" => [
            "egresado",
        ],
        "deny" => [
            "usuario"
        ],
    ],
    'usuario' => [
        "allow" => [
            "index",
            "usuario",
            "reportes",
            'estudiante',
            'anteriores',
            'encuesta',
            'actualizar',
            'inscripcion'
        ],
        "deny" => [],
    ],

];

?>
