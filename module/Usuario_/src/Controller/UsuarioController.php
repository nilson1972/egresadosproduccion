<?php


namespace Usuario\Controller;

use Comun\GenericController;

class UsuarioController extends GenericController
{

    public function __construct()
    {
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();
        $this->tablaVerEncuesta = new \Usuario\Table\VerEncuestaTable();
        $this->tablaVerEncuestaPost = new \Usuario\Table\VerEncuestaPostTable();
        $this->validateVerEncuesta = new \Usuario\Validate\VerEncuestaValidate();
        $this->validateBuscaEstudiante = new \Usuario\Validate\BuscaEstudianteValidate();
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->tablaEncuestaAnteriores = new \Reportes\Table\EncuestaAnterioresTable();
        $this->tablaReportes = new \Reportes\Table\ReportesTable();
        $this->tablaEncuesta = new \Estudiante\Table\EncuestaTable();
        $this->tablaEncuestaPos = new \Estudiante\Table\EncuestaPostTable();
    }

    public function indexAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
            'cortevigente' => $corte,
            'estudiantes' => $this->tablaEstudiante->getTotaEstudiantes($corte['id']),
            'realizadas'  => $this->tablaEstudiante->getTotaEncuestasRealizadas($corte['id']),
        ]);
    }

    public function indexPostAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
            'cortevigente' => $corte,
            'estudiantes' => $this->tablaEstudiante->getTotaEstudiantespost($corte['id']),
            'realizadas'  => $this->tablaEstudiante->getTotaEncuestasRealizadaspost($corte['id']),
        ]);
    }

    public function indexEstudAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModel([
            'cortevigente' => $corte,
            'totalestud' => $this->tablaEstudiante->getTotalEstudiantes(),
            'estudpre' => $this->tablaEstudiante->getTotaEstudiantes($corte['id']),
            'estudpost' => $this->tablaEstudiante->getTotaEstudiantespost($corte['id']),
        ]);
    }

    public function indexEncuestasAnterioresAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
        ]);
    }

    public function indexEncuestasAnterioresPosAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
        ]);
    }

    public function indexActasAction()
    {
        $corteinac = $this->tablaEncuestaAnteriores->periodoInactivosEncuesta();
        return $this->viewModel([
            'cortesinactivos' => $corteinac,
        ]);
    }

    public function indexMenuActualizaAction()
    {
        return $this->viewModel([
            'estudactualizados' => $this->tablaActualiza->getTotalEstudactualiza(),
            'estudactualizxenc' => $this->tablaActualiza->getTotalEstudactualizaXenc(),
        ]);
    }

    public function indexInscripeventoAction()
    {
        $estinsc = $this->tablaActualiza->getTotalEstudinscrip();
        return $this->viewModel([
            'estudinscritos' => $estinsc,
        ]);
    }

    public function indexReportesAction()
    {
        return $this->viewModel([
            'sexomasculinopre' => $this->tablaEncuesta->getTotalEncSexoMas(),
            'sexofemeninopre' => $this->tablaEncuesta->getTotalEncSexoFem(),
            'sexomasculinopos' => $this->tablaEncuestaPos->getTotalEncSexoMas(),
            'sexofemeninopos' => $this->tablaEncuestaPos->getTotalEncSexoFem(),
            'trabmasculinopre' => $this->tablaEncuesta->getTotalEncTrabaMas(),
            'trabfemeninopre' => $this->tablaEncuesta->getTotalEncTrabaFem(),
            'sexomascgraduadopre' =>$this->tablaEncuesta->getTotalGraduadoMas(),
            'sexofemegraduadopre' =>$this->tablaEncuesta->getTotalGraduadoFem(),
            'trabmasculinopos' => $this->tablaEncuestaPos->getTotalEncTrabaMas(),
            'trabfemeninopos' => $this->tablaEncuestaPos->getTotalEncTrabaFem(),
            'notrabmasculinopre' => $this->tablaEncuesta->getTotalEncNoTrabaMas(),
            'notrabfemeninopre' => $this->tablaEncuesta->getTotalEncNoTrabaFem(),
            'notrabmasculinopos' => $this->tablaEncuestaPos->getTotalEncNoTrabaMas(),
            'notrabfemeninopos' => $this->tablaEncuestaPos->getTotalEncNoTrabaFem(),
            'sexomascgraduadopos' =>$this->tablaEncuestaPos->getTotalGraduadoMas(),
            'sexofemegraduadopos' =>$this->tablaEncuestaPos->getTotalGraduadoFem(),
            'gradtrabmasculinopre' =>$this->tablaEncuesta->getTotalGraduadoTrabMas(),
            'gradtrabfemeninopre' =>$this->tablaEncuesta->getTotalGraduadoTrabFem(),
            'gradtrabmasculinopos' =>$this->tablaEncuestaPos->getTotalGraduadoTrabMas(),
            'gradtrabfemeninopos' =>$this->tablaEncuestaPos->getTotalGraduadoTrabFem(),
            'gradnotrabmasculinopre' =>$this->tablaEncuesta->getTotalGraduadoNoTrabMas(),
            'gradnotrabfemeninopre' =>$this->tablaEncuesta->getTotalGraduadoNoTrabFem(),
            'gradnotrabmasculinopos' =>$this->tablaEncuestaPos->getTotalGraduadoNoTrabMas(),
            'gradnotrabfemeninopos' =>$this->tablaEncuestaPos->getTotalGraduadoNoTrabFem(),
        ]);
    }

}