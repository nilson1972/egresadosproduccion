<?php

namespace Usuario\Validate;


use Comun\InputFilterGeneric3;


class BuscaEstudianteValidate extends InputFilterGeneric3
{

    public function validarIdentificacion()
    {
        $this->validarDigito([
            'name'  => 'identificacion',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarNombres()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'El campo Nombres Contiene un Valor no valido ',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function validarIdentificacionenc()
    {
        $this->validarDigito([
            'name'  => 'ident',
            'label' => 'Valor no valido para campo ident',
            'required' => true,
        ]);

        return $this->validar();
    }

}