<?php

namespace Usuario\Validate;


use Comun\InputFilterGeneric3;


class VerEncuestaValidate extends InputFilterGeneric3
{

    public function ValidarDatosVerEncuesta()
    {
        $this->validarDigito([
            'name'  => 'identificacion',
            'label' => 'Valor no valido para campo identificación',
            'required' => true,
        ]);

    /*    $this->validarAlfaNumericoFiltro([
            'name' => 'programa',
            'label' => 'Valor no valido para campo programa',
            'required' => true,
            'max_lenght' => 10,
            'alnum' => false,
        ]); */
        return $this->validar();
    }

}