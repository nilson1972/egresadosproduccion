<?php

namespace Estudiante\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class InscripcionController extends GenericController
{
    public function __construct()
    {
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->validateActualiza = new \Estudiante\Validate\ActualizarValidate();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();

    }

    public function indexValidaInscripEventoAction()
    {
        $nomevento = $this->tablaEstudiante->EventoVigente();
        return $this->viewModel([
            'eventovigente' => $nomevento,
        ]);
    }

    public function actualizarinscripAction(){
        // Validación identificación Estudiante
        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateActualiza->getValue('identificacion'));
        $prog1 = $this->tablaProgramas->Selectprogramas((int)$this->getParam1());
        $prog2 = $this->tablaProgramas->Selectprogramas((int)$this->getParam2());
        //$evento = $this->tablaActualiza->getBuscaEventoVigente();
        //if ($evento['id'] <> 0) {

            if (count($est) == 0) {
                return $this->verMensajeError("El Número de identificación <strong>"
                    . $this->validateActualiza->getValue('identificacion') .
                    "</strong> no existe. Consulte con la Oficina de Atención al Egresado");
            } else {
                $yainscevento = $this->tablaActualiza->getBuscarEstudInscevento($est['id'], $_POST['idevento']);
                if ($yainscevento['total'] == 0) {
                    return $this->viewModelAjax([
                        'estudiante' => $est,
                        'progpre' => $prog1,
                        'progpos' => $prog2,
                        'pais' => $this->tablaActualiza->Listadepaises(),
                        'pais2' => $this->tablaActualiza->Listadepaises(),
                        'otraunivprogpos' => $this->tablaUniversidad->Selectuniversidades(),
                        'otraunivprogpre' => $this->tablaUniversidad->Selectuniversidades(),
                        'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
                        'idevento'=>$_POST['idevento'],
                    ]);
                } else {
                    return $this->verMensajeError("Estudiante con Nro de indetificación <strong>"
                        . $this->validateActualiza->getValue('identificacion') .
                        "</strong> Ya esta inscrito al evento de Egresados");
                }
            }
        //} else {
        //    return $this->verMensajeError("Apreciado Egresado las inscripciones al evento de Egresados ya fueron cerradas ");
        //}
    }

    public function fortalezaAction()
    {
        return $this->viewModelAjax([
            'respuestas2'=>$this->tablaActualiza->ListadeRespuestas2((int)$this->getParam1()),
        ]);
    }

    public function insertarActualizainscripAction()
    {
        // Validación Actualización de datos
        if ( ! $this->validateActualiza->validarDatosActualizados() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }
        //$evento = $this->tablaActualiza->getBuscaEventoVigente();
        if (! $this->tablaActualiza->insertarinscrip($this->validateActualiza->getValue('id_estudiante'),
                                                     $this->validateActualiza->getValue('progpre'),
                                                     $this->validateActualiza->getValue('progpos'),
                                                     $_POST['id_evento'])){
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }else{
            $paisres= $this->tablaActualiza->Busquedadepaises($this->validateActualiza->getValue('paisres'));
            $paislab= $this->tablaActualiza->Busquedadepaises($this->validateActualiza->getValue('paislab'));
            $ciudres= $this->tablaActualiza->Busquedadeciudades($this->validateActualiza->getValue('ciudres'));
            $ciudlab= $this->tablaActualiza->Busquedadeciudades($this->validateActualiza->getValue('ciudlab'));

            $yaactualizo = $this->tablaActualiza->getBuscarEstudActualiza($this->validateActualiza->getValue('id_estudiante'));
            if (! $yaactualizo==0){
                $fila=$this->tablaActualiza->actulizarestadovigen($this->validateActualiza->getValue('id_estudiante'));
            }else{
                if (! $this->tablaActualiza->insertactulizainscrip($this->validateActualiza->getValues(),$paisres['Pais'], $ciudres['ciudad'],
                                                                                                         $paislab['Pais'], $ciudlab['ciudad'])){
                    return $this->verMensajeError($this->validateActualiza->getMensajes());
                }else{
                    return $this->verMensajeInfo("Egresado Inscrito al Evento y Actualizado en Base de dato, satisfactoriamente....");
                }
            }
            if($fila==1) {
                if (! $this->tablaActualiza->insertactulizainscrip($this->validateActualiza->getValues(),$paisres['Pais'], $ciudres['ciudad'],
                                                                                                         $paislab['Pais'], $ciudlab['ciudad'])){
                    return $this->verMensajeError($this->validateActualiza->getMensajes());
                }else{
                    return $this->verMensajeInfo("Egresado Inscrito al Evento y Actualizado en Base de dato, satisfactoriamente....");
                }
            }else{
                return $this->verMensajeInfo("Egresado Inscrito al Evento satisfactoriamente, aun que no se pudo actualizar sus datos....");
            }
        }
    }

    public function seleccionarEventosVigAction(){
        return $this->viewModelAjax([
            'listaeventos' => $this->tablaActualiza->listaEventosVigentes(),
        ]);
    }

    public function listaestudisncrieventoAction(){

        if ( ! $this->validateActualiza->validarIdEvento() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscritos' => $this->tablaActualiza->Listaestudinscritos($this->validateActualiza->getValue('evento')),
            'idevento' => $this->validateActualiza->getValue('evento'),
        ]);

    }

    public function listainscritosidentAction(){

        if ( ! $this->validateActualiza->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscriident' => $this->tablaActualiza->Listaestudinscriident($this->validateActualiza->getValue('identificacion')),
        ]);

    }

    public function listainscritosnombAction(){

        if ( ! $this->validateActualiza->validarNombres() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscrinomb' => $this->tablaActualiza->Listainscritosnomb($this->validateActualiza->getValue('nombres')),
        ]);

    }

    public function seleccionarEventosAntAction(){
        return $this->viewModelAjax([
            'listaeventos' => $this->tablaActualiza->Listadeeventosnovigente(),
        ]);
    }

    public function listaestudinscrieventanteAction(){

        if ( ! $this->validateActualiza->validarIdEvento() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        return $this->viewModelAjax([
            'estudinscrieventant' => $this->tablaActualiza->Listainscritoseventante($this->validateActualiza->getValue('evento')),
            'idevento' => $this->validateActualiza->getValue('evento')
        ]);

    }

    public function nuevoEventoAction(){

        return $this->viewModelAjax([
            'eventos' => $this->tablaActualiza->Listaeventos(),
        ]);

    }

    public function inserteventoAction()
    {
        if ( ! $this->validateActualiza->validarDatosEventoNuevo() ) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }
        /*$filas=$this->tablaActualiza->actualizaestadoevento();*/
        $user=Sesiones::getUsuarioname();
        //if ($filas==0){
        //    return $this->verMensajeError("No se pudo actualizar el estado del evento");
        //}else {
            if (! $this->tablaActualiza->insertarevento($this->validateActualiza->getValues(),$user)){
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            }else {
                return $this->verMensajeInfo("Evento guardado satisfactoriamente....!");
            }
        //}
    }

    public function verdatoseventoAction()
    {
        return $this->viewModelAjax([
            'evento' => $this->tablaActualiza->SelecEvento((int)$this->getParam1()),
        ]);
    }

    public function updateEventoAction()
    {
        if (!$this->validateActualiza->validarDatosEventoNuevo()) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        }

        $idevento =$_POST['idevento'];
        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->actualizaEvento($this->validateActualiza->getValues(),$idevento,$username)) {
            return $this->verMensajeError($this->validateActualiza->getMensajes());
        } else {
            return $this->verMensajeInfo("El Evento fue actualizado satisfactoriamente...!");
        }
    }

    public function cambiarelestadoeventoAction()
    {
        $idevento = (int)$this->getParam1();
        $esta = $this->getParam2();
        $username = Sesiones::getUsuarioname();
        if (!$this->tablaActualiza->cambiaEstadoEvento($idevento,$esta,$username)) {
            return $this->verMensajeError('El Evento no pudo ser cerrado');
        } else {
            return $this->verMensajeInfo("El Evento fue actualizado satisfactoriamente...!");
        }
    }

    // Reportes en Excell
    public function reporteEstudInscripEventoExcelAction()
    {
        ini_set('max_execution_time', 600); //600 seconds = 10 minutes
        $letras= [
            'A', 'B', 'C', 'D', 'E', 'F','G'
        ];

        $idevento = $this->getParam1();
        $parametro= $this->getParam2();

        if ($parametro==1) {
            $estudinscritos = $this->tablaActualiza->Listaestudinscritos($idevento);
        }else{
            $estudinscritos = $this->tablaActualiza->Listainscritoseventante($idevento);
        }

        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Egresados")
            ->setLastModifiedBy("Egresados")
            ->setTitle("Reporte Inscrip. Eventos")
            ->setSubject("")
            ->setDescription("")
            ->setKeywords("")
            ->setCategory("");

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'FECHA INSCRIPCION')
            ->setCellValue('B1', 'IDENTIFICACION')
            ->setCellValue('C1', 'NOMBRES')
            ->setCellValue('D1', 'APELLIDOS')
            ->setCellValue('E1', 'PROGRAMA-PRE')
            ->setCellValue('F1', 'PROGRAMA-POS')
            ->setCellValue('G1', 'EVENTO')

        ;

        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(60);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(70);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(100);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(150);

        $objPHPExcel->getActiveSheet()
            ->getStyle('A1:G1')
            ->getFill()
            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFFF00');

        $objPHPExcel->getActiveSheet()->getStyle('A1:G1')->getFont()->setBold(true);

        // Datos de las celdas

        $fila=2;

        foreach ($estudinscritos as $value) {

            $progpre = $this->tablaProgramas->getProgramaEscojido($value['id_progpre']);
            /*if ($programa['nombre'] == ''){*/
            $progpos = $this->tablaProgramas->getProgramaEscojido($value['id_progpos']);
            /*}*/
            $nomprogpre = $progpre['nombre'];
            $nomprogpos = $progpos['nombre'];

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A'.$fila, $value['fecha'])
                ->setCellValue('B'.$fila, $value['nro_ident'])
                ->setCellValue('C'.$fila, $value['nombres'])
                ->setCellValue('D'.$fila, $value['apellidos'])
                ->setCellValue('E'.$fila, utf8_decode($nomprogpre))
                ->setCellValue('F'.$fila, utf8_decode($nomprogpos))
                ->setCellValue('G'.$fila, $value['nombre'])
            ;

            //$objPHPExcel->getActiveSheet()->getStyle('D'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
            //$objPHPExcel->getActiveSheet()->getStyle('E'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('F'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('G'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('H'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('I'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('J'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('K'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('L'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('N'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            //$objPHPExcel->getActiveSheet()->getStyle('O'.$fila)->getNumberFormat()->setFormatCode(\PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);

            foreach ($letras as $l) {
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getTop()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getBottom()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getLeft()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getBorders()->getRight()->setBorderStyle(\PHPExcel_Style_Border::BORDER_THIN);
                $objPHPExcel->getActiveSheet()->getStyle($l.$fila)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }

            $fila++;
        };


        // ===============================================================
        //   Finalizar Excel
        // ===============================================================

        $objPHPExcel->getActiveSheet()->setTitle('Reporte Inscrip. a Eventos');
        $objPHPExcel->setActiveSheetIndex(0);

        $nombre_archivo='Reporte Inscrip. Eventos_'.'.xlsx';


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$nombre_archivo.'"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0


        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;

    }
    /******************/

}