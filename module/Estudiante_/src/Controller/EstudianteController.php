<?php
namespace Estudiante\Controller;

use Comun\GenericController;
use Comun\Sesiones;

class EstudianteController extends GenericController
{
	public function __construct()
    {
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
        $this->tablaBuscaEncuesta = new \Estudiante\Table\BuscaEncuestaTable();
        $this->tablaBuscaEncuestaPost = new \Estudiante\Table\BuscaEncuestaPostTable();
        $this->tablaUniversidad = new \Estudiante\Table\UniversidadTable();
        $this->tablaProgramas = new \Estudiante\Table\ProgramasTable();
        $this->tablaAutoriza = new \Estudiante\Table\ActualizarTable();
        $this->validateBuscaEstudiante = new \Usuario\Validate\BuscaEstudianteValidate();
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
    }

    public function indexAction()
    {
    }

    public function estudianteAction()
    {
        $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
        return $this->viewModelAjax([
            'corte' => $corte,
        ]);
    }

    public function insertestudAction()
    {
        // Validación identificación del estudiante
        if ( ! $this->validateEstudiante->validarDatosEstudiantes() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        if ( ! $this->validateEstudiante->validarPrograma() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $user=Sesiones::getUsuarioname();
        $est = $this->tablaEstudiante->getEstudianteexiste($this->validateEstudiante->getValue('nro_ident'));
        $selecprog = $this->tablaProgramas->getProgramaEscojido($this->validateEstudiante->getValue('programa'));

        if (is_null($est)) {
            if (! $this->tablaEstudiante->insertarEstudiantes($this->validateEstudiante->getValues(),$user)){
                return $this->verMensajeError($this->tablaEstudiante->getMensajes());
            }else {
                $est1 = $this->tablaEstudiante->getEstudianteexiste($this->validateEstudiante->getValue('nro_ident'));
                if (! $this->tablaEstudiante->insertarAutorizados($est1['id'],
                    $this->validateEstudiante->getValue('programa'),
                    $this->validateEstudiante->getValue('corte'),
                    $user)){
                    return $this->verMensajeError($this->tablaEstudiante->getMensajes());
                }else {
                    return $this->verMensajeInfo("Estudiante con identificacion "
                        . $this->validateEstudiante->getValue('nro_ident') .
                        " fue registrado satisactoriamente en [estudiante] y [autorizados]");
                }
            }
        }else{
            $autorizado = $this->tablaAutoriza->getProgramaEscojido($est['id'],$selecprog['id']);
            if (is_null($autorizado)){
                if (! $this->tablaEstudiante->insertarAutorizados($est['id'],
                    $this->validateEstudiante->getValue('programa'),
                    $this->validateEstudiante->getValue('corte'),
                    $user)){
                    return $this->verMensajeError($this->tablaEstudiante->getMensajes());
                }else {
                    return $this->verMensajeInfo("Estudiante con identificacion "
                        . $this->validateEstudiante->getValue('nro_ident') .
                        " fue registrado satisactoriamente en [autorizados] con el programa <strong>"
                        .$selecprog['nombre']."</strong>");
                }
            }
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateEstudiante->getValue('nro_ident').
                "</strong> Existe en la Tabla Estudiante. y en la Tabla Autorizados con el programa <strong>"
                .$selecprog['nombre']."</strong>");

        }
    }

    public function buscarporidentificacionpreAction()
    {
        return $this->viewModelAjax([

        ]);
    }

    public function buscarporidentificacionpostAction()
    {
        return $this->viewModelAjax([

        ]);
    }

    public function corregirIdentEstudEncuestaAction()
    {
        // Validación del nombre del Estudiante

        if ( ! $this->validateBuscaEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateBuscaEstudiante->getMensajes());
        }
        $tipo=(int)$this->getParam1();
        if ($tipo == 1) {
            //echo $this->validateBuscaEstudiante->getValue('identificacion');
            $estxidepre = $this->tablaBuscaEncuesta->Buscarestudiantexidentifpre($this->validateBuscaEstudiante->getValue('identificacion'));
            //var_dump($estxnom);exit();
            //$estxnom = $this->tablaBuscaEncuesta->Buscarestudiantexnom($this->validateBuscaEstudiante->getValue('nombres'));
            if (!$estxidepre) {
                return $this->verMensajeError("No existen registros con El Parametro de busqueda <strong>"
                    . $this->validateBuscaEstudiante->getValue('identificacion') .
                    "</strong> Consulte con la Oficina de Atención al Egresado");
            } else {
                return $this->viewModelAjax([
                    'viewencuesta' => $estxidepre,
                    'tipo' => $tipo
                ]);
            }
        } else {
            $estxidepos = $this->tablaBuscaEncuesta->Buscarestudiantexidentifpos($this->validateBuscaEstudiante->getValue('identificacion'));
            if (!$estxidepos) {
                return $this->verMensajeError("No existen registros con El Parametro de busqueda <strong>"
                    . $this->validateBuscaEstudiante->getValue('identificacion') .
                    "</strong> Consulte con la Oficina de Atención al Egresado");
            } else {
                return $this->viewModelAjax([
                    'viewencuesta' => $estxidepos,
                    'tipo' => $tipo
                ]);
            }
        }
    }

    public function updateidentestudencAction()
    {
        if ( ! $this->validateEstudiante->validarIdentNombres() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }
        $enc=(int)$this->getParam1();
        $user=Sesiones::getUsuarioname();
        if ($enc==1) {
            $fila1 = $this->tablaEstudiante->actualizaidentificacionxencpre($this->validateEstudiante->getValue('id_encuesta'),
                $this->validateEstudiante->getValue('num_id'),
                $this->validateEstudiante->getValue('saber_pro'),
                $this->validateEstudiante->getValue('f_nac'),
                $this->validateEstudiante->getValue('l_expedic'),
                $user);
        }else{
            $fila1 = $this->tablaEstudiante->actualizaidentificacionxencpos($this->validateEstudiante->getValue('id_encuesta'),
                $this->validateEstudiante->getValue('num_id'),
                $this->validateEstudiante->getValue('saber_pro'),
                $this->validateEstudiante->getValue('f_nac'),
                $this->validateEstudiante->getValue('l_expedic'),
                $user);
        }
        $fila2=$this->tablaEstudiante->actualizaidentnomyapeestudi($this->validateEstudiante->getValue('id_estud'),
            $this->validateEstudiante->getValue('num_id'),
            $this->validateEstudiante->getValue('l_expedic'),
            $this->validateEstudiante->getValue('nombres'),
            $this->validateEstudiante->getValue('apellidos'),
            $this->validateEstudiante->getValue('sexo'),
            $user);

        if ($fila1==0 ){
            return $this->verMensajeInfo("No se pudo actualizar la identificacion en la encuesta.....");
        } else {
            if ($fila2==0 ){
                return $this->verMensajeInfo("No se pudo actualizar la identificacion del egresado (T-Estudiante).....");
            }
            return $this->verMensajeInfo("Toda la Informacion Fue actualizada satisactoriamente....!");
        }
    }

    public function importarestudianteAction()
    {
        return $this->viewModelAjax([

        ]);
    }

    public function ejecimportacionAction()
    {
        $user=Sesiones::getUsuarioname();
        $error=""; $salida=""; $i=0;
        $consestud = $this->tablaEstudiante->consultaultimoestud();
        if(!empty($_FILES["employee_file"]["name"])){
            $extension = pathinfo($_FILES["employee_file"]["name"])['extension'];
            if($extension=="csv"){
                if($this->tablaEstudiante->Importardatos($user)){
                    $estudimport =$this->tablaEstudiante->consultaestudimport($consestud['id_ultimo']);
                    //print_r($estudimport);exit();
                    if (!empty($estudimport)) {
                        foreach ($estudimport as $value) {
                            $i++;
                            $salida .= '<tr><td align="left">' . $value["id"] . '</td>
                                <td align="left">' . $value["nro_ident"] . '</td>
                                <td align="left">' . $value["ciudad_exped"] . '</td>
                                <td align="left">' . $value["nombres"] . '</td>
                                <td align="left">' . $value["apellidos"] . '</td>
                                <td align="left">' . $value["dir_resid"] . '</td>
                                <td align="left">' . $value["celular"] . '</td>
                                <td align="left">' . $value["email"] . '</td>
                                <td align="left">' . $value["sexo"] . '</td>
                            </tr>';
                        }
                        $salida .= '<tr><td colspan="4"></td><td>Total registros</td><td align="center">' . $i . '</td><td colspan="3"></td></tr>';
                        $error = $salida;
                    }else{
                        $error = "Error3";
                    }
                }
                $estudimportautor =$this->tablaEstudiante->consultaestudimport($consestud['id_ultimo']);
                $corte=$this->tablaEstudiante->peridoVigenteEncuesta();
                $i=0;
                foreach ($estudimportautor as $value) {
                    $i++;
                    if (! $this->tablaEstudiante->insertarAutorizadosImpor($value["id"], $value["idpro"], $corte['id'], $user)){
                        return $this->verMensajeError("Error de insercion autorizados");
                    }
                }
            }else{
                $error="Error1";
            }
        }else{
            $error="Error2";
        }
        echo $error;
        $this->viewTemplateAjax();exit();
    }

    public function verestudianteAction()
    {
        // Validación identificación del estudiante
        if ( ! $this->validateEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateEstudiante->getValue('identificacion'));
        $progautorizados = $this->tablaAutoriza->getVerProgramasEstud($est['id']);
        //var_dump($progautorizados);exit();
        if (is_null($est)) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateEstudiante->getValue('identificacion').
                "</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            return $this->viewModelAjax([
                'corte' => $this->tablaEstudiante->peridoVigenteEncuesta(),
                'estudiante' => $est,
                'progautorizados' => $progautorizados,
            ]);
        }
    }

    public function verestudiantexnomAction()
    {
        $identificacion=$this->getParam1();
        $est = $this->tablaEstudiante->getEstudianteIdentificacion($identificacion);
        $progautorizados = $this->tablaAutoriza->getVerProgramasEstud($est['id']);
        //var_dump($progautorizados);exit();
        if (is_null($est)) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateEstudiante->getValue('identificacion').
                "</strong> no existe. Consulte con la Oficina de Atención al Egresado");
        }else{
            return $this->viewModelAjax([
                'corte' => $this->tablaEstudiante->peridoVigenteEncuesta(),
                'estudiante' => $est,
                'progautorizados' => $progautorizados,
            ]);
        }
    }

    public function iniciarPeriodoEncuestaAction()
    {
        $corte = $this->tablaEstudiante->periodoRealizadoEncuesta();
        return $this->viewModelAjax([
            'corte' => $corte,
        ]);
    }

    public function insertcorteAction()
    {
        // Validación periodo ocrte de la encuesta
        if ( ! $this->validateEstudiante->validarCorte() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $user=Sesiones::getUsuarioname();
        $cortes = $this->tablaEstudiante->getperidoVigenteEncuesta($this->validateEstudiante->getValue('corte'));

        if (is_null($cortes)) {

            if (! $this->tablaEstudiante->actualizaestado($user)){
                return $this->verMensajeError("No se pudo actualizar el estado");
            }else{
                if (! $this->tablaEstudiante->insertarCorte($this->validateEstudiante->getValues(),$user)){
                    return $this->verMensajeError($this->tablaEstudiante->getMensajes());
                }else {
                    return $this->verMensajeInfo("El Peridodo <strong> "
                        . $this->validateEstudiante->getValue('corte').
                        "</strong> de la encuesta fue registrado satisactoriamente...!");
                }
            }
        }else {
            return $this->verMensajeError("El Peridodo <strong> "
                .$this->validateEstudiante->getValue('corte').
                "</strong> de la encuesta..Existe en la Tabla Corte.");
        }
    }

    public function programasAction()
    {
        $programas = $this->tablaProgramas->Selectprogramas((int)$this->getParam1());
        return $this->viewModelAjax([
            'programas' => $programas,
        ]);
    }

    public function insertautorizaAction()
    {
        if ( ! $this->validateEstudiante->ValidarVerestudAutoriza() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }
        $est = $this->tablaEstudiante->getEstudianteexiste($this->validateEstudiante->getValue('nro_ident'));
        $selecprog = $this->tablaProgramas->getProgramaEscojido($this->validateEstudiante->getValue('programa'));
        $user=Sesiones::getUsuarioname();
        if (!$this->tablaEstudiante->insertarAutorizados($est['id'],
                                                         $this->validateEstudiante->getValue('programa'),
                                                         $this->validateEstudiante->getValue('corte'),
                                                         $user)){
                return $this->verMensajeError($this->tablaEstudiante->getMensajes());
        }else{
                return $this->verMensajeInfo("Estudiante con identificacion "
                    . $this->validateEstudiante->getValue('nro_ident') .
                    " fue registrado satisactoriamente en [autorizados] con el programa <strong>"
                    . $selecprog['nombre'] . "</strong>");
        }
    }

    public function buscarAction()
    {
        // Validación identificación que ingreso el estudiante
        if ( ! $this->validateEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }
        // Validación programa pre que selecciono el estudiante
        if ( ! $this->validateEstudiante->validarPrograma() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateEstudiante->getValue('identificacion'));
        $selecprog = $this->tablaProgramas->getProgramaEscojido($this->validateEstudiante->getValue('programa'));
        $prog = $this->tablaProgramas->Programas();
        $uni = $this->tablaUniversidad->Selectuniversidades();

        //preguntar si existe el estudiante
        if (is_null($est)) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateEstudiante->getValue('identificacion').
                "</strong> no existe en el programa seleccionado. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya Autorizado para realizar la encuesta

            $autorizado = $this->tablaAutoriza->getProgramaEscojido($est['id'],$selecprog['id']);

            if (is_null($autorizado)){
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateEstudiante->getValue('identificacion').
                    "</strong> No esta Autorizado para realizar la encuesta en el programa <strong>".$selecprog['nombre']."</strong>");
            }else{
                //preguntar si ya realizo la encuesta
                $enc = $this->tablaBuscaEncuesta->getExisteEstudianteEncuesta($est['id'],$selecprog['id']);

                if (! is_null($enc)) {
                    return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                        .$this->validateEstudiante->getValue('identificacion').
                        "</strong> ya realizo la encuesta, para el programa ".$selecprog['nombre']."<strong>");
                }else{
                    return $this->viewModelAjax([
                        'estudiante' => $est,
                        'universidad'=> $uni,
                        'programas'  => $prog,
                        'selecprog'  => $autorizado,
                        'corte'      => $this->tablaEstudiante->peridoVigenteEncuesta(),
                        'paisnac' => $this->tablaActualiza->Listadepaises(),
                        'paisres' => $this->tablaActualiza->Listadepaises(),
                        'paislab' => $this->tablaActualiza->Listadepaises(),
                        'respuestas'=>$this->tablaActualiza->ListadeRespuestas(),
                    ]);
                }
            }
        }
    }

    public function fortalezaAction()
    {
        return $this->viewModelAjax([
            'respuestas2'=>$this->tablaActualiza->ListadeRespuestas2((int)$this->getParam1()),
        ]);
    }

    public function formpostAction()
    {
        // Validación identificación del estudiante

        if ( ! $this->validateEstudiante->validarIdentificacion() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }
        // Validación programa post que selecciono el estudiante
        if ( ! $this->validateEstudiante->validarPrograma() ) {
            return $this->verMensajeError($this->validateEstudiante->getMensajes());
        }

        $est = $this->tablaEstudiante->getEstudianteIdentificacion($this->validateEstudiante->getValue('identificacion'));
        $selecprog = $this->tablaProgramas->getProgramaEscojido($this->validateEstudiante->getValue('programa'));
        $prog = $this->tablaProgramas->Programas();
        $propre = $this->tablaProgramas->Progdepregrado(1);
        $uni = $this->tablaUniversidad->Selectuniversidades();
        $uni2 = $this->tablaUniversidad->Selectuniversidades();
        //echo gettype($est);exit();
        if (is_null($est)) {
            return $this->verMensajeError("El Número de identificación <strong>"
                .$this->validateEstudiante->getValue('identificacion').
                "</strong> no existe en el programa seleccionado. Consulte con la Oficina de Atención al Egresado");
        }else{
            //preguntar si ya Autorizado para realizar la encuesta

            $autorizado = $this->tablaAutoriza->getProgramaEscojido($est['id'],$selecprog['id']);

            if (is_null($autorizado)){
                return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                    .$this->validateEstudiante->getValue('identificacion').
                    "</strong> No esta Autorizado para realizar la encuesta en el programa <strong>".$selecprog['nombre']."</strong>");
            }else{
                //preguntar si ya realizo la encuesta
                $enc = $this->tablaBuscaEncuestaPost->getExisteEstudianteEncuesta($est['id'],$selecprog['id']);

                if (! is_null($enc)) {
                    return $this->verMensajeError("Estudiante con el Número de identificación <strong>"
                        .$this->validateEstudiante->getValue('identificacion').
                        "</strong> ya realizo la encuesta, para el programa ".$selecprog['nombre']."<strong>");
                }else{
                    return $this->viewModelAjax([
                        'estudiante' => $est,
                        'univerpre' => $uni,
                        'univerpost'=> $uni2,
                        'programas' => $prog,
                        'progdepre' => $propre,
                        'selecprog' => $autorizado,
                        'corte' => $this->tablaEstudiante->peridoVigenteEncuesta(),
                        'paisnac' => $this->tablaActualiza->Listadepaises(),
                        'paisres' => $this->tablaActualiza->Listadepaises(),
                        'paislab' => $this->tablaActualiza->Listadepaises(),
                        'respuestas' =>$this->tablaActualiza->ListadeRespuestas(),
                    ]);
                }
            }
        }
    }

}