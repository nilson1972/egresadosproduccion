<?php

namespace Estudiante\Controller;



use Comun\GenericController;



class EncuestaPostController extends GenericController
{



    public function __construct()
    {
        $this->tablaEncuestaPost = new \Estudiante\Table\EncuestaPostTable();
        $this->validateEncuestaPost = new \Estudiante\Validate\EncuestaPostValidate();
        $this->tablaActualiza = new \Estudiante\Table\ActualizarTable();
        $this->validateActualiza = new \Estudiante\Validate\ActualizarValidate();
        $this->tablaEstudiante = new \Estudiante\Table\EstudianteTable();
        $this->validateEstudiante = new \Estudiante\Validate\EstudianteValidate();
    }

    public function inserpostAction()
    {
        // Validación EncuestaPost
        $error1=1; $error2=1; $act=0;
        if ( ! $this->validateEncuestaPost->validarEncuestaPost() ) {
            return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
        }

        if (! $this->tablaEncuestaPost->inserpost($this->validateEncuestaPost->getValues())){
            return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
        }else {
            if ( ! $this->validateActualiza->validarIdEstudiante() ) {
                return $this->verMensajeError($this->validateActualiza->getMensajes());
            }
            $yaactualizo = $this->tablaActualiza->getBuscarEstudActualiza($this->validateActualiza->getValue('id_estudiante'));
            if (!count($yaactualizo) == 0) {
                $fila1 =$this->tablaActualiza->actulizarestadovigen($this->validateActualiza->getValue('id_estudiante'));
                if ($fila1 == 0) {
                    $error1=0;
                }
            }
            $propre=0;
            if (!$this->tablaActualiza->insertactulizadesdeencuesta($this->validateEncuestaPost->getValue('id_estudiante'),
                                                                    $this->validateEncuestaPost->getValue('dir_residencial'),
                                                                    $this->validateEncuestaPost->getValue('rcelestud'),
                                                                    $this->validateEncuestaPost->getValue('remailestud'),
                                                                    $this->validateEncuestaPost->getValue('rtelestud'),
                                                                    ($propre),
                                                                    $this->validateEncuestaPost->getValue('progpost'),
                                                                    $this->validateEncuestaPost->getValue('trabaja_actual'),
                                                                    $this->validateEncuestaPost->getValue('nombre_empresa'),
                                                                    $this->validateEncuestaPost->getValue('direc_emp_trab'),
                                                                    $this->validateEncuestaPost->getValue('telef_emp_trab'),
                                                                    $this->validateEncuestaPost->getValue('ocupacion_trabajo'),
                                                                    $this->validateEncuestaPost->getValue('especializar'))) {
                return $this->verMensajeError($this->validateEncuestaPost->getMensajes());
            }else{
                $act=1;
            }
            $fila2 = $this->tablaEstudiante->actualizaidentestudiante($this->validateEncuestaPost->getValue('id_estudiante'),
                                                                      $this->validateEncuestaPost->getValue('num_id'));
            if ($fila2 == 0) {
                $error2=0;
            }

            //$error=0 no actualiza, 1 actualiza en tautorizados id_corte, 2 error en la actualizacion
            $error3 = 0;
            $corte = $this->tablaEstudiante->peridoVigenteEncuesta();
            if ($corte['id']<>$_POST['id_corte']) {
                $error3 = 1;
                $actidcorte = $this->tablaEncuesta->actualizaCorteAutorizado($this->validateEncuesta->getValue('id_autorizado'), $corte['id']);
                if ($actidcorte == 0) {
                    $error3 = 2;
                }
            }

            return $this->viewModelAjax([
                'mensaje' => "Encuesta de Postgrado registrada satisactoriamente...Act ".$act." Est ".$error2. "Act. IdCorte Autorizado".$error3. "...!!!",
            ]);
        }
    }

}