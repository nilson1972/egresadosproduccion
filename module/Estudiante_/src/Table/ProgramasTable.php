<?php

namespace Estudiante\Table;


use Comun\DB;

class programasTable
{


    private $tablaProgramas = 'tprogramas';

    private $tablaCorte = 'tcorte';


    public function Selectprogramas($tipo)
    {
        $campos_programas = [
            'id','id_estudio','nombre'
        ];

        $campo_order = [
            'nombre' => 'nombre'
        ];

        $where = [
            'id_estudio' => $tipo,
            'ofertado' => 'S'
        ];
        return DB::selectTablaCamposOrder( $this->tablaProgramas, $campos_programas,  $where, $campo_order);
    }


    public function Programas()
    {
        $campos_programas = [
            'id','id_estudio','nombre'
        ];

        $campo_order = [
            'nombre' => 'nombre'
        ];

        $where = [
            'ofertado' => 'S'
        ];
        return DB::selectTablaCamposOrder( $this->tablaProgramas, $campos_programas,  $where, $campo_order);
    }

    public function Progdepregrado($tpo)
    {
        $campos_programas = [
            'id','id_estudio','nombre'
        ];

        $campo_order = [
            'nombre' => 'nombre'
        ];

        $where = [
            'ofertado' => 'S',
            'id_estudio' => $tpo,
        ];
        return DB::selectTablaCamposOrder( $this->tablaProgramas, $campos_programas,  $where, $campo_order);
    }


    public function getProgramaEscojido($idprog)
    {
        $campos_programas = [
            'id','id_estudio','nombre'
        ];

        $where = [
            'id' =>$idprog,
        ];
        return DB::selectRegistroCampos( $this->tablaProgramas, $campos_programas, $where);
    }



    public function SeleccionaCorte()
    {
        return DB::selectTablaOrder( $this->tablaCorte, []);
    }
}