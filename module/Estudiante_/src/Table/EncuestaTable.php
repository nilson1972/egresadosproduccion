<?php
namespace Estudiante\Table;


use Comun\DB;

class encuestaTable
{
    private $tablaEncuesta = 'tencuestapre';
    private $tablaEstudiantes = 'testudiante';
    private  $tablaActas = 'tactas';
    private  $tablaProgramas = 'tprogramas';
    private $tablaAutorizados = 'tautorizados';
    private  $tablaDatosActualizados = 'tdatosestudactualizados';

    public function insertar($datos,$paisres,$ciudres,$paislab,$ciudlab)
    {
        try {
           DB::transactionInit();
           DB::insertar($this->tablaEncuesta,[
                'fec_crea' => date('Y-m-d G:i:s'),
                'fec_encuesta' => date('Y-m-d'),
                'id_autorizado' => $datos['id_autorizado'],
                'id_corte' => $datos['id_corte'],
                'id_estudent' => $datos['id_estudiante'],
                'dp_identestud' => $datos['num_id'],
                'dp_lugarexped' => utf8_encode($datos['l_expedicion']),
                'dp_fecnac' => $datos['f_nac'],
                'pais_nac' => $datos['paisnac'],
                'ciud_nac' => $datos['ciudnac'],
                'dp_dirresidencia' => utf8_encode($datos['dir_residencial']),
                'pais_res' => $datos['paisres'],
                'ciud_res' => $datos['ciudres'],
                'dp_telestud' => $datos['rtelestud'],
                'dp_celestud'  => $datos['rcelestud'],
                'dp_emailestud' => $datos['remailestud'],
                'ha_npsaberpro'  => $datos['snp'],
                'ha_progprecursa'  => $datos['programa'],
                'ha_semacaini'  => $datos['sem_ini'],
                'ha_anosemacaini' => $datos['anyo1'],
                'ha_semacafin'  => $datos['sem_fin'],
                'ha_anosemacafin' => $datos['anyo2'],
                'sedeuniv' => $datos['sedeuniv'],
                'ha_porcsatis' => $datos['porcentaje'],
                'ha_estudotracarrera' => $datos['estudio_otra'],
                'ha_termesacarrera' => $datos['termino_otra'],
                'ha_nomunivotracarrera' => $datos['univer_otra'],
                'ha_nomesacarrera' => utf8_encode($datos['otroprograma']),
                'ep_areaporespec' => utf8_encode($datos['especializar']),
                'il_trabactualmente' => $datos['trabaja_actual'],
                'trabsectprofe' => $datos['sectprof'],
                'il_fecinilab'  => $datos['fecha_inilaboral'],
                'il_tenidotrabremu' => $datos['trabajo_remunera'],
                'il_ocupesetraba' => utf8_encode($datos['ocupacion_trabajo']),
                'il_nomemplab' => utf8_encode($datos['nombre_empresa']),
                'il_actecoemplab' => utf8_encode($datos['actividad_empresa']),
                'ie_nomgteemp' => utf8_encode($datos['nom_gerente_emp_trab']),
                'ie_nomjefinme' => utf8_encode($datos['nom_jefe_emp_trab']),
                'ie_diremplab' => utf8_encode($datos['direc_emp_trab']),
                'pais_lab' => $datos['paislab'],
                'ciud_lab' => $datos['ciudlab'],
                'ie_telemplab' => $datos['telef_emp_trab'],
                'ie_emailemplab' => $datos['email_emp_trab'],
                'id_prog_mejorar' => $datos['mejorar'],
                'id_prog_fortaleza' => $datos['fortaleza'],
                'fulldirecres' => utf8_encode($datos['dir_residencial']).", ".$paisres.", ".$ciudres,
                'fulldireclab' => utf8_encode($datos['direc_emp_trab']).", ".$paislab.", ".$ciudlab
                 ]);
          DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function getTotalEncSexoMas ()
    {
        $campos_encuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudent)')
        ];

        $where = [
            'sexo' => 'M',
        ];

        return DB::selectJoinRegistro( $this->tablaEncuesta, $campos_encuesta,
                                       $this->tablaEstudiantes,  [],
                                       'id_estudent', 'id',
                                        $where,[]);
    }

    public function getTotalEncSexoFem ()
    {
        $campos_encuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudent)')
        ];

        $where = [
            'sexo' => 'F',
        ];

        return DB::selectJoinRegistro( $this->tablaEncuesta, $campos_encuesta,
            $this->tablaEstudiantes,  [],
            'id_estudent', 'id',
            $where,[]);
    }

    public function getTotalEncTrabaMas ()
    {
        $campos_encuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudent)')
        ];

        $where = [
            'sexo' => 'M',
            'il_trabactualmente' => 'on',
        ];

        return DB::selectJoinRegistro( $this->tablaEncuesta, $campos_encuesta,
            $this->tablaEstudiantes,  [],
            'id_estudent', 'id',
            $where,[]);

    }

    public function getTotalEncTrabaFem ()
    {
        $campos_encuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudent)')
        ];

        $where = [
            'sexo' => 'F',
            'il_trabactualmente' => 'on',
        ];

        return DB::selectJoinRegistro( $this->tablaEncuesta, $campos_encuesta,
            $this->tablaEstudiantes,  [],
            'id_estudent', 'id',
            $where,[]);

    }

    public function getTotalEncNoTrabaMas ()
    {
        $campos_encuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudent)')
        ];

        $where = [
            'sexo' => 'M',
        ];

        $predicate = new \Zend\Db\Sql\Predicate\IsNull('il_trabactualmente');

        return DB::selectJoinRegistro( $this->tablaEncuesta, $campos_encuesta,
            $this->tablaEstudiantes,  [],
            'id_estudent', 'id',
            $where, [], $predicate);

    }

    public function getTotalEncNoTrabaFem ()
    {
        $campos_encuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudent)')
        ];

        $where = [
            'sexo' => 'F',
        ];

        $predicate = new \Zend\Db\Sql\Predicate\IsNull('il_trabactualmente');

        return DB::selectJoinRegistro( $this->tablaEncuesta, $campos_encuesta,
            $this->tablaEstudiantes,  [],
            'id_estudent', 'id',
            $where,[], $predicate);

    }

    public function getTotalGraduadoMas ()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuesta),
            'tabla1.ID_AUTORIZADO = tabla2.ID_AUTORIZADO',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiantes),
                'tabla2.id_estudent = tabla3.id',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaProgramas),
                'tabla2.ha_progprecursa = tabla4.id',
                [],
                'left'
            );

        $select->where([
            'tabla3.sexo' =>'M',
        ]);

        //$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        //$select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function getTotalGraduadoFem ()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaEncuesta),
            'tabla1.ID_AUTORIZADO = tabla2.ID_AUTORIZADO',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiantes),
                'tabla2.id_estudent = tabla3.id',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaProgramas),
                'tabla2.ha_progprecursa = tabla4.id',
                [],
                'left'
            );

        $select->where([
            'tabla3.sexo' =>'F',
        ]);

        //$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        //$select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function getTotalGraduadoTrabMas()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.ID_AUTORIZADO = tabla2.ID',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiantes),
                'tabla2.id_estudiante = tabla3.id',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                [],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                [],
                'left'
            );

        $select->where([
            'tabla3.sexo' =>'M',
            'tabla4.trabajo_act' => 'on',
            'tabla4.estado' => 'V',
            'tabla5.id_estudio' => 1
        ]);

        //$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        //$select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function getTotalGraduadoTrabFem()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.ID_AUTORIZADO = tabla2.ID',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiantes),
                'tabla2.id_estudiante = tabla3.id',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                [],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                [],
                'left'
            );

        $select->where([
            'tabla3.sexo' =>'F',
            'tabla4.trabajo_act' => 'on',
            'tabla4.estado' => 'V',
            'tabla5.id_estudio' => 1
        ]);

        //$predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.id_autorizado');
        //$select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function getTotalGraduadoNoTrabMas()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.ID_AUTORIZADO = tabla2.ID',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiantes),
                'tabla2.id_estudiante = tabla3.id',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                [],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                [],
                'left'
            );

        $select->where([
            'tabla3.sexo' =>'M',
            //'tabla4.trabajo_act' => 'on',
            'tabla4.estado' => 'V',
            'tabla5.id_estudio' => 1
        ]);

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.trabajo_act');
        $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function getTotalGraduadoNoTrabFem()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaActas));
        $select->columns([
            'total' => new \Zend\Db\Sql\Expression('COUNT(*)')
        ]) ;
        $select->join(
            array('tabla2' => $this->tablaAutorizados),
            'tabla1.ID_AUTORIZADO = tabla2.ID',
            [],
            'left'
        )
            ->join(
                array('tabla3' => $this->tablaEstudiantes),
                'tabla2.id_estudiante = tabla3.id',
                [],
                'left'
            )
            ->join(
                array('tabla4' => $this->tablaDatosActualizados),
                'tabla3.id = tabla4.id_estudiante',
                [],
                'left'
            )
            ->join(
                array('tabla5' => $this->tablaProgramas),
                'tabla2.id_programa = tabla5.id',
                [],
                'left'
            );

        $select->where([
            'tabla3.sexo' =>'F',
            //'tabla4.trabajo_act' => 'on',
            'tabla4.estado' => 'V',
            'tabla5.id_estudio' => 1
        ]);

        $predicate =  new \Zend\Db\Sql\Predicate\IsNull('tabla4.trabajo_act');
        $select->where->addPredicate($predicate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $res = $statement->execute();
        return $res->current();
    }

    public function actualizaCorteAutorizado($idautoriza, $idcorte)
    {
        //Actualiza corte en autorizados de acuerdo al vigente
        $set=[
            'id_corte'=>$idcorte,
            'user_modi' => 'estudiante',
        ];

        $where=[
            'id' => $idautoriza,
        ];

        return DB::actualizar($this->tablaAutorizados, $set, $where);
    }
}