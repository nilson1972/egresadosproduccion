<?php

namespace Estudiante\Table;

use Comun\DB;

use Zend\db\TableGateway\TableGateway;


class estudianteTable
{
    private $sql;
    private $tablaEstudiante = 'testudiante';
    private $tablaActas = 'tactas';
    private $tablaAutorizados = 'tautorizados';
    private $tablaEstudios = 'testudios';
    private $tablaCorte = 'tcorte';
    private $tablaEncuesta = 'tencuestapre';
    private $tablaEncuestapost = 'tencuestapost';
    private $tablaEventos = 'teventos';
    private $tablaUsuario = 'usuario';
    private $tablaUniversidad = 'tuniversidad';
    private $tablaProgramas = 'tprogramas';
    private $tablaPreguntasAct =  'tpreg_prog_mejoras_o_fortalezas';

    /*parametrizacion*/
    public function SelecUsuarios()
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaUsuario));
        $select->columns(
            [
                'id'      => 'id',
                'nom_del_usu'  => 'nombre',
                'user_name'  => 'username',
                'clave'  => 'password',
                'activo'  => 'activo',
                'tipo_user' => 'id_tipo',
                'correo'=> 'email',
            ]
        ) ;
        //$select->where->like('tabla1.nombres', '%'.$nomestud.'%');
        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**************************/

    public function listaestudiantes($nomestud)
    {
        $sql = new \Zend\Db\Sql\Sql(DB::getAdapter());
        $select = $sql->select();
        $select->from(array('tabla1' => $this->tablaEstudiante));
        $select->columns(
            [
                'id'      => 'id',
                'nombres' => 'nombres',
                'apellidos'  => 'apellidos',
                'identifica'  => 'nro_ident',
                'direccion'  => 'dir_resid',
                'celular' => 'celular',
                'sexo'=> 'sexo',
            ]
        ) ;

        $select->where->like('tabla1.nombres', '%'.$nomestud.'%');

        //echo $select->getSqlString();
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /*Selecciona todos los periodos de la encuesta*/
    public function periodoRealizadoEncuesta()
    {
        $campos_corte = [
            'id', 'nombre', 'estado'
        ];

        $order = [
            'nombre'
        ];

        return DB::selectTablaCamposOrder($this->tablaCorte, $campos_corte, [], $order);

    }

    /*buscar el periodo vigente de la encuesta*/
    public function peridoVigenteEncuesta()
    {
        $campos_corte = [
            'id', 'nombre', 'estado'
        ];

        $where = [
            'estado'=>'V',
        ];

        return DB::selectRegistroCampos( $this->tablaCorte, $campos_corte, $where);
    }

    //sacar nombre evento egresado vigente
    public function EventoVigente()
    {
        $campos_evento = [
            'id', 'nombre', 'estado'
        ];

        $where = [
            'estado'=>'VIG',
        ];

        return DB::selectTablaCamposOrder( $this->tablaEventos, $campos_evento, $where, []);
    }

    /*verificar si existe periodo vigente de la encuesta*/
    public function getperidoVigenteEncuesta($corte)
    {
        $campos_corte = [
            'id', 'nombre', 'estado'
        ];

        $where = [
            'nombre' => $corte,
        ];

        return DB::selectRegistroCampos( $this->tablaCorte, $campos_corte, $where);
    }

    /*buscar estudiante en la tabla estudiante para sacar el id*/
    public function getEstudianteIdentificacion($ident)
    {
        $where = [
            'nro_ident'=>$ident,
        ];

        return DB::selectRegistro( $this->tablaEstudiante, $where);
    }

    /*buscar estudiante en la tabla estudiante por el id*/
    public function getEstudianteId($id)
    {
        $where = [
            'id'=>$id,
        ];

        return DB::selectRegistro( $this->tablaEstudiante, $where);
    }

    /*Total estudintes en base de datos*/
    public function getTotalEstudiantes()
    {
        $campos_estudiantes = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        return DB::selectRegistroCampos($this->tablaEstudiante, $campos_estudiantes,[]);
    }

    /*Total estudintes de pregrado autorizados*/
    public function getTotaEstudiantes($idcorte)
    {
        $campos_autorizados = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(tabla1.id)')
        ];
        $where = [
            'tabla2.id_estudio' => 1,
            'tabla1.id_corte' => $idcorte,
        ];

        return DB::selectJoinTresTablasRegistro( $this->tablaAutorizados, $campos_autorizados,
            $this->tablaProgramas,  [],
            $this->tablaEstudios, [],
            'id_programa', 'id',
            'id_estudio', 'id',
            $where,[]);

    }

    /*Total estudintes de postgrado*/
    public function getTotaEstudiantespost($idcorte)
    {
        $campos_autorizados = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(tabla1.id)')
        ];

        $where = [
            'id_estudio' => 2,
            'tabla1.id_corte' => $idcorte,
        ];

        return DB::selectJoinTresTablasRegistro( $this->tablaAutorizados, $campos_autorizados,
                                                 $this->tablaProgramas,  [],
                                                 $this->tablaEstudios, [],
                                                 'id_programa', 'id',
                                                 'id_estudio', 'id',
                                                  $where,[]);
    }

    public function getTotaEncuestasRealizadas($idcorte)
    {
        $camposEncuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'id_corte' => $idcorte,
        ];

        return DB::selectRegistroCampos($this->tablaEncuesta, $camposEncuesta, $where);
    }

    public function getTotaEncuestasRealizadaspost($idcorte)
    {
        $camposEncuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id)')
        ];

        $where = [
            'id_corte' => $idcorte,
        ];

        return DB::selectRegistroCampos($this->tablaEncuestapost, $camposEncuesta, $where);
    }

    /* codigo para proceso de insertar un estudiante en la tabla estudiante*/
    /* validar si ya existe */
    public function getEstudianteexiste($nro_ident)
    {
        $where = [
            'nro_ident'=>$nro_ident,
         /*  'tipo' => $tipo, */
        ];

        return DB::selectRegistro( $this->tablaEstudiante, $where);
    }

    public function getEstudianteexisteacta($id_autor)
    {
        $where = [
            'id_autorizado'=>$id_autor,
            /*  'tipo' => $tipo, */
        ];

        return DB::selectRegistro( $this->tablaActas, $where);
    }

    /* insrtar estudiante en la tabla estudiante */
    public function insertarEstudiantes($datos,$user)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaEstudiante,[
                'nro_ident' => $datos['nro_ident'],
                'ciudad_exped' => utf8_encode($datos['l_expedicion']),
                'nombres' => utf8_encode($datos['nombres']),
                'apellidos' => utf8_encode($datos['apellidos']),
                'dir_resid' => utf8_encode($datos['dir_residencial']),
                'celular' => $datos['rcelestud'],
                'email' => $datos['email'],
                'sexo' => $datos['sexo'],
                'user_crea' => $user
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    /* insrtar estudiante en la tabla autorizados */
    public function insertarAutorizados($idest, $prog, $idcorte, $usercrea)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaAutorizados,[
                'id_estudiante' => $idest,
                'id_programa' => $prog,
                'id_corte' => $idcorte,
                'user_crea' => $usercrea,
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    /* actualiza el estado del periodo en la tabla corte */
    public function actualizaestado($usermodi)
    {
        $set=[
            'estado'=>'I',
            'user_modi' => $usermodi,
        ];

        $where=[
          'estado'=>'V',
        ];

        return DB::actualizar($this->tablaCorte, $set, $where);
    }


    /* insrtar un periodo a la tabla corte */
    public function insertarCorte($datos,$usercrea)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaCorte,[
                'nombre' => $datos['corte'],
                'estado' => $datos['estado'],
                'user_crea' => $usercrea,
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    /* insertar datos de las actas en la tabla actas */
    public function insertarActas($datos, $usercrea)
    {
        try {
            DB::transactionInit();
            DB::insertar($this->tablaActas,[
                'fec_actas' => $datos['fec_actas'],
                'nro_actgral' => $datos['actagral'],
                'nro_actind' => $datos['actaind'],
                'nro_diploma' => $datos['nro_diplo'],
                'carnet_entregado' => $datos['carnet'],
                'fec_entrecarnet' => $datos['fecentcarnet'],
                'periodo_grado' => $datos['periodo_grado'],
                'id_autorizado' => $datos['id_autorizado'],
                'user_crea' => $usercrea,
            ]);
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    //actualizar nombre apellidos del estudiante desde la captura del # ed actas
    public function actualizanomape($idest, $nom, $ape, $usermodi)
    {
        $set = [
            'nombres' => utf8_encode($nom),
            'apellidos' => utf8_encode($ape),
            'user_modi' => $usermodi,
        ];

        $where = [
            'id' => $idest,
        ];

        return DB::actualizar($this->tablaEstudiante, $set, $where);
    }

    public function actualizaestadoestudpre($idest)
    {
        $set = [
            'epre' => 'S',
        ];

        $where = [
            'id' => $idest,
        ];

        return DB::actualizar($this->tablaEstudiante, $set, $where);
    }

    public function actualizacarnetactas($datos, $usermodi)
    {
        $set=[
            'fec_actas' => $datos['fec_actas'],
            'nro_actgral' => $datos['actagral'],
            'nro_actind' => $datos['actaind'],
            'nro_diploma' => $datos['nro_diplo'],
            'carnet_entregado' => $datos['carnet'],
            'fec_entrecarnet' => $datos['fecentcarnet'],
            'periodo_grado' => $datos['periodo_grado'],
            'user_modi' => $usermodi,
        ];

        $where=[
            'id' => $datos['idtactas'],
        ];

        return DB::actualizar($this->tablaActas, $set, $where);
    }

    public function consultaultimoestud()
    {
        $camposEstudiante = [
            'id_ultimo' => new \Zend\Db\Sql\Expression(' MAX(id)')
        ];

        return DB::selectRegistroCampos($this->tablaEstudiante, $camposEstudiante, []);
    }

    public function getExisteEstudiante($id)
    {
        $camposEncuesta = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(nro_ident)'),
        ];

        $where = [
            'nro_ident' => $id,
        ];

        return DB::selectRegistroCampos($this->tablaEstudiante, $camposEncuesta, $where);
    }

    public function getSacaIdEstudExiste($ident)
    {
        $camposEstudiante = [
            'id' => 'id',
        ];

        $where = [
            'nro_ident' => $ident,
        ];

        return DB::selectRegistroCampos($this->tablaEstudiante, $camposEstudiante, $where);
    }

    public function getExisteAutorizado($id,$prog)
    {
        $camposAutoriza = [
            'total' => new \Zend\Db\Sql\Expression(' COUNT(id_estudiante)')
        ];

        $where = [
            'id_estudiante' => $id,
            'id_programa' => $prog,
        ];

        return DB::selectRegistroCampos($this->tablaAutorizados, $camposAutoriza, $where);
    }

    public function Importardatos($usercrea)
    {
        ini_set('max_execution_time', 300);
        $file_data = fopen($_FILES["employee_file"]["tmp_name"], 'r');
        try {
            DB::transactionInit();
            while (($row = fgetcsv($file_data, 500, ";")) !== FALSE) {
                $existe=$this->getExisteEstudiante($row[0]);
                if($existe['total']==0) {
                    DB::insertar($this->tablaEstudiante, [
                        'nro_ident' => $row[0],
                        'ciudad_exped' => $row[1],
                        'nombres' => utf8_encode($row[2]),
                        'apellidos' => utf8_encode($row[3]),
                        'dir_resid' => utf8_encode($row[4]),
                        'celular' => $row[5],
                        'email' => $row[6],
                        'sexo' => $row[7],
                        'idpro' => $row[8],
                        'importado' => 'S',
                        'user_crea' => $usercrea,
                    ]);
                }else{
                    $estudexiste=$this->getSacaIdEstudExiste($row[0]);
                    $exisaut=$this->getExisteAutorizado($estudexiste['id'],$row[8]);
                    if($exisaut['total']==0) {
                        $corte=$this->peridoVigenteEncuesta();
                        DB::insertar($this->tablaAutorizados, [
                            'id_estudiante' => $estudexiste['id'],
                            'id_programa' => $row[8],
                            'id_corte' => $corte['id'],
                            'user_crea' => $usercrea,
                        ]);
                    }
                }
            }
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function consultaestudimport($id)
    {
        $campos_estudiante = [
            'id', 'nro_ident', 'ciudad_exped', 'nombres', 'apellidos', 'dir_resid', 'celular', 'email', 'sexo', 'idpro'
        ];

        $tabla = new TableGateway($this->tablaEstudiante, DB::getAdapter());

        $select = $tabla->getSql()->select();
        $select->columns($campos_estudiante) ;
        $select->where('id>'. $id);

        return $tabla->selectWith($select);
    }

    /* insrtar autorizados importados */
    public function insertarAutorizadosImpor($idest, $idprog, $idcorte, $usercrea)
    {
        ini_set('max_execution_time', 300);
        try {
            DB::transactionInit();
            $exisaut=$this->getExisteAutorizado($idest, $idprog);
            if($exisaut['total']==0) {
                DB::insertar($this->tablaAutorizados, [
                    'id_estudiante' => $idest,
                    'id_programa' => $idprog,
                    'id_corte' => $idcorte,
                    'user_crea' => $usercrea,
                ]);
            }
            DB::transactionCommit();
            return true;
        } catch (Exception $e) {
            DB::transactionRollback();
            return false;
        }
    }

    public function actualizaidentificacionxencpre($idenc, $nroident, $ecaes, $fecnac, $lexped, $usermodi)
    {
        $set=[
            'dp_identestud' => $nroident,
            'ha_npsaberpro' => $ecaes,
            'dp_fecnac'     => $fecnac,
            'dp_lugarexped' => $lexped,
            'user_modi'     => $usermodi,
        ];

        $where=[
            'id' => $idenc,
        ];

        return DB::actualizar($this->tablaEncuesta, $set, $where);
    }

    public function actualizaidentificacionxencpos($idenc, $nroident, $ecaes, $fecnac, $lexped, $usermodi)
    {
        $set=[
            'dp_identestud' => $nroident,
            'ha_npsaberpro' => $ecaes,
            'dp_fecnac'     => $fecnac,
            'dp_lugarexped' => $lexped,
            'user_modi'     => $usermodi,
        ];

        $where=[
            'id' => $idenc,
        ];

        return DB::actualizar($this->tablaEncuestapost, $set, $where);
    }

    public function actualizaidentnomyapeestudi($idest, $nroident, $lexped, $nom, $ape, $sex, $usermodi)
    {
        $set = [
            'nro_ident' => $nroident,
            'ciudad_exped' => $lexped,
            'nombres' => utf8_encode($nom),
            'apellidos' => utf8_encode($ape),
            'sexo' => $sex,
            'user_modi' => $usermodi,
        ];

        $where = [
            'id' => $idest,
        ];

        return DB::actualizar($this->tablaEstudiante, $set, $where);
    }

    //actualiza datos en testudiante desde la encuesta
    public function actualizaidentestudiante($idest, $nroident, $lexped, $dirres, $cel, $email)
    {
        $set = [
            'nro_ident' => $nroident,
            'ciudad_exped' => $lexped,
            'dir_resid' => $dirres,
            'celular' => $cel,
            'email' => $email,
            'user_modi' => 'estudiante',
        ];

        $where = [
            'id' => $idest,
        ];

        return DB::actualizar($this->tablaEstudiante, $set, $where);
    }

    public function actualizadatosestudiante($idest, $dirres, $cel, $email, $usermodi)
    {
        $set = [
            'dir_resid' => $dirres,
            'celular' => $cel,
            'email' => $email,
            'user_modi' => $usermodi,
        ];

        $where = [
            'id' => $idest,
        ];

        return DB::actualizar($this->tablaEstudiante, $set, $where);
    }

}