<?php

namespace Estudiante\Validate;


use Comun\InputFilterGeneric3;


class ActualizarValidate extends InputFilterGeneric3
{

    public function validarIdentificacion()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'identificacion',
            'label' => 'Valor no valido para campo Identificacion',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarIdEstudiante()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'id_estudiante',
            'label' => 'Valor no valido para campo Id_Estudiante',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarNombres()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombres',
            'label' => 'El campo Nombres Contiene un Valor no valido ',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function validarIdEvento()
    {
        $this->validarDigito([
            'name'  => 'evento',
            'label' => 'Valor no valido para seleccion del campo Evento',
            'required' => true,
        ]);

        return $this->validar();
    }

    public function validarDatosActualizados()
    {

        $this->validarAlfaNumericoFiltro([
            'name' => 'dir_residencial',
            'label' => ' direccion ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paisres',
            'label' => ' pais de residencia ',
            'required' => true,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudres',
            'label' => ' ciudad de residencia ',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'rcelestud',
            'label' => ' celular ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'remailestud',
            'label' => ' e-mail ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'tel_fijo',
            'label' => ' telefono fijo ',
            'required' => true,
        ]);

        $this->validarDigito([
            'name' => 'progpre',
            'label' => ' pregrado ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'progpreotra',
            'label' => ' otro programa de pregrado ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'nomotraunivpre',
            'label' => ' otra universidad de pregrado ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'otracualunivpre',
            'label' => ' nombre otra universidad-cual de pregrado ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'progpos',
            'label' => ' programa postgrado ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'progposotra',
            'label' => ' otro programa de posgrado ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'nomotraunivpos',
            'label' => ' otra universidad de posgrado ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'otracualunivpos',
            'label' => ' nombre otra universidad-cual de postgrado ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'trabaja_actual',
            'label' => ' trabajo actual ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'sectprof',
            'label' => ' sector trabajo ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre_empresa',
            'label' => ' nombre empresa actual ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'direc_emp_trab',
            'label' => ' dirección empresa actual ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'paislab',
            'label' => ' pais donde labora ',
            'required' => false,
            'max_lenght' => 2,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'ciudlab',
            'label' => ' ciudad donde labora ',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'telef_emp_trab',
            'label' => ' telefono empresa actual ',
            'required' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'cargo_trab',
            'label' => ' cargo empresa actual ',
            'required' => false,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'expec_formac',
            'label' => ' En que area le gustaria continuar su formacion ',
            'required' => true,
            'max_lenght' => 150,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'mejorar',
            'label' => ' prog_debe_mejorar ',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'fortaleza',
            'label' => ' prog_fortaleza ',
            'required' => false,
        ]);

        $this->validarDigito([
            'name' => 'id_estudiante',
            'label' => ' id_estudiante ',
            'required' => false,
        ]);

        return $this->validar();
    }

    public function validarDatosEventoNuevo()
    {
        $this->validarFecha([
            'name'  => 'fecini',
            'label' => ' Fecha Inicio ',
            'required' => true,
        ]);

        $this->validarFecha([
            'name'  => 'fecfin',
            'label' => ' fecha Finaliza ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'evento',
            'label' => ' Nombres Del evento ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function ValidarDatosRespuestaActualiza()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'respuesta',
            'label' => ' respuesta ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function ValidarDatosPrograma()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre',
            'label' => ' nombre del programa ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarDigito([
            'name' => 'tipoprog',
            'label' => ' Tipo de programa ',
            'required' => true,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'ofertado',
            'label' => ' programa ofertado ',
            'required' => true,
            'max_lenght' => 1,
            'alnum' => false,
        ]);

        return $this->validar();
    }

    public function ValidarDatosUniversidad()
    {
        $this->validarAlfaNumericoFiltro([
            'name' => 'codigo',
            'label' => ' codigo universidad ',
            'required' => true,
            'max_lenght' => 5,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'nombre',
            'label' => ' nombre universidad ',
            'required' => true,
            'max_lenght' => 100,
            'alnum' => false,
        ]);

        $this->validarAlfaNumericoFiltro([
            'name' => 'ciudad',
            'label' => ' ciudad universidad ',
            'required' => true,
            'max_lenght' => 60,
            'alnum' => false,
        ]);

        return $this->validar();
    }
}